package ru.ulmart.web.app.telecom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import ru.ulmart.api.services.telecom.checku.CheckUPassportService;
import ru.ulmart.api.services.telecom.checku.beans.PassportRecognitionData;
import ru.ulmart.api.services.telecom.checku.beans.RecognitionError;
import ru.ulmart.web.app.BaseController;

import java.io.IOException;

@Controller
@RequestMapping(TelecomController.MAPPING_TELECOM + "/passport")
public class PassportRecognitionController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PassportRecognitionController.class);

    /**
     * Minimum upload size 75Kb.
     */
    private static final long IMAGE_MIN_SIZE = 75 * 1024;

    /**
     * Maximum upload size 3Mb.
     */
    private static final long IMAGE_MAX_SIZE = 3 * 1024 * 1024;

    private static final String CONTENT_TYPE_JPEG = "image/jpeg";

    private CheckUPassportService checkUPassportService;

    @Autowired
    public PassportRecognitionController(CheckUPassportService checkUPassportService) {
        this.checkUPassportService = checkUPassportService;
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody
    PassportRecognitionData extractPassportData(@RequestParam("passport") MultipartFile passportScan) {
        PassportRecognitionData passportRecognitionData = new PassportRecognitionData();
        RecognitionError recognitionError = new RecognitionError();
        if (!passportScan.isEmpty()) {
            if (!CONTENT_TYPE_JPEG.equals(passportScan.getContentType())) {
                return createErrorResponse("Копия паспорта должна быть в формате jpeg");
            }
            if (passportScan.getSize() < IMAGE_MIN_SIZE || passportScan.getSize() > IMAGE_MAX_SIZE) {
                return createErrorResponse("Размер файла должен быть от 75Кб до 3Мб");
             }
            try {
                passportRecognitionData = checkUPassportService.extractPassportData(passportScan.getInputStream());
                if (passportRecognitionData.getState() == PassportRecognitionData.State.ERROR) {
                    RecognitionError error = passportRecognitionData.getError();
                    error.setDescription(translateDescription(error.getCode()));
                }
            } catch (IOException e) {
                LOGGER.debug(e.getMessage(), e);
                return createErrorResponse("Невозможно загрузить файл");
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
                return createErrorResponse("Ошибка распознавания файла");
            }
        } else {
            passportRecognitionData.setState(PassportRecognitionData.State.ERROR);
            passportRecognitionData.setError(recognitionError);
            recognitionError.setDescription("Файл пуст");
        }

        return passportRecognitionData;
    }

    private String translateDescription(Long code) {
        switch (code.intValue()) {
            default:
                return "Невозможно распознать данные паспорта";
        }
    }

    private PassportRecognitionData createErrorResponse(String message) {
        PassportRecognitionData passportRecognitionData = new PassportRecognitionData();
        RecognitionError recognitionError = new RecognitionError();
        passportRecognitionData.setState(PassportRecognitionData.State.ERROR);
        passportRecognitionData.setError(recognitionError);
        recognitionError.setDescription(message);

        return passportRecognitionData;
    }

}
