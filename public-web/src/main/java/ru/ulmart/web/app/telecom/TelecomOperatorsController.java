package ru.ulmart.web.app.telecom;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TariffWithSim;
import ru.ulmart.model.telecom.TelecomType;

import java.util.List;

/**
 * * Контроллер для работы с операторами
 */
@Controller
public class TelecomOperatorsController extends TelecomController {
    public static final String MAPPING_OPERATORS = MAPPING_TELECOM + "/operators";

    private static final String ATTR_OPERATOR = "operator";
    private static final String ATTR_TELECOM_TYPE = "telecomType";

    private static final String VIEW_TELECOM_OPERATORS = "telecom/operators/operators";
    private static final String VIEW_TELECOM_OPERATOR = "telecom/operator";

    @RequestMapping(value = MAPPING_OPERATORS, method = RequestMethod.GET)
    public String renderOperators() {
        if (getAvailableOperators(City.getCityIdFromSession()).isEmpty()) {
            return redirectToNoOperatorsPage();
        }
        setMainBanner();
        return VIEW_TELECOM_OPERATORS;
    }

    @RequestMapping(value = MAPPING_OPERATORS + "/{operator}", method = RequestMethod.GET)
    public String renderOperator(@PathVariable String operator) {
        LOGGER.debug("renderOperator: operator = {}", operator);
        final Long cityId = City.getCityIdFromSession();
        TelecomType telecomType;
        try {
            telecomType = TelecomType.valueOf(operator.toUpperCase());
        } catch (IllegalArgumentException e) {
            return notFound();
        }
        if (!isOperatorAvailable(telecomType, cityId)) {
            return redirectToInfoPage("Юлмарт пока не продает SIM-карты и тарифы оператора " + operator.toUpperCase() +
                    " в вашем регионе.<br/>Вы можете выбрать другой регион.");
        }
        TariffDetails tarifDetailsCommon = telecomFacade.getTariffDetailCommon(cityId, telecomType);

        List<TariffWithSim> tariffs = getTariffsWithSims(telecomType, cityId);

        setMainBanner();
        setOperatorBanner(operator);
        request.setAttribute(ATTR_TELECOM_DETAIL_COMMON, tarifDetailsCommon);
        request.setAttribute(ATTR_TARIFFS, tariffs);
        request.setAttribute(ATTR_OPERATOR, operator);
        request.setAttribute(ATTR_TELECOM_TYPE, telecomType);
        return VIEW_TELECOM_OPERATOR;
    }

    private boolean isOperatorAvailable(TelecomType telecomType, Long cityId) {
        Boolean isOperatorAvailable = telecomFacadeSkipError.hasInThisRegion(cityId, telecomType).get(telecomType);
        return isOperatorAvailable != null && isOperatorAvailable;
    }
}
