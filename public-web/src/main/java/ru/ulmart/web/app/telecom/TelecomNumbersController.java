package ru.ulmart.web.app.telecom;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ulmart.common.Constants;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.web.aspects.annotation.IncludeMainInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.ulmart.api.services.telecom.TelecomUtil.getIntersectedOperators;

/**
 * * Контроллер для работы с номерами
 */
@Controller
public class TelecomNumbersController extends TelecomController {
    public static final String MAPPING_NUMBERS = MAPPING_TELECOM + "/numbers";

    private static final String ATTR_PARAM_QUERY = "searchQuery";
    private static final String ATTR_PARAM_TYPE = "type";
    private static final String ATTR_PARAM_TYPES = "types";
    private static final String ATTR_PARAM_OPERATOR = "operators";
    private static final String ATTR_PARAM_PAGE_NUM = "pageNum";

    private static final String VIEW_NUMBERS = "telecom/numbersList";

    /**
     * Отображение страницы cо списком доступных номеров
     */
    @RequestMapping(value = MAPPING_NUMBERS, method = RequestMethod.GET)
    public String renderTelecomNumbersPage(@RequestParam(value = "take") int take,
                                           @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                           @RequestParam(value = "type", required = false) String numberType,
                                           @RequestParam(value = "searchQuery", required = false) String searchQuery,
                                           @RequestParam(value = "operators", required = false) String operators) {
        List<TelecomType> telecomTypes = getAvailableOperators(City.getCityIdFromSession());
        if (telecomTypes.isEmpty()) {
            return redirectToNoOperatorsPage();
        }
        List<TelecomType> intersectedTypes = getIntersectedOperators(telecomTypes, operators);

        Map<TelecomType, Integer> skipMap = (Map<TelecomType, Integer>) request.getSession().getAttribute(Constants.SESSION_TELECOM_SKIP);
        if (pageNum == 1 || skipMap == null) {
            skipMap = new HashMap<>();
        }

        List<NumberType> types = telecomFacade.getNumberTypes();
        int position = Integer.MAX_VALUE;
        NumberType type = null;
        for (NumberType value : types) {
            if (numberType == null || numberType.isEmpty()) {
                if (position > value.getPosition()) {
                    position = value.getPosition();
                    type = value;
                }
            } else if (numberType.equals(value.getAlias())) {
                type = value;
            }
        }
        List<NumberType> numberTypes = new ArrayList<>();
        numberTypes.add(type);
        FilterRequest filterRequest = FilterRequest.defaultFilterRequest(numberTypes, searchQuery);
        filterRequest.setTake(take);

        List<PhoneNumber> numbers = getPhoneNumbers(filterRequest, type, intersectedTypes, skipMap);
        // Проверка наличия у оператора телефонных номеров. Если их нет - не надо выводить список типов номеров
        if(intersectedTypes.size() == 1) {
            boolean hasNumbers = telecomFacadeSkipError.hasNumbers(intersectedTypes.get(0));
            if(hasNumbers) {
                request.setAttribute(ATTR_PARAM_TYPES, types);
            }
        } else {
            request.setAttribute(ATTR_PARAM_TYPES, types);
        }

        request.setAttribute(ATTR_PARAM_QUERY, searchQuery);
        request.setAttribute(ATTR_PARAM_TYPE, numberType);
        request.setAttribute(ATTR_PARAM_OPERATOR, operators);
        request.setAttribute(ATTR_NUMBERS, numbers);
        request.setAttribute(ATTR_PARAM_PAGE_NUM, pageNum);
        request.getSession().setAttribute(Constants.SESSION_TELECOM_SKIP, skipMap);
        setMainBanner();
        return VIEW_NUMBERS;
    }

    private List<PhoneNumber> getPhoneNumbers(FilterRequest filterRequest, NumberType numberType, List<TelecomType> telecomTypes,
                                              Map<TelecomType, Integer> skipMap) {
        Long cityId = City.getCityIdFromSession();

        Map<TelecomType, List<PhoneNumber>> numberMap = new HashMap<>();
        for (TelecomType telecomType : telecomTypes) {
            Integer skip = skipMap.get(telecomType);
            if (skip == null) {
                skip = 0;
            }
            filterRequest.setSkip(skip);
            Map<NumberType, List<PhoneNumber>> numberTypeMap = telecomFacadeSkipError.listNumbers(
                    cityId, filterRequest, telecomType);
            if(numberTypeMap != null) {
                List<PhoneNumber> numbers = numberTypeMap.get(numberType);
                if (numbers != null) {
                    numberMap.put(telecomType, numbers);
                }
            }
        }

        // Необходимо равномерно перемешать номера разных операторов
        NumberShuffler shuffler = new NumberShuffler(numberMap, skipMap, TelecomType.TELE2);
        return shuffler.getShuffledNumbers(filterRequest.getTake());
    }

    private static class NumberShuffler {
        private final Map<TelecomType, List<PhoneNumber>> numberMap;
        // Таблица счетчиков, глобальная для всех итераций
        private final Map<TelecomType, Integer> skipMap;
        // Таблица счетчиков для текущей итерации
        private final Map<TelecomType, Integer> counterMap = new HashMap<>();
        private TelecomType currentType;

        public NumberShuffler(Map<TelecomType, List<PhoneNumber>> numberMap, Map<TelecomType, Integer> skipMap,
                              TelecomType telecomType) {
            this.numberMap = numberMap;
            this.skipMap = skipMap;
            this.currentType = telecomType;
        }

        public List<PhoneNumber> getShuffledNumbers(int take) {
            List<PhoneNumber> numbers = new ArrayList<>();
            PhoneNumber phoneNumber;
            int index = 0;
            do {
                phoneNumber = getNext();
                if (phoneNumber != null) {
                    numbers.add(phoneNumber);
                }
            } while (!(phoneNumber == null || ++index >= take));
            return numbers;
        }

        private PhoneNumber getNext() {
            List<PhoneNumber> numbers = null;
            TelecomType previousType = currentType;
            boolean notEnoughNumbers = true;
            while (notEnoughNumbers) {
                List<PhoneNumber> originalNumbers = numberMap.get(currentType);
                if (originalNumbers != null) {
                    Integer counter = getCounter();
                    numbers = originalNumbers;
                    notEnoughNumbers = numbers == null || numbers.size() <= counter;
                    if (notEnoughNumbers) {
                        currentType = currentType.next();
                        if (currentType == previousType) {
                            return null;
                        }
                    }
                } else {
                    currentType = currentType.next();
                    if (currentType == previousType) {
                        return null;
                    }
                }
            }

            Integer counter = getCounter();
            counterMap.put(currentType, counter + 1);
            Integer skip = getSkip();
            skipMap.put(currentType, skip + 1);
            currentType = currentType.next();
            return numbers.get(counter);
        }

        private Integer getCounter() {
            Integer counter = counterMap.get(currentType);
            if (counter == null) {
                counter = 0;
                counterMap.put(currentType, 0);
            }
            return counter;
        }

        private Integer getSkip() {
            Integer counter = skipMap.get(currentType);
            if (counter == null) {
                counter = 0;
                skipMap.put(currentType, 0);
            }
            return counter;
        }
    }
}
