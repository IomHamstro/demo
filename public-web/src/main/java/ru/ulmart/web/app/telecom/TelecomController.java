package ru.ulmart.web.app.telecom;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.ulmart.api.services.Static;
import ru.ulmart.api.services.cart.CartManager;
import ru.ulmart.api.services.catalog.CatalogStructure;
import ru.ulmart.domain.cms.ImageBannerGroupInfo;
import ru.ulmart.domain.cms.ImageBannerGroupType;
import ru.ulmart.model.telecom.Sim;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffWithSim;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.service.telecom.TelecomServiceFacade;
import ru.ulmart.web.app.BaseController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Контроллер для работы с телекоммуникационными операторами (Теле2 и МТС)
 *
 */
public abstract class TelecomController extends BaseController {
    protected static final Logger LOGGER = LoggerFactory.getLogger(TelecomController.class);

    public final static int ERROR_CODE_PHONE_IS_RESERVED = 101;
    public final static int ERROR_CODE_ALREADY_EXIST_IN_CART = 201;
    public static final String MAPPING_TELECOM = "/telecom";
    protected static final String ATTR_NUMBERS = "numbers";
    protected static final String ATTR_TARIFFS = "tariffs";
    public static final String ATTR_OPERATOR = "operator";
    public static final String ATTR_TARIFF_TYPE_ALIAS = "tariffType";
    protected static final String ATTR_SIM_TYPES = "simTypes";
    protected static final String ATTR_TELECOM_DETAIL_COMMON = "telecomDetailCommon";
    public static final String ATTR_ERROR_CODE = "errorCode";
    private static final String ATTR_TELECOM_TYPES = "telecomTypes";
    private static final String ATTR_MAIN_BANNER_GROUP = "mainBannerGroup";
    private static final String ATTR_OPERATOR_BANNER_GROUP = "operatorBannerGroup";
    private static final String ATTR_SELECTED_TYPE = "selectedTelecomType";
    public static final String ATTR_MESSAGE = "message";
    public static final String VIEW_INFO_PAGE = "telecom/infoPage";

    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected CartManager cartManager;
    @Autowired
    @Qualifier("telecomServiceFacade")
    protected TelecomServiceFacade telecomFacade;
    @Autowired
    @Qualifier("telecomServiceFacadeSkipError")
    protected TelecomServiceFacade telecomFacadeSkipError;
    @Autowired
    protected City city;
    @Autowired
    protected Static staticService;
    @Autowired
    protected CatalogStructure catalogStructure;

    protected void setMainBanner() {
        final Long cityId = City.getCityIdFromSession();
        String mainAlias = ImageBannerGroupType.TelecomMain.getAlias();
        ImageBannerGroupInfo mainBannerGroupInfo = staticService.listBanners(ApplicationType.SITE_MAIN.getAlias(), cityId, mainAlias);
        request.setAttribute(ATTR_MAIN_BANNER_GROUP, mainBannerGroupInfo);
    }

    protected void setOperatorBanner(String operator) {
        final Long cityId = City.getCityIdFromSession();
        String operatorAlias = String.format(ImageBannerGroupType.TelecomOperatorFormat.getAlias(), operator.toLowerCase());
        ImageBannerGroupInfo operatorBannerGroupInfo = staticService.listBanners(ApplicationType.SITE_MAIN.getAlias(), cityId, operatorAlias);
        request.setAttribute(ATTR_OPERATOR_BANNER_GROUP, operatorBannerGroupInfo);
    }

    protected List<TelecomType> getAvailableOperators(final Long cityId) {
        Map<TelecomType, Boolean> availability = telecomFacadeSkipError.hasInThisRegion(cityId);
        List<TelecomType> types = new ArrayList<>();
        for (TelecomType key : availability.keySet()) {
            if (availability.get(key) != null && availability.get(key))
                types.add(key);
        }
        request.setAttribute(ATTR_TELECOM_TYPES, types);
        return types;
    }

    public String redirectToNoOperatorsPage() {
        return redirectToInfoPage("Юлмарт пока не продает SIM-карты и тарифы в вашем регионе.<br/>" +
                "Вы можете выбрать другой регион.");
    }

    public String redirectToInfoPage(String message) {
        request.setAttribute(ATTR_MESSAGE, message);
        return VIEW_INFO_PAGE;
    }

    protected Map<Long, List<Sim>> getSimMap(long cityId, TelecomType telecomType) {
        List<Sim> sims = telecomFacadeSkipError.getSims(cityId, telecomType);
        Map<Long, List<Sim>> simMap = new HashMap<>();
        if(sims != null) {
            for (Sim sim : sims) {
                List<Sim> simList = simMap.get(sim.getTariffTypeId());
                if (simList == null) {
                    simList = new ArrayList<>();
                    simMap.put(sim.getTariffTypeId(), simList);
                }
                simList.add(sim);
            }
        }
        return simMap;
    }

    protected List<TariffWithSim> getTariffsWithSims(List<TelecomType> intersectedTypes, Long cityId) {
        if(intersectedTypes == null) {
            return Collections.emptyList();
        }
        List<TariffWithSim> tariffs = new ArrayList<>();
        for (TelecomType telecomType : intersectedTypes) {
            List<Tariff> telecomTarifs = telecomFacadeSkipError.listTariffs(cityId, telecomType);
            if (telecomTarifs != null && !telecomTarifs.isEmpty()) {
                Map<Long, List<Sim>> simMap = getSimMap(cityId, telecomType);
                for(Tariff tariff: telecomTarifs) {
                    if(tariff.getTelecomTariffType() == null) {
                        tariff.setTelecomTariffType(telecomFacadeSkipError.getDefaultTariffType());
                    }
                    List<Sim> simList = simMap.get(tariff.getTelecomTariffType().getId());
                    TariffWithSim tariffWithSim = new TariffWithSim(tariff, simList);
                    tariffs.add(tariffWithSim);
                }
            }
        }
        return tariffs;
    }

    protected List<TariffWithSim> getTariffsWithSims(TelecomType telecomType, Long cityId) {
        if(telecomType == null) {
            return Collections.emptyList();
        }
        List<TelecomType> types = Lists.newArrayList(telecomType);
        return getTariffsWithSims(types, cityId);
    }

    protected List<TariffWithSim> filter(List<TariffWithSim> tariffs, List<String> tariffTypes) {
        if (tariffs == null || tariffs.isEmpty()) {
            return Collections.emptyList();
        }
        if (!"".equals(tariffTypes.get(0))) {
            for (Iterator<TariffWithSim> iterator = tariffs.iterator(); iterator.hasNext(); ) {
                Tariff tariff = iterator.next().getTariff();
                if (tariff.getTelecomTariffType() != null && !tariffTypes.contains(tariff.getTelecomTariffType().getAlias())) {
                    iterator.remove();
                }
            }
        }

        return tariffs;
    }
}

