package ru.ulmart.web.app.telecom;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ulmart.model.telecom.*;
import ru.ulmart.web.aspects.annotation.IncludeMainInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.ulmart.api.services.telecom.TelecomUtil.getIntersectedOperators;

/**
 * Контроллер для работы с тарифами
 */
@Controller
public class TelecomTariffsController extends TelecomController {
    public static final String MAPPING_TARIFFS = MAPPING_TELECOM + "/tariffs";

    private static final String ATTR_TARIFFS_TYPES = "tariffsTypes";
    private static final String ATTR_PARAM_TYPES = "types";
    private static final String ATTR_PARAM_OPERATOR = "operators";
    private static final String ATTR_TARIFF_DETAIL = "tariffDetail";

    private static final String VIEW_TARIFFS = "telecom/tariffsList";
    private static final String VIEW_TELECOM_PRODUCT_CARD = "telecom/tariff";

    /**
     * Отображение страницы со списком доступных тарифов
     */
    @IncludeMainInfo
    @RequestMapping(value = MAPPING_TARIFFS, method = RequestMethod.GET)
    public String renderTelecomTariffsPage(@RequestParam(value = "types", required = false, defaultValue = "") String types,
                                           @RequestParam(value = "operators", required = false, defaultValue = "") String operators) {
        List<TelecomType> telecomTypes = getAvailableOperators(City.getCityIdFromSession());
        if (telecomTypes.isEmpty()) {
            return redirectToNoOperatorsPage();
        }
        List<TelecomType> intersectedTypes = getIntersectedOperators(telecomTypes, operators);
        final Long cityId = City.getCityIdFromSession();
        List<TariffWithSim> tariffs = getTariffsWithSims(intersectedTypes, cityId);
        Collections.shuffle(tariffs);
        List<String> tariffTypes = Arrays.asList(types.toLowerCase().split(","));

        request.setAttribute(ATTR_PARAM_OPERATOR, operators);
        request.setAttribute(ATTR_PARAM_TYPES, types);
        request.setAttribute(ATTR_TARIFFS_TYPES, getTariffTypes(tariffs));
        request.setAttribute(ATTR_TARIFFS, filter(tariffs, tariffTypes));
        setMainBanner();
        return VIEW_TARIFFS;
    }

    @IncludeMainInfo
    @RequestMapping(value = MAPPING_TARIFFS + "/{tariffId}", method = RequestMethod.GET)
    public String renderTelecomTariffPage(@PathVariable("tariffId") Long tariffId,
                                          @RequestParam(value = "operator") String operator) {
        if (!StringUtils.isEmpty(operator)) {
            Long cityId = City.getCityIdFromSession();

            TelecomType telecomType;
            try {
                telecomType = TelecomType.valueOf(operator.toUpperCase());
            } catch (IllegalArgumentException e) {
                telecomType = null;
            }
            if (telecomType == null) {
                return redirectToMain();
            }
            List<TelecomType> telecomTypes = getAvailableOperators(cityId);
            if (!telecomTypes.contains(telecomType)) {
                return redirectToNoOperatorsPage();
            }
            String region = telecomFacade.getCurrentRegionId(cityId, telecomType);
            TariffDetails tarifDetails = telecomFacade.getTariffDetail(tariffId, region, telecomType);
            if (tarifDetails == null) {
                return "redirect:" + MAPPING_TARIFFS;
            }
            if(tarifDetails.getTariff().getTelecomTariffType() != null) {
                List<Sim> sims = getSimMap(cityId, telecomType).get(tarifDetails.getTariff().getTelecomTariffType().getId());
                request.setAttribute(ATTR_SIM_TYPES, sims);
            } else {
                request.setAttribute(ATTR_SIM_TYPES, Collections.emptyList());
            }

            // Новый массив нужен, чтобы не испортить кэш
            List<Tariff> telecomTarifs = new ArrayList<>(telecomFacadeSkipError.listTariffs(cityId, telecomType));
            telecomTarifs.remove(tarifDetails.getTariff());
            request.setAttribute(ATTR_TARIFFS, telecomTarifs);

            TariffDetails tarifDetailsCommon = telecomFacade.getTariffDetailCommon(cityId, telecomType);
            request.setAttribute(ATTR_TELECOM_DETAIL_COMMON, tarifDetailsCommon);

            request.setAttribute(ATTR_OPERATOR, operator);
            request.setAttribute(ATTR_TARIFF_DETAIL, tarifDetails);
            setMainBanner();
            return VIEW_TELECOM_PRODUCT_CARD;
        }
        return redirectToMain();
    }

    /**
     * Получение списка типов тарифов из списка тарифов
     *
     * @param tariffs список тарифов
     * @return список типов тарифов
     */
    private List<TelecomTariffType> getTariffTypes(List<TariffWithSim> tariffs) {
        List<TelecomTariffType> types = new ArrayList<>();
        for (TariffWithSim tariffWithSim : tariffs) {
            Tariff tariff = tariffWithSim.getTariff();
            if (tariff.getTelecomTariffType() != null && !types.contains(tariff.getTelecomTariffType())) {
                types.add(tariff.getTelecomTariffType());
            }
        }
        Collections.sort(types);
        return types;
    }
}
