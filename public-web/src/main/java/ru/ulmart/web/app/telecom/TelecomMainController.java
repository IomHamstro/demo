package ru.ulmart.web.app.telecom;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ulmart.model.telecom.*;
import ru.ulmart.web.aspects.annotation.IncludeMainInfo;

import java.util.*;

/**
 * Контроллер для работы с телекоммуникационными операторами
 */
@Controller
public class TelecomMainController extends TelecomController {
    private static final String VIEW_MENU = "telecom/main";

    private static final int PHONE_COMMON_COUNT = 2;
    private static final int PHONE_UNCOMMON_COUNT = 3;
    private static final int PHONE_COUNT = PHONE_UNCOMMON_COUNT + PHONE_COMMON_COUNT + 100;
    private static final int TARIFF_COUNT = 4;

    private static final String TARIFF_INTERNET = "internet_tarif";
    private static final String ATTR_TARIFF = "tariff";

    /**
     * Отображение главной страницы
     */
    @RequestMapping(value = MAPPING_TELECOM, method = RequestMethod.GET)
    public String renderTelecomMainPage(@RequestParam(value = ATTR_ERROR_CODE, required = false) Integer errorCode,
                                        @RequestParam(value = "tariffId", required = false) Long tariffId,
                                        @RequestParam(value = "tariffType", required = false) String tariffAlias,
                                        @RequestParam(value = "operator", required = false) String operator,
                                        @RequestParam(value = "step", required = false) String step) {
        long cityId = City.getCityIdFromSession();

        List<TelecomType> telecomTypes = getAvailableOperators(cityId);
        if (telecomTypes.isEmpty()) {
            return redirectToNoOperatorsPage();
        }

        if(tariffId != null) {
            TelecomType telecomType;
            try {
                telecomType = TelecomType.valueOf(operator.toUpperCase());
            } catch (IllegalArgumentException e) {
                telecomType = null;
            }
            if (telecomType == null) {
                return redirectToMain();
            }
            if (!telecomTypes.contains(telecomType)) {
                return redirectToNoOperatorsPage();
            }
            Tariff tariff = telecomFacade.getTariffOrDefault(tariffId, cityId, telecomType);
            request.setAttribute(ATTR_TARIFF, tariff);
            if(tariff.getTelecomTariffType() != null) {
                List<Sim> sims = getSimMap(cityId, telecomType).get(tariff.getTelecomTariffType().getId());
                request.setAttribute(ATTR_SIM_TYPES, sims);
            } else {
                request.setAttribute(ATTR_SIM_TYPES, Collections.emptyList());
            }
        }
        Collections.shuffle(telecomTypes);
        List<PhoneNumber> numbers = getPhoneNumbers(cityId, telecomTypes);
        request.setAttribute(ATTR_NUMBERS, numbers);

        List<TariffWithSim> tariffs = getTariffs(cityId, telecomTypes);
        request.setAttribute(ATTR_TARIFFS, tariffs);

        request.setAttribute("step", step);
        request.setAttribute(ATTR_ERROR_CODE, errorCode);
        request.setAttribute(ATTR_OPERATOR, operator);
        request.setAttribute(ATTR_TARIFF_TYPE_ALIAS, tariffAlias);
        setMainBanner();
        return VIEW_MENU;
    }

    private List<TariffWithSim> getTariffs(long cityId, List<TelecomType> telecomTypes) {
        List<TariffWithSim> tariffs = new ArrayList<>();
        List<TariffWithSim> allTarrifs = new ArrayList<>();
        for(TelecomType telecomType: telecomTypes) {
            TariffWithSim defaultTariff = null;
            // Новый массив нужен, чтобы не испортить кэш
            List<Tariff> cachedTariffs = telecomFacadeSkipError.listTariffs(cityId, telecomType);
            if(cachedTariffs != null && !cachedTariffs.isEmpty()) {
                List<Tariff> clonedTariffs = new ArrayList<>(cachedTariffs);
                Collections.shuffle(clonedTariffs);
                Map<Long, List<Sim>> simMap = getSimMap(cityId, telecomType);
                boolean found = false;
                for(Iterator<Tariff> iter = clonedTariffs.iterator(); iter.hasNext();) {
                    Tariff tariff = iter.next();
                    if(tariff.getTelecomTariffType() == null) {
                        tariff.setTelecomTariffType(telecomFacadeSkipError.getDefaultTariffType());
                    }
                    List<Sim> simList = simMap.get(tariff.getTelecomTariffType().getId());
                    TariffWithSim tariffWithSim = new TariffWithSim(tariff, simList);
                    if(TARIFF_INTERNET.equals(tariff.getTelecomTariffType().getAlias())) {
                        found = true;
                        tariffs.add(tariffWithSim);
                        iter.remove();
                    } else {
                        // Запоминаем случайный тариф на случай, если не найдем тарифа для интернета
                        if(defaultTariff == null) {
                            defaultTariff = tariffWithSim;
                            iter.remove();
                        } else {
                            allTarrifs.add(tariffWithSim);
                        }
                    }
                }
                // Тариф для интернета не найден - берем случайный тариф
                if(defaultTariff != null) {
                    if (!found) {
                        tariffs.add(defaultTariff);
                    } else {
                        allTarrifs.add(defaultTariff);
                    }
                }
            }
        }
        if(tariffs.size() < TARIFF_COUNT) {
            int count = TARIFF_COUNT - tariffs.size();
            if(count > allTarrifs.size()) {
                count = allTarrifs.size();
            }
            if(count > 0) {
                tariffs.addAll(allTarrifs.subList(0, count));
            }
        } else if(tariffs.size() > TARIFF_COUNT) {
            tariffs = tariffs.subList(0, TARIFF_COUNT);
        }
        return tariffs;
    }

    private List<PhoneNumber> getPhoneNumbers(long cityId, List<TelecomType> telecomTypes) {
        // Получаем базовый набор номер всех категорий, из которого выберем нужные номера:
        // 3 номера должны выбираться из типов выше обычного
        // 2 номера выбираются из обычного типа
        // Если кол-во операторов больше <code>PHONE_COUNT</code>, то во втором блоке обязательно
        // должен быть представлен оператор, не попавший в первый блок
        // Все номера имеют максимальное значение "красивости"
        Map<TelecomType, List<PhoneNumber>> commonNumbersMap = new HashMap<>();
        Map<TelecomType, List<PhoneNumber>> uncommonNumbersMap = new HashMap<>();
        prepareNumberLists(cityId, telecomTypes, commonNumbersMap, uncommonNumbersMap);

        // Список, который будет содержать операторов, которые не вошли в первые 3 номера, и
        // должны появиться в 4 или 5 номере
        List<TelecomType> mandatoryTypes = new ArrayList<>();
        // Обработка необычных номеров (первые 3).
        List<PhoneNumber> numbers = getNumbersFromMap(uncommonNumbersMap, mandatoryTypes, PHONE_UNCOMMON_COUNT);
        int commonNumbersCount = PHONE_COMMON_COUNT;
        if(numbers.size() < PHONE_UNCOMMON_COUNT) {
            commonNumbersCount = PHONE_COMMON_COUNT + (PHONE_UNCOMMON_COUNT - numbers.size());
        }
        // Обработка обычных номеров (4 и 5).
        List<PhoneNumber> commonNumbers = getNumbersFromMap(commonNumbersMap, mandatoryTypes, commonNumbersCount);
        commonNumbersCount = commonNumbers.size();
        numbers.addAll(commonNumbers);
        // Если обычных номеров не хватило - запрашиваем разницу из необычных
        if(commonNumbersCount < PHONE_COMMON_COUNT && numbers.size() == PHONE_UNCOMMON_COUNT) {
            List<PhoneNumber> additionalNumbers = getNumbersFromMap(uncommonNumbersMap, null,
                    PHONE_COMMON_COUNT - commonNumbersCount);
            numbers.addAll(additionalNumbers);
        }
        return numbers;
    }

    private List<PhoneNumber> getNumbersFromMap(Map<TelecomType, List<PhoneNumber>> numbersMap,
                                                List<TelecomType> mandatoryTypes, int count) {
        List<PhoneNumber> resultNumbers = new ArrayList<>();
        List<TelecomType> usedTelecomTypes = Lists.newArrayList();
        for(TelecomType telecomType: numbersMap.keySet()) {
            usedTelecomTypes.add(telecomType);
        }
        int length = usedTelecomTypes.size();
        if(length > 0) {
            Random randomGenerator = new Random();
            int numbersLeft = count;
            for (int i = 0; i < count; i++) {
                List<PhoneNumber> numbers;
                boolean emptyNumbers;
                do {
                    TelecomType telecomType = null;
                    if(mandatoryTypes != null) {
                        for (Iterator<TelecomType> iter = mandatoryTypes.iterator(); iter.hasNext();) {
                            TelecomType mandatoryType = iter.next();
                            if (usedTelecomTypes.contains(mandatoryType)) {
                                telecomType = mandatoryType;
                                iter.remove();
                                break;
                            }
                        }
                    }
                    if(telecomType == null) {
                        telecomType = usedTelecomTypes.get(randomGenerator.nextInt(length));
                    }
                    numbers = numbersMap.get(telecomType);
                    emptyNumbers = numbers == null || numbers.isEmpty();
                    // Если нам осталось получить меньше номеров, чем есть доступных операторов -
                    // удаляем оператора из списка, чтобы не получить его еще раз.
                    if(numbersLeft <= length || (!emptyNumbers && numbers.size() == 1)) {
                        usedTelecomTypes.remove(telecomType);
                    }
                    length = usedTelecomTypes.size();
                } while(emptyNumbers && length > 0);
                if(numbers != null) {
                    PhoneNumber number = numbers.remove(0);
                    resultNumbers.add(number);
                    numbersLeft--;
                }
            }
        }
        mandatoryTypes.addAll(usedTelecomTypes);
        return resultNumbers;
    }

    private void prepareNumberLists(long cityId, List<TelecomType> telecomTypes,
                                    Map<TelecomType, List<PhoneNumber>> commonNumbersMap,
                                    Map<TelecomType, List<PhoneNumber>> uncommonNumbersMap) {
        List<NumberType> numberTypes = telecomFacadeSkipError.getNumberTypes();
        // Для Теле2 будут нужны номера только 2 младших типов ("обычные" и "серебряные")
        int posCommon = Integer.MAX_VALUE;
        int posSilver = Integer.MAX_VALUE;
        NumberType common = null;
        NumberType silver = null;
        for(NumberType numberType: numberTypes) {
            int position = numberType.getPosition();
            if(posCommon > position) {
                silver = common;
                common = numberType;
                posCommon = position;
            }
            if(posSilver > position && posCommon != position) {
                silver = numberType;
                posSilver = position;
            }
        }
        List<NumberType> tele2NumberTypes = new ArrayList<>();
        if(common != null) {
            tele2NumberTypes.add(common);
        }
        if(silver != null) {
            tele2NumberTypes.add(silver);
        }
        FilterRequest tele2FilterRequest = FilterRequest.defaultFilterRequest(tele2NumberTypes, null);
        tele2FilterRequest.setTake(PHONE_COUNT);
        FilterRequest filterRequest = FilterRequest.defaultFilterRequest(numberTypes, null);
        filterRequest.setTake(PHONE_COUNT);
        for(TelecomType telecomType: telecomTypes) {
            Map<NumberType, List<PhoneNumber>> numberTypeMap;
            if(telecomType.equals(TelecomType.TELE2)) {
                numberTypeMap = telecomFacadeSkipError.listNumbers(cityId, tele2FilterRequest, telecomType);
            } else {
                numberTypeMap = telecomFacadeSkipError.listNumbers(cityId, filterRequest, telecomType);
            }
            if(numberTypeMap != null) {
                // Помещаем полученные номера в списки обычных и необычных номеров, которые позже будут перемешаны.
                for(NumberType numberType: numberTypeMap.keySet()) {
                    List<PhoneNumber> numbers = numberTypeMap.get(numberType);
                    if(numbers != null && !numbers.isEmpty()) {
                        if(numberType.equals(common)) {
                            List<PhoneNumber> commonNumbers = commonNumbersMap.get(telecomType);
                            if(commonNumbers == null) {
                                commonNumbers = new ArrayList<>();
                                commonNumbersMap.put(telecomType, commonNumbers);
                            }
                            commonNumbers.addAll(numbers);
                        } else {
                            List<PhoneNumber> uncommonNumbers = uncommonNumbersMap.get(telecomType);
                            if(uncommonNumbers == null) {
                                uncommonNumbers = new ArrayList<>();
                                uncommonNumbersMap.put(telecomType, uncommonNumbers);
                            }
                            uncommonNumbers.addAll(numbers);
                        }
                    }
                }
            }
        }
        // Перемешиваем списки номеров
        for(List<PhoneNumber> numbers: commonNumbersMap.values()) {
            Collections.shuffle(numbers);
        }
        for(List<PhoneNumber> numbers: uncommonNumbersMap.values()) {
            Collections.shuffle(numbers);
        }
    }
}

