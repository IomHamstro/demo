package ru.ulmart.db;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import ru.ulmart.common.data.DataSourceAdapter;
import ru.ulmart.db.auxiliary.DataService;
import ru.ulmart.db.auxiliary.DataServiceScope;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomTariffType;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.model.telecom.db.Tele2DataService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@DataService(scope = DataServiceScope.COMMON, master = false, slave = true)
public class Tele2DataServiceImpl implements Tele2DataService {

    private DataSourceAdapter dataSource;

    public DataSourceAdapter getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSourceAdapter dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public TelecomCity getCityInfo(final Long ulmartCityId) {
        return dataSource.uniqueQuery("sql/tele2/getCityInfo.ftl",
                ImmutableMap.of("ulmartCityId", ulmartCityId),
                new RowMapper<TelecomCity>() {
                    @Override
                    public TelecomCity mapRow(ResultSet rs, int i) throws SQLException {
                        final TelecomCity city = new TelecomCity();
                        city.setTelecomCityId(rs.getLong("tele2_city_id"));
                        city.setTelecomRegionId(rs.getLong("tele2_region_id"));
                        city.setUlmartCityId(ulmartCityId);
                        city.setTelecomType(TelecomType.TELE2);
                        // Tele2 API identifies regions by their aliases. Thus, here alias is set as region id
                        city.setTelecomRegionName(rs.getString("alias"));
                        return city;
                    }
                });
    }

    public Map<String, TariffExtraInfo> getTariffsExtraInfo(final String region, List<Tariff> tariffs) {
        final Map<String, TariffExtraInfo> extraInfoMap = new HashMap<>();
        if (tariffs == null || tariffs.isEmpty()) {
            return extraInfoMap;
        }
        final Map<String, String> mappingMap = new HashMap<>();
        Collection<String> tariffsAliases = Collections2.transform(tariffs, new Function<Tariff, String>() {
            final String postfix = "_" + region;
            @Override
            public String apply(Tariff input) {
                String regionAlias = input.getAlias() + postfix;
                mappingMap.put(regionAlias, input.getAlias());
                return regionAlias;
            }
        });

        String aliases = buildRowsTele2(tariffsAliases);
        processExtraInfo(extraInfoMap, mappingMap, aliases);
        if(extraInfoMap.size() != tariffs.size()) {
            aliases = buildRowsTele2(mappingMap.values());
            processExtraInfo(extraInfoMap, mappingMap, aliases);
        }
        return extraInfoMap;
    }

    private void processExtraInfo(final Map<String, TariffExtraInfo> extraInfoMap,
                                  final Map<String, String> mappingMap, String aliases) {
        dataSource.query("sql/tele2/getTariffsExtraInfo.ftl",
                ImmutableMap.of("tariffsAliases", aliases),
                new RowCallbackHandler() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        final String tariffAlias = rs.getString("tariff_alias");
                        final String imageUrl = rs.getString("image_url");
                        TelecomTariffType type = new TelecomTariffType(rs.getLong("id"), rs.getString("name"),
                                rs.getString("description"),
                                rs.getString("alias"));
                        String originalAlias = mappingMap.get(tariffAlias);
                        if(originalAlias == null) {
                            originalAlias = tariffAlias;
                        }
                        if(!extraInfoMap.containsKey(originalAlias)) {
                            extraInfoMap.put(originalAlias, new TariffExtraInfo(imageUrl, type));
                        }
                    }
                });
    }

    private String buildRowsTele2(Collection<String> aliases) {
        StringBuilder sb = new StringBuilder();
        for (String alias : aliases) {
            sb.append(",").append("'").append(alias).append("'");
        }
        sb = sb.deleteCharAt(0);
        return sb.toString();
    }

    @Override
    public Integer getSalePoint(Long shopId) {
        return dataSource.uniqueQuery("sql/tele2/getSalePoint.ftl",
                ImmutableMap.of("shopId", shopId),
                DataSourceAdapter.SingleColumnIntegerRowMapper);
    }

    @Override
    public Long getArticleForUlmartCity(Long region) {
        return dataSource.uniqueQuery("sql/tele2/getArticleByRegion.ftl", ImmutableMap.of(
                        "regionId", region),
                DataSourceAdapter.SingleColumnLongRowMapper);
    }

}
