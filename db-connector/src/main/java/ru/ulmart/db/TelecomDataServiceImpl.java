package ru.ulmart.db;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ulmart.common.data.DataSourceAdapter;
import ru.ulmart.db.auxiliary.DataService;
import ru.ulmart.db.auxiliary.DataServiceScope;
import ru.ulmart.model.telecom.db.TelecomDataService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service
@DataService(scope = DataServiceScope.COMMON, master = true, slave = true)
public class TelecomDataServiceImpl implements TelecomDataService {
    private static final String NUMBER_STATUS_FREE = "FREE";
    private static final String NUMBER_STATUS_RESERVED = "RESERVED";
    private static final String NUMBER_STATUS_SOLD = "SOLD";

    private DataSourceAdapter dataSource;

    private final Logger logger = LoggerFactory.getLogger(TelecomDataServiceImpl.class);

    public DataSourceAdapter getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSourceAdapter dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public boolean hasInThisRegion(TelecomType telecomType, Long ulmartCityId) {
        TelecomCity telecomCity = getTelecomCity(telecomType, ulmartCityId);
        return telecomCity != null;
    }

    @Override
    public TelecomCity getTelecomCity(TelecomType telecomType, Long ulmartCityId) {
        List<TelecomCity> cities = dataSource.query("sql/telecom/getTelecomCity.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .build(),
                TELECOM_CITY_RM);
        if (cities == null || cities.size() == 0) {
            return null;
        }
        TelecomCity city = cities.get(0);
        city.setTelecomType(telecomType);
        return cities.get(0);
    }

    @Override
    public List<Tariff> getTariffs(TelecomType telecomType, String region) {
        Integer ulmartCityId = getCityId(region);
        List<Tariff> tariffs = dataSource.query("sql/telecom/getTariffs.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .build(),
                TARIFF_RM);
        if (tariffs != null) {
            for (Tariff tariff : tariffs) {
                tariff.setTelecomType(telecomType);
            }
        }

        return tariffs;
    }

    private Integer getCityId(String region) {
        Integer ulmartCityId = 0;
        try {
            ulmartCityId = Integer.valueOf(region);
        } catch (NumberFormatException e) {
            logger.error("region id is not a number");
        }
        return ulmartCityId;
    }

    @Override
    public TariffDetails getTariffDetail(TelecomType telecomType, Long id, String region) {
        Tariff tariff = getTariff(telecomType, id, region);
        if (tariff == null || id == null) {
            return null;
        }

        Integer ulmartCityId = getCityId(region);
        List<TariffParam> params = dataSource.query("sql/telecom/getTariffDetail.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .put("tariff_id", id)
                        .build(),
                TARIFF_PARAM_RM);
        return getTariffDetails(tariff, params);
    }

    private TariffDetails getTariffDetails(Tariff tariff, List<TariffParam> params) {
        Map<Long, TariffParam> paramMap = new HashMap<>();
        List<TariffParam> paramTree = new ArrayList<>();
        if (params != null) {
            // инициализация дерева
            for (TariffParam param : params) {
                paramMap.put(param.getId(), param);
            }
            // привязка дочерних узлов к родительским
            for (TariffParam param : params) {
                if (param.getParentId() == null || param.getParentId() == 0L) {
                    paramTree.add(param);
                } else {
                    TariffParam parent = paramMap.get(param.getParentId());
                    if (parent != null) {
                        parent.getChildren().add(param);
                    }
                }
            }
        }

        TariffDetails details = new TariffDetails();
        details.setTariff(tariff);
        details.setTariffParams(paramTree);
        return details;
    }

    @Override
    public TariffDetails getTariffDetailCommon(TelecomType telecomType, String region) {
        Integer ulmartCityId = getCityId(region);
        List<TariffParam> params = dataSource.query("sql/telecom/getTariffDetailCommon.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .build(),
                TARIFF_PARAM_RM);
        return getTariffDetails(null, params);
    }

    @Override
    public Tariff getTariff(TelecomType telecomType, Long id, String region) {
        if (id == null) {
            return null;
        }
        Integer ulmartCityId = getCityId(region);
        List<Tariff> tariffs = dataSource.query("sql/telecom/getTariff.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .put("tariff_id", id)
                        .build(),
                TARIFF_RM);
        if (tariffs != null && !tariffs.isEmpty()) {
            Tariff tariff = tariffs.get(0);
            tariff.setTelecomType(telecomType);
            return tariff;
        }

        return null;
    }

    @Override
    public Long getCurrentRegionId(TelecomType telecomType, Long ulmartCityId) {
        return ulmartCityId;
    }

    @Override
    public Tariff getDefaultTariff(TelecomType telecomType, String region) {
        Integer ulmartCityId = getCityId(region);
        List<Tariff> defaultTariffs = dataSource.query("sql/telecom/getDefaultTariff.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .build(),
                TARIFF_RM);
        if (defaultTariffs != null && !defaultTariffs.isEmpty()) {
            Tariff tariff = defaultTariffs.get(0);
            tariff.setTelecomType(telecomType);
            return tariff;
        }

        return null;
    }

    @Override
    public TelecomTariffType getDefaultTariffType() {
        List<TelecomTariffType> defaultTariffType = dataSource.query("sql/telecom/getDefaultTariffType.ftl", null,
                TARIFF_TYPE_RM);
        if (defaultTariffType != null && !defaultTariffType.isEmpty()) {
            return defaultTariffType.get(0);
        }

        return null;
    }

    @Override
    public List<PhoneNumber> getNumbers(String region, TelecomType telecomType, FilterRequest filterRequest) {
        if(filterRequest.getTake() == null || filterRequest.getTake() == 0) {
            return Collections.emptyList();
        }
        Integer ulmartCityId = getCityId(region);
        List<PhoneNumber> numbers = dataSource.query("sql/telecom/getNumbersForTypes.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .put("filter", filterRequest)
                        .build(),
                PHONE_NUMBER_RM);

        return numbers;
    }

    @Override
    public List<NumberType> getNumberTypes() {
        return dataSource.query("sql/telecom/getPhoneTypes.ftl", ImmutableMap.<String, Object>builder()
                        .build(),
                NUMBER_TYPE_RM);
    }

    @Override
    public PhoneNumber getNumber(String numberId) {
        List<PhoneNumber> numbers = dataSource.query("sql/telecom/getNumberById.ftl", ImmutableMap.<String, Object>builder()
                        .put("id", numberId)
                        .build(),
                PHONE_NUMBER_RM);

        if (numbers != null && numbers.size() > 0) {
            return numbers.get(0);
        }
        return null;
    }

    @Override
    public PhoneNumber getDefaultNumber(String region, TelecomType telecomType) {
        Integer ulmartCityId = getCityId(region);
        List<NumberType> numberTypes = getNumberTypes();
        for (NumberType numberType : numberTypes) {
            List<PhoneNumber> numbers = dataSource.query("sql/telecom/getDefaultNumber.ftl", ImmutableMap.<String, Object>builder()
                            .put("type", numberType.getAlias())
                            .put("telecom_type_id", telecomType.getId())
                            .put("ulmart_city_id", ulmartCityId)
                            .build(),
                    PHONE_NUMBER_RM);

            if (numbers != null && numbers.size() > 0) {
                return numbers.get(0);
            }
        }
        return null;
    }

    @Override
    public Long getArticleForUlmartCity(Long ulmartCityId, TelecomType telecomType) {
        final List<Long> articles = new ArrayList<>();
        dataSource.query("sql/telecom/getArticle.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .build(),
                new RowCallbackHandler() {
                    @Override
                    public void processRow(ResultSet resultSet) throws SQLException {
                        articles.add(resultSet.getLong(1));
                    }
                });

        if (articles.size() > 0) {
            return articles.get(0);
        }
        return null;
    }

    @Override
    public List<NumberTypeMapping> getNumberTypeMappings(String region, TelecomType telecomType) {
        return dataSource.query("sql/telecom/getPhoneTypeMappings.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("region", region)
                        .build(),
                NUMBER_TYPE_MAPPING_RM);
    }

    @Override
    public List<Sim> getSims(Long ulmartCityId, TelecomType telecomType) {
        return dataSource.query("sql/telecom/getSimTypes.ftl", ImmutableMap.<String, Object>builder()
                        .put("telecom_type_id", telecomType.getId())
                        .put("ulmart_city_id", ulmartCityId)
                        .build(),
                SIM_RM);
    }

    @Override
    @Transactional
    public boolean sellNumber(String orderId, CartTelecomInfo cartTelecomInfo) {
        return changeNumberStatus(cartTelecomInfo.getPhoneNumber().getId(), null, NUMBER_STATUS_SOLD, orderId);
    }

    @Override
    public boolean freeNumber(CartTelecomInfo cartTelecomInfo) {
        return changeNumberStatus(cartTelecomInfo.getPhoneNumber().getId(), null, NUMBER_STATUS_FREE, null);
    }

    @Override
    @Transactional
    public boolean reserveNumber(CartTelecomInfo cartTelecomInfo) {
        return changeNumberStatus(cartTelecomInfo.getPhoneNumber().getId(), NUMBER_STATUS_FREE,
                NUMBER_STATUS_RESERVED, null);
    }

    private boolean changeNumberStatus(String id, String oldStatus, String newStatus, String orderId) {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.<String, Object>builder()
                .put("id", id);
        if (newStatus != null) {
            builder.put("newStatus", newStatus);
        }
        if (oldStatus != null) {
            builder.put("oldStatus", oldStatus);
        }
        if (orderId != null) {
            builder.put("orderId", orderId);
        }
        int count = dataSource.update("sql/telecom/updateNumberStatus.ftl",
                builder.build());
        return count == 1;
    }

    private static final RowMapper<TelecomCity> TELECOM_CITY_RM = new RowMapper<TelecomCity>() {
        @Override
        public TelecomCity mapRow(ResultSet rs, int rowNum) throws SQLException {
            TelecomCity telecomCity = new TelecomCity();
            telecomCity.setTelecomCityId(rs.getLong("telecom_city_id"));
            telecomCity.setTelecomRegionId(rs.getLong("region_id"));
            telecomCity.setTelecomRegionName(rs.getString("region_name"));
            telecomCity.setUlmartCityId(rs.getLong("ulmart_city_id"));
            return telecomCity;
        }
    };

    public static final RowMapper<Tariff> TARIFF_RM = new RowMapper<Tariff>() {
        @Override
        public Tariff mapRow(ResultSet rs, int rowNum) throws SQLException {
            Tariff tariff = new Tariff();
            tariff.setId((long) rs.getInt("id"));
            tariff.setAlias(rs.getString("alias"));
            tariff.setName(rs.getString("title"));
            tariff.setDescription(rs.getString("description"));
            tariff.setPrice(rs.getLong("price"));
            tariff.setImageUrl(rs.getString("image_url"));
            if(rs.getObject("good_id") != null) {
                // rs.getLong("good_id") вернет 0L вместо null
                tariff.setGoodId(rs.getLong("good_id"));
            }
            TelecomTariffType type = new TelecomTariffType();
            type.setId(rs.getLong("type_id"));
            type.setName(rs.getString("type_name"));
            type.setDescription(rs.getString("type_description"));
            type.setAlias(rs.getString("type_alias"));

            tariff.setTelecomTariffType(type);
            tariff.setRegionDefault(rs.getBoolean("default"));
            return tariff;
        }
    };



    public static final RowMapper<TelecomTariffType> TARIFF_TYPE_RM = new RowMapper<TelecomTariffType>() {
        @Override
        public TelecomTariffType mapRow(ResultSet rs, int rowNum) throws SQLException {
            TelecomTariffType type = new TelecomTariffType();
            type.setId(rs.getLong("type_id"));
            type.setName(rs.getString("type_name"));
            type.setDescription(rs.getString("type_description"));
            type.setAlias(rs.getString("type_alias"));
            return type;
        }
    };

    public static final RowMapper<TariffParam> TARIFF_PARAM_RM = new RowMapper<TariffParam>() {
        @Override
        public TariffParam mapRow(ResultSet rs, int rowNum) throws SQLException {
            TariffParam param = new TariffParam();
            param.setId(rs.getLong("id"));
            param.setParentId(rs.getLong("parent_id"));
            param.setIndex(rs.getInt("index"));
            param.setTitle(rs.getString("title"));
            param.setAlias(rs.getString("alias"));
            param.setNote(rs.getString("note"));
            param.setDescription(rs.getString("description"));
            param.setValue(rs.getString("value"));
            param.setValueAsTitle(rs.getBoolean("value_as_title"));
            param.setHidden(rs.getBoolean("hidden"));
            param.setBase(rs.getBoolean("base"));
            param.setCollapsible(rs.getBoolean("collapsible"));
            param.setExpandedByDefault(rs.getBoolean("expandedByDefault"));
            return param;
        }
    };

    public static final RowMapper<Sim> SIM_RM = new RowMapper<Sim>() {
        @Override
        public Sim mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Sim(rs.getString("name"), rs.getString("sim_type"), rs.getLong("good_id"),
                    rs.getLong("tariff_type_id"));
        }
    };

    public static final RowMapper<NumberTypeMapping> NUMBER_TYPE_MAPPING_RM = new RowMapper<NumberTypeMapping>() {
        @Override
        public NumberTypeMapping mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new NumberTypeMapping(rs.getLong("id"), rs.getString("title"), rs.getString("alias"),
                    rs.getInt("position"), rs.getString("operator_number_type_id"), rs.getString("operator_number_type_alias"));
        }
    };

    public static final RowMapper<PhoneNumber> PHONE_NUMBER_RM = new RowMapper<PhoneNumber>() {
        @Override
        public PhoneNumber mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new PhoneNumber(rs.getLong("number_id"),
                    rs.getString("number"),rs.getString("description"),
                    new NumberType(rs.getLong("type_id"), rs.getString("title"), rs.getString("alias"), rs.getInt("position")),
                    rs.getLong("operator_number_type_id"), rs.getInt("type_weight"),
                    rs.getLong("price"), rs.getString("telecom_number_id"));
        }
    };

    public static final RowMapper<NumberType> NUMBER_TYPE_RM = new RowMapper<NumberType>() {
        @Override
        public NumberType mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new NumberType(rs.getLong("id"), rs.getString("title"), rs.getString("alias"), rs.getInt("position"));
        }
    };
}
