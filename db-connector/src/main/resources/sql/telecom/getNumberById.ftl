SELECT tn.id as number_id, tn.number, tn.description, tn.type_id, tn.type_weight, tn.telecom_number_id,
        tnt.id as type_id, tnt.title, tnt.alias, tnt.position, tnr.price, tont.operator_number_type_id
    FROM telecom_numbers tn
    JOIN telecom_number_types as tnt on tn.type_id = tnt.id
    JOIN telecom_numbers_regions as tnr on tn.id = tnr.number_id
    JOIN telecom_regions as tr on tr.id = tnr.telecom_region_id
    JOIN telecom_operator_number_types as tont on tont.number_type_id = tnt.id and tont.telecom_region_id = tr.id
    WHERE tn.id = ${id}