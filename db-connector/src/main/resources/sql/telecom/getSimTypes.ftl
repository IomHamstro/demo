SELECT sim_type, good_id, tariff_type_id, name FROM telecom_regions_sim trs
JOIN telecom_operator as t on t.alias = trs.telecom_type and t.id = ${telecom_type_id}
WHERE trs.ulmart_city_id = ${ulmart_city_id}
ORDER BY trs.position ASC;