SELECT tt.id, tt.alias, tt.title, tt.description, tt.image_url, ttr.price, ttr.`default`, ttt.id as type_id, ttt.name as type_name, ttt.description as type_description,
ttt.alias as type_alias, tt.good_id
FROM telecom_tariffs tt
JOIN telecom_tariff_type ttt on ttt.id = tt.type_id
JOIN telecom_tariffs_regions ttr on ttr.tariff_id = tt.id
JOIN telecom_regions as tr on tr.id = ttr.telecom_region_id and tr.telecom_id = ${telecom_type_id}
JOIN telecom_ulmart_cities as tuc on tuc.telecom_region_id = tr.id AND tuc.ulmart_city_id = ${ulmart_city_id}
WHERE tt.id = ${tariff_id}