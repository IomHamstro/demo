<#list filter.typesAlias as type>
    (SELECT tn.id as number_id, tn.number, tn.description, tn.type_id, tn.type_weight, tn.telecom_number_id,
        tnt.id as type_id, tnt.title, tnt.alias, tnt.position, tnr.price, tont.operator_number_type_id
    FROM telecom_numbers tn
    JOIN telecom_number_types as tnt on tn.type_id = tnt.id AND tnt.alias = '${sql(type)}'
    JOIN telecom_numbers_regions as tnr on tn.id = tnr.number_id
    JOIN telecom_regions as tr on tr.id = tnr.telecom_region_id and tr.telecom_id = ${telecom_type_id}
    JOIN telecom_ulmart_cities as tuc on tuc.telecom_region_id = tr.id AND tuc.ulmart_city_id = ${ulmart_city_id}
    JOIN telecom_operator_number_types as tont on tont.number_type_id = tnt.id and tont.telecom_region_id = tr.id
    WHERE tn.status = 'FREE'
    <#if filter.combination??>
        AND tn.number like '%${sql(filter.combination)}%'
    </#if>
    ORDER BY type_weight DESC, number_id
    LIMIT ${filter.skip}, ${filter.take})
    <#if type_has_next>UNION</#if>
</#list>