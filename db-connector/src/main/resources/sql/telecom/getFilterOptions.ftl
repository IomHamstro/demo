select
MIN(tnr.price) as priceMin,
MAX(tnr.price) as priceMax
from telecom_numbers tn
JOIN telecom_numbers_regions as tnr on tn.id = tnr.number_id
JOIN telecom_regions as tr on tr.id = tnr.telecom_region_id and tr.telecom_id = '${telecom_type_id}'
JOIN telecom_ulmart_cities as tuc on tuc.telecom_region_id = tr.id AND tuc.ulmart_city_id = ${ulmart_city_id}
WHERE tn.status = 'FREE'