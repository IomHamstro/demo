SELECT ttpc.id, ttpc.parent_id, ttpc.index, ttpc.title, ttpc.alias, ttpc.note, ttpc.description, ttpc.value_as_title,
ttpc.hidden, ttpc.base, ttpc.collapsible, ttpc.expandedByDefault, ttpc.value
FROM telecom_tariffs_params_common ttpc
JOIN telecom_regions as tr on tr.id = ttpc.telecom_region_id and tr.telecom_id = ${telecom_type_id}
JOIN telecom_ulmart_cities as tuc on tuc.telecom_region_id = tr.id AND tuc.ulmart_city_id = ${ulmart_city_id}
ORDER BY ttpc.parent_id