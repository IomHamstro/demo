SELECT ttp.id, ttp.parent_id, ttp.index, ttp.title, ttp.alias, ttp.note, ttp.description, ttp.value_as_title, ttp.hidden, ttp.base, ttp.collapsible, ttp.expandedByDefault, ttp.value
FROM telecom_tariffs_params ttp
JOIN telecom_tariffs tt on tt.id = ttp.tariff_id
JOIN telecom_tariffs_regions ttr on ttr.tariff_id = tt.id
JOIN telecom_regions as tr on tr.id = ttr.telecom_region_id and tr.telecom_id = ${telecom_type_id}
JOIN telecom_ulmart_cities as tuc on tuc.telecom_region_id = tr.id AND tuc.ulmart_city_id = ${ulmart_city_id}
WHERE ttp.tariff_id = ${tariff_id}
ORDER BY ttp.parent_id

