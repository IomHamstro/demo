UPDATE telecom_numbers
SET status = '${newStatus}'
<#if orderId??>
, order_id = '${orderId}'
</#if>
WHERE id = ${id}
<#if oldStatus??>
and status = '${oldStatus}'
</#if>