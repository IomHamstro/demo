package main.java.ru.ulmart.service.telecom.problem;

/**
 * @author Mikhail Potter
 *         30.10.2015
 */
@SuppressWarnings("serial")
public class CreateOrderException extends ServiceInvocationException {

    private String reservationToken;

    public CreateOrderException(String errorMessage) {
        super(errorMessage);
    }

    public CreateOrderException(Object request, Object response, String errorMessage, String reservationToken) {
        super(request, response, errorMessage);
        this.reservationToken = reservationToken;
    }

    public String getReservationToken() {
        return reservationToken;
    }

    public void setReservationToken(String reservationToken) {
        this.reservationToken = reservationToken;
    }

}
