package ru.ulmart.service.telecom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.service.sap.cxf.confirmOrderWithDelivery.DtGoods;

import java.math.BigDecimal;
import java.math.BigInteger;

public class TelecomSapUtil {
    private static final Integer MSIDN_LENGTH = 11;
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String toJson(Object object) {
        if (object == null)
            return null;
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return object.toString();
        }
    }

    public static DtGoods telecomInfoToDtGoods(CartTelecomInfo telecomInfo, String sapShopId, String orderNumber) {
        DtGoods goodsReserve = new DtGoods();
        goodsReserve.setGoodsID(String.valueOf(telecomInfo.getUlmartGoodId()));
        goodsReserve.setCount(BigInteger.ONE);
        goodsReserve.setWarehouseID(sapShopId);
        goodsReserve.setOrderNumber(orderNumber);
        goodsReserve.setTelecomType(telecomInfo.getTelecomType().getTitle());
        Tariff tariff = telecomInfo.getTariff();
        goodsReserve.setTariffId(tariff.getId().longValue());
        goodsReserve.setTariffName(tariff.getName());
        goodsReserve.setTariffType(tariff.getTelecomTariffType().getName());
        goodsReserve.setTariffpriceActual(new BigDecimal(tariff.getPrice()));
        goodsReserve.setTariffregion(telecomInfo.getRegion());

        goodsReserve.setSimType(telecomInfo.getSim().getType());
        goodsReserve.setSimType(telecomInfo.getSim().getType() + "_" + telecomInfo.getSim().getGoodId());

        PhoneNumber phoneNumber = telecomInfo.getPhoneNumber();
        goodsReserve.setPhoneId(phoneNumber.getId());
        goodsReserve.setPhoneType(phoneNumber.getNumberType().getTitle());
        goodsReserve.setMsisdn(formatMsisdn(phoneNumber.getMsisdn()));
        goodsReserve.setPhoneNumberCost(BigDecimal.valueOf(phoneNumber.getCost()));
        goodsReserve.setCost(BigDecimal.valueOf(telecomInfo.getPrice()));
        goodsReserve.setPriceItem(String.valueOf(telecomInfo.getPrice()));
        return goodsReserve;
    }

    public static String formatMsisdn(String msisdn) {
        if (MSIDN_LENGTH.equals(msisdn.length())) {
            return msisdn.substring(0, 1) + " " + msisdn.substring(1, 4) + " " +
                    msisdn.substring(4, 7) + "-" + msisdn.substring(7);
        }
        return msisdn;
    }
}
