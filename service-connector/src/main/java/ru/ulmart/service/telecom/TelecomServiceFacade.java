package ru.ulmart.service.telecom;

import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Sim;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomGood;
import ru.ulmart.model.telecom.TelecomTariffType;
import ru.ulmart.model.telecom.TelecomType;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface TelecomServiceFacade {
    Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest,
                                                   TelecomType telecomType);

    Map<NumberType, List<PhoneNumber>> listNumbers(long ulmartCityId, FilterRequest filterRequest,
                                                   TelecomType telecomType);

    List<Sim> getSims(Long ulmartCityId, TelecomType telecomType);

    /**
     * Проверка факта того, что для данного ЦИЗ наличие sim не проверяется
     * @param shopId id ЦИЗ
     * @return признак, что sim всегда доступна для данного ЦИЗ
     */
    Boolean isSimAlwaysAvailable(Long shopId, TelecomType telecomType);

    PhoneNumber getNumberInfo(String region, String numberId,
                              TelecomType telecomType);

    PhoneNumber getNumberInfo(String numberId, Long ulmartCityId,
                              TelecomType telecomType);

    PhoneNumber getNumber(String region, String numberId, TelecomType telecomType);

    List<Tariff> listTariffs(Long ulmartCityId, TelecomType telecomType);

    Map<TelecomType, List<Long>> getGoodTariffIdsMap(long ulmartCityId);

    List<Tariff> listTariffs(String region, TelecomType telecomType);

    TariffDetails getTariffDetail(Long id, String region,
                                  TelecomType telecomType);

    TariffDetails getTariffDetailCommon(Long ulmartCityId,
                                        TelecomType telecomType);

    TariffDetails getTariffDetail(Long id, Long ulmartCityId,
                                  TelecomType telecomType);

    Tariff getDefaultTariff(String region, TelecomType telecomType);

    Tariff getTariffOrDefault(Long id, String region,
                              TelecomType telecomType);

    Tariff getTariffOrDefault(Long id, Long ulmartCityId,
                              TelecomType telecomType);

    TelecomCity getCityInfo(Long ulmartCityId, TelecomType telecomType);

    Map<TelecomType, Boolean> hasInThisRegion(final Long ulmartCityId, TelecomType... telecomTypes);

    String getCurrentRegionId(Long ulmartCityId, TelecomType telecomType);

    Map<TelecomType, String> getCurrentRegionIdMap(Long ulmartCityId, TelecomType... telecomTypes);

    Long getArticleForUlmartCity(Long ulmartCityId, TelecomType telecomType);

    String onBeforeConfirmOrder(String reservationToken, Client client, long shopId,
                                Collection<TelecomGood> cart, TelecomType telecomType);

    Boolean onBeforeAddToCart(CartTelecomInfo cartTelecomInfo, TelecomType telecomType);

    Boolean onAfterRemoveFromCart(CartTelecomInfo cartTelecomInfo, TelecomType telecomType);

    Boolean onAfterAddToOrder(String orderId,
                              CartTelecomInfo cartTelecomInfo,
                              TelecomType telecomType);

    Boolean onAfterRemoveFromOrder(CartTelecomInfo cartTelecomInfo, TelecomType telecomType);

    List<NumberType> getNumberTypes();

    TelecomTariffType getDefaultTariffType();

    boolean hasNumbers(TelecomType telecomType);
}
