package main.java.ru.ulmart.service.telecom.problem;

public class ServiceInvocationException extends RuntimeException {
    private static final long serialVersionUID = 8162026703208673743L;

    private Object request;

    private Object response;

    public ServiceInvocationException(String s) {
        super(s);
    }

    public ServiceInvocationException(Object request, Object response, String s) {
        super(s);
        this.request = request;
        this.response = response;
    }

    public Object getRequest() {
        return request;
    }

    public Object getResponse() {
        return response;
    }

}