package main.java.ru.ulmart.service.exception;

/* Для одиночных обращений ошибку игнорируем, для циклов обрываем запросы*/
public class HybrisUnavailableException extends Exception {
    public HybrisUnavailableException(String message) {
        super(message);
    }
}
