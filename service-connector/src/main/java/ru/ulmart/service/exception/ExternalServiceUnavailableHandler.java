package main.java.ru.ulmart.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import ru.ulmart.service.availability.ExternalServicesAvailabilityException;

import java.io.IOException;

/**
 * Исключение при ошбиках внешнего сервиса
 * Created by Andrey V. Murzin on 23.11.16.
 */
public class ExternalServiceUnavailableHandler extends DefaultResponseErrorHandler  {

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        HttpStatus statusCode;
        try {
            statusCode = response.getStatusCode();
        } catch (IllegalArgumentException e) {
            throw new UnknownHttpStatusCodeException(response.getRawStatusCode(), response.getStatusText(), response.getHeaders(), null, null);
        }
        throw new ExternalServicesAvailabilityException(statusCode);
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return super.hasError(response);
    }
}
