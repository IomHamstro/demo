
package ru.ulmart.service.sap.cxf.confirmOrderWithDelivery;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * <p>Java class for dt_goods complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dt_goods">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="conv_fact" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="goodsID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="brand" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="matnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="goodsName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="priceItem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barcodeID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="warehouseID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="altDelivDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="reserveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="goodStatus" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="acquiredXxlBonus" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="lifnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lifnrPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DLogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PLogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mlogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliverTimeAverage" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="deliverTimeGuarantee" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="issueType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telecomType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="phoneType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="simType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tariffId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tariffType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tariffregion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tariffpriceActual" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PhoneId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneNumberCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dt_goods", propOrder = {
    "convFact",
    "goodsID",
    "brand",
    "matnr",
    "goodsName",
    "priceItem",
    "barcodeID",
    "warehouseID",
    "altDelivDate",
    "count",
    "reserveDate",
    "goodStatus",
    "acquiredXxlBonus",
    "lifnr",
    "lifnrPrice",
    "dLogo",
    "pLogo",
    "mlogo",
    "deliverTimeAverage",
    "deliverTimeGuarantee",
    "issueType",
    "orderNumber",
    "telecomType",
    "tariffName",
    "phoneType",
    "simType",
    "cost",
    "tariffId",
    "tariffType",
    "tariffregion",
    "tariffpriceActual",
    "phoneId",
    "msisdn",
    "phoneNumberCost"
})
public class DtGoods {

    @XmlElement(name = "conv_fact")
    protected BigInteger convFact;
    protected String goodsID;
    protected String brand;
    protected String matnr;
    protected String goodsName;
    protected String priceItem;
    protected String barcodeID;
    protected String warehouseID;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar altDelivDate;
    protected BigInteger count;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reserveDate;
    protected BigInteger goodStatus;
    protected BigInteger acquiredXxlBonus;
    protected String lifnr;
    protected String lifnrPrice;
    @XmlElement(name = "DLogo")
    protected String dLogo;
    @XmlElement(name = "PLogo")
    protected String pLogo;
    @XmlElement(name = "Mlogo")
    protected String mlogo;
    protected BigInteger deliverTimeAverage;
    protected BigInteger deliverTimeGuarantee;
    protected String issueType;
    @XmlElement(name = "OrderNumber")
    protected String orderNumber;
    protected String telecomType;
    @XmlElement(name = "TariffName")
    protected String tariffName;
    protected String phoneType;
    protected String simType;
    protected BigDecimal cost;
    protected Long tariffId;
    protected String tariffType;
    protected String tariffregion;
    protected BigDecimal tariffpriceActual;
    @XmlElement(name = "PhoneId")
    protected String phoneId;
    protected String msisdn;
    @XmlElement(name = "PhoneNumberCost")
    protected BigDecimal phoneNumberCost;

    /**
     * Gets the value of the convFact property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getConvFact() {
        return convFact;
    }

    /**
     * Sets the value of the convFact property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setConvFact(BigInteger value) {
        this.convFact = value;
    }

    /**
     * Gets the value of the goodsID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGoodsID() {
        return goodsID;
    }

    /**
     * Sets the value of the goodsID property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGoodsID(String value) {
        this.goodsID = value;
    }

    /**
     * Gets the value of the brand property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the matnr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMatnr() {
        return matnr;
    }

    /**
     * Sets the value of the matnr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMatnr(String value) {
        this.matnr = value;
    }

    /**
     * Gets the value of the goodsName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * Sets the value of the goodsName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGoodsName(String value) {
        this.goodsName = value;
    }

    /**
     * Gets the value of the priceItem property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPriceItem() {
        return priceItem;
    }

    /**
     * Sets the value of the priceItem property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPriceItem(String value) {
        this.priceItem = value;
    }

    /**
     * Gets the value of the barcodeID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBarcodeID() {
        return barcodeID;
    }

    /**
     * Sets the value of the barcodeID property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBarcodeID(String value) {
        this.barcodeID = value;
    }

    /**
     * Gets the value of the warehouseID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getWarehouseID() {
        return warehouseID;
    }

    /**
     * Sets the value of the warehouseID property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setWarehouseID(String value) {
        this.warehouseID = value;
    }

    /**
     * Gets the value of the altDelivDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getAltDelivDate() {
        return altDelivDate;
    }

    /**
     * Sets the value of the altDelivDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setAltDelivDate(XMLGregorianCalendar value) {
        this.altDelivDate = value;
    }

    /**
     * Gets the value of the count property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setCount(BigInteger value) {
        this.count = value;
    }

    /**
     * Gets the value of the reserveDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getReserveDate() {
        return reserveDate;
    }

    /**
     * Sets the value of the reserveDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setReserveDate(XMLGregorianCalendar value) {
        this.reserveDate = value;
    }

    /**
     * Gets the value of the goodStatus property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getGoodStatus() {
        return goodStatus;
    }

    /**
     * Sets the value of the goodStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setGoodStatus(BigInteger value) {
        this.goodStatus = value;
    }

    /**
     * Gets the value of the acquiredXxlBonus property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getAcquiredXxlBonus() {
        return acquiredXxlBonus;
    }

    /**
     * Sets the value of the acquiredXxlBonus property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setAcquiredXxlBonus(BigInteger value) {
        this.acquiredXxlBonus = value;
    }

    /**
     * Gets the value of the lifnr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLifnr() {
        return lifnr;
    }

    /**
     * Sets the value of the lifnr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLifnr(String value) {
        this.lifnr = value;
    }

    /**
     * Gets the value of the lifnrPrice property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLifnrPrice() {
        return lifnrPrice;
    }

    /**
     * Sets the value of the lifnrPrice property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLifnrPrice(String value) {
        this.lifnrPrice = value;
    }

    /**
     * Gets the value of the dLogo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDLogo() {
        return dLogo;
    }

    /**
     * Sets the value of the dLogo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDLogo(String value) {
        this.dLogo = value;
    }

    /**
     * Gets the value of the pLogo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPLogo() {
        return pLogo;
    }

    /**
     * Sets the value of the pLogo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPLogo(String value) {
        this.pLogo = value;
    }

    /**
     * Gets the value of the mlogo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMlogo() {
        return mlogo;
    }

    /**
     * Sets the value of the mlogo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMlogo(String value) {
        this.mlogo = value;
    }

    /**
     * Gets the value of the deliverTimeAverage property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getDeliverTimeAverage() {
        return deliverTimeAverage;
    }

    /**
     * Sets the value of the deliverTimeAverage property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setDeliverTimeAverage(BigInteger value) {
        this.deliverTimeAverage = value;
    }

    /**
     * Gets the value of the deliverTimeGuarantee property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getDeliverTimeGuarantee() {
        return deliverTimeGuarantee;
    }

    /**
     * Sets the value of the deliverTimeGuarantee property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setDeliverTimeGuarantee(BigInteger value) {
        this.deliverTimeGuarantee = value;
    }

    /**
     * Gets the value of the issueType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIssueType() {
        return issueType;
    }

    /**
     * Sets the value of the issueType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIssueType(String value) {
        this.issueType = value;
    }

    /**
     * Gets the value of the orderNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOrderNumber(String value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the telecomType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTelecomType() {
        return telecomType;
    }

    /**
     * Sets the value of the telecomType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTelecomType(String value) {
        this.telecomType = value;
    }

    /**
     * Gets the value of the tariffName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTariffName() {
        return tariffName;
    }

    /**
     * Sets the value of the tariffName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTariffName(String value) {
        this.tariffName = value;
    }

    /**
     * Gets the value of the phoneType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPhoneType() {
        return phoneType;
    }

    /**
     * Sets the value of the phoneType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPhoneType(String value) {
        this.phoneType = value;
    }

    /**
     * Gets the value of the simType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSimType() {
        return simType;
    }

    /**
     * Sets the value of the simType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSimType(String value) {
        this.simType = value;
    }

    /**
     * Gets the value of the cost property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setCost(BigDecimal value) {
        this.cost = value;
    }

    /**
     * Gets the value of the tariffId property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getTariffId() {
        return tariffId;
    }

    /**
     * Sets the value of the tariffId property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setTariffId(Long value) {
        this.tariffId = value;
    }

    /**
     * Gets the value of the tariffType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTariffType() {
        return tariffType;
    }

    /**
     * Sets the value of the tariffType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTariffType(String value) {
        this.tariffType = value;
    }

    /**
     * Gets the value of the tariffregion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTariffregion() {
        return tariffregion;
    }

    /**
     * Sets the value of the tariffregion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTariffregion(String value) {
        this.tariffregion = value;
    }

    /**
     * Gets the value of the tariffpriceActual property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getTariffpriceActual() {
        return tariffpriceActual;
    }

    /**
     * Sets the value of the tariffpriceActual property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setTariffpriceActual(BigDecimal value) {
        this.tariffpriceActual = value;
    }

    /**
     * Gets the value of the phoneId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPhoneId() {
        return phoneId;
    }

    /**
     * Sets the value of the phoneId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPhoneId(String value) {
        this.phoneId = value;
    }

    /**
     * Gets the value of the msisdn property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the phoneNumberCost property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getPhoneNumberCost() {
        return phoneNumberCost;
    }

    /**
     * Sets the value of the phoneNumberCost property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPhoneNumberCost(BigDecimal value) {
        this.phoneNumberCost = value;
    }

}
