package main.java.ru.ulmart.service.common.logging;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.phase.Phase;

/**
 *
 */
public class ExternalizedLoggingInInterceptor extends BaseExternalizedLoggingInterceptor<LoggingInInterceptor> {
    public ExternalizedLoggingInInterceptor(LoggingInInterceptor handler) {
        super(Phase.RECEIVE, handler);
    }

    public ExternalizedLoggingInInterceptor(String phase, LoggingInInterceptor handler) {
        super(phase, handler);
    }

    public ExternalizedLoggingInInterceptor(String i, String p, LoggingInInterceptor handler) {
        super(i, p, handler);
    }
}
