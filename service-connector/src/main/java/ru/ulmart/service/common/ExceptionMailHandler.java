package main.java.ru.ulmart.service.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ru.ulmart.model.exceptionEvent.ExceptionEvent;
import ru.ulmart.service.mail.ErrorMailingService;

import javax.xml.ws.WebServiceException;
import java.io.IOException;

/**
 * @author Timur Shakurov
 */
public class ExceptionMailHandler {
    public static final String EXCEPTION_PREFIX = "#UlmartServiceException#: ";

    private static final Logger log = LoggerFactory.getLogger(ExceptionMailHandler.class);

    @Autowired
    private ErrorMailingService errorMailingService;

    public TemporaryErpProblem handleException(ExceptionEvent exceptionEvent) {
        exceptionEvent.setEventType("erp_soap_call_error");
        log.error("ERP PROBLEM: error calling method " + exceptionEvent.getMethod());
        Exception ex=exceptionEvent.getException();

        if (errorMailingService == null) {
            log.error("Handling error without sending: exception:" + ex + "; params: " + exceptionEvent.getParameters() + "; method: " + exceptionEvent.getMethod() + "; url: " + exceptionEvent.getUrl());
        } else {
            try {
                if (ex instanceof IOException || ex instanceof WebServiceException) {
                    String subject = String.format("Order Service unavailable (%s)", exceptionEvent.getUrl());
                    errorMailingService.sendServiceUnavailableException(subject, exceptionEvent);
                } else {
                    String subject = String.format("Service Exception: %s (%s)", exceptionEvent.getMethod(), exceptionEvent.getUrl());
                    errorMailingService.sendServiceException(subject, exceptionEvent, exceptionEvent.getParameters());
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return new TemporaryErpProblem("Temporary ERP problems (" + ex.getMessage() + ")", ex);
    }
}
