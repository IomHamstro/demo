package main.java.ru.ulmart.service.common;

/**
 * @author Yuri Sliznikov
 *         25.03.14
 */
public class TemporaryErpProblem extends RuntimeException {

    private static final long serialVersionUID = -396785533202563156L;

    public TemporaryErpProblem() {
    }

    public TemporaryErpProblem(String message) {
        super(message);
    }

    public TemporaryErpProblem(String message, Throwable cause) {
        super(message, cause);
    }

    public TemporaryErpProblem(Throwable cause) {
        super(cause);
    }
}
