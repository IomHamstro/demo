package main.java.ru.ulmart.service.common;

/**
 * @author alexander.shlyannikov
 *         08.04.13 13:42
 */
public class UlmartServiceProblem extends RuntimeException {

    private static final long serialVersionUID = 8931225146875148626L;

    public UlmartServiceProblem() {
    }

    public UlmartServiceProblem(String message) {
        super(message);
    }

    public UlmartServiceProblem(String message, Throwable cause) {
        super(message, cause);
    }

    public UlmartServiceProblem(Throwable cause) {
        super(cause);
    }
}