package main.java.ru.ulmart.service.common.logging;

import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.phase.Phase;

/**
 *
 */
public class ExternalizedLoggingOutInterceptor extends BaseExternalizedLoggingInterceptor<LoggingOutInterceptor> {
    public ExternalizedLoggingOutInterceptor(LoggingOutInterceptor handler) {
        super(Phase.PRE_STREAM, handler);
    }

    public ExternalizedLoggingOutInterceptor(String phase, LoggingOutInterceptor handler) {
        super(phase, handler);
    }

    public ExternalizedLoggingOutInterceptor(String i, String p, LoggingOutInterceptor handler) {
        super(i, p, handler);
    }
}
