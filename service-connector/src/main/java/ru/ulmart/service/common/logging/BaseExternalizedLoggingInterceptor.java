package main.java.ru.ulmart.service.common.logging;

import org.apache.cxf.interceptor.AbstractLoggingInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;

/**
 *
 */
public class BaseExternalizedLoggingInterceptor<T extends AbstractLoggingInterceptor> extends AbstractPhaseInterceptor<Message> {
    private boolean loggingEnabled;
    private T handler;

    public void setLoggingEnabled(boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
    }

    public BaseExternalizedLoggingInterceptor(String phase, T handler) {
        super(phase);
        this.handler = handler;
    }

    public BaseExternalizedLoggingInterceptor(String i, String p, T handler) {
        super(i, p);
        this.handler = handler;
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        if(loggingEnabled) {
            handler.handleMessage(message);
        }
    }
}
