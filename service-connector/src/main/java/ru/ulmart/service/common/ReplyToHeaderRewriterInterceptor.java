package main.java.ru.ulmart.service.common;

import org.apache.xerces.dom.ElementNSImpl;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Node;

import java.util.List;

/**
 * This class rewrites ReplyTo header, because SAP doesn't support /none ReplyTo
 * This is an awful decision - if you find solution with xml config you should use it without doubt
 *
 * @author timur.shakurov
 *         13.07.12 14:54
 */
public class ReplyToHeaderRewriterInterceptor extends AbstractSoapInterceptor {
    public ReplyToHeaderRewriterInterceptor() {
        super(Phase.WRITE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void handleMessage (SoapMessage message ) throws Fault {
        List<Header> headerList = (List<Header>)message.get(Header.HEADER_LIST);
        if(headerList == null) {
            return;
        }
        for(Header header : headerList) {
            //
            if("ReplyTo".equals(header.getName().getLocalPart())) {
                final ElementNSImpl object = (ElementNSImpl) header.getObject();
                final Node firstChild = object.getFirstChild();
                final Node firstChild1 = firstChild.getFirstChild();
                firstChild1.setNodeValue("http://www.w3.org/2005/08/addressing/anonymous");
                break;
            }
        }
    }
}

