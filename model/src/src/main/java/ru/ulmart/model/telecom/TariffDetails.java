package ru.ulmart.model.telecom;

import com.google.common.base.MoreObjects;
import ru.ulmart.model.telecom.tele2.Tele2TariffDetails;
import ru.ulmart.model.telecom.tele2.Tele2TariffParam;

import java.util.ArrayList;
import java.util.List;

public class TariffDetails {
    private Tariff tariff;
    private List<TariffParam> tariffParams;

    public TariffDetails() {
    }

    public TariffDetails(Tele2TariffDetails tele2TariffDetails) {
        this.tariff = new Tariff((tele2TariffDetails.getTele2TariffInfo()));
        if(tele2TariffDetails.getTele2TariffParams() != null) {
            tariffParams = new ArrayList<>();
            for(Tele2TariffParam tele2TariffParam: tele2TariffDetails.getTele2TariffParams()) {
                TariffParam param = new TariffParam(tele2TariffParam);
                tariffParams.add(param);
            }
        }
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public List<TariffParam> getTariffParams() {
        return tariffParams;
    }

    public void setTariffParams(List<TariffParam> tariffParams) {
        this.tariffParams = tariffParams;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tariff", tariff)
                .add("tariffParams", tariffParams)
                .toString();
    }
}
