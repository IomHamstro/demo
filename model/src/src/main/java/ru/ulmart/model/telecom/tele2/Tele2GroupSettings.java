package ru.ulmart.model.telecom.tele2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Настройки группы параметров
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tele2GroupSettings {

    /**
     * Сварачиваемый параметр
     */
    @JsonProperty("IsCollapsible")
    private Boolean isCollapsible;

    /**
     * Открыт по умолчанию
     */
    @JsonProperty("ExpandByDefault")
    private Boolean expandByDefault;

    public Tele2GroupSettings() {
    }

    public Tele2GroupSettings(Boolean isCollapsible, Boolean expandByDefault) {
        this.isCollapsible = isCollapsible;
        this.expandByDefault = expandByDefault;
    }

    public Boolean getIsCollapsible() {
        return isCollapsible;
    }

    public void setIsCollapsible(Boolean isCollapsible) {
        this.isCollapsible = isCollapsible;
    }

    public Boolean getExpandByDefault() {
        return expandByDefault;
    }

    public void setExpandByDefault(Boolean expandByDefault) {
        this.expandByDefault = expandByDefault;
    }

    @Override
    public String toString() {
        return "GroupSettings{" +
                "isCollapsible=" + isCollapsible +
                ", expandByDefault=" + expandByDefault +
                '}';
    }
}
