package ru.ulmart.model.cache;

import java.util.Map;

/**
 * @author alexander.shlyannikov
 */
public interface CacheManager {

    /**
     * Получить карту значений по указанному пути
     * @param path путь (напр. "/catalog/filters")
     * @return карта значений (null если отсутствует)
     */
    Map<Object, Object> getData(String path);

    /**
     * Кол-во эл-тов в кэше
     * @param path путь (напр. "/catalog/filters")
     * @return count
     */
    int getDataSize(String path);

    /**
     * Получить значение по пути и ключу
     * @param path путь (напр. "/catalog/filters")
     * @param key ключ (напр. "123")
     * @return значение (null если отсутствует)
     */
    Object getItem(String path, Object key);

    /**
     * Записать значение по пути и ключу
     * @param path путь (напр. "/catalog/filters")
     * @param key ключ (напр. "123")
     * @param value значение
     */
    void putItem(String path, Object key, Object value);

    /**
     * Записать значение по пути и ключу с фиксированным max временем жизни
     * @param path путь (напр. "/catalog/filters")
     * @param key ключ (напр. "123")
     * @param lifeTime время жизни, s.
     * @param value значение
     */
    void putItem(String path, Object key, Object value, int lifeTime);

    /**
     * Записать значение по пути и ключу с фиксированным max временем жизни
     * @param path путь (напр. "/catalog/filters")
     * @param key ключ (напр. "123")
     * @param value значение
     * @param lifeTime время жизни, s.
     * @param evictionPolicy стратегия очистки кеша
     * @param maxEntries максимальное кол-во записей по данному пути
     */
    void putItem(String path, Object key, Object value, int lifeTime, EvictionPolicy evictionPolicy, int maxEntries);

    /**
     * Удалить карту значений по указанному пути
     * @param path путь (напр. "/catalog/filters")
     */
    void removeData(String path);

    /**
     * Удалить карту значений по указанному пути и ключу
     * @param path путь (напр. "/catalog/filters")
     * @param key ключ (напр. "123")
     */
    void removeItem(String path, Object key);

    /**
     * Clear all cache
     */
    void clear();

    /**
     * Является ли кеш локальным или распределенным
     *
     * @return true если кеш локальный
     */
    boolean isLocal();

    /**
     * Удаляет просроченные ключи
     */
    void evict();
}
