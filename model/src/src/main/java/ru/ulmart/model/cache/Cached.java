package ru.ulmart.model.cache;

import java.lang.annotation.*;

/**
 * @author Foat Akhmadeev
 *         6/4/13
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cached {
    /**
     * время жизни в секундах, время жизни по-умолчанию - 1час
     */
    int lifetime() default 3600;

    String path() default "";

    EvictionPolicy evictionPolicy() default EvictionPolicy.FIFO;

    /**
     * максимальное количество значений ключа по пути {@link Cached#path()},
     * после которых кэш чистится в соответствие с {@link Cached#evictionPolicy()}.
     * по умолчанию настройка пойдёт из имплементации кэша
     */
    int maxEntries() default 0;
}
