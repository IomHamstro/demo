package ru.ulmart.model.telecom;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TelecomCity implements Serializable {
    private Long ulmartCityId;
    private Long telecomCityId;
    private TelecomType telecomType;
    private Long telecomRegionId;
    private String telecomRegionName;

    public Long getUlmartCityId() {
        return ulmartCityId;
    }

    public void setUlmartCityId(Long ulmartCityId) {
        this.ulmartCityId = ulmartCityId;
    }

    public Long getTelecomCityId() {
        return telecomCityId;
    }

    public void setTelecomCityId(Long telecomCityId) {
        this.telecomCityId = telecomCityId;
    }

    public TelecomType getTelecomType() {
        return telecomType;
    }

    public void setTelecomType(TelecomType telecomType) {
        this.telecomType = telecomType;
    }

    public Long getTelecomRegionId() {
        return telecomRegionId;
    }

    public void setTelecomRegionId(Long telecomRegionId) {
        this.telecomRegionId = telecomRegionId;
    }

    public String getTelecomRegionName() {
        return telecomRegionName;
    }

    public void setTelecomRegionName(String telecomRegionName) {
        this.telecomRegionName = telecomRegionName;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("ulmartCityId", ulmartCityId)
                .add("telecomCityId", telecomCityId)
                .add("telecomType", telecomType)
                .add("telecomRegionId", telecomRegionId)
                .add("telecomRegionName", telecomRegionName)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TelecomCity that = (TelecomCity) o;

        if (ulmartCityId != null ? !ulmartCityId.equals(that.ulmartCityId) : that.ulmartCityId != null) return false;
        if (telecomCityId != null ? !telecomCityId.equals(that.telecomCityId) : that.telecomCityId != null)
            return false;
        return telecomType == that.telecomType;

    }

    @Override
    public int hashCode() {
        int result = ulmartCityId != null ? ulmartCityId.hashCode() : 0;
        result = 31 * result + (telecomCityId != null ? telecomCityId.hashCode() : 0);
        result = 31 * result + (telecomType != null ? telecomType.hashCode() : 0);
        return result;
    }
}
