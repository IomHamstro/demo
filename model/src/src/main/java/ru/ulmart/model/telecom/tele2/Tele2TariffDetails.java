package ru.ulmart.model.telecom.tele2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Тело ответа из API - детальная информация о тарифе
 * GET api/{token}/tariff/details/{id}
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tele2TariffDetails {

    @JsonProperty("BaseInfo")
    private Tele2Tariff tele2TariffInfo;
    @JsonProperty("Note")
    private String note;
    @JsonProperty("TariffGrouppedParam")
    private List<Tele2TariffParam> tele2TariffParams;

    public Tele2TariffDetails() {
    }

    public Tele2TariffDetails(Tele2Tariff tele2TariffInfo, String note, List<Tele2TariffParam> tele2TariffParams) {
        this.tele2TariffInfo = tele2TariffInfo;
        this.note = note;
        this.tele2TariffParams = tele2TariffParams;
    }

    public Tele2Tariff getTele2TariffInfo() {
        return tele2TariffInfo;
    }

    public void setTele2TariffInfo(Tele2Tariff tele2TariffInfo) {
        this.tele2TariffInfo = tele2TariffInfo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Tele2TariffParam> getTele2TariffParams() {
        return tele2TariffParams;
    }

    public void setTele2TariffParams(List<Tele2TariffParam> tele2TariffParams) {
        this.tele2TariffParams = tele2TariffParams;
    }

    @Override
    public String toString() {
        return "Tele2TariffDetails{" +
                "tele2TariffInfo=" + tele2TariffInfo +
                ", note='" + note + '\'' +
                ", tele2TariffParams=" + tele2TariffParams +
                '}';
    }
}
