package ru.ulmart.model.telecom;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.MoreObjects;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("serial")
public class NumberTypeMapping implements Serializable {
    private Long numberTypeId;
    private String numberTypeAlias;
    private String numberTypeTitle;
    private String operatorNumberTypeId;
    private String operatorNumberTypeAlias;
    private Integer position;

    public NumberTypeMapping(Long numberTypeId, String numberTypeTitle, String numberTypeAlias, Integer position,
                             String operatorNumberTypeId, String operatorNumberTypeAlias) {
        this.numberTypeId = numberTypeId;
        this.numberTypeTitle = numberTypeTitle;
        this.numberTypeAlias = numberTypeAlias;
        this.operatorNumberTypeId = operatorNumberTypeId;
        this.operatorNumberTypeAlias = operatorNumberTypeAlias;
        this.position = position;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Long getNumberTypeId() {
        return numberTypeId;
    }

    public void setNumberTypeId(Long numberTypeId) {
        this.numberTypeId = numberTypeId;
    }

    public String getNumberTypeAlias() {
        return numberTypeAlias;
    }

    public void setNumberTypeAlias(String numberTypeAlias) {
        this.numberTypeAlias = numberTypeAlias;
    }

    public String getOperatorNumberTypeId() {
        return operatorNumberTypeId;
    }

    public void setOperatorNumberTypeId(String operatorNumberTypeId) {
        this.operatorNumberTypeId = operatorNumberTypeId;
    }

    public String getOperatorNumberTypeAlias() {
        return operatorNumberTypeAlias;
    }

    public void setOperatorNumberTypeAlias(String operatorNumberTypeAlias) {
        this.operatorNumberTypeAlias = operatorNumberTypeAlias;
    }

    public String getNumberTypeTitle() {
        return numberTypeTitle;
    }

    public void setNumberTypeTitle(String numberTypeTitle) {
        this.numberTypeTitle = numberTypeTitle;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("numberTypeId", numberTypeId)
                .add("numberTypeTitle", numberTypeTitle)
                .add("numberTypeAlias", numberTypeAlias)
                .add("position", position)
                .add("operatorNumberTypeId", operatorNumberTypeId)
                .add("operatorNumberTypeAlias", operatorNumberTypeAlias)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NumberTypeMapping that = (NumberTypeMapping) o;

        if (numberTypeId != null ? !numberTypeId.equals(that.numberTypeId) : that.numberTypeId != null) return false;
        return numberTypeAlias != null ? numberTypeAlias.equals(that.numberTypeAlias) : that.numberTypeAlias == null;
    }

    @Override
    public int hashCode() {
        int result = numberTypeId != null ? numberTypeId.hashCode() : 0;
        result = 31 * result + (numberTypeAlias != null ? numberTypeAlias.hashCode() : 0);
        return result;
    }
}
