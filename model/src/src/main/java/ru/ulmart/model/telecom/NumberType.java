package ru.ulmart.model.telecom;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("serial")
public class NumberType implements Serializable {
    @JsonProperty("Id")
    private Long id;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Alias")
    private String alias;
    @JsonProperty("Position")
    private int position;

    public NumberType(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public NumberType(String alias) {
        this.alias = alias;
    }

    public NumberType(Long id, String title, String alias, int position) {
        this.id = id;
        this.title = title;
        this.alias = alias;
        this.position = position;
    }

    public NumberType(NumberType numberType) {
        this.id = numberType.getId();
        this.title = numberType.getTitle();
        this.alias = numberType.getAlias();
        this.position = numberType.getPosition();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("title", title)
                .add("alias", alias)
                .add("position", position)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NumberType)) return false;

        NumberType that = (NumberType) o;

        if (!alias.equals(that.alias)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return alias.hashCode();
    }
}
