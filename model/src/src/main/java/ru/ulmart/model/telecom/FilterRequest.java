package ru.ulmart.model.telecom;

import com.google.common.base.MoreObjects;

import java.util.ArrayList;
import java.util.List;


public class FilterRequest {
    public static final int DEFAULT_TAKE = 64;
    public static final int DEFAULT_SKIP = 0;

    /**
     * Комбинация цифр, которая должна содержаться в номерах
     */
    private String combination;

    /**
     * Список алиасов типов ('обычный', 'золотой' и т.д.)
     */
    private List<String> typesAlias = new ArrayList<>();

    /**
     * Количество записей которые необходимо пропустить
     */
    private Integer skip = DEFAULT_SKIP;

    /**
     * Количество записей которое необходимо выбрать
     */
    private Integer take = DEFAULT_TAKE;

    public FilterRequest() {
    }

    public FilterRequest(String combination) {
        this.combination = combination;
    }

    public FilterRequest(String combination, Integer skip, Integer take) {
        this.combination = combination;
        this.skip = skip;
        this.take = take;
    }

    public FilterRequest(FilterRequest filterRequest) {
        this.combination = filterRequest.getCombination();
        this.typesAlias = filterRequest.getTypesAlias();
        this.skip = filterRequest.getSkip();
        this.take = filterRequest.getTake();
    }

    public String getCombination() {
        return combination;
    }

    public void setCombination(String combination) {
        this.combination = combination;
    }

    public List<String> getTypesAlias() {
        return typesAlias;
    }

    public void setTypesAlias(List<String> typesAlias) {
        this.typesAlias = typesAlias;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public Integer getTake() {
        return take;
    }

    public void setTake(Integer take) {
        this.take = take;
    }

    @Override
    public String toString() {
        String types = null;
        if(typesAlias != null && !typesAlias.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (String type : typesAlias) {
                sb.append(",").append(type);
            }
            types = sb.toString().substring(1);
        }
        return MoreObjects.toStringHelper(this)
                .add("combination", combination)
                .add("typesAlias", types)
                .add("skip", skip)
                .add("take", take)
                .toString();
    }

    public static FilterRequest defaultFilterRequest(List<NumberType> numberTypes, String searchQuery) {
        FilterRequest filterRequest = new FilterRequest();
        filterRequest.setSkip(DEFAULT_SKIP);

        filterRequest.setTake(DEFAULT_TAKE);
        if (numberTypes != null) {
            List<String> types = new ArrayList<>();
            for (NumberType numberType : numberTypes) {
                types.add(numberType.getAlias());
            }
            filterRequest.setTypesAlias(types);
        }
        if(searchQuery != null && !searchQuery.isEmpty()) {
            filterRequest.setCombination(searchQuery);
        }
        return filterRequest;
    }
}
