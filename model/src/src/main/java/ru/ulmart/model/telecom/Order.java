package ru.ulmart.model.telecom;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Order implements Serializable {
    private String id;

    public Order(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
