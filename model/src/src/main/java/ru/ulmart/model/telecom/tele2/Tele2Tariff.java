package ru.ulmart.model.telecom.tele2;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulmart.model.telecom.TelecomTariffType;
import ru.ulmart.model.telecom.TelecomType;

import java.io.Serializable;
import java.util.Objects;

/**
 * Тело ответа из API - информация о тарифе
 * GET api/{token}/tariff/tarifflist
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("serial")
public class Tele2Tariff implements Serializable {
    @JsonProperty("Id")
    private Long id;
    @JsonProperty("ColorCode")
    private String color;
    @JsonProperty("Alias")
    private String alias;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("IsRegionDefault")
    private Boolean isRegionDefault;
    @JsonProperty("IsMnpDefault")
    private Boolean isMnpDefault;
    @JsonProperty("AllowedForMnp")
    private Boolean allowedForMnp;
    @JsonProperty("PriceActual")
    private Double priceActual;
    // Заполняем сами, из API не приходит
    private String imageUrl;
    // Заполняем сами, из API не приходит
    private TelecomTariffType telecomTariffType;
    // Заполняем сами, из API не приходит
    @JsonIgnore
    private TelecomType telecomType;

    public Tele2Tariff(Long id, String name, String color, String description) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.description = description;
    }

    public Tele2Tariff(Long id) {
        this.id = id;
    }

    public Tele2Tariff() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getIsRegionDefault() {
        return isRegionDefault;
    }

    public void setIsRegionDefault(Boolean isRegionDefault) {
        this.isRegionDefault = isRegionDefault;
    }

    public Boolean getIsMnpDefault() {
        return isMnpDefault;
    }

    public void setIsMnpDefault(Boolean isMnpDefault) {
        this.isMnpDefault = isMnpDefault;
    }

    public Boolean getAllowedForMnp() {
        return allowedForMnp;
    }

    public void setAllowedForMnp(Boolean allowedForMnp) {
        this.allowedForMnp = allowedForMnp;
    }

    public Double getPriceActual() {
        return priceActual;
    }

    public void setPriceActual(Double priceActual) {
        this.priceActual = priceActual;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public TelecomTariffType getTelecomTariffType() {
        return telecomTariffType;
    }

    public void setTelecomTariffType(TelecomTariffType telecomTariffType) {
        this.telecomTariffType = telecomTariffType;
    }

    public TelecomType getTelecomType() {
        return telecomType;
    }

    public void setTelecomType(TelecomType telecomType) {
        this.telecomType = telecomType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tele2Tariff tele2Tariff = (Tele2Tariff) o;
        return Objects.equals(getId(), tele2Tariff.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Tele2Tariff{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", alias='" + alias + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", isRegionDefault=" + isRegionDefault +
                ", isMnpDefault=" + isMnpDefault +
                ", allowedForMnp=" + allowedForMnp +
                ", priceActual=" + priceActual +
                ", imageUrl='" + imageUrl + '\'' +
                ", telecomTariffType=" + telecomTariffType +
                '}';
    }
}
