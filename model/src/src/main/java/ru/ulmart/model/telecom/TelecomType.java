package ru.ulmart.model.telecom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Типы операторов связи
 */
public enum TelecomType {

    TELE2(1L, "tele2", "Tele2"),
    MTS(2L, "mts", "МТС"),
    BEELINE(3L, "beeline", "Beeline"),
    YOTA(4L, "yota", "Yota"),
    MEGAFON(5L, "megafon", "Megafon");

    private Long id;
    private String alias;
    private String title;

    TelecomType(Long id, String alias,String title) {
        this.id = id;
        this.alias = alias;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static TelecomType findById(Long id) {
        for (TelecomType telecomType: values()) {
            if (telecomType.getId().equals(id)) {
                return telecomType;
            }
        }

        return null;
    }

    public static TelecomType findByAlias(String alias) {
        for (TelecomType telecomType: values()) {
            if (telecomType.getAlias().equalsIgnoreCase(alias)) {
                return telecomType;
            }
        }

        return null;
    }

    public static List<TelecomType> convertToList(String stringType) {
        if(stringType == null || stringType.isEmpty()) {
            return Collections.emptyList();
        }
        List<String> stringTypes = new ArrayList<>();
        if (stringType != null) {
            stringTypes = Arrays.asList(stringType.split(","));
        }
        List<TelecomType> types = new ArrayList<>();
        for (TelecomType telecomType: values()) {
            for(String type: stringTypes) {
                if (telecomType.getAlias().equalsIgnoreCase(type)) {
                    types.add(telecomType);
                }
            }
        }

        return types;
    }

    public static TelecomType findByTitle(String title) {
        for (TelecomType telecomType : values()) {
            if (telecomType.getTitle().equalsIgnoreCase(title)) {
                return telecomType;
            }
        }
        return null;
    }

    public TelecomType next() {
        return values()[(this.ordinal()+1) % values().length];
    }
}
