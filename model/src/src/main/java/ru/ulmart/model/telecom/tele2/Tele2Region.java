package ru.ulmart.model.telecom.tele2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Информация о регионах Теле2
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tele2Region {

    @JsonProperty("Id")
    private Long id;
    /**
     * Название региона
     */
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Alias")
    private String alias;
    @JsonProperty("IsRegionalCenter")
    private Boolean isRegionalCenter;

    public Tele2Region() {
    }

    public Tele2Region(Long id, String title, String alias, Boolean isRegionalCenter) {
        this.id = id;
        this.title = title;
        this.alias = alias;
        this.isRegionalCenter = isRegionalCenter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Boolean getIsRegionalCenter() {
        return isRegionalCenter;
    }

    public void setIsRegionalCenter(Boolean isRegionalCenter) {
        this.isRegionalCenter = isRegionalCenter;
    }

    @Override
    public String toString() {
        return "Tele2Region{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", alias='" + alias + '\'' +
                ", isRegionalCenter=" + isRegionalCenter +
                '}';
    }
}
