package ru.ulmart.model.telecom;

import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("serial")
public class PhoneNumber implements Serializable {
    private String description;

    private String id;

    private String telecomNumberId;
    /**
     * Номер телефона (например +79061123344)
     */
    private String msisdn;
    /**
     * Тип номера
     */
    private NumberType numberType;

    /**
     * Идентификатор типа номера в классификации оператора
     */
    private Long operatorNumberTypeId;
    /**
     * Вес номера в рамках типа
     */
    private int typeWeight = 0;
    /**
     * Стоимость номера
     */
    private long cost;

    // Заполняем сами, из API не приходит
    private TelecomType telecomType;

    public PhoneNumber(String id, String msisdn, long cost) {
        this.id = id;
        this.msisdn = msisdn;
        this.cost = cost;
    }

    public PhoneNumber() {
    }

    public PhoneNumber(Long numberId, String number, String description, NumberType numberType,
                       Long operatorNumberTypeId, int typeWeight, long price, String telecomNumberId) {
        this.id = String.valueOf(numberId);
        this.msisdn = number;
        this.description = description;
        this.numberType = numberType;
        this.operatorNumberTypeId = operatorNumberTypeId;
        this.typeWeight = typeWeight;
        this.telecomNumberId = telecomNumberId;
        this.cost = price;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public TelecomType getTelecomType() {
        return telecomType;
    }

    public void setTelecomType(TelecomType telecomType) {
        this.telecomType = telecomType;
    }

    public String getTelecomNumberId() {
        return telecomNumberId;
    }

    public void setTelecomNumberId(String telecomNumberId) {
        this.telecomNumberId = telecomNumberId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                "id='" + id + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", telecomNumberId='" + telecomNumberId + '\'' +
                ", numberType=" + numberType +
                ", cost=" + cost +
                ", telecomType=" + telecomType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber that = (PhoneNumber) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public int getTypeWeight() {
        return typeWeight;
    }

    public void setTypeWeight(int typeWeight) {
        this.typeWeight = typeWeight;
    }

    public Long getOperatorNumberTypeId() {
        return operatorNumberTypeId;
    }

    public void setOperatorNumberTypeId(Long operatorNumberTypeId) {
        this.operatorNumberTypeId = operatorNumberTypeId;
    }
}
