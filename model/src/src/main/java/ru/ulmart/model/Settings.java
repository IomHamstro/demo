package ru.ulmart.model;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Set;

public interface Settings {
    String SETTINGS_TELECOM_DELIVERY_ENABLED = "telecom_delivery_enabled";
    String SETTINGS_TELECOM_DELIVERY_POINT_ID = "telecom_delivery_point_id";

    // Города, для которых у Tele2 не проверяется наличие SIM карт в SAP
    String TELE2_ALWAYS_AVAILABLE_SIM_SHOPS = "tele2_always_available_sim_shops";

    
    <T> T getAdminSetting(String key);
}
