package ru.ulmart.model.telecom;

import com.google.common.base.MoreObjects;
import ru.ulmart.model.telecom.tele2.Tele2Tariff;

import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("serial")
public class Tariff implements Serializable {
    private static final long serialVersionUID = -3935531039346745051L;

    private Long id;
    private String alias;
    private String name;
    private String description;
    private boolean regionDefault;
    private Long goodId;
    private long price;
    private String imageUrl;
    private TelecomTariffType telecomTariffType;
    private TelecomType telecomType;

    public Tariff() {
    }

    public Tariff(Tele2Tariff tele2Tariff) {
        this.id = tele2Tariff.getId();
        this.alias = tele2Tariff.getAlias();
        this.name = tele2Tariff.getName();
        this.description = tele2Tariff.getDescription();
        this.regionDefault = tele2Tariff.getIsRegionDefault();
        this.price = tele2Tariff.getPriceActual().longValue();
        this.imageUrl = tele2Tariff.getImageUrl();
        this.telecomTariffType = tele2Tariff.getTelecomTariffType();
        this.telecomType = tele2Tariff.getTelecomType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRegionDefault() {
        return regionDefault;
    }

    public void setRegionDefault(boolean regionDefault) {
        this.regionDefault = regionDefault;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public TelecomTariffType getTelecomTariffType() {
        return telecomTariffType;
    }

    public void setTelecomTariffType(TelecomTariffType telecomTariffType) {
        this.telecomTariffType = telecomTariffType;
    }

    public TelecomType getTelecomType() {
        return telecomType;
    }

    public void setTelecomType(TelecomType telecomType) {
        this.telecomType = telecomType;
    }

    public boolean isCommonGood() {
        return this.goodId != null;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return Objects.equals(getId(), tariff.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("alias", alias)
                .add("name", name)
                .add("description", description)
                .add("regionDefault", regionDefault)
                .add("goodId", goodId)
                .add("price", price)
                .add("imageUrl", imageUrl)
                .add("telecomTariffType", telecomTariffType)
                .add("telecomType", telecomType)
                .toString();
    }
}
