package ru.ulmart.model.telecom.db;

import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;

import java.util.Collection;
import java.util.List;

public interface MTSDataService {
    List<PhoneNumber> getPhoneNumbers(String region, FilterRequest filterRequest);
    PhoneNumber getNumberDetails(String region, String id);
    PhoneNumber getDefaultNumber(String region);
    
    List<Tariff> getTariffs(String region);
    TariffDetails getTariffDetail(Long tariffId);
    Tariff getDefaultTariff(String region);
    String reserveProducts(Collection<String> ids);
    boolean hasInThisRegion(Long ulmartCityId);
    Long getCurrentRegion(Long ulmartCityId);
    Long getArticleForRegion(Long region);
}
