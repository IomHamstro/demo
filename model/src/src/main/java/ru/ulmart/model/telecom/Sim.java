package ru.ulmart.model.telecom;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class Sim implements Serializable {
    private String name;
    private String type;
    private Long goodId;
    private long tariffTypeId;

    public Sim(String name, String type, long goodId, long tariffTypeId) {
        this.name = name;
        this.type = type;
        this.goodId = goodId;
        this.tariffTypeId = tariffTypeId;
    }

    public Sim(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    public long getTariffTypeId() {
        return tariffTypeId;
    }

    public void setTariffTypeId(long tariffTypeId) {
        this.tariffTypeId = tariffTypeId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("type", type)
                .add("goodId", goodId)
                .add("tariffTypeId", tariffTypeId)
                .toString();
    }
}
