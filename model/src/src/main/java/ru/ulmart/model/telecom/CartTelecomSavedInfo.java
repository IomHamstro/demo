package ru.ulmart.model.telecom;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CartTelecomSavedInfo implements Serializable {
    private long ulmartGoodId;
    private TelecomType telecomType;
    private Integer tariffId;
    private String phoneNumberId;
    private String region;
    private String simType;

    public long getUlmartGoodId() {
        return ulmartGoodId;
    }

    public void setUlmartGoodId(long ulmartGoodId) {
        this.ulmartGoodId = ulmartGoodId;
    }

    public TelecomType getTelecomType() {
        return telecomType;
    }

    public void setTelecomType(TelecomType telecomType) {
        this.telecomType = telecomType;
    }

    public Integer getTariffId() {
        return tariffId;
    }

    public void setTariffId(Integer tariffId) {
        this.tariffId = tariffId;
    }

    public String getPhoneNumberId() {
        return phoneNumberId;
    }

    public void setPhoneNumberId(String phoneNumberId) {
        this.phoneNumberId = phoneNumberId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }
}
