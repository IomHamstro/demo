package ru.ulmart.model.telecom;

import com.google.common.base.MoreObjects;
import ru.ulmart.model.telecom.tele2.Tele2TariffParam;

import java.util.ArrayList;
import java.util.List;

public class TariffParam {
    private Long id;

    private Long parentId;

    /**
     * Индекс - определяет последовательность отображения параметров на странице
     */
    private Integer index;

    /**
     * Название параметра
     */
    private String title;

    /**
     * Алиас параметра
     */
    private String alias;

    /**
     * Всплывающая подсказка/заметка
     */
    private String note;

    /**
     * Описание
     */
    private String description;

    /**
     * Признак необходимости отображать значение параметра вместо названия
     */
    private boolean valueAsTitle;

    /**
     * Признак необходимости скрывать параметры
     */
    private boolean hidden;

    /**
     * Значение параметра
     */
    private String value;

    /**
     * Признак необходимости скрывать параметры
     */
    private boolean base;

    /**
     * Признак возможности свернуть группу.
     */
    private boolean collapsible;

    /**
     * Признак необходимости разворачивания группы по-умолчанию
     */
    private boolean expandedByDefault;

    /**
     * Дочерние параметры
     */
    private List<TariffParam> children = new ArrayList<>();

    public TariffParam() {
    }

    public TariffParam(Tele2TariffParam tele2TariffParam) {
        if(tele2TariffParam.getId() != null) {
            this.id = tele2TariffParam.getId().longValue();
        }
        if(tele2TariffParam.getParentId() != null) {
            this.parentId = tele2TariffParam.getParentId().longValue();
        }
        this.index = tele2TariffParam.getIndex();
        this.title = tele2TariffParam.getName();
        this.alias = tele2TariffParam.getName();
        this.note = tele2TariffParam.getNote();
        this.description = tele2TariffParam.getDescription();
        this.valueAsTitle = false;
        this.hidden = false;
        this.value = tele2TariffParam.getValue();
        this.base = false;
        if(tele2TariffParam.getGroupSettings() != null) {
            this.collapsible = tele2TariffParam.getGroupSettings().getIsCollapsible();
            this.expandedByDefault = tele2TariffParam.getGroupSettings().getExpandByDefault();
        }
        if(tele2TariffParam.getChildren() != null) {
            this.children = new ArrayList<>();
            for(Tele2TariffParam child: tele2TariffParam.getChildren()) {
                TariffParam param = new TariffParam(child);
                this.children.add(param);
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isValueAsTitle() {
        return valueAsTitle;
    }

    public void setValueAsTitle(boolean valueAsTitle) {
        this.valueAsTitle = valueAsTitle;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isBase() {
        return base;
    }

    public void setBase(boolean base) {
        this.base = base;
    }

    public boolean isCollapsible() {
        return collapsible;
    }

    public void setCollapsible(boolean collapsible) {
        this.collapsible = collapsible;
    }

    public boolean isExpandedByDefault() {
        return expandedByDefault;
    }

    public void setExpandedByDefault(boolean expandedByDefault) {
        this.expandedByDefault = expandedByDefault;
    }

    public List<TariffParam> getChildren() {
        return children;
    }

    public void setChildren(List<TariffParam> children) {
        this.children = children;
    }

    public boolean isGroup() {
        return this.children != null && !this.children.isEmpty();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("parentId", parentId)
                .add("index", index)
                .add("title", title)
                .add("alias", alias)
                .add("note", note)
                .add("description", description)
                .add("valueAsTitle", valueAsTitle)
                .add("hidden", hidden)
                .add("value", value)
                .add("base", base)
                .add("collapsible", collapsible)
                .add("expandedByDefault", expandedByDefault)
                .add("children", children)
                .toString();
    }
}
