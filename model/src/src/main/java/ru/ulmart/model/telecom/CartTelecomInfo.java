package ru.ulmart.model.telecom;

import java.io.Serializable;

public class CartTelecomInfo implements Serializable, Cloneable {

    private static final long serialVersionUID = 327281476023740312L;

    private Long ulmartGoodId;
    private TelecomType telecomType;
    private Tariff tariff;
    private PhoneNumber phoneNumber;
    private Sim sim;
    private String region;
    private Long cityId;
    private Long price;
    private String telecomOrderNumber;
    public CartTelecomInfo() {
    }

    public CartTelecomInfo(Tariff tariff, PhoneNumber phoneNumber, Sim sim, Long price) {
        this.tariff = tariff;
        this.phoneNumber = phoneNumber;
        this.sim = sim;
        this.price = price;
    }

    public CartTelecomInfo(PhoneNumber phoneNumber, Tariff tariff) {
        this.phoneNumber = phoneNumber;
        this.tariff = tariff;
    }

    public CartTelecomInfo(Long ulmartGoodId, TelecomType telecomType, Tariff tariff, PhoneNumber phoneNumber, Sim sim, Long price) {
        this.ulmartGoodId = ulmartGoodId;
        this.telecomType = telecomType;
        this.tariff = tariff;
        this.phoneNumber = phoneNumber;
        this.sim = sim;
        this.price = price;
    }

    public Long getUlmartGoodId() {
        return ulmartGoodId;
    }

    public void setUlmartGoodId(Long ulmartGoodId) {
        this.ulmartGoodId = ulmartGoodId;
    }

    public TelecomType getTelecomType() {
        return telecomType;
    }

    public void setTelecomType(TelecomType telecomType) {
        this.telecomType = telecomType;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Sim getSim() {
        return sim;
    }

    public void setSim(Sim sim) {
        this.sim = sim;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public long getGoodId() {
        if (phoneNumber == null) {
            return 0L;
        }
        return (long) phoneNumber.getId().hashCode();
    }

    public String getTelecomOrderNumber() {
        return telecomOrderNumber;
    }

    public void setTelecomOrderNumber(String telecomOrderNumber) {
        this.telecomOrderNumber = telecomOrderNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CartTelecomInfo that = (CartTelecomInfo) o;

        if (cityId != null ? !cityId.equals(that.cityId) : that.cityId != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (region != null ? !region.equals(that.region) : that.region != null) return false;
        if (sim != that.sim) return false;
        if (tariff != null ? !tariff.equals(that.tariff) : that.tariff != null) return false;
        if (telecomType != that.telecomType) return false;
        if (ulmartGoodId != null ? !ulmartGoodId.equals(that.ulmartGoodId) : that.ulmartGoodId != null) return false;
        if (telecomOrderNumber != null ? !telecomOrderNumber.equals(that.telecomOrderNumber) : that.telecomOrderNumber != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ulmartGoodId != null ? ulmartGoodId.hashCode() : 0;
        result = 31 * result + (telecomType != null ? telecomType.hashCode() : 0);
        result = 31 * result + (tariff != null ? tariff.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (sim != null ? sim.hashCode() : 0);
        result = 31 * result + (region != null ? region.hashCode() : 0);
        result = 31 * result + (cityId != null ? cityId.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (telecomOrderNumber != null ? telecomOrderNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CartTelecomInfo{" +
                "ulmartGoodId=" + ulmartGoodId +
                ", telecomType=" + telecomType +
                ", tariff=" + tariff +
                ", phoneNumber=" + phoneNumber +
                ", sim=" + sim +
                ", region='" + region + '\'' +
                ", cityId=" + cityId +
                ", price=" + price +
                ", telecomOrderNumber='" + telecomOrderNumber + '\'' +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        CartTelecomInfo cartTelecomInfo = new CartTelecomInfo();
        cartTelecomInfo.setPhoneNumber(getPhoneNumber());
        cartTelecomInfo.setPrice(getPrice());
        cartTelecomInfo.setRegion(getRegion());
        cartTelecomInfo.setCityId(getCityId());
        cartTelecomInfo.setSim(getSim());
        cartTelecomInfo.setTariff(getTariff());
        cartTelecomInfo.setTelecomType(getTelecomType());
        cartTelecomInfo.setUlmartGoodId(getUlmartGoodId());
        cartTelecomInfo.setTelecomOrderNumber(getTelecomOrderNumber());
        return super.clone();
    }

    public String getTelecomTypeTitle() {
        return telecomType.getTitle();
    }
}
