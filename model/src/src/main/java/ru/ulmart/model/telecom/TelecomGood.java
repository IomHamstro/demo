package ru.ulmart.model.telecom;

public class TelecomGood {
    private final TelecomType telecomType;
    /**
     * Идентификатор региона у оператора
     */
    private final String region;
    /**
     * Идентификатор города у оператора
     */
    private final Long cityId;
    /**
     * Номер телефона (обязательный)
     */
    private final PhoneNumber phoneNumber;
    /**
     * Идентификатор тарифа
     */
    private final Long tariffId;
    /**
     * Сим карта
     */
    private final Sim sim;
    
    public TelecomGood(TelecomType telecomType, String region, Long cityId, PhoneNumber phoneNumber, Long tariffId, Sim sim) {
        super();
        this.telecomType = telecomType;
        this.region = region;
        this.cityId = cityId;
        this.phoneNumber = phoneNumber;
        this.tariffId = tariffId;
        this.sim = sim;
    }
    public TelecomType getTelecomType() {
        return telecomType;
    }
    public String getRegion() {
        return region;
    }
    public Long getCityId() {
        return cityId;
    }
    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }
    public Long getTariffId() {
        return tariffId;
    }
    public Sim getSim() {
        return sim;
    }
}
