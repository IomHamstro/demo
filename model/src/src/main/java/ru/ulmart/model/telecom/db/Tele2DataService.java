package ru.ulmart.model.telecom.db;

import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomTariffType;

import java.util.List;
import java.util.Map;

public interface Tele2DataService {
    TelecomCity getCityInfo(Long ulmartCityId);

    Map<String, TariffExtraInfo> getTariffsExtraInfo(String region, List<Tariff> tariffs);

    Integer getSalePoint(Long shopId);

    Long getArticleForUlmartCity(Long region);

    final class TariffExtraInfo {
        private final String imageUrl;
        private final TelecomTariffType type;

        public TariffExtraInfo(String imageUrl, TelecomTariffType type) {
            this.imageUrl = imageUrl;
            this.type = type;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public TelecomTariffType getType() {
            return type;
        }

    }
}
