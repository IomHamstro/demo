package ru.ulmart.model.telecom;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Client implements Serializable {

    public static final String DATE_FORMAT = "dd.MM.yyyy";

    /**
     * Фамилия.
     */
    @NotBlank
    private String lastName;

    /**
     * Имя.
     */
    @NotBlank
    private String firstName;

    /**
     * Отчество.
     */
    @NotBlank
    private String patronymic;

    @NotBlank
    @Pattern(regexp = "\\d+")
    @Length(min = 10, max = 11)
    private String contactPhone;

    @NotBlank
    @Email
    private String contactEmail;

    /**
     * Тип документа
     */
    @NotNull
    private Integer documentType;

    /**
     * Серия и номер документа
     */
    @NotBlank
    private String seriesNumber;

    /**
     * Кем выдан документ
     */
    @NotBlank
    private String issuedBy;

    /**
     * Дата выдачи документа
     */
    @NotNull
    @DateTimeFormat(pattern = DATE_FORMAT)
    @JsonFormat(pattern = DATE_FORMAT)
    private Date dateOfIssuance;

    /**
     * Пол (0 - мужчина; 1 - женщина)
     */
    @NotNull
    private Integer gender;

    /**
     * Дата рождения
     */
    @NotNull
    @DateTimeFormat(pattern = DATE_FORMAT)
    @JsonFormat(pattern = DATE_FORMAT)
    private Date birthDate;

    /**
     * Почтовый индекс.
     */
    @NotBlank
    private String postalCode;

    /**
     * Место выдачи документа.
     */
    @NotBlank
    private String documentIssuedPlace;

    /**
     * Улица.
     */
    @NotBlank
    private String street;

    /**
     * Номер дома.
     */
    @NotBlank
    private String building;

    /**
     * Место рождения
     */
    @NotBlank
    private String birthPlace;

    public Client() {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String phone) {this.contactPhone = truncatePhone(phone);}

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Integer getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Integer documentType) {
        this.documentType = documentType;
    }

    public String getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(String seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public Date getDateOfIssuance() {
        return dateOfIssuance;
    }

    public void setDateOfIssuance(Date dateOfIssuance) {
        this.dateOfIssuance = dateOfIssuance;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getDocumentIssuedPlace() {
        return documentIssuedPlace;
    }

    public void setDocumentIssuedPlace(String documentIssuedPlace) {
        this.documentIssuedPlace = documentIssuedPlace;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @Override
    public String toString() {
        return "Client{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", contactPhone='" + contactPhone + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", documentType=" + documentType +
                ", seriesNumber='" + seriesNumber + '\'' +
                ", issuedBy='" + issuedBy + '\'' +
                ", dateOfIssuance=" + dateOfIssuance +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                ", postalCode='" + postalCode + '\'' +
                ", documentIssuedPlace='" + documentIssuedPlace + '\'' +
                ", street='" + street + '\'' +
                ", building='" + building + '\'' +
                ", birthPlace='" + birthPlace + '\'' +
                '}';
    }

    public static String clearPhone(String phone) {
        phone = phone.replaceAll("[^\\d.]", "");
        if (phone.length() == 10) {
            return phone;
        } else if (phone.length() == 11) {
            return phone.substring(1);
        } else {
            throw new RuntimeException("Incorrect phone length: " + phone);
        }
    }

    private String truncatePhone(String phone) {
        if (phone.length() == 10) {
            return phone;
        } else if (phone.length() == 11) {
            return phone.substring(1);
        } else {
            return ""; //предполагается, что остальные проверки корректности формата телефона возложены на аннотации поля contactPhone
        }
    }
}
