package ru.ulmart.model.cache;

/**
 * Интерфейс для бинов, которые используются в ключе
 *
 * @author Foat Akhmadeev
 *         6/4/13
 */
public interface KeyPart {
    public String part();
}
