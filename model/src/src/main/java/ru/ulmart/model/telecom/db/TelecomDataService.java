package ru.ulmart.model.telecom.db;

import ru.ulmart.model.telecom.*;

import java.util.List;

public interface TelecomDataService {
    boolean hasInThisRegion(TelecomType telecomType, Long ulmartCityId);

    TelecomCity getTelecomCity(TelecomType telecomType, Long ulmartCityId);

    List<Tariff> getTariffs(TelecomType telecomType, String region);

    TariffDetails getTariffDetail(TelecomType telecomType, Long id, String region);

    TariffDetails getTariffDetailCommon(TelecomType telecomType, String region);

    Tariff getTariff(TelecomType telecomType, Long id, String region);

    Long getCurrentRegionId(TelecomType telecomType, Long ulmartCityId);

    Tariff getDefaultTariff(TelecomType telecomType, String region);

    TelecomTariffType getDefaultTariffType();

    List<PhoneNumber> getNumbers(String region, TelecomType telecomType, FilterRequest filterRequest);

    List<NumberType> getNumberTypes();

    PhoneNumber getNumber(String numberId);

    PhoneNumber getDefaultNumber(String region, TelecomType telecomType);

    Long getArticleForUlmartCity(Long ulmartCityId, TelecomType telecomType);

    List<NumberTypeMapping> getNumberTypeMappings(String region, TelecomType telecomType);

    List<Sim> getSims(Long ulmartCityId, TelecomType telecomType);

    boolean reserveNumber(CartTelecomInfo cartTelecomInfo);

    boolean sellNumber(String orderId, CartTelecomInfo cartTelecomInfo);

    boolean freeNumber(CartTelecomInfo cartTelecomInfo);
}
