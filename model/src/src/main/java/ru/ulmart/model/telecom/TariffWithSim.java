package ru.ulmart.model.telecom;

import ru.ulmart.model.telecom.Tariff;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class TariffWithSim implements Serializable {
    private final Tariff tariff;
    private final List<Sim> sims;

    public TariffWithSim(Tariff tariff, List<Sim> sims) {
        this.tariff = tariff;
        this.sims = sims;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public List<Sim> getSims() {
        return sims;
    }
}
