package ru.ulmart.model.telecom.tele2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Значения параметров для тарифов
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tele2TariffParam {

    /**
     * id тарифа
     */
    @JsonProperty("Id")
    private Integer id;

    /**
     * Название параметра
     */
    @JsonProperty("Name")
    private String name;

    /**
     * Значение параметра
     */
    @JsonProperty("Value")
    private String value;

    /**
     * Всплывающая подсказка/заметка
     */
    @JsonProperty("Note")
    private String note;

    /**
     * Описание
     */
    @JsonProperty("Description")
    private String description;

    /**
     * Имеется примечание/подсказка
     */
    @JsonProperty("HasNoteHint")
    private Boolean hasNoteHint;

    /**
     * Индекс - определяет последовательность отображения параметров на странице
     */
    @JsonProperty("Inx")
    private Integer index;

    /**
     * Настройки группы параметров
     */
    @JsonProperty("GroupSettings")
    private Tele2GroupSettings groupSettings;

    /**
     * Флаг, что параметр является групповым
     */
    @JsonProperty("IsGroup")
    private Boolean isGroup;

    /**
     * Дочерние параметры
     */
    @JsonProperty("Children")
    private List<Tele2TariffParam> children;

    private Integer parentId;

    public Tele2TariffParam() {
    }

    public Tele2TariffParam(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Tele2TariffParam(String name, String value, Integer index) {
        this.name = name;
        this.value = value;
        this.index = index;
    }

    public Tele2TariffParam(String name, String value, Integer index, List<Tele2TariffParam> children) {
        this.name = name;
        this.value = value;
        this.index = index;
        this.children = children;
    }

    public Tele2TariffParam(Integer id, String name, String value, String note, String description, Boolean hasNoteHint,
                            Integer index, Tele2GroupSettings groupSettings, Boolean isGroup, List<Tele2TariffParam> children) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.note = note;
        this.description = description;
        this.hasNoteHint = hasNoteHint;
        this.index = index;
        this.groupSettings = groupSettings;
        this.isGroup = isGroup;
        this.children = children;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getHasNoteHint() {
        return hasNoteHint;
    }

    public void setHasNoteHint(Boolean hasNoteHint) {
        this.hasNoteHint = hasNoteHint;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Tele2GroupSettings getGroupSettings() {
        return groupSettings;
    }

    public void setGroupSettings(Tele2GroupSettings groupSettings) {
        this.groupSettings = groupSettings;
    }

    public Boolean getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(Boolean isGroup) {
        this.isGroup = isGroup;
    }

    public List<Tele2TariffParam> getChildren() {
        return children;
    }

    public void setChildren(List<Tele2TariffParam> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Tele2TariffParam{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", note='" + note + '\'' +
                ", description='" + description + '\'' +
                ", hasNoteHint=" + hasNoteHint +
                ", index=" + index +
                ", groupSettings=" + groupSettings +
                ", isGroup=" + isGroup +
                ", children=" + children +
                '}';
    }
}
