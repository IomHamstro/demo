package ru.ulmart.model.telecom;

import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Типы тарифов навешиваемые Юлмартом ("для звонков", "для интернета" и т.д.)
 */
public class TelecomTariffType implements Comparable<TelecomTariffType>, Serializable {
    private static final long serialVersionUID = 1260032389835737770L;
    private Long id;
    private String name;
    private String description;
    private String alias;

    public TelecomTariffType() {
    }

    public TelecomTariffType(String name) {
        this.name = name;
    }

    public TelecomTariffType(Long id, String name, String description, String alias) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.alias = alias;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        return "TelecomTariffType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", alias='" + alias + '\'' +
                '}';
    }

    @Override
    public int compareTo(TelecomTariffType type) {
        if(id < type.getId()) {
            return -1;
        }
        if(id > type.getId()) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelecomTariffType that = (TelecomTariffType) o;
        return Objects.equal(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
