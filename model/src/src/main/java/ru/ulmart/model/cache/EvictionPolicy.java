package ru.ulmart.model.cache;

/**
 * @author alexander.shlyannikov
 *         29.07.13 13:11
 */
public enum EvictionPolicy {
    FIFO,
    LFU
}
