package ru.ulmart.api.services.telecom.tele2;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ulmart.model.telecom.Tariff;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:tele2-test-context.xml"})
public class Tele2TariffsServiceIntegrationTest {

    public static final String MSK_REGION = "msk";

    @Autowired
    private Tele2TariffsService tele2TariffsService;

    @Test
    public void testListTariffsMsk() {
        List<Tariff> tariffs = tele2TariffsService.listTariffs(MSK_REGION);
        Assert.assertTrue(tariffs.size() > 0);
    }

    @Test
    public void testDefaultTariffMsk() {
        Tariff tariff = tele2TariffsService.getDefaultTariff(MSK_REGION);
        Assert.assertNotNull(tariff);
    }

    @Test
    public void testSetTariffImagesAndType() {
        List<Tariff> tariffs = tele2TariffsService.listTariffs(MSK_REGION);
        tele2TariffsService.setTariffImagesAndType(MSK_REGION, tariffs);
    }

}
