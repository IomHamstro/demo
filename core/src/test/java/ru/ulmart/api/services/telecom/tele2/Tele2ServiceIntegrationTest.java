package ru.ulmart.api.services.telecom.tele2;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ulmart.model.telecom.TelecomCity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:tele2-test-context.xml"})
public class Tele2ServiceIntegrationTest {

    public static final Long SPB_ULMART_ID = 18413L;

    @Autowired
    private Tele2Service tele2Service;

    @Test
    public void getCityInfoForSpb() {
        TelecomCity cityInfo = tele2Service.getCityInfo(SPB_ULMART_ID);
        Assert.assertNotNull(cityInfo);
        Assert.assertEquals("spb", cityInfo.getTelecomRegionName());
    }

}
