package ru.ulmart.api.services.telecom.checku;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ulmart.api.services.telecom.checku.beans.PassportData;
import ru.ulmart.api.services.telecom.checku.beans.PassportRecognitionData;
import ru.ulmart.api.services.telecom.checku.beans.RecognitionError;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:checku-test-context.xml"})
public class CheckUServiceIntegrationTest {

    private static final String PASSPORT_IMG_PATH = "img/passport/passport.jpg";
    private static final String ICON_IMG_PATH = "img/passport/icon.jpg";

    @Autowired
    private CheckUPassportService checkUPassportService;

    @Test
    public void testExtractPassportDataPassportImage() throws URISyntaxException {
        DateFormat dateFormat = new SimpleDateFormat(PassportData.DATE_FORMAT);
        InputStream passportStream = ClassLoader.getSystemResourceAsStream(PASSPORT_IMG_PATH);
        PassportRecognitionData recognitionData = checkUPassportService.extractPassportData(passportStream);
        Assert.assertEquals(PassportRecognitionData.State.COMPLETE, recognitionData.getState());

        PassportData passportData = recognitionData.getPassport();
        Assert.assertEquals("01.03.1978", dateFormat.format(passportData.getBirthday()));
        Assert.assertEquals("ГОР. МОСКВА", passportData.getBirthplace());
        Assert.assertEquals("07.03.1998", dateFormat.format(passportData.getDeliverydate()));
        Assert.assertEquals("ГОР. ЯОТДЕЛЕНИЕМ МИЛИЦИИ ГОР. МОСКВЬВ", passportData.getDeliveryplace());
        Assert.assertEquals("070-012", passportData.getDepartmentcode());
        Assert.assertEquals("ДМИТРИЙ", passportData.getName());
        Assert.assertEquals("4578547851", passportData.getNumber());
        Assert.assertEquals("ВАСИЛЬЕВИЧ", passportData.getPatronymic());
        Assert.assertEquals("МУЖ", passportData.getSex());
        Assert.assertEquals("АЛЕКСЕНКО", passportData.getSurname());
    }

    @Test
    public void testExtractPassportDataIconImage() throws URISyntaxException {
        InputStream passportStream = ClassLoader.getSystemResourceAsStream(ICON_IMG_PATH);
        PassportRecognitionData recognitionData = checkUPassportService.extractPassportData(passportStream);
        Assert.assertEquals(PassportRecognitionData.State.ERROR, recognitionData.getState());
        RecognitionError error = recognitionData.getError();
        Assert.assertEquals(100, error.getCode().longValue());
        Assert.assertEquals("Failed to recognize passport", error.getDescription());
    }

}
