package ru.ulmart.api.services.telecom.tele2;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.tele2.Tele2Region;

import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:tele2-test-context.xml"})
public class Tele2NumbersServiceIntegrationTest {

    private static final String MSK_REGION = "msk";

    @Autowired
    private Tele2NumbersService tele2NumbersService;

    @Autowired
    private Tele2Service tele2Service;

    @Test
    public void testListNumber() {
        FilterRequest filterRequest =  new FilterRequest(null, 0, 10);
        Map<NumberType, List<PhoneNumber>> numbersMap= tele2NumbersService.listNumbers(MSK_REGION, filterRequest);
        Assert.assertTrue(numbersMap.size() > 0);
        for(NumberType numberType: numbersMap.keySet()) {
            List<PhoneNumber> numbers = numbersMap.get(numberType);
            Assert.assertTrue(numbers.size() > 0);
        }
    }

    // TODO: move to Tele2ServiceIntegrationTest
    private void testRegion() {
        for (Tele2Region region : tele2Service.getRegion()) {
            System.out.println(region);
        }
    }

    private void testRegionListNumber() {
        Map<NumberType, List<PhoneNumber>> numbersMap = tele2NumbersService.listNumbers("msk",
                new FilterRequest(null, 0, 10));
        for(NumberType numberType: numbersMap.keySet()) {
            List<PhoneNumber> numbers = numbersMap.get(numberType);
            for (PhoneNumber number : numbers) {
                System.out.println(number);
            }
        }
    }

    private void testDefaultNumber() {
        System.out.println(tele2NumbersService.getDefaultNumber("msk"));
    }
}
