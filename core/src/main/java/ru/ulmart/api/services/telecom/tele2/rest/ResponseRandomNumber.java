package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Данные получаемые из API
 * GET api/{token}/catalog/RandomNumber
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseRandomNumber extends ApiResponse {

    @JsonProperty("Data")
    private DataNumbersByFilter data;

    public DataNumbersByFilter getData() {
        return data;
    }

    public void setData(DataNumbersByFilter data) {
        this.data = data;
    }
}
