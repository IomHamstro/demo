package ru.ulmart.api.services.telecom.mts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import ru.ulmart.api.services.telecom.AbstractTelecomService;
import ru.ulmart.api.services.telecom.checku.CheckUSIMService;
import ru.ulmart.api.services.telecom.mts.MTSCommonService;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.model.cache.Cached;
import ru.ulmart.model.telecom.*;
import ru.ulmart.model.telecom.db.MTSDataService;


import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Сервис для работы с номерами и тарифами МТС
 *
 * Telezhnikova Olga
 */
@LogIntegrationExceptions
public class MTSCommonServiceImpl extends AbstractTelecomService implements MTSCommonService {

    @Autowired
    private MTSDataService mtsService;
    @Autowired
    private CheckUSIMService checkUSIMService;

    public MTSCommonServiceImpl() {
        super(TelecomType.MTS);
    }

    /**
     * Получение списка красивых номеров
     *
     * @param region        регион
     * @param filterRequest параметры для фильтрации
     * @return список номеров
     */
    @Override
    public Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest) {
        return Collections.emptyMap();
    }


    /**
     * Получение деталей телефонного номера.
     *
     * @param region регион
     * @param numberId идентифкатор телефонного номера
     * @return детали телефонного номера
     */
    @Override
    public PhoneNumber getNumberInfo(String region, String numberId) {
        PhoneNumber phoneNumber = mtsService.getNumberDetails(region, numberId);
        fillNumberTelecomType(phoneNumber);
        return phoneNumber;
    }

    @Override
    public PhoneNumber getNumber(String region, String numberId) {
        if (numberId != null) {
            PhoneNumber phoneNumber = getNumberInfo(region, numberId);
            if (phoneNumber != null) {
                fillNumberTelecomType(phoneNumber);
                return  phoneNumber;
            }
        }
        return null;
    }

    /**
     * Получение списка тарифов для указанного региона
     *
     * @param region регион
     * @return список тарифов
     */
    @Cached(lifetime = 1800)
    @Override
    public List<Tariff> listTariffs(String region) {
        if(region == null) {
            return null;
        }
        List<Tariff> tariffs = mtsService.getTariffs(region);
        fillTariffTelecomType(tariffs);
        return tariffs;
    }

    /**
     * Получение полной информации по тарифу
     *
     * @param id id тарифа
     * @return детальная информация по тарифу
     */
    @Cached
    @Override
    public TariffDetails getTariffDetail(Long id, String region) {
        TariffDetails tariffDetails = mtsService.getTariffDetail(id);
        fillTariffTelecomType(tariffDetails.getTariff());
        return tariffDetails;
    }

    @Override
    public TariffDetails getTariffDetailCommon(String region) {
        // Для МТС нет общей информации по тарифам
        return null;
    }

    /**
     * Получение тарифа по умолчанию для указанного региона
     *
     * @param region регион
     * @return тариф
     */
    @Cached
    @Override
    public Tariff getDefaultTariff(String region) {
        Tariff tariff = mtsService.getDefaultTariff(region);
        fillTariffTelecomType(tariff);
        return tariff;
    }

    @Override
    public Tariff getTariffOrDefault(String region, Long id) {
        if (id != null) {
            TariffDetails tariffDetails = getTariffDetail(id, region);
            if (tariffDetails != null) {
                fillTariffTelecomType(tariffDetails.getTariff());
                return tariffDetails.getTariff();
            }
        }
        Tariff tariff = getDefaultTariff(region);
        fillTariffTelecomType(tariff);
        return tariff;
    }

    @Cached
    @Override
    public TelecomCity getCityInfo(Long ulmartCityId) {
        TelecomCity city = new TelecomCity();
        city.setUlmartCityId(ulmartCityId);
        city.setTelecomRegionName(getCurrentRegionId(ulmartCityId));
        return city;
    }

    /**
     * Проверяем есть ли в этом регионе оператор МТС
     *
     * @param ulmartCityId id города Юлмарта
     * @return да/нет
     */
    @Cached
    @Override
    public boolean hasInThisRegion(Long ulmartCityId) {
        return mtsService.hasInThisRegion(ulmartCityId);
    }

    @Cached
    @Override
    public String getCurrentRegionId(Long ulmartCityId) {
        Long regionId = mtsService.getCurrentRegion(ulmartCityId);
        return regionId != null ? regionId.toString() : null;
    }

    @Cached
    @Override
    public Long getArticleForUlmartCity(Long region) {
        return mtsService.getArticleForRegion(region);
    }

    @Override
    public List<Sim> getSims(Long ulmartCityId) {
        return Collections.emptyList();
    }

    @Override
    public boolean isSimAlwaysAvailable(Long shopId) {
        return false;
    }

    @Override
    public boolean onBeforeAddToCart(CartTelecomInfo cartTelecomInfo) {
        // Для МТС нет действий
        return true;
    }

    @Override
    public boolean onAfterRemoveFromCart(CartTelecomInfo cartTelecomInfo) {
        // Для МТС нет действий
        return false;
    }

    @Override
    public boolean onAfterRemoveFromOrder(CartTelecomInfo cartTelecomInfo) {
        // Для МТС нет действий.
        return true;
    }

    @Override
    public boolean onAfterAddToOrder(String orderId, CartTelecomInfo cartTelecomInfo) {
        // Для МТС нет действий.
        return true;
    }

    @Override
    public boolean hasTariffAsGood() {
        return false;
    }

    @Override
    public boolean hasNumbers() {
        return true;
    }

    @Override
    public TelecomType getTelecomType() {
        return TelecomType.MTS;
    }

}
