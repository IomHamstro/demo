package ru.ulmart.api.services.telecom.checku;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.ulmart.api.services.telecom.TelecomUtil;
import ru.ulmart.service.telecom.problem.CreateOrderException;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.api.services.telecom.tele2.rest.RequestCreateOrder;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseCreateOrder;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.Order;
import ru.ulmart.model.telecom.TelecomGood;

import java.util.Collection;

import static ru.ulmart.api.services.telecom.TelecomUtil.toJson;

/**
 * @author Telezhnikova Olga
 */
@LogIntegrationExceptions
public class CheckUSIMService extends CheckUService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckUSIMService.class);

    private static final String ORDER_DATA = "orderdata";

    /**
     * Создание заказа в МТС
     *
     * Возможные тексты ошибок:
     * 1:  "Wrong login or password" - неверный логин или пароль
     * 2:  "No parameter 'orderdata'" -  не указан параметр 'orderdata'
     * 3:  "Incorrect parameter 'orderdata'" - некорректный параметр 'orderdata'
     * 4:  "Wrong json for addition data" - неверный формат параметра 'orderdata'(не хватает всех обазательных полей)
     * 5:  "Incorrect amount of products" - неверное кол-во продуктов
     *
     * @param salePointId      идентификатор точки продаж
     * @param client           информация о покупателе
     * @param cart             информация о содержимом корзины
     * @return номер заказа
     */
    public Order confirmOrder(Integer salePointId, Client client, Collection<TelecomGood> cart) {
        RequestCreateOrder requestAPI = TelecomUtil.convert(client, salePointId, cart);
        LOGGER.info("MTS confirmOrder: request = {}", toJson(requestAPI));
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.set(ORDER_DATA, requestAPI);
        ResponseCreateOrder response = restTemplate.exchange(apiUrl, HttpMethod.POST, createEntity(body), ResponseCreateOrder.class).getBody();
        LOGGER.info("MTS confirmOrder: response = {}", toJson(response));
        if (Boolean.TRUE.equals(response.getIsSuccess()) && response.getData() != null) {
            LOGGER.info("MTS confirmOrder: MTS order is confirmed");
            return TelecomUtil.convert(response);
        } else {
            throw new CreateOrderException(requestAPI, response, response.getErrorReason(), "mts");
        }
    }
}
