package ru.ulmart.api.services.telecom.beeline;

import ru.ulmart.api.services.telecom.TelecomGoodService;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.model.telecom.TelecomType;

/**
 * Для каждого оператора должен быть свой класс с сервисами.
 * Это необходимо для корректной работы системы кэширования.
 */
@LogIntegrationExceptions
public class BeelineService extends TelecomGoodService {
    public BeelineService() {
        super(TelecomType.BEELINE);
    }
}
