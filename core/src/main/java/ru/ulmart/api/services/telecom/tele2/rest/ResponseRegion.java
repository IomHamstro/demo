package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulmart.model.telecom.tele2.Tele2Region;

import java.util.List;

/**
 * Данные получаемяе из API
 * GET api/{token}/regionsales/regions
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseRegion extends ApiResponse {

    @JsonProperty("Data")
    private List<Tele2Region> data;

    public List<Tele2Region> getData() {
        return data;
    }

    public void setData(List<Tele2Region> data) {
        this.data = data;
    }
}
