package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulmart.model.telecom.tele2.Tele2TariffDetails;

/**
 * Данные получаемяе из API
 * GET api/{token}/tariff/details/{id}
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseTariffDetails extends ApiResponse {

    @JsonProperty("Data")
    private Tele2TariffDetails data;

    public Tele2TariffDetails getData() {
        return data;
    }

    public void setData(Tele2TariffDetails data) {
        this.data = data;
    }
}
