package ru.ulmart.api.services.telecom.checku.beans;

public class RecognitionError {

    private Long code;

    private String description;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
