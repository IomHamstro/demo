package ru.ulmart.api.services.telecom.tele2;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import ru.ulmart.api.services.telecom.AbstractTelecomService;
import ru.ulmart.model.telecom.TelecomType;

abstract class AbstractTele2Service extends AbstractTelecomService {

    private HttpHeaders httpHeaders;

    public AbstractTele2Service() {
        super(TelecomType.TELE2);
        httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    }

    protected  <T> HttpEntity<T> createEntity(T body) {
        return new HttpEntity<>(body, httpHeaders);
    }

}
