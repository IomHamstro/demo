package ru.ulmart.api.services.telecom;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TelecomGood;
import ru.ulmart.model.telecom.TelecomType;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractTelecomService {
    private TelecomType telecomType;

    public AbstractTelecomService(TelecomType telecomType) {
        this.telecomType = telecomType;
    }

    protected TelecomType getTelecomType() {
        return telecomType;
    }

    protected void fillNumberTelecomType(PhoneNumber phoneNumber) {
        if (phoneNumber == null) {
            return;
        }
        phoneNumber.setTelecomType(getTelecomType());
    }

    protected void fillTariffTelecomType(List<Tariff> tariffs) {
        for (Tariff tariff : tariffs) {
            fillTariffTelecomType(tariff);
        }
    }

    protected void fillTariffTelecomType(Tariff tariff) {
        if (tariff == null) {
            return;
        }
        tariff.setTelecomType(getTelecomType());
    }

    private final Predicate<TelecomGood> byType = new Predicate<TelecomGood>() {
        @Override
        public boolean apply(TelecomGood input) {
            return input.getTelecomType() == getTelecomType();
        }
    };

    protected Collection<TelecomGood> filterTelecomCartByType(Collection<TelecomGood> cart) {
        if (cart == null) {
            return new LinkedList<>();
        }
        return Collections2.filter(cart, byType);
    }
}