package ru.ulmart.api.services.telecom.universal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.model.telecom.db.TelecomDataService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@LogIntegrationExceptions
class TelecomNumbersService {
    @Autowired
    @Qualifier("telecomDataServiceSlave")
    private TelecomDataService telecomDataServiceSlave;
    @Autowired
    @Qualifier("telecomDataServiceMaster")
    private TelecomDataService telecomDataServiceMaster;

    /**
     * Получение списка номеров, удовлетворяющих фильтру
     *
     * @param region        регион
     * @param filterRequest значения фильтров
     * @param telecomType   оператор
     * @return список номеров
     */
    public Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest,
                                                          TelecomType telecomType) {
        List<PhoneNumber> phoneNumbers = telecomDataServiceSlave.getNumbers(region, telecomType, filterRequest);
        Map<NumberType, List<PhoneNumber>> numberMap = new HashMap<>();
        for (PhoneNumber phoneNumber : phoneNumbers) {
            phoneNumber.setTelecomType(telecomType);
            NumberType numberType = phoneNumber.getNumberType();
            List<PhoneNumber> numbers = numberMap.get(numberType);
            if (numbers == null) {
                numbers = new ArrayList<>();
            }
            numbers.add(phoneNumber);
            numberMap.put(numberType, numbers);
        }
        return numberMap;
    }

    /**
     * Получение телефонного номера по его идентификатору
     *
     * @param numberId id телефонного номера
     * @return номер телефона
     */
    public PhoneNumber getNumber(String numberId) {
        if (numberId == null) {
            return null;
        }
        return telecomDataServiceSlave.getNumber(numberId);
    }

    /**
     * Получение номера по умолчанию (случайный номер из категории самых дешевых номеров)
     *
     * @param region      регион
     * @param telecomType оператор
     * @return номер телефона
     */
    public PhoneNumber getDefaultNumber(String region, TelecomType telecomType) {
        return telecomDataServiceSlave.getDefaultNumber(region, telecomType);
    }

    /**
     * Резервирование номера перед добавлением товара в корзину
     *
     * @param cartTelecomInfo информация о товаре
     * @return признак успешного резервирования номера
     */
    public boolean reserveNumber(CartTelecomInfo cartTelecomInfo) {
        if (cartTelecomInfo.getPhoneNumber() == null || cartTelecomInfo.getPhoneNumber().getId() == null) {
            return false;
        }
        return telecomDataServiceMaster.reserveNumber(cartTelecomInfo);
    }

    /**
     * Установка признака "SOLD" при подтверждении заказа
     *
     * @param orderId         id заказа
     * @param cartTelecomInfo информация о товаре
     * @return признак успешной установки признака "SOLD" при подтверждении заказа
     */
    public boolean sellNumber(String orderId, CartTelecomInfo cartTelecomInfo) {
        if (cartTelecomInfo.getPhoneNumber() == null || cartTelecomInfo.getPhoneNumber().getId() == null) {
            return false;
        }
        return telecomDataServiceMaster.sellNumber(orderId, cartTelecomInfo);
    }

    /**
     * Установка признака "FREE" при отмене заказа/удалении товара из корзины
     *
     * @param cartTelecomInfo информация о товаре
     * @return признак успешной установки признака "FREE" при отмене заказа/удалении товара из корзины
     */
    public boolean freeNumber(CartTelecomInfo cartTelecomInfo) {
        if (cartTelecomInfo.getPhoneNumber() == null || cartTelecomInfo.getPhoneNumber().getId() == null) {
            return false;
        }
        return telecomDataServiceMaster.freeNumber(cartTelecomInfo);
    }
}
