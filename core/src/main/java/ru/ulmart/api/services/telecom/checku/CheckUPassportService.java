package ru.ulmart.api.services.telecom.checku;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.ulmart.api.services.telecom.checku.beans.PassportRecognitionData;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Telezhnikova Olga
 */
@LogIntegrationExceptions
public class CheckUPassportService extends CheckUService {

    private static final String HEADER_TYPE = "Ptype";
    private static final String HEADER_TYPE_VALUE = "1";

    private static final String HEADER_METHOD = "Pmethod";
    private static final String HEADER_METHOD_VALUE_WITHOUT_MANUAL_CORRECTION = "1";
    private static final String HEADER_METHOD_VALUE_WITH_MANUAL_CORRECTION = "2";

    private static final String FIELD_IMGPASS = "imgpass";


    public PassportRecognitionData extractPassportData(InputStream passportScanStream) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HEADER_TYPE, HEADER_TYPE_VALUE);
        httpHeaders.set(HEADER_METHOD, HEADER_METHOD_VALUE_WITHOUT_MANUAL_CORRECTION);
        httpHeaders.set("Content-Type", "multipart/form-data");
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        InputStreamResource passportStreamResource = new InputStreamResource(passportScanStream) {
            // set upload file name
            @Override
            public String getFilename() {
                return "passport.jpg";
            }

            // avoid calling getInputStream() twice
            @Override
            public long contentLength() throws IOException {
                return -1L;
            }
        };
        body.set(FIELD_IMGPASS, passportStreamResource);
        ResponseEntity<PassportRecognitionData> response
                = restTemplate.exchange(apiUrl, HttpMethod.POST, createEntity(body, httpHeaders), PassportRecognitionData.class);
        return response.getBody();
    }
}
