package ru.ulmart.api.services.telecom.tele2;

import com.google.common.base.Splitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import ru.ulmart.api.services.telecom.TelecomService;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.service.telecom.problem.ServiceInvocationException;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseRegion;
import ru.ulmart.model.Settings;
import ru.ulmart.model.cache.Cached;
import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.Order;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Sim;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomGood;
import ru.ulmart.model.telecom.db.Tele2DataService;
import ru.ulmart.model.telecom.db.TelecomDataService;
import ru.ulmart.model.telecom.tele2.Tele2Region;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/*
 * Для каждого оператора должен быть свой класс с сервисами.
 * Это необходимо для корректной работы системы кэширования.
 */
public class Tele2Service extends AbstractTele2Service implements TelecomService {
    private static final Logger LOGGER = LoggerFactory.getLogger(Tele2Service.class);

    @Value("${tele2.apiUrl}")
    private String apiUrl;
    @Value("${tele2.apiToken}")
    private String apiToken;
    @Autowired
    @Qualifier("globalRestTemplate")
    private RestTemplate restTemplate;
    @Autowired
    private Tele2DataService tele2Service;
    @Autowired
    private Tele2NumbersService numbersService;
    @Autowired
    private Tele2TariffsService tariffsService;
    @Autowired
    private Tele2OrderService orderService;
    @Autowired
    @Qualifier("telecomDataServiceSlave")
    private TelecomDataService telecomDataService;
    @Autowired
    private Settings settings;

    /**
     * Получение регионов Теле2
     * GET api/{token}/regionsales/regions
     *
     * @return список регионов
     */
    public List<Tele2Region> getRegion() {
        ResponseRegion response = restTemplate.getForObject(
                apiUrl + apiToken + "/regionsales/regions",
                ResponseRegion.class);
        if (Boolean.TRUE.equals(response.getIsSuccess())) {
            return response.getData();
        } else {
            throw new ServiceInvocationException(null, response, response.getErrorReason());
        }
    }

    /**
     * Получение информации о городе Теле2 по id города Юлмарта
     *
     * @param ulmartCityId id города Юлмарта
     * @return город Теле2
     */
    @Cached
    public TelecomCity getCityInfo(Long ulmartCityId) {
        return tele2Service.getCityInfo(ulmartCityId);
    }

    public Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest) {
        return numbersService.listNumbers(region, filterRequest);
    }
    public PhoneNumber getNumberInfo(String region, String numberId) {
        return numbersService.getNumberInfo(region, numberId);
    }

    public PhoneNumber getNumber(String region, String numberId) {
        return numbersService.getNumber(region, numberId);
    }

    @Cached
    public List<Tariff> listTariffs(String region) {
        List<Tariff> tariffs = tariffsService.listTariffs(region);
        if(tariffs == null || tariffs.isEmpty()) {
            return Collections.emptyList();
        }
        return tariffs;
    }

    @Cached
    public TariffDetails getTariffDetail(Long id, String region) {
        return tariffsService.getTariffDetail(id, region);
    }

    public TariffDetails getTariffDetailCommon(String region) {
        return null;
    }

    @Cached
    public Tariff getDefaultTariff(String region) {
        return tariffsService.getDefaultTariff(region);
    }

    @Cached
    public Tariff getTariffOrDefault(String region, Long id) {
        return tariffsService.getTariffOrDefault(region, id);
    }

    private String reserveProducts(Collection<TelecomGood> cart, String reservationToken) {
        return orderService.reserveProducts(filterTelecomCartByType(cart), reservationToken);
    }

    public Order confirmOrder(Integer salePointId, Client client, Collection<TelecomGood> cart, String reservationToken) {
        return orderService.confirmOrder(salePointId, client, cart, reservationToken);
    }

    @Cached
    public boolean hasInThisRegion(Long ulmartCityId) {
        return getCityInfo(ulmartCityId) != null;
    }

    @Cached
    public String getCurrentRegionId(Long ulmartCityId) {
        TelecomCity city = getCityInfo(ulmartCityId);
        if (city == null || city.getTelecomRegionName() == null) {
            return null;
        }
        return city.getTelecomRegionName();
    }

    @Cached
    public Long getArticleForUlmartCity(Long region) {
        return tele2Service.getArticleForUlmartCity(region);
    }

    public String onBeforeConfirmOrder(String reservationToken, Client client, long shopId, Collection<TelecomGood> cart) {
        Collection<TelecomGood> tele2Goods = filterTelecomCartByType(cart);
        if (!tele2Goods.isEmpty()) {
            if (reservationToken != null) {
                LOGGER.info("fillRequest: saved token is restored = {}", reservationToken);
            }
            String token = reserveProducts(tele2Goods, reservationToken);
            Integer salePointId = getSalePointID(shopId);
            Order order = confirmOrder(salePointId, client, tele2Goods, token);
            return order.getId();
        }
        return null;
    }

    @Cached
    public List<Sim> getSims(Long ulmartCityId) {
        List<Sim> sims = telecomDataService.getSims(ulmartCityId, getTelecomType());
        return sims;
    }

    @Cached
    public boolean isSimAlwaysAvailable(Long shopId) {
        String shopIdsStr = settings.getAdminSetting(Settings.TELE2_ALWAYS_AVAILABLE_SIM_SHOPS);
        List<String> shops = Splitter.on(',').omitEmptyStrings().trimResults().splitToList(shopIdsStr);
        return shops.contains(shopId.toString());
    }

    public boolean onBeforeAddToCart(CartTelecomInfo cartTelecomInfo) {
        // Для Теле2 нет действий
        return true;
    }

    public boolean onAfterRemoveFromCart(CartTelecomInfo cartTelecomInfo) {
        // Для Теле2 нет действий
        return false;
    }

    public boolean onAfterRemoveFromOrder(CartTelecomInfo cartTelecomInfo) {
        // Для Теле2 нет действий.
        return true;
    }

    public boolean onAfterAddToOrder(String orderId, CartTelecomInfo cartTelecomInfo) {
        // Для Теле2 нет действий.
        return true;
    }

    public boolean hasTariffAsGood() {
        return false;
    }

    @Override
    public boolean hasNumbers() {
        return true;
    }

    /**
     * Получаем SalePointID для отправки его в Теле2
     *
     * @param shopId id магазина Юлмарт
     */
    private Integer getSalePointID(Long shopId) {
        Integer salePointId;
        if (shopId != null) {
            salePointId = tele2Service.getSalePoint(shopId);
        } else {
            final boolean isDeliveryEnabled = (boolean) settings.getAdminSetting(Settings.SETTINGS_TELECOM_DELIVERY_ENABLED);
            if (isDeliveryEnabled) {      // если доставка включена, то берем значение для SalePointId из таблицы констант
                salePointId = (Integer) settings.getAdminSetting(Settings.SETTINGS_TELECOM_DELIVERY_POINT_ID);
            } else {
                throw new IllegalArgumentException("ShopId not found for create Telecom order.");
            }
        }
        return salePointId;
    }
}

