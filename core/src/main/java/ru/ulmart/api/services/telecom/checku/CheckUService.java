package ru.ulmart.api.services.telecom.checku;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ru.ulmart.model.telecom.TelecomType;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.Collections;

/**
 * Сервис для работы с API CheckU
 *
 * @author Telezhnikova Olga
 */
public abstract class CheckUService {

    @Autowired
    @Qualifier("globalRestTemplate")
    protected RestTemplate restTemplate;
    @Value("${checkU.apiUrl}")
    protected String apiUrl;
    @Value("${checkU.username}")
    private String username; 
    @Value("${checkU.password}")
    private String password;
    private HttpHeaders httpHeaders;

    @SuppressWarnings("serial")
    @PostConstruct
    private void createAuthHeaders() {
        this.httpHeaders = new HttpHeaders(){
            {
                String auth = String.format("%s:%s", username, password);
                byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
                String authHeader = "Basic " + new String(encodedAuth);
                set(AUTHORIZATION, authHeader);
                setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            }
        };
    }

    protected HttpEntity<?> createEntity(MultiValueMap<String, Object> body) {
        return createEntity(body, httpHeaders);
    }

    protected HttpEntity<?> createEntity(MultiValueMap<String, Object> body, HttpHeaders httpHeaders) {
        httpHeaders.putAll(this.httpHeaders);
        return new HttpEntity<Object>(body, httpHeaders);
    }

    public TelecomType getType() {
        return TelecomType.MTS;
    }
}
