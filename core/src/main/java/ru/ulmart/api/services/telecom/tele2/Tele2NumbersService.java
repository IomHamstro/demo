package ru.ulmart.api.services.telecom.tele2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import ru.ulmart.api.services.telecom.TelecomUtil;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.service.telecom.problem.ServiceInvocationException;
import ru.ulmart.api.services.telecom.tele2.rest.RequestNumbersByFilter;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseNumbersByFilter;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseRandomNumber;
import ru.ulmart.model.cache.Cached;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.NumberTypeMapping;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.db.Tele2DataService;
import ru.ulmart.model.telecom.db.TelecomDataService;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Tele2NumbersService extends AbstractTele2Service {
    @Value("${tele2.apiUrl}")
    private String apiUrl;
    @Value("${tele2.apiToken}")
    private String apiToken;
    @Autowired
    @Qualifier("globalRestTemplate")
    private RestTemplate restTemplate;
    @Autowired
    private Tele2DataService tele2Service;
    @Autowired
    @Qualifier("telecomDataServiceSlave")
    private TelecomDataService telecomDataService;

    /**
     * Получение списка номеров для витрины, удовлетворяющих фильтру
     * POST api/{token}/catalog/{region}/numbersbyfilter
     *
     * @param region        регион
     * @param filterRequest значения фильтров
     * @return список номеров
     */
    public Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest) {
        if (region == null) return null;

        Map<NumberType, List<PhoneNumber>> numbersMap = new HashMap<>();
        // Для запроса номеров нескольких типов необходимо для каждого типа сделать свой запрос
        List<String> typeAliases = filterRequest.getTypesAlias();
        Map<String, NumberTypeMapping> telecomTypeMapping = getTelecomAliasTypeMapping(region);
        FilterRequest clonedRequest = new FilterRequest(filterRequest);
        for(String typeAlias: typeAliases) {
            NumberTypeMapping mapping = telecomTypeMapping.get(typeAlias);
            if(mapping != null) {
                NumberType numberType = new NumberType(mapping.getNumberTypeAlias());
                numberType.setId(mapping.getNumberTypeId());

                List<PhoneNumber> phoneNumbers = listNumbers(region, clonedRequest, mapping, numberType);
                numbersMap.put(numberType, phoneNumbers);
            }
        }
        filterRequest.setTypesAlias(typeAliases);
        return numbersMap;
    }

    private List<PhoneNumber> listNumbers(String region, FilterRequest filterRequest, NumberTypeMapping mapping, NumberType numberType) {
        if(filterRequest.getTake() == null || filterRequest.getTake() == 0) {
            return Collections.emptyList();
        }
        List<String> aliases = Arrays.asList(mapping.getOperatorNumberTypeAlias());
        filterRequest.setTypesAlias(aliases);
        RequestNumbersByFilter requestAPI = TelecomUtil.convert(filterRequest);
        ResponseNumbersByFilter response = restTemplate.postForObject(
                apiUrl + apiToken + "/catalog/" + region + "/numbersbyfilter",
                createEntity(requestAPI), ResponseNumbersByFilter.class);
        if (Boolean.TRUE.equals(response.getIsSuccess())) {
            List<PhoneNumber> phoneNumbers = TelecomUtil.convertPhone(response);

            for (PhoneNumber phoneNumber: phoneNumbers) {
                fillNumberTelecomType(phoneNumber);
                phoneNumber.setNumberType(numberType);
            }

            return phoneNumbers;
        } else {
            throw new ServiceInvocationException(requestAPI, response, response.getErrorReason());
        }
    }

    /**
     * Получаем номер телефона по умолчанию
     * GET api/{token}/catalog/{region}/RandomNumber
     *
     * @param region регион
     * @return номер телефона
     */
    public PhoneNumber getDefaultNumber(String region) {
        if (region == null) return null;
        ResponseRandomNumber response = restTemplate.getForObject(
                apiUrl + apiToken + "/catalog/" + region + "/RandomNumber",
                ResponseRandomNumber.class);
        if (Boolean.TRUE.equals(response.getIsSuccess())) {
            if (response.getData() == null)
                throw new IllegalArgumentException("API /catalog/" + region + "/RandomNumber return null in Data!");
            PhoneNumber phoneNumber = TelecomUtil.convert(response.getData());
            fillNumberTelecomType(phoneNumber);
            Long typeId = response.getData().getTypeId();
            Map<String, NumberTypeMapping> mappingMap = getTele2IdTypeMapping(region);
            NumberTypeMapping mapping = mappingMap.get(String.valueOf(typeId));
            if(mapping != null) {
                NumberType numberType = new NumberType(mapping.getNumberTypeAlias());
                numberType.setId(mapping.getNumberTypeId());
                phoneNumber.setNumberType(numberType);
            }
            return phoneNumber;
        } else {
            throw new ServiceInvocationException(null, response, response.getErrorReason());
        }
    }

    /**
     * Получение описания телефонного номера
     * GET api/{token}/catalog/{region}/phone/{id}
     *
     * @param region   регион
     * @param numberId id телефонного номера
     * @return номер телефона
     */
    public PhoneNumber getNumberInfo(String region, String numberId) {
        if (region == null) return null;
        ResponseRandomNumber response = restTemplate.getForObject(
                apiUrl + apiToken + "/catalog/" + region + "/phone/" + numberId,
                ResponseRandomNumber.class);
        if (Boolean.TRUE.equals(response.getIsSuccess())) {
            if (response.getData() == null)
                throw new IllegalArgumentException("API /catalog/" + region + "/phone return null in Data!");
            PhoneNumber phoneNumber = TelecomUtil.convert(response.getData());
            Map<String, NumberTypeMapping> mappingMap = getTele2IdTypeMapping(region);
            NumberTypeMapping mapping = mappingMap.get(String.valueOf(response.getData().getTypeId()));
            if(mapping != null) {
                NumberType numberType = new NumberType(mapping.getNumberTypeAlias());
                numberType.setId(mapping.getNumberTypeId());
                numberType.setTitle(mapping.getNumberTypeTitle());
                phoneNumber.setNumberType(numberType);
            }
            fillNumberTelecomType(phoneNumber);
            return phoneNumber;
        } else {
            throw new ServiceInvocationException(null, response, response.getErrorReason());
        }
    }

    /**
     * Получение описания телефонного номера
     * GET api/{token}/catalog/{region}/phone/{id}
     *
     * @param region   регион
     * @param numberId id телефонного номера
     * @return номер телефона соответствующий id, либо номер по умолчанию
     */
    public PhoneNumber getNumber(String region, String numberId) {
        if (numberId != null) {
            PhoneNumber phoneNumber = getNumberInfo(region, numberId);
            if (phoneNumber != null) {
                return phoneNumber;
            }
        }

        return getDefaultNumber(region);
    }

    /**
     * Получение маппингов типов телеком на типы Теле2 с ключом по типу Теле2
     * @param region регион
     * @return словарь маппингов
     */
    @Cached
    public Map<String, NumberTypeMapping> getTele2AliasTypeMapping(String region) {
        List<NumberTypeMapping> mappings = telecomDataService.getNumberTypeMappings(region, getTelecomType());
        if(mappings == null) {
            return Collections.emptyMap();
        }
        Map<String, NumberTypeMapping> mappingMap = new HashMap<>();
        for(NumberTypeMapping mapping: mappings) {
            mappingMap.put(mapping.getOperatorNumberTypeAlias(), mapping);
        }
        return mappingMap;
    }

    /**
     * Получение маппингов типов телеком на типы Теле2 с ключом по типу Теле2
     * @param region регион
     * @return словарь маппингов
     */
    @Cached
    public Map<String, NumberTypeMapping> getTele2IdTypeMapping(String region) {
        List<NumberTypeMapping> mappings = telecomDataService.getNumberTypeMappings(region, getTelecomType());
        if(mappings == null) {
            return Collections.emptyMap();
        }
        Map<String, NumberTypeMapping> mappingMap = new HashMap<>();
        for(NumberTypeMapping mapping: mappings) {
            mappingMap.put(mapping.getOperatorNumberTypeId(), mapping);
        }
        return mappingMap;
    }

    /**
     * Получение маппингов типов телеком на типы Теле2 с ключом по типу телеком
     * @param region регион
     * @return словарь маппингов
     */
    @Cached
    public Map<String, NumberTypeMapping> getTelecomAliasTypeMapping(String region) {
        List<NumberTypeMapping> mappings = telecomDataService.getNumberTypeMappings(region, getTelecomType());
        if(mappings == null) {
            return Collections.emptyMap();
        }
        Map<String, NumberTypeMapping> mappingMap = new HashMap<>();
        for(NumberTypeMapping mapping: mappings) {
            mappingMap.put(mapping.getNumberTypeAlias(), mapping);
        }
        return mappingMap;
    }
}
