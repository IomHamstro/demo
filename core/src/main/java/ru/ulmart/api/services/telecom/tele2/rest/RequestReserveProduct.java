package ru.ulmart.api.services.telecom.tele2.rest;

import ru.ulmart.model.telecom.Sim;

/**
 * Параметры отправляемые в API
 * POST api/{token}/catalog/reserveproduct
 *
 */
public class RequestReserveProduct {

    /**
     * Идентификатор номера телефона (обязательный)
     */
    private String phoneId;

    /**
     * Идентификатор тарифа
     */
    private Long tariffId;

    /**
     * Тип сим карты
     */
    private Sim sim;

    public RequestReserveProduct() {
    }

    public RequestReserveProduct(String phoneId, Long tariffId, Sim sim) {
        this.phoneId = phoneId;
        this.tariffId = tariffId;
        this.sim = sim;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String phoneId) {
        this.phoneId = phoneId;
    }

    public Long getTariffId() {
        return tariffId;
    }

    public void setTariffId(Long tariffId) {
        this.tariffId = tariffId;
    }

    public Sim getSim() {
        return sim;
    }

    public void setSim(Sim sim) {
        this.sim = sim;
    }
}
