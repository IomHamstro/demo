package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Тело ответа из API
 * POST api/{token}/order/create/{ReservationToken}
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataCreateOrder {

    @JsonProperty("HasError")
    private Boolean hasError;
    @JsonProperty("ValidationError")
    private String validationError;
    @JsonProperty("ReservationResult")
    private DataReserveProduct reservationResult;
    @JsonProperty("HasSingleReservationError")
    private Boolean hasSingleReservationError;
    @JsonProperty("OrderNumber")
    private String orderNumber;
    @JsonProperty("IsSuccess")
    private Boolean isSuccess;

    public DataCreateOrder() {
    }

    public DataCreateOrder(Boolean hasError, String validationError, DataReserveProduct reservationResult,
                           Boolean hasSingleReservationError, String orderNumber, Boolean isSuccess) {
        this.hasError = hasError;
        this.validationError = validationError;
        this.reservationResult = reservationResult;
        this.hasSingleReservationError = hasSingleReservationError;
        this.orderNumber = orderNumber;
        this.isSuccess = isSuccess;
    }

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getValidationError() {
        return validationError;
    }

    public void setValidationError(String validationError) {
        this.validationError = validationError;
    }

    public DataReserveProduct getReservationResult() {
        return reservationResult;
    }

    public void setReservationResult(DataReserveProduct reservationResult) {
        this.reservationResult = reservationResult;
    }

    public Boolean getHasSingleReservationError() {
        return hasSingleReservationError;
    }

    public void setHasSingleReservationError(Boolean hasSingleReservationError) {
        this.hasSingleReservationError = hasSingleReservationError;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
}
