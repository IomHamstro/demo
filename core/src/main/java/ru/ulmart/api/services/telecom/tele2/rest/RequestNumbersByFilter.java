package ru.ulmart.api.services.telecom.tele2.rest;

import java.util.List;

/**
 * Параметры отправляемые в API
 * POST api/{token}/catalog/numbersbyfilter
 *
 */
public class RequestNumbersByFilter {
    private static final double DEFAULT_PRICE_MAX = 1000000D;

    /**
     * Список алиасов типов (необязательный)
     */
    private List<String> typeAliases;

    /**
     * Строка запроса поиска по вхождению комбинации цифр. Пример - "123" (необязательный)
     */
    private String query;

    /**
     * Максимальная цена товара (обязательный)
     */
    private double priceTo = DEFAULT_PRICE_MAX;

    /**
     * Количество записей которые необходимо пропустить (обязательный)
     */
    private Integer skip;

    /**
     * Количество записей которое необходимо выбрать (обязательный)
     */
    private Integer take;

    /**
     * Флаг, указывающий необходимость включения акционных номеров в выборку (необязательный)
     */
    private Boolean showAction;

    public RequestNumbersByFilter() {
    }

    public RequestNumbersByFilter(Double priceTo, Integer skip, Integer take) {
        this.priceTo = priceTo;
        this.skip = skip;
        this.take = take;
    }

    public RequestNumbersByFilter(List<String> typeAliases,
                                  String query, Integer skip, Integer take, Boolean showAction) {
        this.typeAliases = typeAliases;
        this.query = query;
        this.skip = skip;
        this.take = take;
        this.showAction = showAction;
    }

    public List<String> getTypeAliases() {
        return typeAliases;
    }

    public void setTypeAliases(List<String> typeAliases) {
        this.typeAliases = typeAliases;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Double getPriceTo() {
        return priceTo;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public Integer getTake() {
        return take;
    }

    public void setTake(Integer take) {
        this.take = take;
    }

    public Boolean getShowAction() {
        return showAction;
    }

    public void setShowAction(Boolean showAction) {
        this.showAction = showAction;
    }

    @Override
    public String toString() {
        return "RequestNumbersByFilter{" +
                ", typeAliases=" + typeAliases +
                ", query='" + query + '\'' +
                ", priceTo=" + priceTo +
                ", skip=" + skip +
                ", take=" + take +
                ", showAction=" + showAction +
                '}';
    }
}