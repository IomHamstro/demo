package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Тело ответа из API
 * POST api/{token}/catalog/groupednumbersbyfilter
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataGroupedNumbersByFilter {

    @JsonProperty("Numbers")
    private List<DataNumbersByFilter> numbers;
    @JsonProperty("ActualPrice")
    private Double actualPrice;
    @JsonProperty("HasSpecialPrice")
    private Boolean hasSpecialPrice;
    @JsonProperty("Mask")
    private String mask;
    @JsonProperty("Digit")
    private Integer digit;
    @JsonProperty("IsFake")
    private Boolean isFake;

    public DataGroupedNumbersByFilter() {
    }

    public DataGroupedNumbersByFilter(List<DataNumbersByFilter> numbers, Double actualPrice,
                                      Boolean hasSpecialPrice, String mask, Integer digit, Boolean isFake) {
        this.numbers = numbers;
        this.actualPrice = actualPrice;
        this.hasSpecialPrice = hasSpecialPrice;
        this.mask = mask;
        this.digit = digit;
        this.isFake = isFake;
    }

    public List<DataNumbersByFilter> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<DataNumbersByFilter> numbers) {
        this.numbers = numbers;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Boolean getHasSpecialPrice() {
        return hasSpecialPrice;
    }

    public void setHasSpecialPrice(Boolean hasSpecialPrice) {
        this.hasSpecialPrice = hasSpecialPrice;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public Integer getDigit() {
        return digit;
    }

    public void setDigit(Integer digit) {
        this.digit = digit;
    }

    public Boolean getIsFake() {
        return isFake;
    }

    public void setIsFake(Boolean isFake) {
        this.isFake = isFake;
    }

    @Override
    public String toString() {
        return "DataGroupedNumbersByFilter{" +
                "numbers=" + numbers +
                ", actualPrice=" + actualPrice +
                ", hasSpecialPrice=" + hasSpecialPrice +
                ", mask='" + mask + '\'' +
                ", digit=" + digit +
                ", isFake=" + isFake +
                '}';
    }
}
