package ru.ulmart.api.services.telecom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.ulmart.api.services.telecom.tele2.rest.DataNumbersByFilter;
import ru.ulmart.api.services.telecom.tele2.rest.RequestCreateOrder;
import ru.ulmart.api.services.telecom.tele2.rest.RequestNumbersByFilter;
import ru.ulmart.api.services.telecom.tele2.rest.RequestReserveProduct;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseCreateOrder;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseNumbersByFilter;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.Order;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.TelecomGood;
import ru.ulmart.model.telecom.TelecomType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TelecomUtil {
    private static final Integer MSIDN_LENGTH = 11;
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static List<PhoneNumber> convertPhone(ResponseNumbersByFilter response) {
        List<PhoneNumber> result = new ArrayList<>();
        for (DataNumbersByFilter number : response.getData()) {
            result.add(convert(number));
        }
        return result;
    }

    public static PhoneNumber convert(DataNumbersByFilter number) {
        return new PhoneNumber(number.getId(), String.valueOf(number.getMsisdn()), number.getActualPrice().longValue());
    }

    public static RequestNumbersByFilter convert(FilterRequest filterRequest) {
        return new RequestNumbersByFilter(filterRequest.getTypesAlias(),
                filterRequest.getCombination(), filterRequest.getSkip(), filterRequest.getTake(), null);
    }

    public static List<RequestReserveProduct> convert(Collection<TelecomGood> cart) {
        List<RequestReserveProduct> result = new ArrayList<>();
        for (TelecomGood cartTelecomInfo : cart) {
            if (cartTelecomInfo.getPhoneNumber() == null || cartTelecomInfo.getPhoneNumber().getId() == null)
                throw new IllegalArgumentException("Phone number cannot be null!");

            result.add(new RequestReserveProduct(
                    cartTelecomInfo.getPhoneNumber().getId(),
                    cartTelecomInfo.getTariffId(),
                    cartTelecomInfo.getSim()));
        }
        return result;
    }

    public static RequestCreateOrder convert(Client client, Integer salePoint, Collection<TelecomGood> cart) {
        RequestCreateOrder result = new RequestCreateOrder();
        RequestCreateOrder.OrderInfo order = result.new OrderInfo();
        RequestCreateOrder.OrderInfo.CustomerContact customerContact = order.new CustomerContact();
        customerContact.setFirstName(client.getFirstName());
        customerContact.setLastName(client.getLastName());
        customerContact.setPatronymic(client.getPatronymic());
        customerContact.setContactPhone(client.getContactPhone());
        customerContact.setEmail(client.getContactEmail());

        RequestCreateOrder.OrderInfo.CustomerIdentity customerIdentity = order.new CustomerIdentity();
        customerIdentity.setBirthDate(client.getBirthDate());
        customerIdentity.setBirthPlace(client.getBirthPlace());
        customerIdentity.setDocumentIssuedBy(client.getIssuedBy());
        customerIdentity.setDocumentIssuedDate(client.getDateOfIssuance());
        customerIdentity.setDocumentIssuedPlace(client.getDocumentIssuedPlace());
        customerIdentity.setDocumentNumber(client.getSeriesNumber());
        customerIdentity.setDocumentType(client.getDocumentType());
        customerIdentity.setGender(client.getGender());
        customerIdentity.setPostalCode(client.getPostalCode());
        customerIdentity.setStreet(client.getStreet());
        customerIdentity.setBuilding(client.getBuilding());

        order.setCustomerContact(customerContact);
        order.setCustomerIdentity(customerIdentity);
        Long cityId = cart.iterator().next().getCityId();
        if (cityId != null) {
            order.setCityId(cityId.intValue());
        }
        order.setSalePoint(salePoint);
        result.setOrderInfo(order);
        result.setProducts(convert(cart));
        return result;
    }

    public static Order convert(ResponseCreateOrder response) {
        return new Order(response.getData().getOrderNumber());
    }

    public static String toJson(Object object) {
        if (object == null)
            return null;
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return object.toString();
        }
    }

    public static String formatMsisdn(String msisdn) {
        if (MSIDN_LENGTH.equals(msisdn.length())) {
            return msisdn.substring(0, 1) + " " + msisdn.substring(1, 4) + " " +
                    msisdn.substring(4, 7) + "-" + msisdn.substring(7);
        }
        return msisdn;
    }

    public static List<TelecomType> getIntersectedOperators(final List<TelecomType> availableTypes,
                                                            final TelecomType selectedType) {
        if (selectedType == null) {
            return availableTypes;
        }
        List<TelecomType> selectedTypes = Arrays.asList(selectedType);
        return getIntersectedOperators(availableTypes, selectedTypes);
    }

    public static List<TelecomType> getIntersectedOperators(final List<TelecomType> availableTypes,
                                                            final List<TelecomType> selectedTypes) {
        if (availableTypes == null) {
            return Collections.emptyList();
        }
        if (selectedTypes == null || selectedTypes.isEmpty()) {
            return availableTypes;
        }
        List<TelecomType> intersectedTypes = new ArrayList<>(availableTypes);
        intersectedTypes.retainAll(selectedTypes);

        return intersectedTypes;
    }

    public static List<TelecomType> getIntersectedOperators(final List<TelecomType> availableTypes, final String stringType) {
        List<TelecomType> selectedTypes = TelecomType.convertToList(stringType);
        return getIntersectedOperators(availableTypes, selectedTypes);
    }
}
