package ru.ulmart.api.services.telecom;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.ulmart.model.cache.Cached;
import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Sim;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomGood;
import ru.ulmart.model.telecom.TelecomTariffType;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.model.telecom.db.TelecomDataService;
import ru.ulmart.service.telecom.TelecomServiceFacade;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class TelecomServiceFacadeImpl implements TelecomServiceFacade {
    private Map<TelecomType, TelecomService> servicesMap;
    private Set<TelecomType> availableServices;
    private boolean skipError;

    @Autowired
    @Qualifier("telecomDataServiceSlave")
    protected TelecomDataService telecomDataService;

    public TelecomServiceFacadeImpl(Map<TelecomType, TelecomService> servicesMap, boolean skipError) {
        this.skipError = skipError;
        this.servicesMap = servicesMap;
        availableServices = new HashSet<>();
        if (this.servicesMap != null) {
            for (TelecomType key : this.servicesMap.keySet()) {
                availableServices.add(key);
            }
        }
    }

    /**
     * Получение списка красивых номеров.
     *
     * @param region        регион
     * @param filterRequest параметры для фильтрации
     * @param telecomType   оператор
     * @return список номеров
     */
    @Override
    public Map<NumberType, List<PhoneNumber>> listNumbers(final String region, final FilterRequest filterRequest,
                                                          TelecomType telecomType) {
        return retrieve(new Command<Map<NumberType, List<PhoneNumber>>>() {
            @Override
            public Map<NumberType, List<PhoneNumber>> execute(TelecomService telecomService) {
                if (region == null) {
                    return Collections.emptyMap();
                }
                return telecomService.listNumbers(region, filterRequest);
            }
        }, telecomType);
    }

    /**
     * Получение списка красивых номеров.
     *
     * @param ulmartCityId  id города
     * @param filterRequest параметры для фильтрации
     * @param telecomType   оператор
     * @return список номеров
     */
    @Override
    public Map<NumberType, List<PhoneNumber>> listNumbers(final long ulmartCityId, final FilterRequest filterRequest,
                                                          TelecomType telecomType) {
        return retrieve(new Command<Map<NumberType, List<PhoneNumber>>>() {
            @Override
            public Map<NumberType, List<PhoneNumber>> execute(TelecomService telecomService) {
                String region = telecomService.getCurrentRegionId(ulmartCityId);
                if (region == null) {
                    return Collections.emptyMap();
                }
                return telecomService.listNumbers(region, filterRequest);
            }
        }, telecomType);
    }

    /**
     * Получение списка доступных сим-карт.
     *
     * @param ulmartCityId id города
     * @param telecomType  оператор
     * @return список сим-карт
     */
    @Override
    public List<Sim> getSims(final Long ulmartCityId, TelecomType telecomType) {
        return retrieve(new Command<List<Sim>>() {
            @Override
            public List<Sim> execute(TelecomService telecomService) {
                return telecomService.getSims(ulmartCityId);
            }
        }, telecomType);
    }

    /**
     * Проверка факта того, что для данного ЦИЗ наличие sim не проверяется
     *
     * @param shopId      id ЦИЗ
     * @param telecomType тип оператора
     * @return признак, что sim всегда доступна для данного ЦИЗ
     */
    @Override
    public Boolean isSimAlwaysAvailable(final Long shopId, final TelecomType telecomType) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.isSimAlwaysAvailable(shopId);
            }
        }, telecomType);
    }

    /**
     * Получение деталей телефонного номера.
     *
     * @param region      регион
     * @param numberId    идентифкатор телефонного номера
     * @param telecomType оператор
     * @return детали телефонного номера
     */
    @Override
    public PhoneNumber getNumberInfo(final String region, final String numberId,
                                     TelecomType telecomType) {
        return retrieve(new Command<PhoneNumber>() {
            @Override
            public PhoneNumber execute(TelecomService telecomService) {
                if (region == null) {
                    return null;
                }
                return telecomService.getNumberInfo(region, numberId);
            }
        }, telecomType);
    }

    /**
     * Получение деталей телефонного номера.
     *
     * @param numberId     идентифкатор телефонного номера
     * @param ulmartCityId id города
     * @param telecomType  оператор
     * @return детали телефонного номера
     */

    @Override
    public PhoneNumber getNumberInfo(final String numberId, final Long ulmartCityId,
                                     TelecomType telecomType) {
        return retrieve(new Command<PhoneNumber>() {
            @Override
            public PhoneNumber execute(TelecomService telecomService) {
                String region = telecomService.getCurrentRegionId(ulmartCityId);
                if (region == null) {
                    return null;
                }
                return telecomService.getNumberInfo(region, numberId);
            }
        }, telecomType);
    }

    /**
     * Получение деталей телефонного номера.
     *
     * @param region      регион (может быть null)
     * @param numberId    идентификатор телефонного номера
     * @param telecomType оператор
     * @return номер телефона соответствующий идентификатору
     */
    @Override
    public PhoneNumber getNumber(final String region, final String numberId,
                                          TelecomType telecomType) {
        return retrieve(new Command<PhoneNumber>() {
            @Override
            public PhoneNumber execute(TelecomService telecomService) {
                if (region == null) {
                    return null;
                }
                return telecomService.getNumber(region, numberId);
            }
        }, telecomType);
    }

    /**
     * Получение списка тарифов для указанного региона.
     *
     * @param ulmartCityId id города
     * @param telecomType  оператор
     * @return список тарифов
     */
    @Override
    public List<Tariff> listTariffs(final Long ulmartCityId, TelecomType telecomType) {
        return retrieve(new Command<List<Tariff>>() {
            @Override
            public List<Tariff> execute(TelecomService telecomService) {
                String region = telecomService.getCurrentRegionId(ulmartCityId);
                if (region == null) {
                    return Collections.emptyList();
                }
                return telecomService.listTariffs(region);
            }
        }, telecomType);
    }

    /**
     * Получение списка идентификаторов тарифов для указанного региона.
     * В список попадают только те тарифы, которые являются обычными товарами.
     *
     * @param ulmartCityId id города
     * @return список тарифов
     */
    @Override
    public Map<TelecomType, List<Long>> getGoodTariffIdsMap(long ulmartCityId) {
        Map<TelecomType, Boolean> tariffAsGoodMap = getTariffAsGoodMap();
        Map<TelecomType, List<Long>> tariffIdsMap = new HashMap<>();
        Function<Tariff, Long> tariffToId = new Function<Tariff, Long>() {
            @Override
            public Long apply(Tariff tariff) {
                return tariff.getGoodId();
            }
        };
        Predicate<Tariff> isGoodTariff = new Predicate<Tariff>() {
            @Override
            public boolean apply(Tariff input) {
                return input.isCommonGood();
            }
        };
        for (TelecomType telecomType : tariffAsGoodMap.keySet()) {
            Boolean hasTariffAsGood = tariffAsGoodMap.get(telecomType);
            if (hasTariffAsGood) {
                List<Tariff> tariffs = listTariffs(ulmartCityId, telecomType);
                Lists.newArrayList(Iterables.filter(tariffs, isGoodTariff));
                List<Long> ids = Lists.transform(tariffs, tariffToId);
                if (!ids.isEmpty()) {
                    tariffIdsMap.put(telecomType, ids);
                }
            }
        }
        return tariffIdsMap;
    }

    /**
     * Получение списка тарифов для указанного региона.
     *
     * @param region      регион оператора
     * @param telecomType оператор
     * @return список тарифов
     */
    @Override
    public List<Tariff> listTariffs(final String region, TelecomType telecomType) {
        return retrieve(new Command<List<Tariff>>() {
            @Override
            public List<Tariff> execute(TelecomService telecomService) {
                if (region == null) {
                    return Collections.emptyList();
                }
                return telecomService.listTariffs(region);
            }
        }, telecomType);
    }

    /**
     * Получение полной информации по тарифу.
     *
     * @param id          id тарифа
     * @param region      регион оператора
     * @param telecomType оператор
     * @return детальная информация по тарифу
     */
    @Override
    public TariffDetails getTariffDetail(final Long id, final String region,
                                         TelecomType telecomType) {
        return retrieve(new Command<TariffDetails>() {
            @Override
            public TariffDetails execute(TelecomService telecomService) {
                if (region == null) {
                    return null;
                }
                return telecomService.getTariffDetail(id, region);
            }
        }, telecomType);
    }

    /**
     * Получение общей информации по тарифам в регионе.
     *
     * @param ulmartCityId идентификатор города в Юлмарт
     * @param telecomType  оператор
     * @return общая информация по тарифам в регионе.
     */
    @Override
    public TariffDetails getTariffDetailCommon(final Long ulmartCityId,
                                               TelecomType telecomType) {
        return retrieve(new Command<TariffDetails>() {
            @Override
            public TariffDetails execute(TelecomService telecomService) {
                String region = telecomService.getCurrentRegionId(ulmartCityId);
                if (region == null) {
                    return null;
                }
                return telecomService.getTariffDetailCommon(region);
            }
        }, telecomType);
    }

    /**
     * Получение полной информации по тарифу.
     *
     * @param id           id тарифа
     * @param ulmartCityId идентификатор города в Юлмарт
     * @param telecomType  оператор
     * @return детальная информация по тарифу
     */
    @Override
    public TariffDetails getTariffDetail(final Long id, final Long ulmartCityId,
                                         TelecomType telecomType) {
        return retrieve(new Command<TariffDetails>() {
            @Override
            public TariffDetails execute(TelecomService telecomService) {
                String region = telecomService.getCurrentRegionId(ulmartCityId);
                if (region == null) {
                    return null;
                }
                return telecomService.getTariffDetail(id, region);
            }
        }, telecomType);
    }

    /**
     * Получение тарифа по умолчанию для указанного региона.
     *
     * @param region      регион
     * @param telecomType оператор
     * @return тариф
     */
    @Override
    public Tariff getDefaultTariff(final String region, TelecomType telecomType) {
        return retrieve(new Command<Tariff>() {
            @Override
            public Tariff execute(TelecomService telecomService) {
                if (region == null) {
                    return null;
                }
                return telecomService.getDefaultTariff(region);
            }
        }, telecomType);
    }

    /**
     * Получение детальной информации о тарифе.
     *
     * @param id          идентификатор тарифа
     * @param region      регион (может быть null)
     * @param telecomType оператор
     * @return полная информация о тарифе соответствующем идентификатору, либо тариф по умолчанию
     */
    @Override
    public Tariff getTariffOrDefault(final Long id, final String region,
                                     TelecomType telecomType) {
        return retrieve(new Command<Tariff>() {
            @Override
            public Tariff execute(TelecomService telecomService) {
                if (region == null) {
                    return null;
                }
                return telecomService.getTariffOrDefault(region, id);
            }
        }, telecomType);
    }

    /**
     * Получение детальной информации о тарифе.
     *
     * @param id          идентификатор тарифа
     * @param ulmartCityId идентификатор города в Юлмарт
     * @param telecomType оператор
     * @return полная информация о тарифе соответствующем идентификатору, либо тариф по умолчанию
     */
    @Override
    public Tariff getTariffOrDefault(final Long id, final Long ulmartCityId,
                                     TelecomType telecomType) {
        return retrieve(new Command<Tariff>() {
            @Override
            public Tariff execute(TelecomService telecomService) {
                String region = telecomService.getCurrentRegionId(ulmartCityId);
                if (region == null) {
                    return null;
                }
                return telecomService.getTariffOrDefault(region, id);
            }
        }, telecomType);
    }

    /**
     * Получение информации о городе телеком оператора по идентификатору годода в Юлмарт.
     *
     * @param ulmartCityId идентификатор города в Юлмарт
     * @param telecomType  оператор
     * @return информация о городе телеком оператора
     */
    @Override
    public TelecomCity getCityInfo(final Long ulmartCityId, TelecomType telecomType) {
        return retrieve(new Command<TelecomCity>() {
            @Override
            public TelecomCity execute(TelecomService telecomService) {
                return telecomService.getCityInfo(ulmartCityId);
            }
        }, telecomType);
    }

    /**
     * Оператор присутствует в данном регионе.
     *
     * @param ulmartCityId идентификатор города
     * @param telecomTypes список операторов
     * @return true, если присутствует
     */
    @Override
    public Map<TelecomType, Boolean> hasInThisRegion(final Long ulmartCityId, TelecomType... telecomTypes) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.hasInThisRegion(ulmartCityId);
            }
        }, telecomTypes);
    }

    /**
     * Получение идентификатора текущего региона оператора
     *
     * @param ulmartCityId идентификатор города
     * @param telecomType  оператор
     * @return идентификатор текущего региона оператора
     */
    @Override
    public String getCurrentRegionId(final Long ulmartCityId, TelecomType telecomType) {
        return retrieve(new Command<String>() {
            @Override
            public String execute(TelecomService telecomService) {
                return telecomService.getCurrentRegionId(ulmartCityId);
            }
        }, telecomType);
    }

    /**
     * Получение идентификатора текущего региона оператора
     *
     * @param ulmartCityId идентификатор города
     * @param telecomTypes список операторов
     * @return идентификатор текущего региона оператора
     */
    @Override
    public Map<TelecomType, String> getCurrentRegionIdMap(final Long ulmartCityId, TelecomType... telecomTypes) {
        return retrieve(new Command<String>() {
            @Override
            public String execute(TelecomService telecomService) {
                return telecomService.getCurrentRegionId(ulmartCityId);
            }
        }, telecomTypes);
    }

    /**
     * Получение артикула товара
     *
     * @param ulmartCityId идентификатор города
     * @param telecomType  оператор
     * @return артикул товара
     */
    @Override
    public Long getArticleForUlmartCity(final Long ulmartCityId, TelecomType telecomType) {
        return retrieve(new Command<Long>() {
            @Override
            public Long execute(TelecomService telecomService) {
                return telecomService.getArticleForUlmartCity(ulmartCityId);
            }
        }, telecomType);
    }

    /**
     * Выполнение доп. действий с товаром перед подтверждением заказа в sap
     *
     * @param reservationToken токен, полученный при резервировании заказа
     * @param client           описание клиента
     * @param shopId           идентификатор магазина
     * @param cart             список товаров телеком
     * @param telecomType      оператор
     */
    @Override
    public String onBeforeConfirmOrder(final String reservationToken, final Client client, final long shopId,
                                       final Collection<TelecomGood> cart, TelecomType telecomType) {
        return retrieve(new Command<String>() {
            @Override
            public String execute(TelecomService telecomService) {
                return telecomService.onBeforeConfirmOrder(reservationToken, client, shopId, cart);
            }
        }, telecomType);
    }

    /**
     * Выполнение доп. действий с товаром перед добавлением товара в корзину
     *
     * @param cartTelecomInfo информация о товаре
     * @param telecomType     оператор
     * @return признак успешного выполнения действия
     */
    @Override
    public Boolean onBeforeAddToCart(final CartTelecomInfo cartTelecomInfo, TelecomType telecomType) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.onBeforeAddToCart(cartTelecomInfo);
            }
        }, telecomType);
    }

    /**
     * Выполнение доп. действий с товаром после удаления товара из корзины
     *
     * @param cartTelecomInfo информация о товаре
     * @param telecomType     оператор
     * @return признак успешного выполнения действия
     */
    @Override
    public Boolean onAfterRemoveFromCart(final CartTelecomInfo cartTelecomInfo, TelecomType telecomType) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.onAfterRemoveFromCart(cartTelecomInfo);
            }
        }, telecomType);
    }

    /**
     * Выполнение доп. действий с товаром после оформления заказа
     *
     * @param orderId         id заказа
     * @param cartTelecomInfo информация о товаре
     * @param telecomType     оператор
     * @return признак успешного выполнения действия
     */
    @Override
    public Boolean onAfterAddToOrder(final String orderId,
                                     final CartTelecomInfo cartTelecomInfo,
                                     TelecomType telecomType) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.onAfterAddToOrder(orderId, cartTelecomInfo);
            }
        }, telecomType);
    }

    /**
     * Выполнение доп. действий с товаром после удаления товара из заказа
     *
     * @param cartTelecomInfo информация о товаре
     * @param telecomType     оператор
     * @return признак успешного выполнения действия
     */
    @Override
    public Boolean onAfterRemoveFromOrder(final CartTelecomInfo cartTelecomInfo,
                                          TelecomType telecomType) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.onAfterRemoveFromOrder(cartTelecomInfo);
            }
        }, telecomType);
    }

    @Override
    @Cached
    public List<NumberType> getNumberTypes() {
        return telecomDataService.getNumberTypes();
    }

    /**
     * Получить тип тарифа по-умолчанию.
     * @return тип тарифа по-умолчанию.
     */
    @Override
    public TelecomTariffType getDefaultTariffType() {
        return telecomDataService.getDefaultTariffType();
    }

    /**
     * Получить признак наличия у оператора номеров
     * @param telecomType оператор
     * @return признак наличия у оператора номеров
     */
    @Override
    public boolean hasNumbers(TelecomType telecomType) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.hasNumbers();
            }
        }, telecomType);
    }

    /**
     * Выполняет команду на извлечение данных для заданного телеком оператора.
     *
     * @param telecomType тип телеком оператора
     * @param command     команда
     * @param <T>         тип возвращаемого значения
     * @return извлеченное значение для заданного телеком оператора или null, если оператор не поддерживается
     */
    private <T> T retrieve(TelecomType telecomType, Command<T> command) {
        if (!availableServices.contains(telecomType)) {
            return null;
        }
        TelecomService telecomService = servicesMap.get(telecomType);
        if (telecomService == null) {
            return null;
        }
        return command.safeExecute(telecomService);
    }

    /**
     * Выполняет команду на извлечение данных для заданнх телеком оператора.
     *
     * @param telecomType тип телеком оператора
     * @param command     команда
     * @param <T>         тип возвращаемого значения
     * @return извлеченное значение
     */
    private <T> T retrieve(Command<T> command, TelecomType telecomType) {
        if (telecomType != null) {
            if (availableServices.contains(telecomType)) {
                return retrieve(telecomType, command);
            }
        }
        return null;
    }

    /**
     * Выполняет команду на извлечение данных для всех доступных телеком сервисов.
     *
     * @param command команда
     * @param <T>     тип возвращаемого значения
     * @return хэш-таблица, содержащая пары: тип телеком оператора - извлеченное значение
     */
    public <T> Map<TelecomType, T> retrieve(Command<T> command) {
        Map<TelecomType, T> result = new HashMap<>();
        for (TelecomType telecomType : servicesMap.keySet()) {
            if (availableServices.contains(telecomType)) {
                result.put(telecomType, retrieve(telecomType, command));
            }
        }

        return result;
    }

    /**
     * Выполняет команду на извлечение данных для заданнх телеком оператора.
     *
     * @param telecomTypes типы телеком операторов
     * @param command      команда
     * @param <T>          тип возвращаемого значения
     * @return хэш-таблица, содержащая пары: тип телеком оператора - извлеченное значение
     */
    private <T> Map<TelecomType, T> retrieve(Command<T> command, TelecomType... telecomTypes) {
        if (telecomTypes == null || telecomTypes.length == 0) {
            return retrieve(command);
        } else {
            Map<TelecomType, T> result = new HashMap<>();
            for (TelecomType telecomType : telecomTypes) {
                if (availableServices.contains(telecomType)) {
                    result.put(telecomType, retrieve(telecomType, command));
                }
            }

            return result;
        }
    }

    private Map<TelecomType, Boolean> getTariffAsGoodMap(TelecomType... telecomType) {
        return retrieve(new Command<Boolean>() {
            @Override
            public Boolean execute(TelecomService telecomService) {
                return telecomService.hasTariffAsGood();
            }
        }, telecomType);
    }

    private abstract class Command<T> {
        T safeExecute(TelecomService telecomService) {
            try {
                return execute(telecomService);
            } catch (Exception ex) {
                if (skipError)
                    return null;
                else
                    throw ex;
            }
        }

        abstract T execute(TelecomService telecomService);
    }
}
