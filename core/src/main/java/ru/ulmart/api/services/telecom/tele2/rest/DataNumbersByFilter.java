package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Тело ответа из API
 * POST api/{token}/catalog/numbersbyfilter
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataNumbersByFilter {

    @JsonProperty("Msisdn")
    private Long msisdn;
    @JsonProperty("Id")
    private String id;
    @JsonProperty("CategoryId")
    private Long categoryId;
    @JsonProperty("TypeId")
    private Long typeId;
    @JsonProperty("PriceBase")
    private Double priceBase;
    @JsonProperty("PriceSpecial")
    private Double priceSpecial;
    @JsonProperty("Mnemocode")
    private String mnemocode;
    @JsonProperty("HasSpecialPrice")
    private Boolean hasSpecialPrice;
    @JsonProperty("ActualPrice")
    private Double actualPrice;

    public DataNumbersByFilter() {
    }

    public DataNumbersByFilter(Long msisdn, String id, Long categoryId, Long typeId, Double priceBase,
                               Double priceSpecial, String mnemocode, Boolean hasSpecialPrice, Double actualPrice) {
        this.msisdn = msisdn;
        this.id = id;
        this.categoryId = categoryId;
        this.typeId = typeId;
        this.priceBase = priceBase;
        this.priceSpecial = priceSpecial;
        this.mnemocode = mnemocode;
        this.hasSpecialPrice = hasSpecialPrice;
        this.actualPrice = actualPrice;
    }

    public Long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Long msisdn) {
        this.msisdn = msisdn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Double getPriceBase() {
        return priceBase;
    }

    public void setPriceBase(Double priceBase) {
        this.priceBase = priceBase;
    }

    public Double getPriceSpecial() {
        return priceSpecial;
    }

    public void setPriceSpecial(Double priceSpecial) {
        this.priceSpecial = priceSpecial;
    }

    public String getMnemocode() {
        return mnemocode;
    }

    public void setMnemocode(String mnemocode) {
        this.mnemocode = mnemocode;
    }

    public Boolean getHasSpecialPrice() {
        return hasSpecialPrice;
    }

    public void setHasSpecialPrice(Boolean hasSpecialPrice) {
        this.hasSpecialPrice = hasSpecialPrice;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    @Override
    public String toString() {
        return "DataNumbersByFilter{" +
                "msisdn=" + msisdn +
                ", id='" + id + '\'' +
                ", categoryId=" + categoryId +
                ", typeId=" + typeId +
                ", priceBase=" + priceBase +
                ", priceSpecial=" + priceSpecial +
                ", mnemocode='" + mnemocode + '\'' +
                ", hasSpecialPrice=" + hasSpecialPrice +
                ", actualPrice=" + actualPrice +
                '}';
    }
}
