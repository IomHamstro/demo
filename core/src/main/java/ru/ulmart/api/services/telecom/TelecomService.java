package ru.ulmart.api.services.telecom;

import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Sim;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomGood;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Mikhail Potter
 *         22.10.2015
 */
public interface TelecomService {
    /**
     * Получение списка красивых номеров.
     *
     * @param region        регион
     * @param filterRequest параметры для фильтрации
     * @return список номеров, сгруппированных по типу
     */
    Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest);

    /**
     * Получение деталей телефонного номера.
     *
     * @param region регион
     * @param numberId идентифкатор телефонного номера
     * @return детали телефонного номера
     */
    PhoneNumber getNumberInfo(String region, String numberId);

    /**
     * Получение деталей телефонного номера.
     *
     * @param region регион (может быть null)
     * @param numberId иентификатор телефонного номера
     * @return номер телефона соответствующий идентификатору
     */
    PhoneNumber getNumber(String region, String numberId);

    /**
     * Получение списка тарифов для указанного региона.
     *
     * @param region регион
     * @return список тарифов
     */
    List<Tariff> listTariffs(String region);

    /**
     * Получение полной информации по тарифу.
     *
     * @param id id тарифа
     * @return детальная информация по тарифу
     */
    TariffDetails getTariffDetail(Long id, String region);

    /**
     * Получение общей информации по тарифам в регионе
     *
     * @param region регион
     * @return общая информация по тарифам в регионе
     */
    TariffDetails getTariffDetailCommon(String region);

    /**
     * Получение тарифа по умолчанию для указанного региона.
     *
     * @param region регион
     * @return тариф
     */
    Tariff getDefaultTariff(String region);

    /**
     * Получение детальной информации о тарифе.
     *
     * @param region регион (может быть null)
     * @param id идентификатор тарифа
     * @return полная информация о тарифе соответствующем идентификатору, либо тариф по умолчанию
     */
    Tariff getTariffOrDefault(String region, Long id);

    /**
     * Получение информации о городе телеком оператора по идентификатору годода в Юлмарт.
     *
     * @param ulmartCityId идентификатор города в Юлмарт
     * @return информация о городе телеком оператора
     */
    TelecomCity getCityInfo(Long ulmartCityId);

    /**
     * Оператор присутствует в данном регионе.
     *
     * @param ulmartCityId идентификатор города
     * @return true, если присутствует
     */
    boolean hasInThisRegion(Long ulmartCityId);

    String getCurrentRegionId(Long ulmartCityId);

    Long getArticleForUlmartCity(Long region);

    String onBeforeConfirmOrder(String reservationToken, Client client, long shopId, Collection<TelecomGood> cart);

    /**
     * Получение списка доступных сим-карт.
     *
     * @param ulmartCityId  id города
     * @return список сим-карт
     */
    List<Sim> getSims(Long ulmartCityId);

    /**
     * Проверка факта того, что для данного ЦИЗ наличие sim не проверяется
     * @param shopId id ЦИЗ
     * @return признак, что sim всегда доступна для данного ЦИЗ
     */
    boolean isSimAlwaysAvailable(Long shopId);

    /**
     * Выполнение доп. действий с товаром перед добавлением товара в корзину
     *
     * @param cartTelecomInfo    информация о товаре
     * @return признак успешного резервирования номера
     */
    boolean onBeforeAddToCart(CartTelecomInfo cartTelecomInfo);

    /**
     * Выполнение доп. действий с товаром после удаления товара из корзины
     *
     * @param cartTelecomInfo    информация о товаре
     * @return признак успешного выполнения действия
     */
    boolean onAfterRemoveFromCart(CartTelecomInfo cartTelecomInfo);

    /**
     * Выполнение доп. действий с товаром после удаления товара из заказа
     *
     * @param cartTelecomInfo    информация о товаре
     * @return признак успешного выполнения действия
     */
    boolean onAfterRemoveFromOrder(CartTelecomInfo cartTelecomInfo);

    /**
     * Выполнение доп. действий с товаром после добавления товара в заказ
     *
     * @param orderId id заказа
     * @param cartTelecomInfo    информация о товаре
     * @return признак успешного выполнения действия
     */
    boolean onAfterAddToOrder(String orderId, CartTelecomInfo cartTelecomInfo);

    /**
     * Признак наличия тарифов, которые являлются обычными товарами
     * @return true - у оператора есть тарифы, являющиеся обычными товарами
     */
    boolean hasTariffAsGood();

    boolean hasNumbers();
}
