package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Шаблон для ответов из API
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponse {

    @JsonProperty("IsSuccess")
    private Boolean isSuccess;

    @JsonProperty("ErrorReason")
    private String errorReason;

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getErrorReason() {
        return errorReason;
    }

    public void setErrorReason(String errorReason) {
        this.errorReason = errorReason;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "isSuccess=" + isSuccess +
                ", errorReason='" + errorReason + '\'' +
                '}';
    }
}
