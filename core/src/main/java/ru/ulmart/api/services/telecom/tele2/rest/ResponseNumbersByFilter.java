package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Данные получаемяе из API
 * POST api/{token}/catalog/numbersbyfilter
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseNumbersByFilter extends ApiResponse {

    @JsonProperty("Data")
    private List<DataNumbersByFilter> Data;

    public List<DataNumbersByFilter> getData() {
        return Data;
    }

    public void setData(List<DataNumbersByFilter> data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "ResponseNumbersByFilter{" +
                "Data=" + Data +
                ", isSuccess=" + this.getIsSuccess() +
                ", errorReason='" + this.getErrorReason() + '\'' +
                '}';
    }
}
