package ru.ulmart.api.services.telecom.universal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.ulmart.api.services.telecom.AbstractTelecomService;
import ru.ulmart.api.services.telecom.TelecomService;
import ru.ulmart.model.cache.Cached;
import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Sim;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomGood;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.model.telecom.db.TelecomDataService;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Сервис для работы с тарифами.
 * Нельзя инстанцировать. Для каждого оператора должен создаваться класс-наследник.
 */
public abstract class TelecomServiceImpl extends AbstractTelecomService implements TelecomService {
    @Autowired
    @Qualifier("telecomDataServiceSlave")
    private TelecomDataService telecomDataService;
    @Autowired
    private TelecomNumbersService numbersService;
    @Autowired
    private TelecomTariffsService tariffsService;

    public TelecomServiceImpl(TelecomType telecomType) {
        super(telecomType);
    }

    /**
     * Получение списка красивых номеров
     *
     * @param region        регион
     * @param filterRequest параметры для фильтрации
     * @return список номеров
     */
    @Override
    public Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest) {
        return numbersService.listNumbers(region, filterRequest, getTelecomType());
    }

    /**
     * Получение деталей телефонного номера
     *
     * @param region регион
     * @param numberId идентифкатор телефонного номера
     * @return детали телефонного номера
     */
    @Override
    public PhoneNumber getNumberInfo(String region, String numberId) {
        PhoneNumber number = numbersService.getNumber(numberId);
        if(number != null) {
            number.setTelecomType(getTelecomType());
        }
        return number;
    }

    @Override
    public PhoneNumber getNumber(String region, String numberId) {
        if (numberId != null) {
            PhoneNumber phoneNumber = getNumberInfo(region, numberId);
            if (phoneNumber != null) {
                return  phoneNumber;
            }
        }
        return null;
    }


    /**
     * Получение списка тарифов для указанного региона
     *
     * @param region регион
     * @return список тарифов
     */
    @Cached
    @Override
    public List<Tariff> listTariffs(String region) {
       return tariffsService.listTariffs(getTelecomType(), region);
    }

    /**
     * Получение полной информации по тарифу
     *
     * @param id id тарифа
     * @param region регион
     * @return детальная информация по тарифу
     */
    @Cached
    @Override
    public TariffDetails getTariffDetail(Long id, String region) {
        return tariffsService.getTariffDetail(getTelecomType(), id, region);
    }

    /**
     * Получение общей информации по тарифам в регионе
     *
     * @param region регион
     * @return общая информация по тарифам в регионе
     */
    @Cached
    @Override
    public TariffDetails getTariffDetailCommon(String region) {
        return tariffsService.getTariffDetailCommon(getTelecomType(), region);
    }

    /**
     * Получение тарифа по умолчанию для указанного региона
     *
     * @param region регион
     * @return тариф
     */
    @Cached
    @Override
    public Tariff getDefaultTariff(String region) {
        return tariffsService.getDefaultTariff(getTelecomType(), region);
    }

    /**
     * Получение тарифа по умолчанию или существующего тарифа
     *
     * @param region регион
     * @param id идентификатор тарифа (может быть null)
     * @return тариф
     */
    @Cached
    @Override
    public Tariff getTariffOrDefault(String region, Long id) {
        if (id != null) {
            TariffDetails tariffDetails = getTariffDetail(id, region);
            if (tariffDetails != null) {
                return tariffDetails.getTariff();
            }
        }
        return getDefaultTariff(region);
    }

    @Cached
    @Override
    public TelecomCity getCityInfo(Long ulmartCityId) {
        return telecomDataService.getTelecomCity(getTelecomType(), ulmartCityId);
    }

    /**
     * Проверяем есть ли в этом регионе оператор
     *
     * @param ulmartCityId id города Юлмарта
     * @return да/нет
     */
    @Cached
    @Override
    public boolean hasInThisRegion(Long ulmartCityId) {
        return telecomDataService.hasInThisRegion(getTelecomType(), ulmartCityId);
    }

    @Cached
    @Override
    public String getCurrentRegionId(Long ulmartCityId) {
        Long regionId = telecomDataService.getCurrentRegionId(getTelecomType(), ulmartCityId);
        return regionId != null ? regionId.toString() : null;
    }

    @Cached
    @Override
    public Long getArticleForUlmartCity(Long ulmartCityId) {
        return telecomDataService.getArticleForUlmartCity(ulmartCityId, getTelecomType());
    }

    @Override
    public String onBeforeConfirmOrder(String reservationToken, Client client, long shopId,
                                       Collection<TelecomGood> cart) {
        // Действий перед подтверждением заказа не требуется
        return null;
//        Collection<CartTelecomInfo> mtsGoods = filterTelecomCartByType(cartInfo.getTelecomGoods());
//        if (!mtsGoods.isEmpty()) {
//            String token = reserveProducts(mtsGoods);
//            Order order = checkUSIMService.confirmOrder(cartInfo.getShopId().intValue(), cartInfo.getClient(), mtsGoods);
//            for (CartTelecomInfo telecomInfo : mtsGoods) {
//                DtGoods goodsReserve = telecomInfoToDtGoods(telecomInfo, cartInfo.getSapShopId(), order.getId());
//                req.getGoodsReserve().add(goodsReserve);
//            }
//            // TODO: #4775 set mtsGoods info for SAP processing
//            // TODO: add buyNumber
//            /* for (CartTelecomInfo telecomInfo : mtsGoods) {
//                DtXxlSert sapCert = new DtXxlSert();
//                sapCert.setGoodsId(cert.getId().toString());
//                sapCert.setWarehouseId(cartInfo.getSapShopId());
//                sapCert.setCount(cert.getAmount() != null ? cert.getAmount().longValue() : 1L);
//                sapCert.setPriceItem(BigDecimal.valueOf(cert.getNominal()));
//                req.getXxlSert().add(sapCert);
//            } */
//        }
    }

    @Override
    @Cached
    public List<Sim> getSims(Long ulmartCityId) {
        return telecomDataService.getSims(ulmartCityId, getTelecomType());
    }

    @Override
    public boolean isSimAlwaysAvailable(Long shopId) {
        return false;
    }

    @Override
    public boolean onBeforeAddToCart(CartTelecomInfo cartTelecomInfo) {
        return numbersService.reserveNumber(cartTelecomInfo);
    }

    @Override
    public boolean onAfterRemoveFromCart(CartTelecomInfo cartTelecomInfo) {
        return numbersService.freeNumber(cartTelecomInfo);
    }

    @Override
    public boolean onAfterRemoveFromOrder(CartTelecomInfo cartTelecomInfo) {
        return numbersService.freeNumber(cartTelecomInfo);
    }

    @Override
    public boolean onAfterAddToOrder(String orderId, CartTelecomInfo cartTelecomInfo) {
        return numbersService.sellNumber(orderId, cartTelecomInfo);
    }

    @Override
    public boolean hasTariffAsGood() {
        return false;
    }

    @Override
    public boolean hasNumbers() {
        return true;
    }
}
