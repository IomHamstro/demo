package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Данные получаемяе из API
 * POST api/{token}/order/create/{ReservationToken}
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseCreateOrder extends ApiResponse {

    @JsonProperty("Data")
    private DataCreateOrder data;

    public DataCreateOrder getData() {
        return data;
    }

    public void setData(DataCreateOrder data) {
        this.data = data;
    }
}
