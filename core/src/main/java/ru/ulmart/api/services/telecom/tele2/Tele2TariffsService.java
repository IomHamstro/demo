package ru.ulmart.api.services.telecom.tele2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.service.telecom.problem.ServiceInvocationException;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseTariffDetails;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseTariffList;
import ru.ulmart.common.util.UlmartUtils;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TelecomTariffType;
import ru.ulmart.model.telecom.db.Tele2DataService;
import ru.ulmart.model.telecom.db.TelecomDataService;
import ru.ulmart.model.telecom.tele2.Tele2Tariff;
import ru.ulmart.model.telecom.tele2.Tele2TariffDetails;
import ru.ulmart.model.telecom.tele2.Tele2TariffParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

class Tele2TariffsService extends AbstractTele2Service {
    private static final Logger LOGGER = LoggerFactory.getLogger(Tele2TariffsService.class);

    @Value("${tele2.apiUrl}")
    private String apiUrl;
    @Value("${tele2.apiToken}")
    private String apiToken;
    @Autowired
    @Qualifier("globalRestTemplate")
    private RestTemplate restTemplate;
    @Autowired
    private Tele2DataService tele2Service;
    @Autowired
    @Qualifier("telecomDataServiceSlave")
    private TelecomDataService telecomDataService;

    /**
     * Получение списка тарифов
     * GET api/{token}/tariff/{region}/tarifflist
     *
     * @param region регион
     * @return список тарифок
     */
    public List<Tariff> listTariffs(String region) {
        if (region == null) return null;
        ResponseTariffList response;
        final String url = apiUrl + apiToken + "/tariff/" + region + "/tarifflist";
        try {
            response = restTemplate.getForObject(url, ResponseTariffList.class);
        } catch (HttpServerErrorException e) {
            throw new ServiceInvocationException(null, null, UlmartUtils.formatLogMessage("Tele2 server error at url {}", url));
        }
        if (Boolean.TRUE.equals(response.getIsSuccess())) {
            if (response.getData() == null)
                throw new IllegalArgumentException("API /tariff/" + region + "/tarifflist return null in Data!");
            List<Tele2Tariff> tele2Tariffs = response.getData();
            List<Tariff> tariffs = new ArrayList<>(tele2Tariffs.size());
            for (Tele2Tariff tele2Tariff : tele2Tariffs) {
                Tariff tariff = new Tariff(tele2Tariff);
                tariff.setTelecomType(getTelecomType());
                tariffs.add(tariff);
            }
            setTariffImagesAndType(region, tariffs);
            return tariffs;
        } else {
            throw new ServiceInvocationException(null, response, response.getErrorReason());
        }
    }

    /**
     * Получить детальную информацию о тарифе
     * GET api/{token}/tariff/details/{id}
     *
     * @param id id тарифа
     * @return полная информация о тарифе
     */
    public TariffDetails getTariffDetail(Long id, String region) {
        if (region == null) return null;
        ResponseTariffDetails response;
        final String url = apiUrl + apiToken + "/tariff/" + region + "/details/" + id;
        try {
            response = restTemplate.getForObject(url, ResponseTariffDetails.class);
        } catch (HttpServerErrorException e) {
            throw new ServiceInvocationException(null, null, UlmartUtils.formatLogMessage("Tele2 server error at url {}", url));
        }
        System.out.println(response);
        if (Boolean.TRUE.equals(response.getIsSuccess())) {
            Tele2TariffDetails tele2TariffDetails = response.getData();
            if (tele2TariffDetails == null)
                return null;
            sortParam(tele2TariffDetails.getTele2TariffParams());
            Tele2Tariff tele2Tariff = tele2TariffDetails.getTele2TariffInfo();
            tele2Tariff.setTelecomType(getTelecomType());
            TariffDetails tariffDetails = new TariffDetails(tele2TariffDetails);
            setTariffImagesAndType(region, tariffDetails);
            return tariffDetails;
        } else {
            throw new ServiceInvocationException(null, response, response.getErrorReason());
        }
    }

    /**
     * Получаем тариф по умолчанию
     *
     * @param region регион (может быть null, тогда будет отправлен запрос на другой адрес)
     */
    public Tariff getDefaultTariff(String region) {
        List<Tariff> tariffs = listTariffs(region);
        for (Tariff tariff : tariffs) {
            if (Boolean.TRUE.equals(tariff.isRegionDefault()))
                return tariff;
        }
        // если тарифа по умолчанию не оказалось, возвращаем первый попавшийся тариф
        return tariffs.get(0);
    }

    /**
     * Получить детальную информацию о тарифе
     * GET api/{token}/tariff/details/{id}
     *
     * @param id     id тарифа
     * @param region регион
     * @return полная информация о тарифе соответствующем id, либо тариф по умолчанию
     */
    public Tariff getTariffOrDefault(String region, Long id) {
        if (id != null) {
            TariffDetails tariffDetails = getTariffDetail(id, region);
            if (tariffDetails != null) {
                return tariffDetails.getTariff();
            }
        }
        return getDefaultTariff(region);
    }

    public void setTariffImagesAndType(String region, TariffDetails tariffDetail) {
        if (tariffDetail == null || tariffDetail.getTariff() == null)
            return;
        setTariffImagesAndType(region, Collections.singletonList(tariffDetail.getTariff()));
    }

    /**
     * К тарифам полученным из АПИ добавляем url их изображения и TelecomTariffType
     *
     * @param tariffs список тарифов
     */
    public void setTariffImagesAndType(String region, List<Tariff> tariffs) {
        if (tariffs == null || tariffs.isEmpty()) {
            return;
        }
        Map<String, Tele2DataService.TariffExtraInfo> extraInfoMap = tele2Service.getTariffsExtraInfo(region, tariffs);
        TelecomTariffType defaultType = null;
        for (Tariff tariff : tariffs) {
            Tele2DataService.TariffExtraInfo extraInfo = extraInfoMap.get(tariff.getAlias());
            if (extraInfo != null) {
                tariff.setImageUrl(extraInfo.getImageUrl());
                tariff.setTelecomTariffType(extraInfo.getType());
            } else {
                if(defaultType == null) {
                    defaultType = telecomDataService.getDefaultTariffType();
                }
                tariff.setTelecomTariffType(defaultType);
                LOGGER.warn("setTariffImagesAndType: extra info not found for tariff with id = {}", tariff.getAlias());
            }
        }
    }

    private void sortParam(List<Tele2TariffParam> tele2TariffParams) {
        if (tele2TariffParams == null || tele2TariffParams.isEmpty())
            return;

        Collections.sort(tele2TariffParams, new Comparator<Tele2TariffParam>() {
            @Override
            public int compare(Tele2TariffParam o1, Tele2TariffParam o2) {
                return o1.getIndex().compareTo(o2.getIndex());
            }
        });
        for (Tele2TariffParam param : tele2TariffParams) {
            sortParam(param.getChildren());
        }
    }
}
