package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

/**
 * Параметры отправляемые в API
 * POST api/{token}/order/create/{ReservationToken}
 *
 */
public class RequestCreateOrder {

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Информация о заказе
     */
    private OrderInfo orderInfo;

    /**
     * Список запрашиваемых для заказа продуктов
     */
    private List<RequestReserveProduct> products;

    public RequestCreateOrder() {
    }

    public RequestCreateOrder(OrderInfo orderInfo, List<RequestReserveProduct> products) {
        this.orderInfo = orderInfo;
        this.products = products;
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public List<RequestReserveProduct> getProducts() {
        return products;
    }

    public void setProducts(List<RequestReserveProduct> products) {
        this.products = products;
    }

    public class OrderInfo {

        /**
         * Контактные данные покупателя
         */
        private CustomerContact customerContact;

        /**
         * Данные покупателя
         */
        private CustomerIdentity customerIdentity;

        private boolean isDelivery = false;

        /**
         * Идентификатор города в котором оформляется заказ
         */
        private Integer cityId;

        /**
         * Идентификатор точки продаж на которую оформляется заказ
         */
        private Integer salePoint;

        public OrderInfo() {
        }

        public OrderInfo(CustomerContact customerContact, CustomerIdentity customerIdentity, Integer salePoint) {
            this.customerContact = customerContact;
            this.customerIdentity = customerIdentity;
            this.salePoint = salePoint;
        }

        public CustomerContact getCustomerContact() {
            return customerContact;
        }

        public void setCustomerContact(CustomerContact customerContact) {
            this.customerContact = customerContact;
        }

        public CustomerIdentity getCustomerIdentity() {
            return customerIdentity;
        }

        public void setCustomerIdentity(CustomerIdentity customerIdentity) {
            this.customerIdentity = customerIdentity;
        }

        public boolean isDelivery() {
            return isDelivery;
        }

        public void setIsDelivery(boolean isDelivery) {
            this.isDelivery = isDelivery;
        }

        public Integer getCityId() {
            return cityId;
        }

        public void setCityId(Integer cityId) {
            this.cityId = cityId;
        }

        public Integer getSalePoint() {
            return salePoint;
        }

        public void setSalePoint(Integer salePoint) {
            this.salePoint = salePoint;
        }

        public class CustomerContact {

            /**
             * Фамилия.
             */
            private String lastName;

            /**
             * Имя.
             */
            private String firstName;

            /**
             * Отчество.
             */
            private String patronymic;

            /**
             * Контактный телефон
             */
            private String contactPhone;

            /**
             * Адрес электронной почты
             */
            private String email;

            public CustomerContact() {
            }

            public String getFullName() {
                return String.format("%s %s %s", lastName, firstName, patronymic);
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getPatronymic() {
                return patronymic;
            }

            public void setPatronymic(String patronymic) {
                this.patronymic = patronymic;
            }

            public String getContactPhone() {
                return contactPhone;
            }

            public void setContactPhone(String contactPhone) {
                this.contactPhone = contactPhone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }
        }

        public class CustomerIdentity {

            /**
             * Тип документа (код 1 - Паспорт РФ)
             */
            private Integer documentType;

            /**
             * Номер документа
             */
            private String documentNumber;

            /**
             * Кем выдан документ
             */
            private String documentIssuedBy;

            /**
             * Дата выдачи документа
             */
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
            private Date documentIssuedDate;

            /**
             * Пол (0 - мужчина; 1 - женщина)
             */
            private Integer gender;

            /**
             * Дата рождения
             */
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
            private Date birthDate;

            /**
             * Почтовый индекс.
             */
            private String postalCode;

            /**
             * Место выдачи документа.
             */
            private String documentIssuedPlace;

            /**
             * Улица.
             */
            private String street;

            /**
             * Номер дома.
             */
            private String building;

            /**
             * Место рождения
             */
            private String birthPlace;

            public CustomerIdentity() {
            }

            public Integer getDocumentType() {
                return documentType;
            }

            public void setDocumentType(Integer documentType) {
                this.documentType = documentType;
            }

            public String getDocumentNumber() {
                return documentNumber;
            }

            public void setDocumentNumber(String documentNumber) {
                this.documentNumber = documentNumber;
            }

            public String getDocumentIssuedBy() {
                return documentIssuedBy;
            }

            public void setDocumentIssuedBy(String documentIssuedBy) {
                this.documentIssuedBy = documentIssuedBy;
            }

            public Date getDocumentIssuedDate() {
                return documentIssuedDate;
            }

            public void setDocumentIssuedDate(Date documentIssuedDate) {
                this.documentIssuedDate = documentIssuedDate;
            }

            public Integer getGender() {
                return gender;
            }

            public void setGender(Integer gender) {
                this.gender = gender;
            }

            public Date getBirthDate() {
                return birthDate;
            }

            public void setBirthDate(Date birthDate) {
                this.birthDate = birthDate;
            }

            public String getPostalCode() {
                return postalCode;
            }

            public void setPostalCode(String postalCode) {
                this.postalCode = postalCode;
            }

            public String getDocumentIssuedPlace() {
                return documentIssuedPlace;
            }

            public void setDocumentIssuedPlace(String documentIssuedPlace) {
                this.documentIssuedPlace = documentIssuedPlace;
            }

            public String getStreet() {
                return street;
            }

            public void setStreet(String street) {
                this.street = street;
            }

            public String getBuilding() {
                return building;
            }

            public void setBuilding(String building) {
                this.building = building;
            }

            public String getBirthPlace() {
                return birthPlace;
            }

            public void setBirthPlace(String birthPlace) {
                this.birthPlace = birthPlace;
            }
        }
    }

}
