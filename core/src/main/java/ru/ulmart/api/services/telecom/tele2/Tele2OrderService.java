package ru.ulmart.api.services.telecom.tele2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.ulmart.api.services.telecom.TelecomUtil;
import ru.ulmart.service.telecom.problem.CreateOrderException;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.service.telecom.problem.ServiceInvocationException;
import ru.ulmart.api.services.telecom.tele2.rest.RequestCreateOrder;
import ru.ulmart.api.services.telecom.tele2.rest.RequestReserveProduct;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseCreateOrder;
import ru.ulmart.api.services.telecom.tele2.rest.ResponseReserveProduct;
import ru.ulmart.model.telecom.Client;
import ru.ulmart.model.telecom.Order;
import ru.ulmart.model.telecom.TelecomGood;

import java.util.Collection;
import java.util.List;

import static ru.ulmart.api.services.telecom.TelecomUtil.toJson;

class Tele2OrderService extends AbstractTele2Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Tele2OrderService.class);

    @Value("${tele2.apiUrl}")
    private String apiUrl;
    @Value("${tele2.apiToken}")
    private String apiToken;
    @Autowired
    @Qualifier("globalRestTemplate")
    private RestTemplate restTemplate;

    /**
     * Резервирование номера/тарифа
     * POST api/{token}/catalog/reserveproduct
     *
     * @param cart содержимое телеком корзины
     * @param reservationToken токен заказа (может быть null). Необходим в случае если заказ был зарезервирован,
     *                      но так и не был оформлен
     * @return номер резерва
     */
    public String reserveProducts(Collection<TelecomGood> cart, String reservationToken) {
        List<RequestReserveProduct> requestAPI = TelecomUtil.convert(cart);
        String region = cart.iterator().next().getRegion();
        String url = apiUrl + apiToken + "/catalog/" + region + "/reserveproduct" + (reservationToken == null ? "" : "/" + reservationToken);
        LOGGER.info("Tele2 reserveProducts: url = {}, request = {}", url, toJson(requestAPI));
//        ResponseReserveProduct response = restTemplate.postForObject(url, createEntity(requestAPI), ResponseReserveProduct.class);
        ResponseEntity<ResponseReserveProduct> responseEntity
                = restTemplate.exchange(url, HttpMethod.POST, createEntity(requestAPI), ResponseReserveProduct.class);
        ResponseReserveProduct response = responseEntity.getBody();
        LOGGER.info("Tele2 reserveProducts: response = {}, header = {} ", toJson(response), responseEntity.getHeaders().toString());
        if (Boolean.TRUE.equals(response.getIsSuccess()) &&
                response.getData() != null && Boolean.TRUE.equals(response.getData().getIsSuccess())) {
            String receivedReservationToken = response.getData().getReservationToken();
            LOGGER.info("Tele2 reserveProducts: Tele2 order reserved, token = {}", receivedReservationToken);
            return receivedReservationToken;
        } else {
            throw new ServiceInvocationException(requestAPI, response, response.getErrorReason());
        }
    }

    /**
     * Создание заказа
     * POST api/{token}/order/create/{ReservationToken}
     *
     * @param salePointId      идентификатор точки продаж Теле2
     * @param client           информация о покупателе
     * @param cart             информация о содержимом корзины
     * @param reservationToken номер резерва
     * @return заказ
     */
    public Order confirmOrder(Integer salePointId, Client client, Collection<TelecomGood> cart, String reservationToken) {
        RequestCreateOrder requestAPI = TelecomUtil.convert(client, salePointId, cart);
        String region = cart.iterator().next().getRegion();
        String url = apiUrl + apiToken + "/order/" + region + "/create/" + reservationToken;
        LOGGER.info("Tele2 confirmOrder: url = {}, request = {}", url, toJson(requestAPI));
//        ResponseCreateOrder response = restTemplate.postForObject(url, createEntity(requestAPI), ResponseCreateOrder.class);
        ResponseEntity<ResponseCreateOrder> responseEntity
                = restTemplate.exchange(url, HttpMethod.POST, createEntity(requestAPI), ResponseCreateOrder.class);
        ResponseCreateOrder response = responseEntity.getBody();
        LOGGER.info("Tele2 confirmOrder: response = {}, header = {} ", toJson(response), responseEntity.getHeaders().toString());
        if (Boolean.TRUE.equals(response.getIsSuccess()) &&
                response.getData() != null && Boolean.TRUE.equals(response.getData().getIsSuccess())) {
            LOGGER.info("Tele2 confirmOrder: Tele2 order is confirmed");
            return TelecomUtil.convert(response);
        } else {
            throw new CreateOrderException(requestAPI, response, response.getErrorReason(), reservationToken);
        }
    }
}
