package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulmart.api.services.telecom.tele2.rest.inner.RestrictedProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Тело ответа из API
 * POST api/{token}/catalog/reserveproduct
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataReserveProduct {

    @JsonProperty("ReservationToken")
    private String reservationToken;
    @JsonProperty("RestrictedProduct")
    private List<RestrictedProduct> restrictedProduct = new ArrayList<>();
    @JsonProperty("IsSuccess")
    private Boolean isSuccess;

    public DataReserveProduct() {
    }

    public DataReserveProduct(String reservationToken, List<RestrictedProduct> restrictedProduct, Boolean isSuccess) {
        this.reservationToken = reservationToken;
        this.restrictedProduct = restrictedProduct;
        this.isSuccess = isSuccess;
    }

    public String getReservationToken() {
        return reservationToken;
    }

    public void setReservationToken(String reservationToken) {
        this.reservationToken = reservationToken;
    }

    public List<RestrictedProduct> getRestrictedProduct() {
        return restrictedProduct;
    }

    public void setRestrictedProduct(List<RestrictedProduct> restrictedProduct) {
        this.restrictedProduct = restrictedProduct;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

}
