package ru.ulmart.api.services.telecom.megafon;

import ru.ulmart.api.services.telecom.TelecomGoodService;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.model.telecom.TelecomType;

/**
 * Для каждого оператора должен быть свой класс с сервисами.
 * Это необходимо для корректной работы системы кэширования.
 */
@LogIntegrationExceptions
public class MegafonService extends TelecomGoodService {
    public MegafonService() {
        super(TelecomType.MEGAFON);
    }
}
