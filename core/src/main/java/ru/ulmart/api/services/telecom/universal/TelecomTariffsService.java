package ru.ulmart.api.services.telecom.universal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.model.telecom.db.TelecomDataService;

import java.util.Collections;
import java.util.List;

@LogIntegrationExceptions
class TelecomTariffsService {
    @Autowired
    @Qualifier("telecomDataServiceSlave")
    private TelecomDataService telecomDataService;

    /**
     * Получение списка тарифов для указанного региона
     *
     * @param telecomType оператор
     * @param region регион
     * @return список тарифов
     */
    public List<Tariff> listTariffs(TelecomType telecomType, String region) {
        if(region == null) {
            return Collections.emptyList();
        }
        List<Tariff> Tariffs = telecomDataService.getTariffs(telecomType, region);
        return Tariffs;
    }

    /**
     * Получение тарифа по умолчанию для указанного региона
     *
     * @param telecomType оператор
     * @param region регион
     * @return тариф
     */
    public Tariff getDefaultTariff(TelecomType telecomType, String region) {
        return telecomDataService.getDefaultTariff(telecomType, region);
    }

    /**
     * Получение полной информации по тарифу
     *
     * @param telecomType оператор
     * @param id id тарифа
     * @param region регион
     * @return детальная информация по тарифу
     */
    public TariffDetails getTariffDetail(TelecomType telecomType, Long id, String region) {
        return telecomDataService.getTariffDetail(telecomType, id, region);
    }

    /**
     * Получение общей информации по тарифам в регионе
     *
     * @param telecomType оператор
     * @param region регион
     * @return общая информация по тарифам в регионе
     */
    public TariffDetails getTariffDetailCommon(TelecomType telecomType, String region) {
        return telecomDataService.getTariffDetailCommon(telecomType, region);
    }
}
