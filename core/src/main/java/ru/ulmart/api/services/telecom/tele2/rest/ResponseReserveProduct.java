package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Данные получаемяе из API
 * POST api/{token}/catalog/reserveproduct
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseReserveProduct extends ApiResponse {

    @JsonProperty("Data")
    private DataReserveProduct Data;

    public DataReserveProduct getData() {
        return Data;
    }

    public void setData(DataReserveProduct data) {
        this.Data = data;
    }
}
