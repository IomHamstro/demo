package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulmart.model.telecom.tele2.Tele2Tariff;

import java.util.List;

/**
 * Данные получаемяе из API
 * GET api/{token}/tariff/tarifflist
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseTariffList extends ApiResponse {

    @JsonProperty("Data")
    private List<Tele2Tariff> data;

    public List<Tele2Tariff> getData() {
        return data;
    }

    public void setData(List<Tele2Tariff> data) {
        this.data = data;
    }
}
