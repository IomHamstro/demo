package ru.ulmart.api.services.telecom.checku.beans;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class PassportRecognitionData {

    private static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

    @JsonCreator
    public static State forValue(String value) {
        return State.valueOf(value);
    }

    private Long id;

    private State state;

    @JsonProperty("state_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_FORMAT)
    private Date stateDate;

    @JsonProperty("date_create")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_FORMAT)
    private Date dateCreate;

    @JsonProperty("date_complete")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_FORMAT)
    private Date dateComplete;

    private PassportData passport;

    private RecognitionError error;

    /**
     * Error code.
     */
    private Long code;

    /**
     * Error description.
     */
    private String description;

    private String key1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getStateDate() {
        return stateDate;
    }

    public void setStateDate(Date stateDate) {
        this.stateDate = stateDate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateComplete() {
        return dateComplete;
    }

    public void setDateComplete(Date dateComplete) {
        this.dateComplete = dateComplete;
    }

    public PassportData getPassport() {
        return passport;
    }

    public void setPassport(PassportData passport) {
        this.passport = passport;
    }

    public RecognitionError getError() {
        return error;
    }

    public void setError(RecognitionError error) {
        this.error = error;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public enum State {
        /**
         * Заявка обрабатывается в автоматическом режиме.
         */
        PROCESSING,
        /**
         * Заявка ожидает ручной обработки.
         */
        WAIT,
        /**
         * Заявка обрабатывается в ручном режиме.
         */
        PROCESSING_MANUAL,
        /**
         * Обработка заявки завершена.
         */
        COMPLETE,
        /**
         * Обработка заявки завершилась ошибкой.
         */
        ERROR,
        /**
         * Заявка была отклонена в ручном режиме.
         */
        DECLINE
    }

}
