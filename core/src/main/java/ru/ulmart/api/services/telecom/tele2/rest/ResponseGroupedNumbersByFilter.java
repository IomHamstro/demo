package ru.ulmart.api.services.telecom.tele2.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Данные получаемяе из API
 * POST api/{token}/catalog/groupednumbersbyfilter
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseGroupedNumbersByFilter extends ApiResponse {

    @JsonProperty("Data")
    private List<DataGroupedNumbersByFilter> data;

    public List<DataGroupedNumbersByFilter> getData() {
        return data;
    }

    public void setData(List<DataGroupedNumbersByFilter> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseGroupedNumbersByFilter{" +
                "data=" + data +
                "} " + super.toString();
    }
}
