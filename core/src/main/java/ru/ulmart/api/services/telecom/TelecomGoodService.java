package ru.ulmart.api.services.telecom;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ulmart.api.services.good.Good;
import ru.ulmart.api.services.good.GoodUtil;
import ru.ulmart.api.services.telecom.problem.LogIntegrationExceptions;
import ru.ulmart.api.services.telecom.universal.TelecomServiceImpl;
import ru.ulmart.api.services.user.UserUtil;
import ru.ulmart.domain.GoodBriefInfo;
import ru.ulmart.domain.GoodExtendedInfo;
import ru.ulmart.domain.good.Characteristic;
import ru.ulmart.domain.good.GroupInfo;
import ru.ulmart.model.cache.Cached;
import ru.ulmart.model.telecom.CartTelecomInfo;
import ru.ulmart.model.telecom.FilterRequest;
import ru.ulmart.model.telecom.NumberType;
import ru.ulmart.model.telecom.PhoneNumber;
import ru.ulmart.model.telecom.Sim;
import ru.ulmart.model.telecom.Tariff;
import ru.ulmart.model.telecom.TariffDetails;
import ru.ulmart.model.telecom.TariffParam;
import ru.ulmart.model.telecom.TelecomCity;
import ru.ulmart.model.telecom.TelecomType;
import ru.ulmart.model.user.UserType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Сервис для работы с тарифами, которые являются обычными товарами.
 * Нельзя инстанцировать. Для каждого оператора, у которого есть такие товары, должен создаваться класс-наследник.
 */
@LogIntegrationExceptions
public abstract class TelecomGoodService extends TelecomServiceImpl {
    private static final String COMMON_INFO = "Общая информация";

    private static final String ADDITIONAL_INFO = "Дополнительная информация";

    private static final String NUMBER_TYPE = "Тип номера";

    private static final String SETTLEMENT_SYSTEM = "Система расчетов";

    private static final String SUBSCRIPTION_PRICE = "Абонентская плата";

    private static final String MONTH_PRICE = "Стоимость в месяц";

    private static final String MOBILE_INTERNET = "Мобильный интернет";

    private static final String SPEED = "Скорость";

    private static final String TRAFFIC_UNLIMITED = "Трафик без ограничения скорости";
    public static final String IMAGE_CHARACTERISTIC_NAME = "Изображение";

    private Map<String, String> characteristics;

    @Autowired
    private Good goodService;

    public TelecomGoodService(TelecomType telecomType) {
        super(telecomType);
        characteristics = new HashMap<>();
        characteristics.put(NUMBER_TYPE, NUMBER_TYPE);
        characteristics.put(SETTLEMENT_SYSTEM, SETTLEMENT_SYSTEM);
        characteristics.put(MONTH_PRICE, SUBSCRIPTION_PRICE);
        characteristics.put(ADDITIONAL_INFO, ADDITIONAL_INFO);
        characteristics.put(TRAFFIC_UNLIMITED, MOBILE_INTERNET);
        characteristics.put(SPEED, SPEED);
    }

    /**
     * Получение списка красивых номеров.
     *
     * @param region        регион
     * @param filterRequest параметры для фильтрации
     * @return список номеров
     */
    @Override
    public Map<NumberType, List<PhoneNumber>> listNumbers(String region, FilterRequest filterRequest) {
        // В настоящее время услуги по продаже номеров телефонов нет
        return Collections.emptyMap();
    }

    /**
     * Получение деталей телефонного номера.
     *
     * @param region   регион
     * @param numberId идентифкатор телефонного номера
     * @return детали телефонного номера
     */
    @Override
    public PhoneNumber getNumberInfo(String region, String numberId) {
        // В настоящее время услуги по продаже номеров телефонов нет
        return null;
    }

    @Override
    public PhoneNumber getNumber(String region, String numberId) {
        // В настоящее время услуги по продаже номеров телефонов нет
        return null;
    }

    /**
     * Получение списка тарифов для указанного региона
     *
     * @param region регион
     * @return список тарифов
     */
    @Cached
    @Override
    public List<Tariff> listTariffs(final String region) {
        List<Tariff> tariffs = super.listTariffs(region);
        // Доп. логика для тарифов, которые идут как обычные товары
        List<Long> commonTariffIds = new ArrayList<>();
        Map<Long, Tariff> tariffMap = new HashMap<>();
        for(Iterator<Tariff> iter = tariffs.iterator(); iter.hasNext();) {
            Tariff tariff = iter.next();
            if(tariff.isCommonGood()) {
                commonTariffIds.add(tariff.getGoodId());
                tariffMap.put(tariff.getGoodId(), tariff);
                // Будет заменен на полученный из обычного товара далее
                iter.remove();
            }
        }
        if(!commonTariffIds.isEmpty()) {
            final long cityId = City.getCityIdFromSession();
            final long spaId = City.getSpaIdFromSession();
            final UserType userType = UserUtil.getUserType();
            // Тарифы Билайн и Мегафон идут как обычные товары, запрашиваем стандартный сервис получения информации о товарах
            List<GoodBriefInfo> goodBriefInfos = goodService.getGoods(commonTariffIds, cityId, spaId, userType);

            if (goodBriefInfos != null) {
                // Для телекома используется своя структура данных, отличная от стандартной структуры данных
                // с информацией о товарах
                List<Tariff> commonTariffs = convertGoodsToTariffs(goodBriefInfos, tariffMap);
                tariffs.addAll(commonTariffs);
            }
        }
        return tariffs;
    }

    /**
     * Преобразование стандартной структуры данных о товаре в структуру данных телеком
     *
     * @param goodBriefInfos список, содержащий стандартные данные о товаре
     * @param tariffMap карта тарифов
     * @return список, содержащий телеком-данные о товаре
     */
    private List<Tariff> convertGoodsToTariffs(final List<GoodBriefInfo> goodBriefInfos, Map<Long, Tariff> tariffMap) {
        List<Tariff> tariffs = new ArrayList<>(goodBriefInfos.size());
        for (GoodBriefInfo info : goodBriefInfos) {
            Tariff tariff = new Tariff();
            tariff.setName(info.getName());
            tariff.setGoodId(info.getId());
            tariff.setDescription(info.getLongName());
            tariff.setImageUrl(goodService.getGoodCharactericticValueByName(info.getId(), IMAGE_CHARACTERISTIC_NAME));
            tariff.setPrice(info.getPrice() != null ? info.getPrice() : 0);
            tariff.setTelecomType(getTelecomType());

            Tariff baseTariff = tariffMap.get(tariff.getGoodId());
            if(baseTariff != null) {
                tariff.setTelecomTariffType(baseTariff.getTelecomTariffType());
                tariff.setId(baseTariff.getId());
            }

            tariffs.add(tariff);
        }
        return tariffs;
    }

    /**
     * Получение списка идентификаторов тарифов Билайн или Мегафон
     *
     * @return список идентификаторов тарифов Билайн или Мегафон
     */
    @Cached
    public List<Long> getTariffIds() {
        final long cityId = City.getCityIdFromSession();
        List<Tariff> tariffs = listTariffs(String.valueOf(cityId));
        List<Long> tariffIds = Lists.newArrayList(Iterables.transform(tariffs, new Function<Tariff, Long>() {
            @Override
            public Long apply(Tariff input) {
                return input.getId();
            }
        }));
        return tariffIds;
    }

    /**
     * Получение детальной информации по тарифу
     *
     * @param id id тарифа
     * @return детальная информация по тарифу
     */
    @Cached
    @Override
    public TariffDetails getTariffDetail(Long id, String region) {
        TariffDetails tariffDetails = super.getTariffDetail(id, region);
        if(tariffDetails != null) {
            Tariff tariff = tariffDetails.getTariff();
            if (tariff.isCommonGood()) {
                final long cityId = City.getCityIdFromSession();
                final long spaId = City.getSpaIdFromSession();
                final UserType userType = UserUtil.getUserType();
                GoodExtendedInfo info = goodService.getGoodExtendedInfo(cityId, spaId, tariff.getGoodId(), userType, false, false);
                // Для телекома используется своя структура детальных данных, отличная от стандартной структуры данных
                // с детальной информацией о товарах
                return convertInfoToDetails(tariffDetails.getTariff(), info);
            }
        }
        return tariffDetails;
    }

    @Override
    public TariffDetails getTariffDetailCommon(String region) {
        // Для Билайн и Мегафон нет общей информации по тарифам
        return null;
    }

    /**
     * Преобразование стандартной расширенной структуры данных о товаре в расширенную структуру данных телеком
     *
     *
     * @param tariff тариф
     * @param info   расширенная структура данных о товаре
     * @return
     */
    private TariffDetails convertInfoToDetails(Tariff tariff, GoodExtendedInfo info) {
        TariffDetails details = new TariffDetails();
        if (info != null) {
            tariff.setName(info.getName());
            tariff.setGoodId(tariff.getGoodId());
            tariff.setPrice(info.getPrice());
            tariff.setDescription(info.getLongName());
            tariff.setTelecomType(getTelecomType());
            List<String> photoUrls = GoodUtil.getPhotoUrls(info.getPhotoId(), "medium", 1);
            if (photoUrls != null && !photoUrls.isEmpty()) {
                tariff.setImageUrl(photoUrls.get(0));
            }
            details.setTariff(tariff);

            List<GroupInfo> groupInfos = goodService.getGoodCharacteristics(tariff.getGoodId());
            List<TariffParam> params = new ArrayList<>(groupInfos.size());
            // Получение из списка характеристик товара нужных характеристик. Остальные характеристики игнорируются
            for (GroupInfo groupInfo : groupInfos) {
                if (groupInfo.getCharacteristics() != null) {
                    for (Characteristic characteristic : groupInfo.getCharacteristics()) {
                        if (characteristics.containsKey(characteristic.getName())) {
                            String title = characteristics.get(characteristic.getName());
                            TariffParam param = new TariffParam();

                            if (COMMON_INFO.equals(groupInfo.getGroupName()) && !ADDITIONAL_INFO.equals(characteristic.getName())) {
                                param.setTitle(title);
                                param.setIndex(characteristic.getSort());
                                param.setValue(getCharacteristicValue(characteristic.getValue()));
                            } else {
                                param.setTitle(title);
                                param.setIndex(characteristic.getSort());

                                param.setCollapsible(false);
                                param.setExpandedByDefault(true);
                                param.setChildren(new ArrayList<TariffParam>());

                                TariffParam child = new TariffParam();
                                child.setTitle(getCharacteristicValue(characteristic.getValue()));
                                child.setIndex(characteristic.getSort());

                                param.getChildren().add(child);
                            }

                            params.add(param);
                        }
                    }
                }
            }
            details.setTariffParams(params);
        }
        return details;
    }

    private String getCharacteristicValue(Object value) {
        if (value instanceof String) {
            return (String) value;
        }
        if (value instanceof List && ((List) value).size() > 0) {
            return ((List) value).get(0).toString();
        }
        return value.toString();
    }

    /**
     * Получение тарифа по умолчанию для указанного региона
     *
     * @param region регион
     * @return тариф
     */
    @Cached
    @Override
    public Tariff getDefaultTariff(String region) {
        List<Tariff> tariffs = listTariffs(region);
        if (tariffs != null && !tariffs.isEmpty()) {
            return tariffs.get(0);
        }
        return null;
    }

    /**
     * Получение информации о городе телеком оператора по идентификатору годода в Юлмарт.
     *
     * @param ulmartCityId идентификатор города в Юлмарт
     * @return информация о городе телеком оператора
     */
    @Cached
    @Override
    public TelecomCity getCityInfo(Long ulmartCityId) {
        // Для тарифов Билайн и Мегафон не требуется, т.к. используется логика заказа обычного товара, а не товара телеком
        TelecomCity city = new TelecomCity();
        city.setUlmartCityId(ulmartCityId);
        city.setTelecomRegionName(getCurrentRegionId(ulmartCityId));
        return city;
    }

    @Override
    public Long getArticleForUlmartCity(Long region) {
        // Для тарифов Билайн и Мегафон не требуется, т.к. используется логика заказа обычного товара, а не товара телеком
        return 0L;
    }

    @Override
    public List<Sim> getSims(Long ulmartCityId) {
        // Для тарифов Билайн и Мегафон сейчас нет списка сим-карт
        return Collections.emptyList();
    }

    @Override
    public boolean onBeforeAddToCart(CartTelecomInfo cartTelecomInfo) {
        // Для тарифов Билайн и Мегафон нет действий.
        return true;
    }

    @Override
    public boolean onAfterRemoveFromCart(CartTelecomInfo cartTelecomInfo) {
        // Для тарифов Билайн и Мегафон нет действий.
        return true;
    }

    @Override
    public boolean onAfterRemoveFromOrder(CartTelecomInfo cartTelecomInfo) {
        // Для тарифов Билайн и Мегафон нет действий.
        return true;
    }

    @Override
    public boolean onAfterAddToOrder(String orderId, CartTelecomInfo cartTelecomInfo) {
        // Для тарифов Билайн и Мегафон нет действий.
        return true;
    }

    @Override
    public boolean hasTariffAsGood() {
        return true;
    }

    @Override
    public boolean hasNumbers() {
        return false;
    }
}
