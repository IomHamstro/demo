package ru.ulmart.api.services.telecom.tele2.rest.inner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestrictedProduct {

    @JsonProperty("ItemType")
    private String itemType;
    @JsonProperty("IdGuid")
    private String idGuid;
    @JsonProperty("IdInt")
    private Integer idInt;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("HasRelatedProduct")
    private Boolean hasRelatedProduct;
    @JsonProperty("IsGuidId")
    private Boolean isGuidId;

    public RestrictedProduct() {
    }

    public RestrictedProduct(String itemType, String idGuid, Integer idInt, String name,
                             Boolean hasRelatedProduct, Boolean isGuidId) {
        this.itemType = itemType;
        this.idGuid = idGuid;
        this.idInt = idInt;
        this.name = name;
        this.hasRelatedProduct = hasRelatedProduct;
        this.isGuidId = isGuidId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getIdGuid() {
        return idGuid;
    }

    public void setIdGuid(String idGuid) {
        this.idGuid = idGuid;
    }

    public Integer getIdInt() {
        return idInt;
    }

    public void setIdInt(Integer idInt) {
        this.idInt = idInt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasRelatedProduct() {
        return hasRelatedProduct;
    }

    public void setHasRelatedProduct(Boolean hasRelatedProduct) {
        this.hasRelatedProduct = hasRelatedProduct;
    }

    public Boolean getIsGuidId() {
        return isGuidId;
    }

    public void setIsGuidId(Boolean isGuidId) {
        this.isGuidId = isGuidId;
    }
}