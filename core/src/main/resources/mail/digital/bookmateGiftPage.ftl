<#if !url??><#assign url = "www.ulmart.ru" /></#if>
<#if type=='DI_BOOKMATE'>
    <#assign isBooks = true/>
<#else>
    <#assign isBooks = false/>
</#if>
<#if isBooks>
    <#if subscriptionId == 1>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 1 месяц на всю электронную библиотеку Букмейт. Активируйте подписку и наслаждайтесь чтением на телефоне, планшете или компьютере. Даже без интернета."/>
    <#elseif subscriptionId == 2>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 3 месяца на всю электронную библиотеку Букмейт. Активируйте подписку и наслаждайтесь чтением на телефоне, планшете или компьютере. Даже без интернета."/>
    <#elseif subscriptionId == 3>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 6 месяцев на всю электронную библиотеку Букмейт. Активируйте подписку и наслаждайтесь чтением на телефоне, планшете или компьютере. Даже без интернета."/>
    <#elseif subscriptionId == 4>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 12 месяцев на всю электронную библиотеку Букмейт. Активируйте подписку и наслаждайтесь чтением на телефоне, планшете или компьютере. Даже без интернета."/>
    </#if>
<#else>
    <#if subscriptionId == 1>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 1 месяц на музыкальный сервис Zvooq. Активируйте подписку и наслаждайтесь любимой музыкой на телефоне, планшете или компьютере. Даже без интернета."/>
    <#elseif subscriptionId == 2>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 3 месяца на музыкальный сервис Zvooq. Активируйте подписку и наслаждайтесь любимой музыкой на телефоне, планшете или компьютере. Даже без интернета."/>
    <#elseif subscriptionId == 3>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 6 месяцев на музыкальный сервис Zvooq. Активируйте подписку и наслаждайтесь любимой музыкой на телефоне, планшете или компьютере. Даже без интернета."/>
    <#elseif subscriptionId == 4>
        <#assign mailMesage ="Ваш друг, ${friendName}, воспользовался предложением для друзей и подарил вам подписку
        на 12 месяцев на музыкальный сервис Zvooq. Активируйте подписку и наслаждайтесь любимой музыкой на телефоне, планшете или компьютере. Даже без интернета."/>
    </#if>
</#if>
<html>
<head>
    <title>#</title>
    <meta charset="utf-8">
</head>
<body>
<table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: #fff; font-family: Arial;">
    <tr>
        <td style="background: #333;">
            <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 30%; padding: 7px 20px;">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td style="padding: 0 4px 0 0;">
                                    <img src="${url}/resources/img/mail/new/user_icon.png" width="15" height="15" alt="">
                                </td>
                                <td style="color: #fff; font-size: 11px; line-height: 15px; font-family: Arial;">
                                    Здраствуйте, ${username}
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="width: 70%; padding: 7px 20px;">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td style="color: #fff; font-size: 11px; line-height: 15px; font-family: Arial;">
                                </td>
                                <td style="padding: 0 0 0 4px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 50%; padding: 14px 20px;">
                        <img src="${url}/resources/img/mail/new/ulmart-logo.png" width="199" height="37" alt="Ulmart Digital" style="vertical-align: middle;">
                    </td>
                    <td align="right" style="width: 50%; padding: 14px 20px;">
                        <p style="padding: 0; margin: 0; font-size: 13px; color: #000; line-height: normal; font-family: Arial;">
                            Кругосуточная служба поддержки<br>
                            <span style="font-size: 18px;">(${city.countryPhonePrefix} ${city.phoneCode}) <span style="font-weight: bold;">${city.phone}</span></span>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <table cellpadding="0" cellspacing="0" style="width: 760px; border-collapse: collapse;">
        <tr>
            <td style="padding-left:20px;">
                <h1 style="margin: 0; padding: 0; margin-bottom: 30px;">
            <span style="font-size: 34px; line-height: normal; color: #000; font-family: Arial; font-weight: normal;white-space: nowrap;">
              Поздравляем, Вы получаете подарок!
            </span>
                </h1>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px 20px 20px;">
                <!-- двухколоночная таблица для контента -->
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="margin:0; padding:0;vertical-align: top;">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="font-family: Arial;color: #000000;font-size: 13px;line-height:18px;padding-bottom:13px;">
                                        ${mailMesage!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p style="font-family: Arial;color: #000000;font-size: 13px;line-height: normal;padding-bottom:13px;">Сообщение от вашего друга:</p>
                                    </td>
                                </tr>
                                <tr>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" style="padding-right: 10px;">
                                                <img src="${url}/resources/img/mail/new/commas.jpg" height="25" width="30" alt="">
                                            </td>
                                            <td style="font-family: Arial;color: #010101;font-size: 13px;width:448;height:113px;vertical-align:top;background: #eeeeee;border-radius: 15px;font-style: italic;line-height: 20px;padding: 5px 10px;">
                                                ${message!}
                                            </td>
                                        </tr>
                                    </table>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                                <tr>
                                    <td style="padding: 20px 10px 10px 40px;">
                                        <a href="${suborderUrl}" target="_blank" style="display: inline-block; border-radius: 4px; font-family: Arial; font-size: 13px; line-height: normal; background: #e62939; border: 10px solid #e62939; border-width: 10px 20px;color: #fff; text-decoration: none;">Активировать подарочную подписку</a>
                                    </td>
                                </tr>
                                <#if !isUlmartUser>
                                    <tr>
                                        <td style="padding: 0px 10px 10px 40px;">
                                            <a href="${url}/registration" target="_blank" style="display: inline-block; border-radius: 4px; font-family: Arial; font-size: 13px; line-height: normal; background: #e62939; border: 10px solid #e62939; border-width: 10px 20px;color: #fff; text-decoration: none;">Регистрация</a>
                                        </td>
                                    </tr>
                                </#if>
                            </table>
                        </td>
                        <td>
                            <#if isBooks>
                                <img src="${url}/resources/img/mail/new/bookmate-gift.png" height="223" width="327" alt="">
                            <#else>
                                <img src="${url}/resources/img/mail/new/zvooq-gift.png" height="223" width="327" alt="">
                            </#if>
                        </td>
                    </tr>
                </table>
                <#--<table cellpadding="0" cellspacing="0">-->
                    <#--<tr>-->
                        <#--<#if isBooks>-->
                            <#--<td style="font-family: Arial;color: #999999;font-size: 11px;line-height: normal;">-->
                                <#--С подпиской на Bookmate вы можете читать любые книги из самой большой библиотеки на русском языке на телефоне, планшете или компьютере.-->
                                <#--Для чтения на телефоне и планшете вам необязательно иметь доступ в интернет.</td>-->
                        <#--<#else>-->
                            <#--<td style="font-family: Arial;color: #999999;font-size: 11px;line-height: normal;">-->
                                <#--С подпиской на Zvooq вы можете прослушивать миллионы любимых треков на телефоне, планшете или компьютере.-->
                                <#--Для прослушивания на телефоне и планшете вам необязательно иметь доступ в интернет.-->
                            <#--</td>-->
                        <#--</#if>-->

                    <#--</tr>-->
                <#--</table>-->
                <!-- двухколоночная таблица для контента закончилась -->
            </td>
        </tr>
    </table>

    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                <tr>
                    <td colspan="2" style="height: 10px; background-color: #fafafa; background-image: url(${url}/resources/img/mail/new/footer_bg.png);"></td>
                </tr>
                <tr>
                    <td style="width: 57%; background: #fafafa; padding: 5px 10px 10px 20px;" valign="top">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td style="padding: 0 15px 0 0;">
                                    <a href="https://market.yandex.ru/shop/${city.yandexRatingId}/reviews?from=${city.yandexRatingId}" target="_blank" title="Яндекс.Маркет"><img src="${url}/resources/img/mail/new/yandex_market.png" width="310" height="29" alt="Яндекс.Маркет" style="vertical-align: middle;"></a>
                                </td>
                                <td>
                                    <a href="http://www.akit.ru/1234567/" target="_blank" title="АКИТ">
                                        <img src="${url}/resources/img/mail/new/akit.png" width="82" height="33" alt="Ассоциация компаний интернет-торговли" style="vertical-align: middle;">
                                    </a>
                                </td>
                            </tr>
                        </table>

                        <p style="font-family: Arial; font-size: 10px; line-height: normal; color: #666; margin: 10px 0 0; padding: 0;">
                            Вы получили данное предложение, так как ваш адрес указали в специальной форме отправки подарочной подписки на сервис ${isBooks?string("Bookmate", "Zvooq")} ​на сайте ulmart.ru.
                            Компания Юлмарт не несет никакой ответственности за содержание сообщения, размещенного в поле “Cообщение от вашего друга”.
                        </p>
                        <p style="font-family: Arial; font-size: 10px; line-height: normal; color: #666; margin: 0; padding: 0;">
                            Мы приносим извинения, если данное сообщение попало к вам ошибочно. Подробности на сайте <a href="https://www.ulmart.ru">https://www.ulmart.ru</a>.
                        </p>
                    </td>
                    <td align="right" style="padding: 5px 20px 10px 10px; width: 43%; background: #fafafa;" valign="top">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td style="padding: 0 2px;">
                                    <a href="https://vk.com/ulmart" target="_blank" title="ВКонтакте"><img src="${url}/resources/img/mail/new/vk.png" width="27" height="27" alt="ВКонтакте"></a>
                                </td>
                                <td style="padding: 0 2px;">
                                    <a href="https://www.facebook.com/I.love.Ulmart" target="_blank" title="Facebook"><img src="${url}/resources/img/mail/new/fb.png" width="27" height="27" alt="Facebook"></a>
                                </td>
                                <td style="padding: 0 2px;">
                                    <a href="https://www.odnoklassniki.ru/iloveulmart" target="_blank" title="Odnoklassniki"><img src="${url}/resources/img/mail/new/ok.png" width="27" height="27" alt="Odnoklassniki"></a>
                                </td>
                                <td style="padding: 0 2px;">
                                    <a href="https://www.twitter.com/Ulmart_ru" target="_blank" title="Twitter"><img src="${url}/resources/img/mail/new/tw.png" width="27" height="27" alt="Twitter"></a>
                                </td>
                                <td style="padding: 0 2px;">
                                    <a href="https://www.instagram.com/iloveulmart" target="_blank" title="Instagram"><img src="${url}/resources/img/mail/new/inst.png" width="27" height="27" alt="Instagram"></a>
                                </td>
                                <td style="padding: 0 2px;">
                                    <a href="https://www.youtube.com/user/ulmartofficial" target="_blank" title="YouTube"><img src="${url}/resources/img/mail/new/youtube.png" width="27" height="27" alt="YouTube"></a>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td style="padding: 20px 2px 0;">
                                    <a href="https://itunes.apple.com/ru/app/ulmart/id867679958" target="_blank" title="App Store"><img src="${url}/resources/img/mail/new/app_store.png" width="76" height="27" alt="App Store"></a>
                                </td>
                                <td style="padding: 20px 2px 0;">
                                    <a href="https://play.google.com/store/apps/details?id=ru.ulmart.mobapp&referrer=utm_source%3Dulmart%26utm_medium%3Dicon" target="_blank" title="Google play"><img src="${url}/resources/img/mail/new/google_play.png" width="75" height="27" alt="Google play"></a>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="background: #333; height: 30px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>