<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>#</title>
    <meta charset="utf-8">
</head>
<body style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 1; font-family: Arial; margin: 0; padding: 0; border: 0;">
<style type="text/css">
    blockquote:before { content: none !important; }
    blockquote:after { content: none !important; }
    q:before { content: none !important; }
    q:after { content: none !important; }
</style>
<table width="100%" style="width: 1030px; font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0; border: 0;"><tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
    <td style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0 20px; border: 0;" valign="baseline">
        <table width="100%" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0 0 14px; border: 0;"><tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
            <td style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;" valign="baseline"><img src="${url}/resources/img/mail/digital/logo.png" height="35" width="189" style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;"></td>
            <td style="vertical-align: top; text-align: right; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0; padding: 0; border: 0;" align="right" valign="top">+7 812 <span style="vertical-align: top; color: #000000; top: -1px; position: relative; font-style: normal; font-variant: normal; font-weight: normal; font-size: 18px; line-height: normal; font-family: Arial; margin: 0; padding: 0; border: 0;">336-3777, 389-3677</span>
            </td>
        </tr></table>
        <hr style="border-style: none none solid; border-bottom-width: 1px; border-bottom-color: #E5E5E5; margin-bottom: 16px;">
        <img src="${url}/resources/img/mail/digital/bookmate_logo.png" height="15" width="125" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0 0 20px; padding: 0; border: 0;"><p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">${userName}, добро пожаловать в Юлмарт!</p>
        <p style="vertical-align: baseline; color: #000000; width: 900px; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">Поздравляем! Теперь вы можете читать любые книги на Букмейте. Активируйте бесплатную подписку на первый месяц и наслаждайтесь чтением.</p>
        <a style="vertical-align: baseline; color: #FFFFFF; border-radius: 3px; width: 170px; height: 30px; display: block; text-align: center; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 30px; font-family: Arial; background-image: linear-gradient(-180deg, #E85538 8%, #E4003A 95%); background-color: #e84b39; margin: 0 0 10px; padding: 0; border: 0;" href="${activationUrl}">Активировать подписку</a>
        <hr style="border-bottom-width: 1px; border-bottom-color: #E5E5E5; margin-bottom: 16px; margin-top: 20px; border-style: none none solid;">
        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">Букмейт — самый удобный способ читать и находить интересные книги.</p>
        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">Если у вас есть вопросы, обратитесь в контактный центр Юлмарта по телефону 8-800-775-10-10 или напишите на <a href="mailto:bookmate@ulmart.ru" style="vertical-align: baseline; color: #137FB7; text-decoration: none; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">bookmate@ulmart.ru</a></p>
    </td>
</tr></table>
</body>
</html>
