<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <!--
    if gte mso 9
    xml
    o:officedocumentsettings
    o:allowpng
    o:pixelsperinch 96
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>${subject}</title>
    <style>
      .mso-hide {
      mso-hide:all;
      }
      @media only screen and (max-width: 640px) {
      .normal_hide {
      display: block !important;
      width: auto !important;
      overflow: visible !important;
      float: none !important;
      max-height: none !important;
      }
      table.normal_hide {
      display: table !important;
      }
      tr.normal_hide {
      display: table-row !important;
      }
      .q_hide {
      display: none !important;
      width: 0 !important;
      overflow: hidden !important;
      float: left !important;
      max-height: 0 !important;
      }
      /* */
      .socbtn {
      width: 300px !important;
      }
      .socbtn__first_part {
      display: block !important;
      padding-bottom: 3px !important;
      width: 100% !important;
      }
      .socbtn__second_part {
      display: block !important;
      width: 100% !important;
      }
      .socbtn__in__first {
      /*padding-right: 2px !important;*/
      }
      .socbtn__in__first img {
      margin: 0 auto !important;
      }
      .socbtn__in__secont img {
      margin: 0 auto !important;
      }
      /* */
      /* */
      .short_top__lspace {
      display: none !important;
      }
      .short_top__logo {
      display: block !important;
      width: 100% !important;
      text-align: center !important;
      font-size: 0 !important;
      }
      .short_top__logo a {
      display: inline-block !important;
      font-size: 0 !important;
      }
      .short_top__logo a img {
      display: inline-block !important;
      }
      .short_top__rlink {
      display: block !important;
      width: 100% !important;
      text-align: center !important;
      padding-top: 5px !important;
      }
      /* */
      .pcode_top_text_1 {
      width: 300px !important;
      }
      .pcode_top_text_1 td {
      line-height: 20px !important;
      }
      /* */
      .pcode_top_text_2 {
      }
      .pcode_top_text_2 nobr {
      white-space: normal !important;
      }
      .pcode_top_text_2 .text_part_1 {
      white-space: nowrap !important;
      display: block !important;
      }
      .pcode_top_text_2 .text_part_2 {
      display: block !important;
      }
      /* */
      .pcode_mid_pcline {
      margin: 0 auto !important;
      }
      /* */
      .pcode_mid_pctext td {
      text-align: center !important;
      }
      /* */
      .go_purchase_btn {
      width: 100% !important;
      }
      .go_purchase_btn td.go_purchase_btn__cell_btn {
      font-size: 0 !important;
      text-align: center !important;
      display: block !important;
      width: 100% !important;
      }
      .go_purchase_btn td.go_purchase_btn__cell_btn a {
      display: inline-block !important;
      font-size: 0 !important;
      }
      .go_purchase_btn td.go_purchase_btn__cell_btn a img {
      display: inline-block !important;
      }
      .go_purchase_btn td.go_purchase_btn__cell_space {
      display: none !important;
      }
      .go_purchase_btn td.go_purchase_btn__cell_link {
      display: block !important;
      text-align: center;
      padding-top: 13px;
      }
      /* */
      .pcode_mid_pcsmalltext td {
      text-align: center !important;
      }
      /* */
      .cont_lr_pad_30__left img {
      width: 10px !important;
      }
      .cont_lr_pad_30__right img {
      width: 10px !important;
      }
      .mainwidth {
      width: 320px !important;
      }
      .for_mobile_ver_hide {
      display: none !important;
      }
      .for_mobile_ver_show {
      display:block !important;
      width: auto !important;
      overflow: visible !important;
      float: none !important;
      max-height: none !important;
      }
      .ten-percent {
      height: 143px !important;
      }
      .ten-percent img {
      width: 132px !important;
      height: 122px !important;
      }
      .lock img {
      width: 151px !important;
      height: 136px !important;
      }
      .socbtn {
      width: 300px !important;
      }
      .socbtn__first_part {
      display: block !important;
      padding-bottom: 3px !important;
      width: 100% !important;
      }
      .socbtn__second_part {
      display: block !important;
      width: 100% !important;
      }
      .socbtn__in__first {
      }
      .socbtn__in__first img {
      margin: 0 auto !important;
      }
      .socbtn__in__secont img {
      margin: 0 auto !important;
      }
      .cont_lr_pad_25__left img,
      .cont_lr_pad_25__right img {
      width: 20px !important;
      }
      .cleanContainer_horizontal4 img{
      	height: 27px !important;
      }
      .cleanContainer_horizontal5 img{
      	height: 28px !important;
      }
      .cleanContainer_horizontal6 img{
      	height: 25px !important;
      }
      .cleanContainer_horizontal9 img{
      	height: 16px !important;
      }
      .cleanContainer_horizontal10 img{
      	height: 20px !important;
      }
      .cleanContainer_horizontal11 img{
      	height: 12px !important;
      }
      .cleanContainer_horizontal12 img{
      	height: 20px !important;
      }
      .cleanContainer_horizontal13 img{
      	height: 15px !important;
      }
      .cleanContainer_horizontal14 img{
      	height: 20px !important;
      }
      .cleanContainer_horizontal15 img{
      	height: 39px !important;
      }
      .cleanContainer_horizontal16 img{
      	height: 15px !important;
      }
      .description{
      width: 300px!important;
      }
      .btmshadow td{
      height:38px !important;
      }
      .paybtn__mid {
      background-color: #E50329;
      }
      .pcode_mid_pcline__left {
      line-height: 19px !important;
      }
      }
    </style>
  </head>
  <body width="100%" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#ebebeb;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;" class="mainbackground">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;line-height:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;mso-table-lspace:0;mso-table-rspace:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;">
      <tr>
        <td valign="top" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;">
          <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;" class="mainwidth">
            <tr>
              <td valign="top" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;">
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;">
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal4"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="39" style="-ms-interpolation-mode:bicubic;width:1px;height:39px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="0" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:0;" class="short_top">
                  <tr>
                    <td valign="middle" align="center" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;text-align:center;vertical-align:middle;" class="short_top__logo"><a href="http://www.ulmart.ru" target="_blank" style="color:#000001;font-family:Arial;text-decoration:none;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/top_ul_logo.png" width="187" height="21" style="-ms-interpolation-mode:bicubic;border-style:none;display:block;outline-style:none;text-decoration:none;"/></a></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="expander expander35">
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;" class="cleanContainer_horizontal5"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="46" style="-ms-interpolation-mode:bicubic;width:1px;height:46px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="pcode_top_text_1">
                  <tr>
                    <td valign="middle" align="center" style="border-collapse:collapse;color:#000000;font-family:Arial, sans-serif;font-size:18px;font-weight:400;text-align:center;vertical-align:middle;">Добро пожаловать в Юлмарт!</td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="expander expander12">
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal10"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="23" style="-ms-interpolation-mode:bicubic;width:1px;height:23px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="pcode_top_text_2">
                  <tr>
                    <td valign="middle" align="center" style="border-collapse:collapse;color:#000000;font-weight:700;font-family:Arial, sans-serif;font-size:15px;line-height:16px;text-align:center;vertical-align:middle;"><span class="for_mobile_ver_hide">Вы оформили заказ в Юлмарте как незарегистрированный<br/> пользователь.</span>
                      <!--if !mso| if !mso| [if !mso]
                      --><span style="mso-hide:all; display:none;" class="for_mobile_ver_show mso-hide">Вы оформили заказ в Юлмарте <br/>как незарегистрированный<br/> пользователь.</span>
                      <!-- <![endif]-->
                    </td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;">
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="28" style="-ms-interpolation-mode:bicubic;width:1px;height:28px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="background:#fff; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; mso-table-lspace:0; mso-table-rspace:0; width:100%" class="winheader">
                  <tr>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/wintop_left.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winheader__left"></td>
                    <td valign="middle" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px;background:#f8f8f8;"></td>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/wintop_right.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winheader__right"></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;" class="short_full_banner_top">
                  <tr>
                    <td width="27.5%" valign="middle">
                      <table align="center" cellpadding="0" cellspacing="0" border="0" width="0" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;">
                        <!--if !mso| if !mso| [if !mso]
                        -->
                        <tr style="mso-hide:all;" class="mso-hide">
                          <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;mso-hide:all;" class="mso-hide cleanContainer_horizontal1"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="16" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none; width: 1px;цш height: 16px;"/></td>
                        </tr>
                        <!-- <![endif]-->
                        <tr>
                          <td valign="middle" width="1" style="mso-hide:all;border-collapse:collapse; " class="for_mobile_ver_show mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="15" height="39" style="-ms-interpolation-mode:bicubic;width:15px;display:block;outline-style:none;text-decoration:none; height: 39px;"/></td>
                          <td valign="middle" width="100" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;text-align:center;" class="lock"><img src="${portalBindUrl}/resources/img/mail/softRegistration/account_top-b-320.png" width="100" height="90" style="-ms-interpolation-mode:bicubic;display:inline-block;outline-style:none;text-decoration:none;width:100px;height:90px;"/></td>
                        </tr>
                      </table>
                    </td>
                    <td width="72.5%" style="border-collapse:collapse;width:72.5%;" class="for_mobile_ver_hide">
                      <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;">
                        <tr>
                          <td valign="middle" width="1" style="border-collapse:collapse;" class="cleanContainer_horizontal1"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="39" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none; height: 39px;"/></td>
                        </tr>
                        <tr>
                          <td style="border-collapse:collapse;color:#000000;font-family:Arial, sans-serif;font-size:18px;mso-font-size:18px;vertical-align:middle;line-height:24px;">Чтобы вы могли <b>отслеживать статус заказа <br/>и получать бонусы с покупок, </b>мы создали <br/>вам <a href="${portalBindUrl}/cabinet/orders" target="_blank" align="left" style="color:#008fd5;font-family:Arial; font-weight:600;line-height:24px;text-align:left;text-decoration:underline;" class="bluelink"> <span style="color:#008fd5">личный кабинет</span></a>.</td>
                        </tr>
                        <tr>
                          <td valign="middle" width="1" style="border-collapse:collapse;vertical-align:middle;" class="cleanContainer_horizontal3"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="33" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none; height: 44px"/></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <!--if !mso| if !mso| [if !mso]
                      -->
                      <div width="0" style="display:none;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="for_mobile_ver_show">
                        <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;mso-hide:all;" class="mso-hide">
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;mso-hide:all;" class="mso-hide cleanContainer_horizontal1"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="20" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none; width:20px; height: 20px;"/></td>
                          </tr>
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td style="border-collapse:collapse;color:#000000;font-family:Arial, sans-serif;font-size:18px;text-align:center;mso-font-size:18px;vertical-align:middle;line-height:24px;mso-hide:all;" class="mso-hide">Чтобы вы могли <b>отслеживать <br/>статус заказа и получать <br/>бонусы с покупок, </b>мы создали <br/>вам <a href="${portalBindUrl}/cabinet/orders" target="_blank" align="left" style="color:#008fd5;font-family:Arial; font-weight:600;line-height:24px;text-align:left;text-decoration:underline;" class="bluelink"> <span style="color:#008fd5">личный кабинет</span></a>.</td>
                          </tr>
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;mso-hide:all;" class="mso-hide cleanContainer_horizontal3"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="25" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none;mso-hide:all; width: 1px; height: 25px;" class="mso-hide"/></td>
                          </tr>
                        </table>
                      </div>
                      <!-- <![endif]-->
                    </td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="cont_lr_pad_30">
                  <tr>
                    <td valign="middle" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;vertical-align:middle;" class="cont_lr_pad_30__left"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="30" height="1" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;text-decoration:none;"/></td>
                    <td valign="middle" width="100%" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;vertical-align:middle;width:100%;" class="cont_lr_pad_30__mid">
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="expander expander33">
                        <tr>
                          <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal6"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="27" style="-ms-interpolation-mode:bicubic;width:1px;height:27px;display:block;outline-style:none;"/></td>
                        </tr>
                      </table>
                      <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:15px;mso-table-lspace:0;mso-table-rspace:0;" class="pcode_mid_pcline">
                        <tr>
                          <td valign="middle" style="border-collapse:collapse;color:#000000;font-family:Arial, sans-serif;font-size:15px;line-height:20px;vertical-align:middle;text-align:center;" class="pcode_mid_pcline__left">Логин — это адрес вашей
                            <!--if !mso| if !mso| [if !mso]
                            --><br width="0" style="display:none;float:left;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="normal_hide mso-hide"/>
                            <!-- <![endif]-->электронной почты:<br/><a href="mailto:${email}" target="_blank" style="color:#008fd5;font-family:Arial, sans-serif;font-size:15px;line-height:20px;">${email}</a>
                          </td>
                        </tr>
                      </table>
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;">
                        <tr>
                          <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal9"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="17" style="-ms-interpolation-mode:bicubic;width:1px;height:17px;display:block;outline-style:none;"/></td>
                        </tr>
                      </table>
                      <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:15px;mso-table-lspace:0;mso-table-rspace:0;" class="pcode_mid_pcline">
                        <tr>
                          <td valign="middle" style="border-collapse:collapse;color:#000000;font-family:Arial, sans-serif;font-size:15px;line-height:20px;vertical-align:middle;text-align:center;" class="pcode_mid_pcline__left">Чтобы пользоваться личным
                            <!--if !mso| if !mso| [if !mso]
                            --><br width="0" style="display:none;float:left;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="normal_hide mso-hide"/>
                            <!-- <![endif]-->кабинетом, осталось сделать всего
                            <!--if !mso| if !mso| [if !mso]
                            --><br width="0" style="display:none;float:left;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="normal_hide mso-hide"/>
                            <!-- <![endif]-->один шаг.
                          </td>
                        </tr>
                      </table>
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="expander expander25">
                        <tr>
                          <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal13"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="17" style="-ms-interpolation-mode:bicubic;width:1px;height:17px;display:block;outline-style:none;"/></td>
                        </tr>
                      </table>
                      <table cellpadding="0" cellspacing="0" border="0" align="center" height="38" style="background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;color:#fff;cursor:pointer;font-family:Arial, sans-serif;font-size:14px;height:38px;mso-table-lspace:0;mso-table-rspace:0;" class="paybtn q_hide">
                        <tr height="38" style="height:38px;">
                          <td valign="middle" height="38" width="22" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;width:22px;" class="paybtn__left">
                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="22" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:22px;">
                              <tr height="38" style="height:38px;">
                                <td valign="middle" height="38" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;" class="cleanContainer_horizontal11"><img src="${portalBindUrl}/resources/img/mail/softRegistration/bg_pay_btn_left.png" width="22" height="38" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;text-decoration:none;"/></td>
                              </tr>
                            </table>
                          </td>
                          <td valign="middle" height="38" style="border-collapse:collapse;background-color:#e50329;color:#fff;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;text-align:center;white-space:nowrap;" class="paybtn__mid">
                              <a style="color:#fff;text-decoration: none;" href="${portalBindUrl}/remindUpdateSoft?${urlParams}" target="_blank">
                                  Подтвердить регистрацию и создать пароль
                              </a>
                          </td>
                          <td valign="middle" height="38" width="22" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;width:22px;" class="paybtn__right">
                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="22" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:22px;mso-cellspacing:0px;mso-padding-alt:0px 0px 0px 0px;">
                              <tr height="38" style="height:38px;">
                                <td valign="middle" height="38" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/bg_pay_btn_right_without.png" width="22" height="38" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;text-decoration:none;"/></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!--if !mso| if !mso| [if !mso]
                      -->
                      <table cellpadding="0" cellspacing="0" border="0" align="center" style="display:none;background-color:#ef012f;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;color:#fff;cursor:pointer;font-family:Arial, sans-serif;font-size:14px;height:38px;mso-table-lspace:0;mso-table-rspace:0;width:88% !important;mso-hide:all;" class="paybtn normal_hide mso-hide">
                        <tr height="54" style="height:54px;mso-hide:all;" class="mso-hide">
                          <td valign="middle" height="54" width="24" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;width:24px;mso-hide:all;" class="paybtn__left mso-hide">
                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="24" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:24px;mso-hide:all;" class="mso-hide">
                              <tr height="38" style="height:54px;mso-hide:all;" class="mso-hide">
                                <td valign="middle" height="54" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/bg_pay_btn_left_54.png" width="24" height="54" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;text-decoration:none;mso-hide:all;" class="mso-hide"/></td>
                              </tr>
                            </table>
                          </td>
                          <td valign="middle" height="38" style="border-collapse:collapse;color:#fff;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;text-align:center;white-space:nowrap;line-height:19px;mso-hide:all;" class="paybtn__mid mso-hide">
                              <a style="color:#fff;text-decoration: none;" href="${portalBindUrl}/remindUpdateSoft?${urlParams}" target="_blank">
                                Подтвердить регистрацию<br/>и создать пароль
                              </a>
                          </td>
                          <td valign="middle" height="54" width="24" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;width:24px;mso-hide:all;" class="paybtn__right mso-hide">
                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="24" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:24px;mso-hide:all;" class="mso-hide">
                              <tr height="54" style="height:54px;mso-hide:all;" class="mso-hide">
                                <td valign="middle" height="38" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;height:38px;vertical-align:middle;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/bg_pay_btn_right_without_54.png" width="24" height="54" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;text-decoration:none;mso-hide:all;" class="mso-hide"/></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- <![endif]-->
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;mso-cellspacing:0px;mso-padding-alt:0px 0px 0px 0px;" class="expander expander25">
                        <tr>
                          <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal14"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="29" style="-ms-interpolation-mode:bicubic;width:1px;height:29px;display:block;outline-style:none;"/></td>
                        </tr>
                      </table>
                    </td>
                    <td valign="middle" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;vertical-align:middle;" class="cont_lr_pad_30__right"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="30" height="1" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;text-decoration:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="background:#fff; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; mso-table-lspace:0; mso-table-rspace:0; width:100%" class="winfootender">
                  <tr>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/winfooter_left.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winfootender__left"></td>
                    <td valign="middle" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px"></td>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/winfooter_right.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winfootender__right"></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="width:90%;" class="btmshadow">
                  <tr>
                    <td valign="middle" height="36" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:top; width: 100%"><img src="${portalBindUrl}/resources/img/mail/softRegistration/bottom_shadow.png" width="100%" height="20" style="-ms-interpolation-mode:bicubic;width:100% ;height:17px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="pcode_top_text_2">
                  <tr>
                    <td valign="middle" align="center" style="border-collapse:collapse;color:#000000;font-weight:400;font-family:Arial, sans-serif;font-size:15px;line-height:17px;text-align:center;vertical-align:middle;"><span class="for_mobile_ver_hide">В вашем заказе есть <b>номер и тарифный план оператора сотовой связи.</b>  Напоминаем, подключаться в Юлмарте особенно выгодно!</span>
                      <!--if !mso| if !mso| [if !mso]
                      --><span width="0" style="display:none;float:left;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="for_mobile_ver_show mso-hide">В вашем заказе есть <b>номер и <br/>тарифный план оператора сотовой <br/>связи. </b>Напоминаем, подключаться<br/>в Юлмарте особенно выгодно!</span>
                      <!-- <![endif]-->
                    </td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal15"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="35" style="-ms-interpolation-mode:bicubic;width:1px;height:35px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="background:#fff; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; mso-table-lspace:0; mso-table-rspace:0; width:100%" class="winheader">
                  <tr>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/wintop_left.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winheader__left"></td>
                    <td valign="middle" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px;background:#f8f8f8;"></td>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/wintop_right.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winheader__right"></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;" class="short_full_banner_top">
                  <tr>
                    <td width="27.5%" valign="middle">
                      <table align="center" cellpadding="0" cellspacing="0" border="0" width="0" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;">
                        <tr>
                          <td valign="middle" width="88" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;text-align:center;" class="ten-percent"><img src="${portalBindUrl}/resources/img/mail/softRegistration/ten_percent_bonus.png" width="88" height="82" style="-ms-interpolation-mode:bicubic;display:inline-block;outline-style:none;text-decoration:none;width:88px;height:82px;"/></td>
                        </tr>
                      </table>
                    </td>
                    <td width="72.5%" style="border-collapse:collapse;width:72.5%;" class="for_mobile_ver_hide">
                      <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;">
                        <tr>
                          <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;" class="cleanContainer_horizontal1"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="22" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none;"/></td>
                        </tr>
                        <tr>
                          <td style="border-collapse:collapse;color:#000000;font-family:Arial, sans-serif;font-size:16px;mso-font-size:16px;vertical-align:middle;line-height:20px;"><b>Возвращаем 10% расходов на мобильную связь </b><br/><b>XXL-бонусами. </b>Каждый месяц.</td>
                        </tr>
                        <tr>
                          <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal2"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="11" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none;"/></td>
                        </tr>
                        <tr>
                          <td style="border-collapse:collapse;color:#008fd5;font-family:Arial, sans-serif;font-size:13px;vertical-align:middle;"><a href="https://fast.ulmart.ru/docs/telecom_xxl.pdf" style="color:#008fd5;font-family:Arial, sans-serif;font-size:13px;text-decoration:underline;" target="_blank"><span style="color:#008fd5;mso-hide:all;">Полные условия акции</span></a></td>
                        </tr>
                        <tr>
                          <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal3"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="25" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none;"/></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <!--if !mso| if !mso| [if !mso]
                      -->
                      <div width="0" style="display:none;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="for_mobile_ver_show">
                        <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;mso-hide:all;" class="mso-hide">
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;mso-hide:all;" class="mso-hide cleanContainer_horizontal1"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="5" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none;"/></td>
                          </tr>
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td style="border-collapse:collapse;color:#000000;font-family:Arial, sans-serif;font-size:16px;text-align:center;mso-font-size:16px;vertical-align:middle;line-height:20px;mso-hide:all;" class="mso-hide"><b>
                                Возвращаем 10% расходов на мобильную связь
                                XXL-бонусами.</b><br/>Каждый месяц.</td>
                          </tr>
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;mso-hide:all;" class="mso-hide cleanContainer_horizontal2"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="15" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none;"/></td>
                          </tr>
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td style="border-collapse:collapse;text-align:center;vertical-align:middle;mso-hide:all;" class="mso-hide"><a href="https://fast.ulmart.ru/docs/telecom_xxl.pdf" target="_blank" style="color:#008fd5;font-family:Arial, sans-serif;font-size:13px;text-decoration:underline;mso-hide:all;" class="mso-hide"><span style="color:#008fd5;mso-hide:all;" class="mso-hide">Полные условия акции</span></a></td>
                          </tr>
                          <tr style="mso-hide:all;" class="mso-hide">
                            <td valign="middle" width="1" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;mso-hide:all;" class="mso-hide cleanContainer_horizontal3"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="25" style="-ms-interpolation-mode:bicubic;width:1px;display:block;outline-style:none;text-decoration:none;mso-hide:all;" class="mso-hide"/></td>
                          </tr>
                        </table>
                      </div>
                      <!-- <![endif]-->
                    </td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="white__block">
                  <tr>
                    <td valign="middle" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cont_lr_pad_25__left"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="25" height="1" style="-ms-interpolation-mode:bicubic;width:25px;height:0;display:block;outline-style:none;text-decoration:none;"/></td>
                    <td valign="middle" align="center" width="100%" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;width:100%;" class="cont_lr_pad__mid">
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;width:100%;">
                        <tr>
                          <td valign="middle" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="30" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;height:30px;"/></td>
                        </tr>
                        <tr>
                          <td valign="top" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:top;">
                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="auto" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
                              <tr>
                                <td valign="top"><a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;">
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="123" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;width:123px;">
                                      <tr>
                                        <td align="center" valign="middle" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:middle;">
                                            <a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;color:#5f5f5f;" target="_blank">
                                                <img src="${portalBindUrl}/resources/img/mail/softRegistration/step1.png" width="47" height="56" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:47px;height:56px;border:0;"/>
                                            </a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="13" style="width:1px;height:13px;"/></td>
                                      </tr>
                                      <tr>
                                        <td align="center" style="border-collapse:collapse;color:#5f5f5f;font-family:Arial, sans-serif;font-size:13px;font-weight:400;vertical-align:middle;text-align:center;">
                                          <a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;color:#5f5f5f;" target="_blank">Подключайтесь <br/>В Юлмарте</a>
                                        </td>
                                      </tr>
                                    </table></a></td>
                                <td align="center" valign="middle" width="19" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:middle;width:19px;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/vertical_delimiter.png" width="1" height="108px" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:1px;height:108px;"/></td>
                                <td valign="top"><a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;">
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="123" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;width:123px;">
                                      <tr>
                                        <td align="center" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:top;">
                                            <a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;color:#5f5f5f;" target="_blank">
                                              <img src="${portalBindUrl}/resources/img/mail/softRegistration/step2.png" width="82" height="56" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:82px;height:56px;border:0;"/></td>
                                            </a>
                                      </tr>
                                      <tr>
                                        <td><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="13" style="width:1px;height:13px;"/></td>
                                      </tr>
                                      <tr>
                                        <td align="center" style="border-collapse:collapse;color:#5f5f5f;font-family:Arial, sans-serif;font-size:13px;font-weight:400;vertical-align:middle;text-align:center;">
                                            <a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;color:#5f5f5f;" target="_blank">Общайтесь,<br/>пользуйтесь<br/>интернетом</a>
                                        </td>
                                      </tr>
                                    </table></a></td>
                                <td align="center" valign="middle" width="19" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:middle;width:19px;" class="for_mobile_ver_hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/vertical_delimiter.png" width="1" height="108px" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:1px;height:108px;"/></td>
                                <td valign="top" class="for_mobile_ver_hide"><a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;">
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="123" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;width:123px;">
                                      <tr>
                                        <td align="center" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:top;">
                                            <a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;color:#5f5f5f;" target="_blank">
                                              <img src="${portalBindUrl}/resources/img/mail/softRegistration/step3.png" width="55" height="51" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:55px;height:51px;border:0;"/>
                                            </a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="18" style="width:1px;height:18px;"/></td>
                                      </tr>
                                      <tr>
                                        <td align="center" style="border-collapse:collapse;color:#5f5f5f;font-family:Arial, sans-serif;font-size:13px;font-weight:400;vertical-align:middle;text-align:center;" class="for_mobile_ver_hide">
                                            <a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;color:#5f5f5f;" target="_blank">Получайте<br/>XXL-бонусы<br/>ежемесячно</a>
                                        </td>
                                      </tr>
                                    </table></a></td>
                                <td align="center" valign="middle" width="19" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:middle;width:19px;" class="for_mobile_ver_hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/vertical_delimiter.png" width="1" height="108px" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:1px;height:108px;"/></td>
                                <td valign="top" class="for_mobile_ver_hide"><a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;">
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="123" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;width:123px;">
                                      <tr>
                                        <td align="center" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:top;">
                                            <a href="http://www.ulmart.ru/telecom/operators" target="_blank">
                                              <img src="${portalBindUrl}/resources/img/mail/softRegistration/step4.png" width="43" height="46" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:43px;height:46px;border:0;"/>
                                            </a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="23" style="width:1px;height:23px;"/></td>
                                      </tr>
                                      <tr>
                                        <td align="center" style="border-collapse:collapse;color:#5f5f5f;font-family:Arial, sans-serif;font-size:13px;font-weight:400;vertical-align:middle;text-align:center;">
                                            <a href="http://www.ulmart.ru/telecom/operators" style="text-decoration:none;color:#5f5f5f;" target="_blank">Получайте товары<br/>со скидкой</a>
                                        </td>
                                      </tr>
                                    </table></a></td>
                              </tr>
                              <!--if !mso| if !mso| [if !mso]
                              -->
                              <tr>
                                <td colspan="3">
                                  <div width="0" style="display:none;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="for_mobile_ver_show mso-hide">
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="auto" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;mso-hide:all;" class="mso-hide">
                                      <tr style="mso-hide:all;" class="mso-hide">
                                        <td colspan="3" style="mso-hide:all;" class="mso-hide">
                                          <table align="center" style="mso-hide:all;" class="mso-hide">
                                            <tr style="mso-hide:all;" class="mso-hide">
                                              <td align="center" height="41" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;height:41px;border-collapse:collapse;vertical-align:middle;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/horizontal_delimiter.png" width="108" height="1" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:108px;height:1px;mso-hide:all;" class="mso-hide"/></td>
                                              <td align="center" height="41" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;height:41px;vertical-align:middle;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="50" height="1" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:50px;height:1px;mso-hide:all;" class="mso-hide"/></td>
                                              <td align="center" height="41" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;height:41px;vertical-align:middle;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/horizontal_delimiter.png" width="108" height="1" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:108px;height:1px;mso-hide:all;" class="mso-hide"/></td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                      <tr style="mso-hide:all;" class="mso-hide">
                                        <td valign="top" style="mso-hide:all;" class="mso-hide"><a href="http://www.ulmart.ru/telecom/operators" style="mso-hide:all;text-decoration:none;" class="mso-hide">
                                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="123" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;width:123px;mso-hide:all;" class="mso-hide">
                                              <tr style="mso-hide:all;" class="mso-hide">
                                                <td align="center" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:top;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/step3.png" width="55" height="51" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:55px;height:51px;mso-hide:all;border:0;" class="mso-hide"/></td>
                                              </tr>
                                              <tr style="mso-hide:all;" class="mso-hide">
                                                <td style="mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="16" style="width:1px;height:16px;mso-hide:all;" class="mso-hide"/></td>
                                              </tr>
                                              <tr style="mso-hide:all;" class="mso-hide">
                                                <td align="center" style="border-collapse:collapse;color:#5f5f5f;font-family:Arial, sans-serif;font-size:13px;font-weight:400;vertical-align:middle;text-align:center;mso-hide:all;" class="mso-hide">Получайте<br/>XXL-бонусы<br/>ежемесячно</td>
                                              </tr>
                                            </table></a></td>
                                        <td align="center" valign="middle" width="19" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:middle;width:19px;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/vertical_delimiter.png" width="1" height="108px" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:1px;height:108px;"/></td>
                                        <td valign="top" style="mso-hide:all;" class="mso-hide"><a href="http://www.ulmart.ru/telecom/operators" style="mso-hide:all;text-decoration:none;" class="mso-hide">
                                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="123" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;width:123px;mso-hide:all;" class="mso-hide">
                                              <tr style="mso-hide:all;" class="mso-hide">
                                                <td align="center" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;vertical-align:top;mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/step4.png" width="43" height="46" style="-ms-interpolation-mode:bicubic;display:block;outline-style:none;width:43px;height:46px;mso-hide:all;border:0;" class="mso-hide"/></td>
                                              </tr>
                                              <tr style="mso-hide:all;" class="mso-hide">
                                                <td style="mso-hide:all;" class="mso-hide"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="21" style="width:1px;height:21px;mso-hide:all;" class="mso-hide"/></td>
                                              </tr>
                                              <tr style="mso-hide:all;" class="mso-hide">
                                                <td align="center" style="border-collapse:collapse;color:#5f5f5f;font-family:Arial, sans-serif;font-size:13px;font-weight:400;vertical-align:middle;text-align:center;mso-hide:all;" class="mso-hide">Получайте товары<br/>со скидкой</td>
                                              </tr>
                                            </table></a></td>
                                      </tr>
                                    </table>
                                  </div>
                                </td>
                              </tr>
                              <!-- <![endif]-->
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td valign="middle" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="18" style="-ms-interpolation-mode:bicubic;width:1px;height:18px;display:block;outline-style:none;"/></td>
                        </tr>
                      </table>
                    </td>
                    <td valign="middle" style="background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cont_lr_pad_25__right"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="25" height="1" style="-ms-interpolation-mode:bicubic;display:block;width:25px;height:1px;outline-style:none;text-decoration:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="background:#fff; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; mso-table-lspace:0; mso-table-rspace:0; width:100%" class="winfootender">
                  <tr>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/winfooter_left.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winfootender__left"></td>
                    <td valign="middle" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px"></td>
                    <td valign="middle" style="background:url(${portalBindUrl}/resources/img/mail/softRegistration/winfooter_right.png) no-repeat; border-collapse:collapse; font-family:Arial, sans-serif; font-size:14px; padding-top:10px; width:10px" width="10" class="winfootender__right"></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="width:90%;" class="btmshadow">
                  <tr>
                    <td valign="middle" height="36" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:top; width: 100%"><img src="${portalBindUrl}/resources/img/mail/softRegistration/bottom_shadow.png" width="100%" height="20" style="-ms-interpolation-mode:bicubic;width:100% ;height:17px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="ulsign">
                  <tr>
                    <td valign="middle" align="center" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:18px;text-align:center;vertical-align:middle;">Спасибо, что выбираете Юлмарт!</td>
                  </tr>
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="35" style="-ms-interpolation-mode:bicubic;width:1px;height:35px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;color:#000;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;letter-spacing:0.42px;mso-table-lspace:0;mso-table-rspace:0;text-align:center;" class="soc_title">
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;">Юлмарт в социальных сетях</td>
                  </tr>
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;" class="cleanContainer_horizontal16"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="18" style="-ms-interpolation-mode:bicubic;width:1px;height:18px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="socbtn">
                  <tr>
                    <td valign="middle" width="50%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;width:50%;" class="socbtn__first_part">
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;" class="socbtn__in">
                        <tr>
                          <td valign="middle" width="50%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;width:50%;" class="socbtn__in__first"><a href="https://www.facebook.com/I.love.Ulmart" target="_blank" style="color:#000001;font-family:Arial;text-decoration:none;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/soc_btn_fb.png" width="147" height="40" style="-ms-interpolation-mode:bicubic;border-style:none;display:block;outline-style:none;text-decoration:none;"/></a></td>
                          <td valign="middle" width="50%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;width:50%;" class="socbtn__in__secont"><a href="https://twitter.com/Ulmart_ru" target="_blank" style="color:#000001;font-family:Arial;text-decoration:none;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/soc_btn_tw.png" width="147" height="40" style="-ms-interpolation-mode:bicubic;border-style:none;display:block;outline-style:none;text-decoration:none;"/></a></td>
                        </tr>
                      </table>
                    </td>
                    <td valign="middle" width="50%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;width:50%;" class="socbtn__second_part">
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;" class="socbtn__in">
                        <tr>
                          <td valign="middle" width="50%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;width:50%;" class="socbtn__in__first"><a href="https://vk.com/ulmart" target="_blank" style="color:#000001;font-family:Arial;text-decoration:none;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/soc_btn_vk.png" width="147" height="40" style="-ms-interpolation-mode:bicubic;border-style:none;display:block;outline-style:none;text-decoration:none;"/></a></td>
                          <td valign="middle" width="50%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;width:50%;" class="socbtn__in__secont"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td valign="middle" colspan="2" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="13" style="-ms-interpolation-mode:bicubic;width:1px;height:13px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="lastlink">
                  <tr>
                    <td valign="middle" align="center" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;text-align:center;"><a href="http://www.ulmart.ru" target="_blank" align="left" style="color:#008fd5;font-family:Arial;font-size:12px;font-weight:400;line-height:24px;text-align:left;text-decoration:none;" class="bluelink"> <span style="color:#008fd5">Узнать больше</span></a></td>
                  </tr>
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="16" style="-ms-interpolation-mode:bicubic;width:1px;height:16px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;mso-table-lspace:0;mso-table-rspace:0;width:100%;" class="description">
                  <tr>
                    <td valign="middle" align="center" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:12px;color:#a1a1a1;text-align:center;line-height:18px;"><span class="for_mobile_ver_hide">Если вы не оформляли заказ в Юлмарте, вероятно, кто-то без вашего ведома указал этот e-mail. <br/>Сообщите нам по адресу <a href="mailto:sales@ulmart.ru" target="_blank" style="color:#E50329;font-weight:600;text-decoration:none;"> <span style="color:#e50329">sales@ulmart.ru</span></a>, мы постараемся разобраться в ситуации. Или не<br/>обращайте внимания на это письмо.</span>
                      <!--if !mso| if !mso| [if !mso]
                      --><span width="0" style="display:none;float:left;max-height:0;mso-hide:all;overflow:hidden;width:0;" class="for_mobile_ver_show mso-hide">Если вы не оформляли заказ в Юлмарте, <br/>вероятно, кто-то без вашего ведома указал этот <br/>e-mail. Сообщите нам по адресу <a href="mailto:sales@ulmart.ru" target="_blank" style="color:#E50329;font-weight:600;text-decoration:none;"> <span style="color:#e50329">sales@ulmart.ru</span></a>, <br/>мы постараемся разобраться в ситуации. Или не<br/>обращайте внимания на это письмо.</span>
                      <!-- <![endif]-->
                    </td>
                  </tr>
                  <tr>
                    <td valign="middle" style="border-collapse:collapse;font-family:Arial, sans-serif;font-size:14px;mso-font-size:14px;vertical-align:middle;"><img src="${portalBindUrl}/resources/img/mail/softRegistration/empty.gif" width="1" height="31" style="-ms-interpolation-mode:bicubic;width:1px;height:31px;display:block;outline-style:none;"/></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>