<#-- @ftlvariable name="url" type="java.lang.String" -->
<#-- @ftlvariable name="name" type="java.lang.String" -->
<#-- @ftlvariable name="phonePrefix" type="java.lang.String" -->
<#-- @ftlvariable name="phoneSuffix" type="java.lang.String" -->
<#include "image.ftl"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Заказ оплачен</title><meta name="viewport" id="viewport" content="width=device-width, user-scalable=1">
    <link rel="shortcut icon" href="#" />
    <link rel="stylesheet" href="${url}/resources/s/reset.css" />
    <link rel="stylesheet" href="${url}/resources/s/style.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${url}/resources/s/style-ie78.css" />
    <![endif]-->
    <#assign orderPrice = orderPriceTotal>
<#if orderId?? && serviceType?? && agentId??>
    <#assign orderLink="https://www.ulmart.ru/cabinet/orders?orderId=${orderId}&type=${serviceType}&agentId=${agentId}"/>
    <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                    "@type": "Order",
                    "merchant": {
                        "@type": "Organization",
                        "name": "ЗАО Юлмарт"
                    },
                "orderNumber": "${orderId}",
                "priceCurrency": "RUR",
                "price": "${orderPrice?string.number}",
                "acceptedOffer": [
                    <#list goods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "${good.name}",
                                "url": "https://www.ulmart.ru/goods/${good.goodId}",
                                "image": "<@imageUrl good.photoId!good.goodId 'large'/>",
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "${good.amount}"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                    <#list tspGoods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "${good.name}",
                                "url": "https://www.ulmart.ru/autoparts/${good.goodId}?ultimaId=${good.ultimaId!}&cat=${cat!0}",
                                 <#if good.photoUrl??>
                                        "image": "<img src="${good.photoUrl}" alt="" width="100px" class="b-img2__img"/>",
                                    <#else>
                                        "image": "<@imageUrl good.photoId!good.goodId 'large'/>",
                                </#if>
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "${good.count}"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                ],
                "url": "${orderLink}",
                "priceSpecification": [
                    {
                        "@type": "UnitPriceSpecification",
                        "price": "${orderPrice?string.number}",
                        "priceCurrency": "RUR"
                    }<#if deliveryType?? && deliveryCost?? && deliveryCost?has_content>,
                        {
                           "@type": "DeliveryChargeSpecification",
                           "price": "${deliveryCost?string.number}",
                           "priceCurrency": "RUR"
                        }
                    </#if>
                ],
                "orderStatus": "http://schema.org/OrderProcessing"
            }
        </script>
</#if>
</head>
<body>
<div style="padding:0 30px;">
    <div style="width:100%;margin-bottom:35px;">
			<span style="float: right; font: 16px Arial; color: rgb(118, 122, 123); margin-top:15px;">
				<span class="wmi-callto"><small style="position:relative; top:-.2em;">(${phonePrefix})</small> <span style="color:#CC0000; font-size:25px;">${phoneSuffix}</span></span>
			</span>
        <div style="padding:10px 0 8px;">
            <img src="${url}/resources/desktop.blocks/b-head-logo/ulmart-logo.png">
        </div>
        <div style="width: 100%; background-color: rgb(215, 215, 215); height:1px;"></div>
    </div>
    <div style="font-size: 1.077em; line-height: 1.43; margin:35px 0;">
        <p style="margin:0 0 1.15em;">Уважаемый (ая) ${name!}, благодарим Вас за покупку в кибермаркете <a href="https://www.ulmart.ru/" target="_blank">www.ulmart.ru</a></p>
        <p style="margin:0 0 1.15em;">Номер заказа: <b style="font-weight:bold;"><span class="wmi-callto">${orderId}</span></b> <#if creationDate??>от ${creationDate}г</#if></p>
        <table width="100%" cellspacing="0" cellpadding="3px" border="0" style="font-size:100%; margin-bottom: 1.15em; margin-left: -10px; margin-top: 0.385em;"><tbody>
        <tr>
            <td width="5%" align="left" style="padding:5px 10px; font-weight:bold;">Артикул</td>
            <td width="60%" align="left" style="padding:5px 10px; font-weight:bold;">Наименование товара</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Кол-во</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Цена</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Сумма</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">XXL-Бонус</td>
        </tr>
        <tr>
            <td colspan="6" style="border-top:1px solid #E4E4E4; height:5px;"></td>
        </tr>

        <#list goods as good>
        <tr>
            <#assign sum = good.price * good.amount>
            <td aling="left" style="padding:5px 10px;">${good.goodId}</td>
            <td aling="left" style="padding:5px 10px;"><a href="https://www.ulmart.ru/goods/${good.goodId}">${good.name}</a></td>
            <td align="right" style="padding:5px 10px;">${good.amount}</td>
            <td align="right" style="padding:5px 10px;">${good.price?string.number}</td>
            <td align="right" style="padding:5px 10px;">${sum?string.number}</td>
            <td align="right" style="padding:5px 10px;">${good.xxlBonus!"0"}</td>
        </tr>
        </#list>
        <#list tspGoods as good>
        <tr>
            <#assign sum = good.price * good.count>
            <td aling="left" style="padding:5px 10px;">${good.goodId}</td>
            <td aling="left" style="padding:5px 10px;"><a href="https://www.ulmart.ru/autoparts/${good.goodId}?ultimaId=${good.ultimaId!}&cat=${cat!0}">${good.name}</a></td>
            <td align="right" style="padding:5px 10px;">${good.count}</td>
            <td align="right" style="padding:5px 10px;">${good.price?string.number}</td>
            <td align="right" style="padding:5px 10px;">${sum?string.number}</td>
            <td align="right" style="padding:5px 10px;">${good.xxlBonus!"0"}</td>
        </tr>
        </#list>
        <#list certs as cert>
        <tr>
            <#if cert.id?? && cert.amount?? && cert.nominal??>
                <#assign sum2 = cert.nominal * cert.amount>
                <td aling="left" style="padding:5px 10px;">${cert.id}</td>
                <td aling="left" style="padding:5px 10px;"><a href="https://www.ulmart.ru/goods/${cert.id}">${cert.name!"Подарочный сертификат"}</a></td>
                <td align="right" style="padding:5px 10px;">${cert.amount}</td>
                <td align="right" style="padding:5px 10px;">${cert.nominal?string.number}</td>
                <td align="right" style="padding:5px 10px;">${sum2?string.number}</td>
                <td align="right" style="padding:5px 10px;">&mdash;</td>
            </#if>
        </tr>
        </#list>
        <#if deliveryType?? && deliveryCost?? && deliveryCost?has_content><tr>
            <td aling="left" style="padding:5px 10px;"></td>
            <td aling="left" style="padding:5px 10px;">Доставка</td>
            <td align="right" style="padding:5px 10px;"></td>
            <td align="right" style="padding:5px 10px;">${deliveryCost?string.number}</td>
            <td align="right" style="padding:5px 10px;">${deliveryCost?string.number}</td>
        </tr></#if>

        <tr>
            <td colspan="6" style="border-bottom:1px solid #E4E4E4;height:5px;"></td>
        </tr>
        <#if xxlBonus?? && xxlBonus != 0>
        <tr>
            <td colspan="2" style="padding:5px 10px;">&nbsp;</td>
            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Итого:</td>
            <td align="right" style="padding:5px 10px;">${orderPrice?string.number}&nbsp;р.</b></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="padding:5px 10px;">&nbsp;</td>
            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Оплачено бонусами:</td>
            <td align="right" style="padding:5px 10px;">${xxlBonus?string.number}&nbsp;р.</b></td>
            <td>&nbsp;</td>
        </tr>
        </#if>
        </tbody></table>
        <ul style="margin: 0.77em 0 1.15em;">
        <#if (isTaksaOrder!false)>
            <li style="margin: 0.385em 0;">Ваша программа Такса будет активирована в личном кабинете в течение нескольких минут.</li>
        <#else>
            <li style="margin: 0.385em 0;">Способ оплаты: <b style="font-weight:bold;">${paymentType}</b></li>
        </#if>
        <#if isNPL?? && !isNPL>
            <#if deliveryType??&&deliveryType>
                <li style="margin: 0.385em 0;">Способ доставки: <b style="font-weight:bold;">доставка курьером</b></li>
                <#if tariff??><li style="margin: 0.385em 0;">Тариф: <b style="font-weight:bold;">${tariff}</b></li></#if>
                <li style="margin: 0.385em 0;">Время доставки: <b style="font-weight:bold;">${deliveryDate}</b></li>
                <li style="margin: 0.385em 0;">Адрес доставки: <b style="font-weight:bold;">${deliveryCity}, ${deliveryAddress}</b></li>
            <#elseif deliveryType??&&!deliveryType>
                <li style="margin: 0.385em 0;">Тип отправления: <b style="font-weight:bold;">${postType}</b></li>
                <li style="margin: 0.385em 0;">Дата доставки: <b style="font-weight:bold;">ориентировочно ${deliveryDate}</b></li>
                <li style="margin: 0.385em 0;"><#if serviceCode=='1'||serviceCode=='5'>Где забрать: <#else>Адрес доставки: </#if><b style="font-weight:bold;">${deliveryAddress}</b></li>
                <#if serviceCode=='post'><li style="margin: 0.385em 0;">Почтовое отделение: <b style="font-weight:bold;">${postOffice}</b></li></#if>
                <#if postWorktime??&&(postWorktime?length>0)><li style="margin: 0.385em 0;">Режим работы: <b style="font-weight:bold;">${postWorktime}</b></li></#if>
            <#else>
                <li style="margin: 0.385em 0;">Способ доставки: <b style="font-weight:bold;">самовывоз</b></li>
                <li style="margin: 0.385em 0;">Когда можно забрать: <b style="font-weight:bold;"><#if storageDueToDate??>${storageDueToDate}<#else>после оплаты</#if></b></li>
                <li style="margin: 0.385em 0;">Где забрать: <a href="${baseUrl}${shopUrl!"/help/current/contacts"}"><b style="font-weight:bold;">г. ${cityName}, ${shopCatalogAddress}</b></a></li>
                <li style="margin: 0.385em 0;">Срок хранения: <b style="font-weight:bold;">${storageUntilDate!}</b></li>
            </#if>
        </#if>
        <#if phone?? && phone != ""><li style="margin: 0.385em 0;">Телефон: <b style="font-weight:bold;"><span class="wmi-callto">+7 ${phone}</span></b></li></#if>
        </ul>
    <#if deliveryType??&&deliveryType><p style="margin:0 0 1.15em;">
        В резерве присутствуют позиции, которые будут доставлены по адресу: ${deliveryCity}, ${deliveryAddress}, <b>${deliveryDate}</b>. В течение трех рабочих часов оператор свяжется с Вами для уточнения деталей доставки.
    </p></#if>
    <#if yandexRatingId?? && yandexMarketRating??>
        <a href="https://market.yandex.ru/shop/${yandexRatingId}/reviews?from=${yandexRatingId}" target="_blank">
            <img src="${url}/resources/img/ya.market-${yandexMarketRating}.png" width="310" height="29" />
        </a>
    </#if>
        <div style="width: 100%; text-align: right; border-top:1px solid rgb(242, 242, 242); padding:5px 0; font-style:italic;">
            <span style="font-weight: bold;">Мы знаем, что у Вас есть выбор. Спасибо, что выбрали нас</span>
            <br/>
            <em>Команда Юлмарт</em>
        </div>
    </div>
</div>
</body>
</html>