<#-- @ftlvariable name="url" type="java.lang.String" -->
<#-- @ftlvariable name="name" type="java.lang.String" -->
<#-- @ftlvariable name="phonePrefix" type="java.lang.String" -->
<#-- @ftlvariable name="phoneSuffix" type="java.lang.String" -->
<#-- @ftlvariable name="digitalOrderDetail" type="ru.ulmart.domain.DigitalESDOrderInfo" -->
<#-- @ftlvariable name="good" type="ru.ulmart.domain.DigitalOrderItem" -->
<!DOCTYPE html>
<html style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8"/>
    <title>#</title>
</head>
<body style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 1; font-family: Arial; margin: 0; padding: 0; border: 0;">
<table width="100%"
       style="width: 1030px; font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0; border: 0;">
    <tbody>
    <tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
        <td style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0 20px; border: 0;"
            valign="baseline">
            <table width="100%"
                   style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0 0 14px; border: 0;">
                <tbody>
                <tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
                    <td style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;"
                        valign="baseline"><img src="${url}/resources/desktop.blocks/b-head-logo/ulmart-logo.png"
                                               height="34" width="220"
                                               style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
                    </td>
                    <td style="vertical-align: top; text-align: right; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0; padding: 0; border: 0;"
                        align="right" valign="top">+7 812 <span
                            style="vertical-align: top; color: #000000; top: -6px; position: relative; font-style: normal; font-variant: normal; font-weight: normal; font-size: 18px; line-height: 30px; font-family: Arial; margin: 0; padding: 0; border: 0;">336-3777, 389-3677</span>
                    </td>
                </tr>
                </tbody>
            </table>
            <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;"><#if name??>
                Уважаемый(ая) ${name!}, б<#else>Б</#if>лагодарим вас за то, что вы
                воспользовались услугами Кибермаркета <a href="${url}"
                                                         style="vertical-align: baseline; color: #137FB7; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">ulmart.ru</a>
            </p>
            <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
                Ваш заказ: <b
                    style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: bold; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">${digitalOrderDetail.id}</b>
                от ${digitalOrderDetail.formattedDate}г. успешно оплачен.</p>
            <p style="vertical-align: baseline; color: #808080; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: 15px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
                Ваш заказ</p>
            <table style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; border-radius: 3px; width: 100%; background-color: #FFFFFF; margin: 0 0 18px; padding: 8px 0 0; border: 1px solid #e5e5e5;"
                   bgcolor="#FFFFFF">
                <tbody>
                <tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
                    <td style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;"
                        valign="baseline">
                        <#list digitalOrderDetail.items as item>
                            <#if item.bookmate>
                                <#if item.subscriptionId == '1'>
                                    <#assign goodHeader = "Подписка на Bookmate на 1 месяц"/>
                                <#elseif item.subscriptionId == '2'>
                                    <#assign goodHeader = "Подписка на Bookmate на 3 месяца"/>
                                <#elseif item.subscriptionId == '3'>
                                    <#assign goodHeader = "Подписка на книжный сервис Bookmate на 6 месяцев"/>
                                <#elseif item.subscriptionId == '4'>
                                    <#assign goodHeader = "Подписка на книжный сервис Bookmate на 12 месяцев"/>
                                </#if>
                                <#assign goodImage = "${url}/resources/img/mail/digital/bookmate-thumb-sm.jpg"/>
                            <#elseif item.zvooq>
                                <#if item.subscriptionId == '1'>
                                    <#assign goodHeader = "Подписка на Zvooq на 1 месяц"/>
                                <#elseif item.subscriptionId == '2'>
                                    <#assign goodHeader = "Подписка на Zvooq на 3 месяца"/>
                                <#elseif item.subscriptionId == '3'>
                                    <#assign goodHeader = "Подписка на Zvooq на 6 месяцев"/>
                                <#elseif item.subscriptionId == '4'>
                                    <#assign goodHeader = "Подписка на Zvooq на 12 месяцев"/>
                                </#if>
                                <#assign goodImage = "${url}/resources/img/mail/digital/zvooq-thumb-sm.jpg"/>
                            <#else>
                                <#assign goodHeader = item.description/>
                                <#assign goodImage = "${url}/resources/img/mail/digital/games.png"/>
                            </#if>
                            <table style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; border-bottom-style: solid; border-bottom-color: #E5E5E5; width: 100%; margin: 0 0 18px; padding: 0 15px 21px; border-width: 0 0 1px;">
                                <tbody>
                                <tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
                                    <td width="100.625315%"
                                        style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; width: 10.172939%; margin: 0; padding: 0 15px 0 0; border: 0;"
                                        valign="baseline"><img src="${goodImage}" height="100"
                                                               width="100" alt=""
                                                               style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
                                    </td>
                                    <td width="63.157894%"
                                        style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: top; text-align: left; margin: 0; padding: 0; border: 0;"
                                        align="left" valign="top">
                                        <p style="vertical-align: baseline; color: #000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
                                            ${goodHeader}</p>
                                        <#if item.softkey>
                                            <p style="vertical-align: baseline; color: #808080; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: 15px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">Для активации продукта используйте ваши персональные данные</p>
                                            <#if item.keys??>
                                            <#list item.keys?keys as key>
                                                <p style="vertical-align: baseline; color: #000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;"><span style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: #808080; margin: 0; padding: 0; border: 0;">Ключ:</span> ${key}</p>
                                            </#list>
                                            </#if>
                                        <#else>
                                            <p style="vertical-align: baseline; color: #808080; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: 15px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
                                            ${item.description2}</p>
                                        </#if>
                                    </td>
                                    <td width="25.263157%" align="right"
                                        style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: top; margin: 0; padding: 0; border: 0;"
                                        valign="top">
                                        <table width="100%"
                                               style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
                                            <tbody>
                                            <tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
                                                <#if item.softkey>
                                                    <td align="right" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; border-radius: 3px; width: 185px; height: 135px; text-align: left; background-color: #FAFAFA; margin: 0; padding: 13px 14px; border: 0;" bgcolor="#FAFAFA" valign="baseline">

                                                    <#if item.keys??>
                                                        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">Ссылки для скачивания:</p>
                                                            <#list item.keys?keys as key>
                                                                <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;"><a href="${item.keys[key]}" style="vertical-align: baseline; color: #137FB7; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0; padding: 0; border: 0;">Ссылка</a></p>
                                                            </#list>
                                                            <#assign waiting = (item.count - item.keys?size)/>
                                                            <#if (waiting > 0)>
                                                                <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">Количество ожидаемых ключей: ${waiting}</p>
                                                            </#if>
                                                        </#if>

                                                    <#else>
                                                    <td align="right"
                                                        style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;"
                                                        valign="baseline">
                                                        <a target="_blank" href="${item.urlLink}" style="vertical-align: baseline; color: #FFFFFF; border-radius: 3px; width: 170px; height: 30px; display: block; text-align: center; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 30px; font-family: Arial; background-image: linear-gradient(-180deg, #E85538 8%, #E4003A 95%); background-color: #E4003A; margin: 0 0 10px; padding: 0; border: 0;">Активировать
                                                            подписку</a>
                                                        <#if name??>
                                                        <a target="_blank" href="${url}/cabinet/orders?order=${digitalOrderDetail.id}"
                                                           style="vertical-align: baseline; color: #137FB7; border-radius: 3px; width: 170px; height: 30px; display: block; text-align: center; text-decoration: none; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; background-color: #FFFFFF; margin: 0; padding: 0; border: 1px solid #e5e5e5;"><img
                                                                src="img/gift.png" height="17" width="16"
                                                                style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; position: relative; right: 4px; top: 2px; margin: 0; padding: 0; border: 0;"><span
                                                                style="vertical-align: baseline; color: #000000; text-decoration: none; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 30px; font-family: Arial; margin: 0; padding: 0; border: 0;">Отправить в подарок</span></a>
                                                        </#if>
                                                    </#if>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </#list>
                    </td>
                </tr>
                </tbody>
            </table>
            <#if name??>
            <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
                Активировать подписку или подарить ее другу вы можете в <a href="${url}/cabinet/orders?type=digital"
                                                                           style="vertical-align: baseline; color: #137FB7; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">личном
                кабинете</a></p>
            <#else>
                <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">Зарегистрируйтесь на сайте <a href="${url}" style="vertical-align: baseline; color: #137FB7; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">ulmart.ru</a> и сможете за каждую покупку получать бонусы на любые покупки в Юлмарте. Кроме того, получите доступ к <a href="${url}/cabinet/orders" style="vertical-align: baseline; color: #137FB7; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">личному кабинету</a>, в котором сможете редактировать заказы и просматривать их историю, активировать и дарить подписки, а также получите множество других сервисов.</p>
                <a target="_blank" href="${url}/registration" style="vertical-align: baseline; color: #FFFFFF; border-radius: 3px; width: 300px; height: 35px; display: block; text-align: center; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 35px; font-family: Arial; background-image: linear-gradient(-180deg, #E85538 8%, #E4003A 95%); background-color: #E4003A; margin: 0 0 19px; padding: 0; border: 0;">Зарегистрироваться</a>
            </#if>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>