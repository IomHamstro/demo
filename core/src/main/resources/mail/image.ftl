<#-- TODO: This template is for public-web only. In case of changes don't forget to change mobile-web's too -->
<#-- TODO: should be function -->
<#macro imageUrl goodId type custom_suffix="">
<@compress single_line=true>
    <#-- @ftlvariable name="goodId" type="java.lang.Long" -->
    <#-- @ftlvariable name="type" type="java.lang.String" -->
    <#local goodIdString = goodId?string?left_pad(6, '0')/>
    <#local first = goodIdString?substring(0, goodIdString?length - 4)/>
    <#local second = goodIdString?substring(0, goodIdString?length - 2)/>
    <#switch type>
        <#case "small">
            <#local prefix="small"/>
            <#local suffix="s"/>
            <#break />
        <#case "medium">
            <#local prefix="mid"/>
            <#local suffix=""/>
            <#break />
        <#case "large">
            <#local prefix="big"/>
            <#local suffix=""/>
            <#break />
        <#case "gen">
            <#local prefix="gen"/>
            <#local suffix=""/>
            <#break />
        <#default>
            <#stop "Unknown type: " + type/>
    </#switch>
    https://p.fast.ulmart.ru/p/${prefix}/${first}/${second}/${goodId}${custom_suffix}${suffix}.jpg
</@compress>
</#macro>

<#macro image goodId type class="" width="" height="" alt="" custom_suffix="">
    <#-- @ftlvariable name="goodId" type="java.lang.Long" -->
    <#-- @ftlvariable name="type" type="java.lang.String" -->
    <img src="<@imageUrl goodId=goodId type=type custom_suffix=custom_suffix/>"
         <#if class?has_content>class="${class}"</#if>
         <#if width?has_content>width=${width}</#if>
         <#if height?has_content>height=${height}</#if>
         <#if alt?has_content>alt="${alt}"</#if>>
</#macro>