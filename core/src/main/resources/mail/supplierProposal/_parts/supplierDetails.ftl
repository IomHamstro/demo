<#macro supplierDetails supplierBean fileUploadMapBean priceListFiles="">
    <#-- @ftlvariable name="supplierBean" type="ru.ulmart.domain.state.bargain.supplier.proposal.ProposalSupplierBean" -->
    <#-- @ftlvariable name="fileUploadMapBean" type="ru.ulmart.domain.state.upload.FileUploadMapBean" -->
    <table class="user-data" width="100%" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0 30px 30px 8px;text-align:left;vertical-align:top;word-wrap:break-word">
                <h5 style="Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal">Поставщик</h5>
                <p class="value last" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:18px;margin:0;margin-bottom:30px;padding:0;text-align:left">
                    ${supplierBean.organizationName}<br>
                    ${supplierBean.representativeName}<br>
                    +7 ${supplierBean.representativePhone}
                </p>

                <h5 style="Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal">Данные юрлица</h5>
                <p class="label" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:10px;font-weight:400;line-height:18px;margin:0;margin-bottom:0;padding:0;text-align:left">
                    Юридический адрес
                </p>
                <p class="value" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:18px;margin:0;margin-bottom:10px;padding:0;text-align:left">
                    ${supplierBean.juridicalAddress}
                </p>
                <p class="label" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:10px;font-weight:400;line-height:18px;margin:0;margin-bottom:0;padding:0;text-align:left">
                    Фактический адрес
                </p>
                <#if supplierBean.actualAddress??>
                <p class="value" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:18px;margin:0;margin-bottom:10px;padding:0;text-align:left">
                    ${supplierBean.actualAddress}
                </p>
                </#if>
                <p class="label" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:10px;font-weight:400;line-height:18px;margin:0;margin-bottom:0;padding:0;text-align:left">
                    ИНН
                </p>
                <p class="value" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:18px;margin:0;margin-bottom:10px;padding:0;text-align:left">
                    ${supplierBean.inn}
                </p>
                <p class="label" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:10px;font-weight:400;line-height:18px;margin:0;margin-bottom:0;padding:0;text-align:left">
                    КПП
                </p>
                <p class="value last" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:18px;margin:0;margin-bottom:30px;padding:0;text-align:left">
                    ${supplierBean.kpp}
                </p>

                <#if priceListFiles?has_content>
                    <#local priceListFileHandle = priceListFiles?split(",")[0]/>
                    <#local priceListFileUpload = fileUploadMapBean.getFileUpload("priceListFiles", priceListFileHandle)/>

                    <#if priceListFileUpload??>
                        <a class="link" href="${priceListFileUpload.fileLink}" style="Margin:0;color:#1782b9;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">
                            <img src="https://fast.ulmart.ru/ns/stock/mail/doc_logo.png" width="14px" height="16px" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:inline-block;max-width:100%;outline:0;text-decoration:none;width:auto">
                            Прайс-лист
                        </a>
                    </#if>
                </#if>
            </td>
        </tr>
        </tbody>
    </table>
</#macro>