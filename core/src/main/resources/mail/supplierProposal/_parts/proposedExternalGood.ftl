<#macro proposedExternalGood goodBean fileUploadMapBean>
    <#-- @ftlvariable name="goodBean" type="ru.ulmart.domain.state.bargain.supplier.proposal.ProposalGoodBean" -->
    <#-- @ftlvariable name="fileUploadMapBean" type="ru.ulmart.domain.state.upload.FileUploadMapBean" -->
    <table class="row new-product-list" style="border-collapse:collapse;border-spacing:0;display:table;font-size:0;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
        <th class="new-product-item small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;padding-top:0 0 20px!important;text-align:left;width:100%">
            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                        <table class="new-product" width="100%" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                            <col width="140px"><col>
                            <tbody>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;border-top:1px solid #e5e5e5;border-top-left-radius:3px;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:16px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Товар
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;border-top:1px solid #e5e5e5;border-top-right-radius:3px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:16px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    ${goodBean.proposedExternalGoodName}
                                </td>
                            </tr>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Кол-во, шт.
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    ${goodBean.proposedExternalGoodAmount}
                                </td>
                            </tr>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Цена за шт.
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    ${goodBean.proposedExternalGoodPrice}₽
                                </td>
                            </tr>
                            <#if goodBean.proposedExternalGoodCategory??>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Категория товара
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    ${goodBean.proposedExternalGoodCategory}
                                </td>
                            </tr>
                            </#if>
                            <#if goodBean.proposedExternalGoodSubcategory??>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Подкатегория
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                ${goodBean.proposedExternalGoodSubcategory}
                                </td>
                            </tr>
                            </#if>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Производители
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    ${goodBean.proposedExternalGoodHandle}
                                </td>
                            </tr>
                            <#if goodBean.proposedExternalGoodDescription??>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Описание
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    ${goodBean.proposedExternalGoodDescription}
                                </td>
                            </tr>
                            </#if>
                            <#if goodBean.proposedExternalGoodPhotoFiles?has_content>
                                <#local photoFileUploads = fileUploadMapBean.getFileUploadsByHandles("good.proposedExternalGoodPhotoFiles", goodBean.proposedExternalGoodPhotoFiles)/>
                                <#if photoFileUploads?has_content>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-bottom:1px solid #e5e5e5;border-bottom-left-radius:3px;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                            Фото
                                        </td>
                                        <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #e5e5e5;border-bottom-right-radius:3px;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                            <#list photoFileUploads as uploadFile>
                                                <a class="link" href="${uploadFile.fileLink}" style="Margin:0;color:#1782b9;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    ${uploadFile.fileName}
                                                </a>
                                                <br/>
                                            </#list>
                                        </td>
                                    </tr>
                                </#if>
                            </#if>
                            <#if goodBean.proposedExternalGoodInstructionFiles?has_content>
                                <#local instructionFileUploads = fileUploadMapBean.getFileUploadsByHandles("good.proposedExternalGoodInstructionFiles", goodBean.proposedExternalGoodInstructionFiles)/>
                                <#if instructionFileUploads?has_content>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-bottom:1px solid #e5e5e5;border-bottom-left-radius:3px;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                            Инструкции
                                        </td>
                                        <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #e5e5e5;border-bottom-right-radius:3px;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                            <#list instructionFileUploads as uploadFile>
                                                <a class="link" href="${uploadFile.fileLink}" style="Margin:0;color:#1782b9;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    ${uploadFile.fileName}
                                                </a>
                                                <br/>
                                            </#list>
                                        </td>
                                    </tr>
                                </#if>
                            </#if>
                            <#if goodBean.proposedExternalGoodCertificateFiles?has_content>
                                <#local certificateFileUploads = fileUploadMapBean.getFileUploadsByHandles("good.proposedExternalGoodCertificateFiles", goodBean.proposedExternalGoodCertificateFiles)/>
                                <#if certificateFileUploads?has_content>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-bottom:1px solid #e5e5e5;border-bottom-left-radius:3px;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                            Сертификаты,<br/> лицензии,<br/> разрешения
                                        </td>
                                        <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #e5e5e5;border-bottom-right-radius:3px;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                            <#list certificateFileUploads as uploadFile>
                                                <a class="link" href="${uploadFile.fileLink!''}" style="Margin:0;color:#1782b9;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    ${uploadFile.fileName}
                                                </a>
                                                <br/>
                                            </#list>
                                        </td>
                                    </tr>
                                </#if>
                            </#if>
                            <#if goodBean.proposedExternalGoodComment??>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                    Комментарий
                                </td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:9px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word">
                                ${goodBean.proposedExternalGoodComment}
                                </td>
                            </tr>
                            </#if>
                            <#--Необходимо оставить пустым, здесь border-bottom:1px, неверная верстка-->
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td class="property" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#fafafa;border-bottom:1px solid #e5e5e5;border-bottom-left-radius:3px;border-collapse:collapse!important;border-left:1px solid #e5e5e5;border-right:1px solid #e5e5e5;color:#72797f;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:15px;padding-right:10px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word"></td>
                                <td class="value" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #e5e5e5;border-bottom-right-radius:3px;border-collapse:collapse!important;border-right:1px solid #e5e5e5;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;padding-bottom:16px;padding-left:25px;padding-right:25px;padding-top:9px;text-align:left;vertical-align:top;word-wrap:break-word"></td>
                            </tr>
                            </tbody>
                        </table>
                    </th>
                    <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                </tr>
            </table>
        </th>
    </tr></tbody></table>
</#macro>