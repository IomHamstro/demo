<#setting locale="ru_RU">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Новый заказ в Юлмарт</title>
<#include "orderScheme.ftl"/>
</head>
<body style="width:100% !important;min-width:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;margin:0;padding:0;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;background:#fff;color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;text-align:left;line-height:1.2;">
<center data-parsed="" style="width: 100%;
    min-width: 584px;
    display:block;">
    <table class="body" border="0" cellspacing="0" cellpadding="0" width="600" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;margin:0;line-height:1.2;background:#fff;height:100%;width:600px;"><tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
        <td align="center" valign="top" width="600" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:600px;">
            <table class="container float-center" border="0" cellspacing="0" cellpadding="0" width="600" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:inherit;width:600px;margin:0 auto;"><tr style="padding: 0;
    vertical-align: top;
    text-align: left;"><td width="600" align="center" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:600px;">
                <table class="spacer" border="0" cellspacing="0" cellpadding="0" width="600" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:600px;"><tr style="padding: 0;
    vertical-align: top;
    text-align: left;"><td height="15" width="600" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;mso-line-height-rule:exactly;width:600px;height:15px;">&nbsp;</td></tr></table>
                <table border="0" cellspacing="0" cellpadding="0" width="544" align="center" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:544px;"><tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                    <td valign="top" width="544" height="130" background="https://fast.ulmart.ru/pics/ryady/_headbg.png" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:544px;height:130px;">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:408pt;height:97.5pt;">
                            <v:fill type="frame" src="https://fast.ulmart.ru/pics/ryady/_headbg.png" ></v:fill>
                            <v:textbox inset="0,0,0,0">
                        <![endif]-->
                        <table border="0" cellspacing="0" cellpadding="0" width="544" height="130" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:544px;height:130px;"><tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                            <td width="306" height="130" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:306px;height:130px;">&nbsp;</td>
                            <td width="200" height="130" align="right" class="text-right fontbold head-text headtitle" style="font-weight:bold;text-align:right;box-sizing:border-box;color:#fff;font-family:Arial, Helvetica, sans-serif;padding:15px 0 0;margin:0;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;display:block;width:200px;height:130px;">
                                <span class="font13" style="font-size: 13px;">&nbsp;<br>&nbsp;<br></span>Информационное<br>письмо
                            </td>
                            <td width="38" height="130" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:38px;height:130px;min-height:130px;">&nbsp;</td>
                        </tr></table>
                        <!--[if gte mso 9]>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr></table>
                <table align="center" width="544" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:544px;"><tbody><tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                    <td valign="top" background="https://fast.ulmart.ru/pics/ryady/c-left-shadow.png" width="7" style="font-size:0;color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:7px;height:600px;font-size:0;">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:5.3pt;height:450pt;">
                            <v:fill type="frame" src="https://fast.ulmart.ru/pics/ryady/c-left-shadow.png" ></v:fill>
                            <v:textbox inset="0,0,0,0">
                                &nbsp;
                                <!--[if gte mso 9]>
                                </v:textbox>
                                </v:rect>
<![endif]-->
                    </td>
                    <td valign="top" bgcolor="#f4f4f4" width="30" style="font-size:0;color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:30px;height:600px;font-size:0;">&nbsp;</td>
                    <td valign="top" bgcolor="#f4f4f4" width="470" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:470px;height:600px;">
                         
                        <table class="content-body" width="470" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:470px;min-width:470px;"><tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                            <td width="470" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:470px;">
                                <p class="font5" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:center;line-height:1.2;font-size:5px;">&nbsp;</p>
                                <p style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:center;line-height:1.2;"><img src="https://fast.ulmart.ru/pics/ryady/dotline.png" alt="dotline" height="7" width="470" style="outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:470px;max-width:470px; height:7px;clear:both;display:block;"></p>

                                <h1 class="fontbold font16 special-font" style="color:#470171;font-family:Arial, Helvetica, sans-serif;font-weight:bold;padding:0;margin:0 0 18px;text-align:left;line-height:1.2;font-size:16px;">${name!}</h1>

                                <p class="font5" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:center;line-height:1.2;font-size:5px;"> </p>

                                <p class="text-left font14" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:left;line-height:1.2;font-size:14px;">Ваш заказ <span class="font18 special-font fontbold" style="font-weight:bold;font-size:18px;color:#470171;">№ ${orderId}</span> от ${creationDate} принят.</p>

                                <p class="text-left font13" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:left;line-height:1.2;font-size:13px;">Счет на оплату и доверенность для получения заказа отправлены на e-mail, указанный в Личном Кабинете.</p>

                                <p class="text-left font13" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:left;line-height:1.2;font-size:13px;">Предварительная стоимость вашего заказа: <strong>${orderPriceTotal?string.number} ₽</strong><br>
                                    В ближайшее время с Вами обязательно свяжется оператор и уточнит детали заказа, либо вы можете самостоятельно позвонить по номеру телефона: 8 800 3333 800</p>

                                <p class="text-left font13 special-font fontbold" style="color:#470171;font-family:Arial, Helvetica, sans-serif;font-weight:bold;padding:0;margin:0 0 18px;text-align:left;line-height:1.2;font-size:13px;">Состав заказа:</p>

                                <#list goods as good>
                                <div class="text-left font13" style="font-size: 13px;">
                                    <span class="list-style">• </span>${good.name}<span class="etc">:</span> <span class="price special-font fontbold" style="font-weight:bold;color:#470171;">${good.amount}х ${good.price?string.number} ₽</span>
                                </div>
                                </#list>

                                <p class="font5" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:center;line-height:1.2;font-size:5px;"> </p>

                                <p style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:center;line-height:1.2;"><img src="https://fast.ulmart.ru/pics/ryady/dotline.png" alt="dotline" height="7" width="470" style="outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
    width: 470px; height:7px;
    max-width: 470px;
    clear: both;
    display: block;"></p>

                                <p class="text-left font13 warning" style="color:#e72e2e;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:left;line-height:1.2;font-size:13px;">Пожалуйста, при обращении к администрации Оптоклуба «РЯДЫ»
                                    ОБЯЗАТЕЛЬНО указывайте <strong class="fontbold" style="font-weight: bold;">номер Вашего заказа - № ${orderId}.</strong></p>

                                <p class="text-left font13 special-font" style="color:#470171;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:left;line-height:1.2;font-size:13px;">Спасибо за покупку!</p>

                                <p style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0 0 18px;text-align:center;line-height:1.2;"><img src="https://fast.ulmart.ru/pics/ryady/dotline.png" alt="dotline" height="7" width="470" style="outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
    width: 470px; height:7px;
    max-width: 470px;
    clear: both;
    display: block;"></p>
                            </td>
                        </tr></table>
                    </td>
                    <td valign="top" bgcolor="#f4f4f4" width="30" style="font-size:0;color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:30px;height:600px;font-size:0;">&nbsp;</td>
                    <td valign="top" background="https://fast.ulmart.ru/pics/ryady/c-right-shadow.png" width="7" style="font-size:0;color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:7px;height:600px;font-size:0;">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:5.3pt;height:450pt;">
                            <v:fill type="frame" src="https://fast.ulmart.ru/pics/ryady/c-right-shadow.png" ></v:fill>
                            <v:textbox inset="0,0,0,0">
                                &nbsp;
                                <!--[if gte mso 9]>
                                </v:textbox>
                                </v:rect>
<![endif]-->
                        &nbsp;
                        <!--[if gte mso 9]>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr></tbody></table>
                <table border="0" cellspacing="0" cellpadding="0" width="590" height="385" align="center" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:590px;height:385px;"><tbody><tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                    <td width="590" height="385" background="https://fast.ulmart.ru/pics/ryady/mail-body2.png" align="center" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:590px;height:385px;">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:441.8pt;height:288.8pt;">
                            <v:fill type="frame" src="https://fast.ulmart.ru/pics/ryady/mail-body2.png" ></v:fill>
                            <v:textbox inset="0,0,0,0">
                        <![endif]-->
                        <table class="email-body" width="470" height="385" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:470px; height:385px;"><tbody>
                        <tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                            <td colspan="3" width="470" height="110" style="font-size:1px;color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:470px;height:110px;">&nbsp;</td>
                        </tr>
                        <tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                            <td valign="top" width="200" height="145" class="text-left font14" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:5px 0 0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;font-size:14px;width:200px;height:145px;">
                                <i>От:</i><br><b>Оптоклуба «РЯДЫ»</b>
                            </td>
                            <td colspan="2" width="270" height="145" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:270px;height:145px;">&nbsp;</td>
                        </tr>
                        <tr style="padding: 0;
    vertical-align: top;
    text-align: left;">
                            <td colspan="2" width="325" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;width:325px;">&nbsp;</td>
                            <td valign="top" class="text-left font14" width="145" style="color:#404040;font-family:Arial, Helvetica, sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.2;word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;font-size:14px;width:145px;">
                                <i>Кому:</i><br>
                                Покупателю<br><b>Оптоклуба «РЯДЫ»</b>
                            </td>
                        </tr>
                        </tbody></table>
                        <!--[if gte mso 9]>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr></tbody></table>
            </td></tr></table>
        </td>
    </tr></table>
</center>
<!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none; white-space:nowrap; font:15px courier; line-height:0;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>