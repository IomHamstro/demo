<#-- @ftlvariable name="orderPrice" type="java.lang.Long" -->
<#-- @ftlvariable name="orderPriceTotal" type="java.lang.Long" -->
<#-- @ftlvariable name="xxlBonus" type="java.lang.Long" -->
<#-- @ftlvariable name="goods" type="java.util.List<ru.ulmart.domain.CartGoodInfo>" -->
<#-- @ftlvariable name="tspGoods" type="java.util.List<ru.ulmart.domain.cart.CartTspGoodInfo>" -->
<#-- @ftlvariable name="telecomGoods" type="java.util.List<ru.ulmart.domain.telecom.CartTelecomInfo>" -->
<#-- @ftlvariable name="checkoutTentCartGoodInfos" type="java.util.List<ru.ulmart.domain.CartGoodInfo>" -->
<#-- @ftlvariable name="checkoutUlmartDriveCartGoodInfos" type="java.util.List<ru.ulmart.domain.CartGoodInfo>" -->
<#-- @ftlvariable name="deliveryCity" type="java.lang.String" -->
<#-- @ftlvariable name="paymentType" type="java.lang.String" -->
<#-- @ftlvariable name="deliveryType" type="boolean" -->
<#-- @ftlvariable name="tariff" type="java.lang.String" -->
<#-- @ftlvariable name="serviceCode" type="java.lang.String" -->
<#-- @ftlvariable name="deliveryAddress" type="java.lang.String" -->
<#-- @ftlvariable name="deliveryCost" type="java.math.BigDecimal" -->
<#-- @ftlvariable name="deliveryDate" type="java.util.Date" -->
<#-- @ftlvariable name="postWorktime" type="java.lang.String" -->
<#-- @ftlvariable name="postType" type="java.lang.String" -->
<#-- @ftlvariable name="shopCatalogAddress" type="java.lang.String" -->
<#-- @ftlvariable name="storageDueToDate" type="java.lang.String" -->
<#-- @ftlvariable name="storageUntilDate" type="java.lang.String" -->
<#-- @ftlvariable name="phone" type="java.lang.String" -->
<#-- @ftlvariable name="postOffice" type="java.lang.String" -->

<#include "image.ftl"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Новый заказ в Юлмарт</title>
    <meta name="viewport" id="viewport" content="width=device-width, user-scalable=1">
    <link rel="shortcut icon" href="#"/>
    <link rel="stylesheet" href="${url}/resources/s/reset.css"/>
    <link rel="stylesheet" href="${url}/resources/s/style.css"/>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${url}/resources/s/style-ie78.css"/>
    <![endif]-->
<#if orderId?? && serviceType?? && agentId??>
    <#assign orderLink="https://www.ulmart.ru/cabinet/orders?orderId=${orderId}&type=${serviceType}&agentId=${agentId}"/>
    <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                    "@type": "Order",
                    "merchant": {
                        "@type": "Organization",
                        "name": "ЗАО Юлмарт"
                    },
                "orderNumber": "${orderId}",
                "priceCurrency": "RUR",
                "price": "${orderPrice?string.number}",
                "acceptedOffer": [
                    <#list goods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "${good.name}",
                                "url": "https://wishni.ulmart.ru/coupons/good/${good.goodId}",
                                "image": "<@imageUrl good.photoId!good.goodId 'large'/>",
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "${good.amount}"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>

                ],
                "url": "${orderLink}",
                "priceSpecification": [
                    {
                        "@type": "UnitPriceSpecification",
                        "price": "${orderPrice?string.number}",
                        "priceCurrency": "RUR"
                    }
                ],
                "orderStatus": "http://schema.org/OrderProcessing"
            }

    </script>
</#if>
</head>
<body>
<div style="padding:0 30px;">
    <div style="width:100%;margin-bottom:35px;">
			<span style="float: right; font: 16px Arial; color: #ffffff; margin-top:15px;">
				<span class="wmi-callto"><small style="position:relative; top:-.2em;">(${phonePrefix})</small> <span
                        style="color:#fa6676; font-size:25px;">${phoneSuffix}</span></span>
			</span>
        <div style="padding:10px 0 8px;background: #9e092c;">
            <img src="${url}/resources/img/theme_coupons/logo2.png">
        </div>
        <div style="width: 100%; background-color: rgb(215, 215, 215); height:1px;"></div>
    </div>
    <div style="font-size: 1.077em; line-height: 1.43; margin:35px 0;">
        <p style="margin:0 0 1.15em;">Уважаемый (ая) ${name!}, благодарим Вас за то, что Вы воспользовались услугами системы электронного резервирования <a
                href="https://wishni.ulmart.ru/" target="_blank">wishni.ulmart.ru</a></p>
        <p style="margin:0 0 1.15em;">Номер резерва: <b style="font-weight:bold;"><span class="wmi-callto">${orderId}</span></b> от ${creationDate}г</p>
        <table width="100%" cellspacing="0" cellpadding="3px" border="0" style="font-size:100%; margin-bottom: 1.15em; margin-left: -10px; margin-top: 0.385em;">
            <tbody>
            <tr>
                <td width="5%" align="left" style="padding:5px 10px; font-weight:bold;">Артикул</td>
                <td width="60%" align="left" style="padding:5px 10px; font-weight:bold;">Наименование купона/ваучера</td>
                <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Кол-во</td>
                <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Цена</td>
                <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Сумма</td>
            </tr>
            <tr>
                <td colspan="6" style="border-top:1px solid #E4E4E4; height:5px;"></td>
            </tr>

            <#list goods as good>
            <tr>
                <#assign sum = good.price * good.amount>
                <#assign bonus = good.amount * good.xxlBonus!0>

                <td aling="left" style="padding:5px 10px;">${good.goodId}</td>
                <td aling="left" style="padding:5px 10px;"><a href="https://wishni.ulmart.ru/coupons/good/${good.goodId}">${good.name}</a></td>
                <td align="right" style="padding:5px 10px;">${good.amount}</td>
                <td align="right" style="padding:5px 10px;">${good.price?string.number}</td>
                <td align="right" style="padding:5px 10px;">${sum?string.number}</td>
            </tr>
            </#list>


            <tr>
                <td colspan="6" style="border-bottom:1px solid #E4E4E4;height:5px;"></td>
            </tr>

            <tr>
                <td colspan="2" style="padding:5px 10px;">&nbsp;</td>
                <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Итого к оплате:</td>
                <td align="right" style="padding:5px 10px;"><b>${orderPriceTotal?string.number} р.</b></td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>



        <ul style="margin: 0.77em 0 1.15em;">
            <li style="margin: 0.385em 0;">Когда можно забрать: <b style="font-weight:bold;">прямо сейчас</b></li>
            <li style="margin: 0.385em 0;">Срок хранения: <b style="font-weight:bold;">${storageUntilDate!}</b></li>
        <#if phone?? && phone != "">
            <li style="margin: 0.385em 0;">Телефон: <b style="font-weight:bold;"><span class="wmi-callto">+7 ${phone}</span></b></li></#if>
        </ul>

    <#if yandexRatingId?? && yandexMarketRating??>
        <a href="https://market.yandex.ru/shop/${yandexRatingId}/reviews?from=${yandexRatingId}" target="_blank">
            <img src="${url}/resources/img/ya.market-${yandexMarketRating}.png" width="310" height="29"/>
        </a>
    </#if>
        <div style="width: 100%; text-align: right; border-top:1px solid rgb(242, 242, 242); padding:5px 0; font-style:italic;">
            <span style="font-weight: bold;">Мы знаем, что у Вас есть выбор. Спасибо, что выбрали нас</span>
            <br/>
            <em>Команда Юлмарт</em>
        </div>
    </div>
</div>
</body>
</html>
