<#-- @ftlvariable name="url" type="java.lang.String" -->
<#-- @ftlvariable name="digitalOrderDetail" type="ru.ulmart.domain.DigitalESDOrderInfo" -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>#</title>

        <link rel="shortcut icon" href="#" />
        <link rel="stylesheet" href="${url}/resources/s/reset.css" />
        <link rel="stylesheet" href="${url}/resources/s/style.css" />
        <!--[if lt IE 9]>
        <link rel="stylesheet" href="${url}/resources/s/style-ie78.css" />
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" src="${url}/resources/j/jquery.custominput.js"></script>
        <script type="text/javascript" src="${url}/resources/j/scroll-toolbar.js"></script>
        <script type="text/javascript" src="${url}/resources/j/jquery.interface.js"></script>
        <style type="text/css">.g-hidden { display:none !important; }</style>
    </head>
    <body style="background:#fff;color:#000;">
        <div style="padding:0 30px;">
            <div style="width:100%;margin-bottom:35px;">
                <span style="float: right; font: 16px Arial; color: rgb(118, 122, 123); margin-top:15px;">
                    <span class="wmi-callto"><small style="position:relative; top:-.2em;">(${phonePrefix})</small> <span style="color:#CC0000; font-size:25px;">${phoneSuffix}</span></span>
                </span>
                <div style="padding:10px 0 8px;">
                    <img src="${url}/resources/desktop.blocks/b-head-logo/ulmart-logo.png">
                </div>
                <div style="width: 100%; background-color: rgb(215, 215, 215); height:1px;"></div>
            </div>
            <div style="font-size: 1.077em; line-height: 1.43; margin:35px 0;">
                <p style="margin:0 0 1.15em;">Уважаемый (ая) ${name!}, благодарим Вас за то, что Вы воспользовались услугами Кибермаркета <a href="https://www.ulmart.ru/" target="_blank">www.ulmart.ru</a></p>
                <p style="margin:0 0 1.15em;"><span style="font-weight: bold;">Заказ:</span> <b style="font-weight:bold;"><span class="wmi-callto">${digitalOrderDetail.id}</span></b> от ${digitalOrderDetail.formattedDate}г.</p>
                <table width="100%" cellspacing="0" cellpadding="3px" border="0" style="font-size:100%; margin-bottom: 1.15em; margin-left: -10px; margin-top: 0.385em;">
                    <tbody>
                        <tr>
                            <td width="60%" align="left" style="padding:5px 10px; font-weight:bold;">Наименование товара</td>
                            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Кол-во</td>
                            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Цена</td>
                            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Сумма</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border-top:1px solid #E4E4E4; height:5px;"></td>
                        </tr>
                        <#list digitalOrderDetail.items as good>
                        <tr>
                            <td aling="left" style="padding:5px 10px;">
                                <#assign isBook = good.partnerId == 'DI_BOOKMATE'/>
                                <#assign isSoftware = good.partnerId = 'SOFTKEY'/>
                                <#if isSoftware>
                                    <#assign href='/software/'+good.subscriptionId/>
                                <#else>
                                    <#assign href='/digital/'+isBook?string("bookmate","zvooq")+'/subscription?id=' + good.subscriptionId/>
                                </#if>
                                <a href="https://www.ulmart.ru${href}" target="_blank">${good.description}</a>
                            </td>
                            <td align="right" style="padding:5px 10px;">${good.count}</td>
                            <td align="right" style="padding:5px 10px;">${good.price}</td>
                            <td align="right" style="padding:5px 10px;">${good.price * good.count}</td>
                        </tr>
                        </#list>
                        <tr>
                            <td colspan="4" style="border-bottom:1px solid #E4E4E4;height:5px;"></td>
                        </tr>
                        <tr>
                            <td style="padding:5px 10px;">&nbsp;</td>
                            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Итого:</td>
                            <td align="right" style="padding:5px 10px;"><b style="font-weight:bold;">${digitalOrderDetail.sum} р.</b></td>
                        </tr>
                        <tr>
                            <#assign bonus= 0/>
                            <#if digitalOrderDetail.bonus??>
                                <#assign bonus = digitalOrderDetail.bonus/>
                            </#if>
                            <td style="padding:5px 10px;">&nbsp;</td>
                            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Оплачено бонусами:</td>
                            <td align="right" style="padding:5px 10px;"><b style="font-weight:bold;">${bonus} р.</b></td>
                        </tr>
                        <tr>
                            <td style="padding:5px 10px;">&nbsp;</td>
                            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Итого к оплате:</td>
                            <td align="right" style="padding:5px 10px;"><b style="font-weight:bold;">${digitalOrderDetail.sum - bonus} р.</b></td>
                        </tr>
                    </tbody>
                </table>
                <p style="margin:0 0 1.15em;">Оплатить заказ Вы можете в течение 24 часов, перейдя на страницу заказа в <a href="https://www.ulmart.ru/cabinet/orders?type=digital&tab=completed" target="_blank">Личном Кабинете</a>.</p>
                <ul style="margin: 0.77em 0 1.15em;">
                    <li style="margin: 0.385em 0;">Способ оплаты: <b style="font-weight:bold;">онлайн-оплата.</b></li>
                    <li style="margin: 0.385em 0;">Способ доставки: <b style="font-weight:bold;">электронная доставка.</b></li>
                    <li style="margin: 0.385em 0;">Срок доставки: <b style="font-weight:bold;">моментальная доставка в Личный Кабинет после оплаты.</b></li>
                    <li style="margin: 0.385em 0;">Телефон: <b style="font-weight:bold;"><span class="wmi-callto">${phone}</span></b></li>
                </ul>

                <div style="width: 100%; text-align: right; border-top:1px solid rgb(242, 242, 242); padding:5px 0; font-style:italic;">
                    <span style="font-weight: bold;">Мы знаем, что у Вас есть выбор. Спасибо, что выбрали нас</span>
                    <br/>
                    <em>Команда Юлмарт</em>
                </div>
            </div>
        </div>
    </body>
</html>