<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>#</title><meta name="viewport" id="viewport" content="width=device-width, user-scalable=1">

    <link rel="shortcut icon" href="#" />
    <link rel="stylesheet" href="${url}/resources/s/reset.css" />
    <link rel="stylesheet" href="${url}/resources/s/style.css" />
</head>
<body>
<div style="padding:0 30px;">
    <div style="width:100%;margin-bottom:35px;">
			<span style="float: right; font: 16px Arial; color: rgb(118, 122, 123); margin-top:0px;">
				<span class="wmi-callto"><small style="position:relative; top:-.2em;">(${phonePrefix})</small> <span style="color:#CC0000; font-size:25px;">${phoneSuffix}</span></span>
			</span>
        <div style="padding:10px 0 8px;">
            <img src="${url}/resources/desktop.blocks/b-head-logo/ulmart-logo.png">
        </div>
        <div style="width: 100%; background-color: rgb(215, 215, 215); height:1px;"></div>
    </div>
    <div style="font-size: 1.077em; line-height: 1.43; margin:35px 0;">
    ${message}
    </div>
    <div style="font-size:1em; line-height:1.43; color: rgb(190, 190, 190); border-top:3px solid rgb(201, 201, 201); padding:10px 0 13px;">
        <i>Служба технической поддержки компании Юлмарт</i>
        <br/>
        <a href="mailto:sales.msk@ulmart.ru" target="_blank">sales.msk@ulmart.ru</a>
        <br/>
        <a href="https://www.ulmart.ru/" target="_blank">www.ulmart.ru</a>
        <br/>
        <i>
            <span class="wmi-callto">(${phonePrefix}) ${phoneSuffix}</span> - многоканальная линия
            <br/>
            Письмо создано автоматической системой оповещения
            <br/>
            Дата и время создания сообщения ${date}
        </i>
    </div>
</div>
</body>
</html>