---
subject: My Email Subject
---
<#-- @ftlvariable name="mail" type="ru.ulmart.mail.dto.bargain.FlashPromoCodeMail" -->
<#-- @ftlvariable name="mainUrl" type="java.lang.String" -->
<container>
    <!--[if gte mso 9]>
    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;">
        <v:fill type="tile" src="${mainUrl}/resources/img/mail/flashPromoCode/pattern.jpg" color="#ffffff" />
        <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
    <![endif]-->
    <div>
        <img src="${mainUrl}/resources/img/mail/flashPromoCode/banner.jpg" />
        <row class="header">
            <columns small="12" large="12">
                <h1 class="mail-title bold">Поздравляем! Вы сделали первый шаг к тому, чтобы принять участие во флеш-распродаже!</h1>
                <center>
                    <row class="promocode">
                        <columns small="7" large="7" class="promocode-title">
                            Ваш промокод:
                        </columns>
                        <columns small="5" large="5" class="promocode-num">${mail.promoCode}</columns>
                    </row>
                </center>
            </columns>
        </row>
        <row class="main-content">
            <columns small="12" large="12">
                <h2 class="h2 uppercase">
                    Промокод действителен на товар:
                </h2>
                <p class="good-name">
                    <a href="${bargainUrl}${mail.goodUrl}">${mail.goodName}</a>
                </p>
                <p class="price">
                    1 шт.<br />
                    <del class="red">${mail.goodInitialPrice}.-</del><br />
                    <span class="price-result">${mail.goodRealPrice}.-</span>
                </p>
                <p class="profit semibold">
                    <span class="uppercase">Ваша выгода:</span> <span class="red">${mail.profit}.-</span>
                </p>
                <p>
                    Напоминаем, что указанная цена действительна только до <span class="red">${mail.formattedFlashDateTo}</span><br />
                    Спешите экономить!
                </p>
            </columns>
        </row>
        <row class="main-content">
            <columns small="12" large="12" class="captain-contain">
                <table>
                    <tr>
                        <td class="captain-ahead">
                            <h2 class="h2 uppercase">
                                Что дальше?
                            </h2>
                            <ul>
                                <li>Выберите товар, участвующий в акции..</li>
                                <li>Добавьте его в корзину.</li>
                                <li>Нажмите кнопку «Оформить заказ».</li>
                                <li>На этапе подтверждения введите промокод в окне «промокод» в окне заказа.</li>
                                <li>Поздравляем с выгодной покупкой!</li>
                            </ul>
                        </td>
                        <td class="captain">
                            <img src="${mainUrl}/resources/img/mail/flashPromoCode/captain.png" />
                        </td>
                    </tr>
                </table>
            </columns>
        </row>
        <row class="main-conten">
            <columns small="12" large="12">

                <h2 class="h2 uppercase">
                    Как сэкономить ещё больше? Загляните в другие наши разделы!
                </h2>
                <ul>
                    <li>«Распродажа». Тысячи товаров со скидками до 15 %!</li>
                    <li>«Сток». Скидок много не бывает: до –75 % от регулярной цены на самые востребованные товары!</li>
                    <li>«Товарные каталоги».Интересные цены в интересной форме: интерьерные решения, идеи для ремонта, спорт и активный отдых в тематических подборках</li>
                </ul>

                <row class="stock-sections">
                    <columns small="4" large="4">
                        <table class="stock-tile low">
                            <tr>
                                <td class="ico">
                                    <a href="${bargainUrl}/stock/sale">
                                        <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-stock-1.png" width="32" />
                                    </a>
                                </td>
                                <td class="name">
                                    <a href="${bargainUrl}/stock/sale">
                                        <span class="uppercase bold">распродажа</span><br />
                                        скидки до 15%
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </columns>
                    <columns small="4" large="4">
                        <table class="stock-tile medium">
                            <tr>
                                <td class="ico">
                                    <a href="${bargainUrl}/stock/stock">
                                        <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-stock-2.png" />
                                    </a>
                                </td>
                                <td class="name">
                                    <a href="${bargainUrl}/stock/stock">
                                        <span class="uppercase bold">СТОК</span><br />
                                        скидки до 75%
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </columns>
                    <columns small="4" large="4">
                        <table class="stock-tile high">
                            <tr>
                                <td class="ico">
                                    <a href="${bargainUrl}/dynamic/page/promo_catalogs">
                                        <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-stock-3.png" />
                                    </a>
                                </td>
                                <td class="name">
                                    <a href="${bargainUrl}/dynamic/page/promo_catalogs">
                                        <span class="uppercase bold">ТОВАРНЫЕ каталоги</span><br />
                                        скидки до 90%
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </columns>
                </row>

                <h2 class="h2 uppercase semibold h2-margin-small-bot">
                    Есть вопросы?
                </h2>
                <p>
                    Посетите наш раздел FAQ, где собраны ответы на самые популярные вопросы наших клиентов. Вы поставщик и хотите участвовать в нашем проекте? <br />
                    Вам сюда: stock@ulmart.ru
                </p>
            </columns>
        </row>

        <table class="catalog" width="100%">
            <tr>
                <td class="catalog-td">
                    <table class="section" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/auto">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-1-auto.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/auto">
                                    Автотовары
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/digital">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-2-media.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/digital">
                                    Цифровой контент
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/computers_notebooks">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-3-notebook.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/computers_notebooks">
                                    Компьютеры
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/93299">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-4-washer.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/93299">
                                    Бытовая техника
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/93306">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-5-cosmetic.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/93306">
                                    Красота и здоровье
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/93416">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-6-children.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/93416">
                                    Детские товары
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/country_house_diy">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-7-house.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/country_house_diy">
                                    Дом и ремонт
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="${mainUrl}/catalog/tourism_goods">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-8-sport.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="${mainUrl}/catalog/tourism_goods">
                                    Спорт и туризм
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="catalog-td">
                    <table class="section">
                        <tr>
                            <td class="ico">
                                <a href="https://thelabels.ulmart.ru/">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-9-cloth.png" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">
                                <a href="https://thelabels.ulmart.ru/">
                                    Одежда
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table class="links" width="100%">
            <tr>
                <td>
                    <a href="http://www.akit.ru/1234567/"><img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-akit.png" /></a>
                </td>
                <td class="text-right">
                    <a href="https://itunes.apple.com/ru/app/ulmart/id867679958">
                        <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-appstore.png" />
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="https://play.google.com/store/apps/details?id=ru.ulmart.mobapp&referrer=utm_source%3Dulmart%26utm_medium%3Dicon">
                        <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-googleplay.png" />
                    </a>
                </td>
            </tr>
        </table>

        <row class="social">
            <columns small="12" large="12">
                <center>
                    <table class="soc-table">
                        <tr>
                            <td class="soc-item">
                                <a href="https://vk.com/ulmart"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-vk.png" /></a>
                            </td>
                            <td class="soc-item">
                                <a href="https://www.facebook.com/I.love.Ulmart"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-fb.png" /></a>
                            </td>
                            <td class="soc-item">
                                <a href="https://www.odnoklassniki.ru/iloveulmart"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-ok.png" /></a>
                            </td>
                            <td class="soc-item">
                                <a href="https://my.mail.ru/community/ulmart_ru/"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-mm.png" /></a>
                            </td>
                            <td class="soc-item">
                                <a href="https://www.twitter.com/Ulmart_ru"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-tw.png" /></a>
                            </td>
                            <td class="soc-item">
                                <a href="https://www.instagram.com/iloveulmart"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-ig.png" /></a>
                            </td>
                            <td class="soc-item">
                                <a href="https://www.youtube.com/user/ulmartofficial"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-yt.png" /></a>
                            </td>
                        </tr>
                    </table>
                </center>

            </columns>
        </row>

        <table width="100%" class="annotation">
            <tr>
                <td>
                    Цена, в т. ч. размер скидки, указаны в рублях с учётом НДС и действительны до ${mail.formattedFlashDateTo} при вводе промокода «${mail.promoCode}» на этапе оформления заказа в поле «промокод». Количество товара ограниченно.  XXXL-бонусы не начисляются. Приведённое изображение может отличаться от внешнего вида товара в центрах исполнения заказов. Существенная информация — у консультантов в городских центрах исполнения заказов Юлмарт и на сайте www.ulmart.ru. Категория сайта 18+Непубличное акционерное общество «Юлмарт», 197227, Санкт-Петербург, пр. Сизова, д. 2, лит. А, ОГРН 1089848006423; ООО «Юлмарт РСК», 197227, Санкт-Петербург, пр. Сизова, д. 2, лит. А, ОГРН 1117847607900. Предложение действительно для зарегистрированных покупателей - физических лиц.
                </td>
            </tr>
        </table>
    </div>
    <!--[if gte mso 9]>
    </v:textbox>
    </v:rect>
    <![endif]-->
</container>
