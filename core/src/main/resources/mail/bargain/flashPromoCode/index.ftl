
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
  </head>
  <body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important">
    <style>@media only screen{html{min-height:100%;background:#f3f3f3}}@media only screen and (max-width:624px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}.small-text-center{text-align:center!important}.small-text-left{text-align:left!important}.small-text-right{text-align:right!important}}@media only screen and (max-width:624px){.hide-for-large{display:block!important;width:auto!important;overflow:visible!important;max-height:none!important;font-size:inherit!important;line-height:inherit!important}}@media only screen and (max-width:624px){table.body table.container .hide-for-large,table.body table.container .row.hide-for-large{display:table!important;width:100%!important}}@media only screen and (max-width:624px){table.body table.container .callout-inner.hide-for-large{display:table-cell!important;width:100%!important}}@media only screen and (max-width:624px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:624px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .column,table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:24px!important;padding-right:24px!important}table.body .column .column,table.body .column .columns,table.body .columns .column,table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .column,table.body .collapse .columns{padding-left:0!important;padding-right:0!important}td.small-1,th.small-1{display:inline-block!important;width:8.33333%!important}td.small-2,th.small-2{display:inline-block!important;width:16.66667%!important}td.small-3,th.small-3{display:inline-block!important;width:25%!important}td.small-4,th.small-4{display:inline-block!important;width:33.33333%!important}td.small-5,th.small-5{display:inline-block!important;width:41.66667%!important}td.small-6,th.small-6{display:inline-block!important;width:50%!important}td.small-7,th.small-7{display:inline-block!important;width:58.33333%!important}td.small-8,th.small-8{display:inline-block!important;width:66.66667%!important}td.small-9,th.small-9{display:inline-block!important;width:75%!important}td.small-10,th.small-10{display:inline-block!important;width:83.33333%!important}td.small-11,th.small-11{display:inline-block!important;width:91.66667%!important}td.small-12,th.small-12{display:inline-block!important;width:100%!important}.column td.small-12,.column th.small-12,.columns td.small-12,.columns th.small-12{display:block!important;width:100%!important}table.body td.small-offset-1,table.body th.small-offset-1{margin-left:8.33333%!important;Margin-left:8.33333%!important}table.body td.small-offset-2,table.body th.small-offset-2{margin-left:16.66667%!important;Margin-left:16.66667%!important}table.body td.small-offset-3,table.body th.small-offset-3{margin-left:25%!important;Margin-left:25%!important}table.body td.small-offset-4,table.body th.small-offset-4{margin-left:33.33333%!important;Margin-left:33.33333%!important}table.body td.small-offset-5,table.body th.small-offset-5{margin-left:41.66667%!important;Margin-left:41.66667%!important}table.body td.small-offset-6,table.body th.small-offset-6{margin-left:50%!important;Margin-left:50%!important}table.body td.small-offset-7,table.body th.small-offset-7{margin-left:58.33333%!important;Margin-left:58.33333%!important}table.body td.small-offset-8,table.body th.small-offset-8{margin-left:66.66667%!important;Margin-left:66.66667%!important}table.body td.small-offset-9,table.body th.small-offset-9{margin-left:75%!important;Margin-left:75%!important}table.body td.small-offset-10,table.body th.small-offset-10{margin-left:83.33333%!important;Margin-left:83.33333%!important}table.body td.small-offset-11,table.body th.small-offset-11{margin-left:91.66667%!important;Margin-left:91.66667%!important}table.body table.columns td.expander,table.body table.columns th.expander{display:none!important}table.body .right-text-pad,table.body .text-pad-right{padding-left:10px!important}table.body .left-text-pad,table.body .text-pad-left{padding-right:10px!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.small-vertical td,table.menu.small-vertical th,table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}table.button.small-expand,table.button.small-expanded{width:100%!important}table.button.small-expand table,table.button.small-expanded table{width:100%}table.button.small-expand table a,table.button.small-expanded table a{text-align:center!important;width:100%!important;padding-left:0!important;padding-right:0!important}table.button.small-expand center,table.button.small-expanded center{min-width:0}}</style>
    <span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span>
    <table class="body" style="Margin:0;background:#f3f3f3;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:100%;line-height:1.4;margin:0;padding:0;text-align:left;vertical-align:top;width:100%">
      <tr style="padding:0;text-align:left;vertical-align:top">
        <td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
          <center data-parsed="" style="min-width:600px;width:100%">
            <table align="center" class="container float-center" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:600px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background:url(http://d5.spb.ru/ulmart/img/pattern.jpg);border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0;text-align:left;vertical-align:top;width:600px;word-wrap:break-word">
                <!--[if gte mso 9]>
                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;">
                    <v:fill type="tile" src="${mainUrl}/resources/img/mail/flashPromoCode/pattern.jpg" color="#ffffff" />
                    <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                <![endif]-->
                <div>
                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/banner.jpg" style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
                    <table class="row header" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:23px;padding-left:24px;padding-right:24px;text-align:left;width:576px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left">
                            <h1 class="mail-title bold" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:21px;font-weight:600;line-height:1.4;margin:19px 51px 8px;margin-bottom:10px;padding:0;text-align:center;text-transform:uppercase;word-wrap:normal">Поздравляем! Вы сделали первый шаг к тому, чтобы принять участие во флеш-распродаже!</h1>
                            <center data-parsed="" style="min-width:528px;width:100%">
                                <table align="center" class="row promocode float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;display:table;float:none;margin:0 auto;padding:0;position:relative;text-align:center;vertical-align:top;width:392px!important"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                                    <th class="promocode-title small-7 large-7 columns first" style="Margin:0 auto;background-color:#ffc706;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;padding-top:14px;text-align:left;width:58.33333%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:20px;font-weight:600;line-height:1.4;margin:0;padding:0;padding-left:30px;text-align:right;text-transform:uppercase">
                                        Ваш промокод:
                                    </th></tr></table></th>
                                    <th class="promocode-num small-5 large-5 columns last" style="Margin:0 auto;background-color:#ffc706;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;padding-top:14px;text-align:left;width:41.66667%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#9043c1;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:20px;font-weight:600;line-height:1.4;margin:0;padding:0;text-align:center">${mail.promoCode}</th></tr></table></th>
                                </tr></tbody></table>
                            </center>
                        </th></tr></table></th>
                    </tr></tbody></table>
                    <table class="row main-content" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:24px;text-align:left;width:576px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left">
                            <h2 class="h2 uppercase" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:17px 0 20px;margin-bottom:10px;padding:0;text-align:left;text-transform:uppercase;word-wrap:normal">
                                Промокод действителен на товар:
                            </h2>
                            <p class="good-name" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:underline">
                                <a href="${bargainUrl}${mail.goodUrl}" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:underline">${mail.goodName}</a>
                            </p>
                            <p class="price" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.6em;margin:0;margin-bottom:10px;padding:0;text-align:left">
                                1 шт.<br>
                                <del class="red">${mail.goodInitialPrice}.-</del><br />
								<span class="price-result">${mail.goodRealPrice}.-</span>
                            </p>
                            <p class="profit semibold" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:600;line-height:1.4;margin:0;margin-bottom:10px;padding:0;text-align:left">
                                <span class="uppercase" style="text-transform:uppercase">Ваша выгода:</span> <span class="red" style="color:red">${mail.profit}.-</span>
                            </p>
                            <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;margin-bottom:10px;padding:0;text-align:left">
                                Напоминаем, что указанная цена действительна только до <span class="red" style="color:red">${mail.formattedFlashDateTo}</span><br>
                                Спешите экономить!
                            </p>
                        </th>
<th class="expander" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th>
                    </tr></tbody></table>
                    <table class="row main-content" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="captain-contain small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:0!important;text-align:left;width:576px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="captain-ahead" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                        <h2 class="h2 uppercase" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:17px 0 20px;margin-bottom:10px;padding:0;text-align:left;text-transform:uppercase;word-wrap:normal">
                                            Что дальше?
                                        </h2>
                                        <ul style="line-height:1.5em;padding-left:10px">
                                            <li>Выберите товар, участвующий в акции..</li>
                                            <li>Добавьте его в корзину.</li>
                                            <li>Нажмите кнопку «Оформить заказ».</li>
                                            <li>На этапе подтверждения введите промокод в окне «промокод» в окне заказа.</li>
                                            <li>Поздравляем с выгодной покупкой!</li>
                                        </ul>
                                    </td>
                                    <td class="captain" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0!important;text-align:left;vertical-align:top;width:77px;word-wrap:break-word">
                                        <img src="${mainUrl}/resources/img/mail/flashPromoCode/captain.png" style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
                                    </td>
                                </tr>
                            </table>
                        </th>
<th class="expander" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th>
                    </tr></tbody></table>
                    <table class="row main-conten" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:24px;text-align:left;width:576px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left">
            
                            <h2 class="h2 uppercase" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:17px 0 20px;margin-bottom:10px;padding:0;text-align:left;text-transform:uppercase;word-wrap:normal">
                                Как сэкономить ещё больше? Загляните в другие наши разделы!
                            </h2>
                            <ul>
                                <li>«Распродажа». Тысячи товаров со скидками до 15 %!</li>
                                <li>«Сток». Скидок много не бывает: до –75 % от регулярной цены на самые востребованные товары!</li>
                                <li>«Товарные каталоги».Интересные цены в интересной форме: интерьерные решения, идеи для ремонта, спорт и активный отдых в тематических подборках</li>
                            </ul>
            
                            <table class="row stock-sections" style="border-collapse:collapse;border-spacing:0;display:table;margin:35px 0 20px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                                <th class="small-4 large-4 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left">
                                    <table class="stock-tile low" style="border-collapse:collapse;border-spacing:0;font-family:Arial,sans-serif;padding:0;text-align:left;vertical-align:top;width:100%">
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#9043c1;border-collapse:collapse!important;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:68px;hyphens:auto;line-height:1.4;margin:0;padding:0;padding-left:13px;padding-right:8px;text-align:left;vertical-align:middle;width:40px;word-wrap:break-word">
                                                <a href="${bargainUrl}/stock/sale" style="Margin:0;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-stock-1.png" width="32" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
                                                </a>
                                            </td>
                                            <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#9043c1;border-collapse:collapse!important;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:68px;hyphens:auto;line-height:1.4;margin:0;padding:0;text-align:left;vertical-align:middle;word-wrap:break-word">
                                                <a href="${bargainUrl}/stock/sale" style="Margin:0;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    <span class="uppercase bold" style="font-weight:700;text-transform:uppercase">распродажа</span><br>
                                                    скидки до 15%
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </th></tr></table></th>
                                <th class="small-4 large-4 columns" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left">
                                    <table class="stock-tile medium" style="border-collapse:collapse;border-spacing:0;font-family:Arial,sans-serif;padding:0;text-align:left;vertical-align:top;width:100%">
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#622589;border-collapse:collapse!important;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:68px;hyphens:auto;line-height:1.4;margin:0;padding:0;padding-left:13px;padding-right:8px;text-align:left;vertical-align:middle;width:40px;word-wrap:break-word">
                                                <a href="${bargainUrl}/stock/stock" style="Margin:0;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-stock-2.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
                                                </a>
                                            </td>
                                            <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#622589;border-collapse:collapse!important;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:68px;hyphens:auto;line-height:1.4;margin:0;padding:0;text-align:left;vertical-align:middle;word-wrap:break-word">
                                                <a href="${bargainUrl}/stock/stock" style="Margin:0;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    <span class="uppercase bold" style="font-weight:700;text-transform:uppercase">СТОК</span><br>
                                                    скидки до 75%
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </th></tr></table></th>
                                <th class="small-4 large-4 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left">
                                    <table class="stock-tile high" style="border-collapse:collapse;border-spacing:0;font-family:Arial,sans-serif;padding:0;text-align:left;vertical-align:top;width:100%">
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#461764;border-collapse:collapse!important;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:68px;hyphens:auto;line-height:1.4;margin:0;padding:0;padding-left:13px;padding-right:8px;text-align:left;vertical-align:middle;width:40px;word-wrap:break-word">
                                                <a href="${bargainUrl}/dynamic/page/promo_catalogs" style="Margin:0;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-stock-3.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
                                                </a>
                                            </td>
                                            <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#461764;border-collapse:collapse!important;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:68px;hyphens:auto;line-height:1.4;margin:0;padding:0;text-align:left;vertical-align:middle;word-wrap:break-word">
                                                <a href="${bargainUrl}/dynamic/page/promo_catalogs" style="Margin:0;color:#ffda11;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                                    <span class="uppercase bold" style="font-weight:700;text-transform:uppercase">ТОВАРНЫЕ каталоги</span><br>
                                                    скидки до 90%
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </th></tr></table></th>
                            </tr></tbody></table>
            
                            <h2 class="h2 uppercase semibold h2-margin-small-bot" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:600;line-height:1.4;margin:17px 0 20px;margin-bottom:8px;padding:0;text-align:left;text-transform:uppercase;word-wrap:normal">
                                Есть вопросы?
                            </h2>
                            <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;margin-bottom:10px;padding:0;text-align:left">
                                Посетите наш раздел FAQ, где собраны ответы на самые популярные вопросы наших клиентов. Вы поставщик и хотите участвовать в нашем проекте? <br>
                                Вам сюда: stock@ulmart.ru
                            </p>
                        </th></tr></table></th>
                    </tr></tbody></table>
            
                    <table class="catalog" width="100%" style="border-collapse:collapse;border-spacing:0;margin:13px 0 10px;padding:0;text-align:left;vertical-align:top">
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;padding-left:12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/auto" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-1-auto.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/auto" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Автотовары
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/digital" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-2-media.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/digital" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Цифровой контент
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/computers_notebooks" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-3-notebook.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/computers_notebooks" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Компьютеры
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/93299" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-4-washer.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/93299" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Бытовая техника
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;padding-right:12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/93306" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-5-cosmetic.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/93306" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Красота и здоровье
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;padding-left:12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/93416" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-6-children.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/93416" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Детские товары
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/country_house_diy" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-7-house.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/country_house_diy" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Дом и ремонт
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/tourism_goods" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-8-sport.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="${mainUrl}/catalog/tourism_goods" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Спорт и туризм
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="catalog-td" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 6px 12px;text-align:left;vertical-align:top;width:20%;word-wrap:break-word">
                                <table class="section" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:105px">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="ico" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#446f85 #446f85 #fff #446f85;border-style:solid;border-width:2px 2px 0 2px;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;height:70px;hyphens:auto;line-height:1.4;margin:0;padding:5px 5px 0;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://thelabels.ulmart.ru/" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-9-cloth.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="name" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-color:#fff #446f85 #446f85 #446f85;border-style:solid;border-width:0 2px 2px 2px;color:#446f85;font-family:Arial,sans-serif;font-size:11px;font-weight:400;height:25px;hyphens:auto;line-height:12px;margin:0;padding:0 5px 3px;text-align:center;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://thelabels.ulmart.ru/" style="Margin:0;color:#446f85;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:12px;margin:0;padding:0;text-align:left;text-decoration:none">
                                                Одежда
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
            
                    <table class="links" width="100%" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:10px 12px;text-align:left;vertical-align:top;word-wrap:break-word">
                                <a href="http://www.akit.ru/1234567/" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-akit.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:inline-block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                            </td>
                            <td class="text-right" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:10px 12px;text-align:right;vertical-align:top;word-wrap:break-word">
                                <a href="https://itunes.apple.com/ru/app/ulmart/id867679958" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-appstore.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:inline-block;max-width:100%;outline:0;text-decoration:none;width:auto">
                                </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://play.google.com/store/apps/details?id=ru.ulmart.mobapp&referrer=utm_source%3Dulmart%26utm_medium%3Dicon" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none">
                                    <img src="${mainUrl}/resources/img/mail/flashPromoCode/icon-googleplay.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:inline-block;max-width:100%;outline:0;text-decoration:none;width:auto">
                                </a>
                            </td>
                        </tr>
                    </table>
            
                    <table class="row social" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-12 large-12 columns first last" style="Margin:0 auto;background-color:#446f85;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0 auto;padding:10px 12px 12px;padding-bottom:16px;padding-left:24px;padding-right:24px;text-align:center;width:576px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:center">
                            <center data-parsed="" style="min-width:528px;width:100%">
                                <table class="soc-table float-center" align="center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:290px!important">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td class="soc-item" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 10px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://vk.com/ulmart" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-vk.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                                        </td>
                                        <td class="soc-item" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 10px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://www.facebook.com/I.love.Ulmart" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-fb.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                                        </td>
                                        <td class="soc-item" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 10px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://www.odnoklassniki.ru/iloveulmart" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-ok.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                                        </td>
                                        <td class="soc-item" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 10px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://my.mail.ru/community/ulmart_ru/" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-mm.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                                        </td>
                                        <td class="soc-item" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 10px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://www.twitter.com/Ulmart_ru" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-tw.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                                        </td>
                                        <td class="soc-item" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 10px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://www.instagram.com/iloveulmart" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-ig.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                                        </td>
                                        <td class="soc-item" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.4;margin:0;padding:0 10px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                            <a href="https://www.youtube.com/user/ulmartofficial" style="Margin:0;color:#000;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;line-height:1.4;margin:0;padding:0;text-align:left;text-decoration:none"><img src="${mainUrl}/resources/img/mail/flashPromoCode/soc-yt.png" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a>
                                        </td>
                                    </tr>
                                </table>
                            </center>
            
                        </th>
<th class="expander" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.4;margin:0;padding:0!important;text-align:center;visibility:hidden;width:0"></th></tr></table></th>
                    </tr></tbody></table>
            
                    <table width="100%" class="annotation" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#446f85;font-family:Arial,sans-serif;font-size:9px;font-weight:400;hyphens:auto;line-height:10px;margin:0;padding:18px 12px 30px;text-align:left;vertical-align:top;word-wrap:break-word">
                                Цена, в т. ч. размер скидки, указаны в рублях с учётом НДС и действительны до ${mail.formattedFlashDateTo} при вводе промокода «${mail.promoCode}» на этапе оформления заказа в поле «промокод». Количество товара ограниченно.  XXXL-бонусы не начисляются. Приведённое изображение может отличаться от внешнего вида товара в центрах исполнения заказов. Существенная информация — у консультантов в городских центрах исполнения заказов Юлмарт и на сайте www.ulmart.ru. Категория сайта 18+Непубличное акционерное общество «Юлмарт», 197227, Санкт-Петербург, пр. Сизова, д. 2, лит. А, ОГРН 1089848006423; ООО «Юлмарт РСК», 197227, Санкт-Петербург, пр. Сизова, д. 2, лит. А, ОГРН 1117847607900. Предложение действительно для зарегистрированных покупателей - физических лиц.
                            </td>
                        </tr>
                    </table>
                </div>
                <!--[if gte mso 9]>
                </v:textbox>
                </v:rect>
                <![endif]-->
            </td></tr></tbody></table>
            
          </center>
        </td>
      </tr>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none;white-space:nowrap;font:15px courier;line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </body>
</html>
