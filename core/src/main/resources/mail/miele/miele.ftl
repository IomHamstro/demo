<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>#</title><meta name="viewport" id="viewport" content="width=device-width, user-scalable=1">

    <link rel="shortcut icon" href="#" />
</head>
<body>


    <#if goodId?? && (goodId?size > 0)>
        <p>Id товара: ${goodId}</p>
    </#if>

    <#if goodName?? && (goodName?size > 0)>
        <p>Наименование товара: ${goodName}</p>
    </#if>

    <#if goodPrice?? && (goodPrice?size > 0)>
        <p>Цена товара: ${goodPrice}</p>
    </#if>

    <#if count?? && (count?size > 0)>
        <p>Количество товара: ${count}</p>
    </#if>

    <#if clientId?? && (clientId?size > 0)>
        <p>Id клиента: ${clientId}</p>
    </#if>

    <#if fio?? && (fio?size > 0)>
        <p>ФИО: ${fio}</p>
    </#if>
    <#if email?? && (email?size > 0)>
        <p>Email: ${email}</p>
    </#if>
    <#if phone?? && (phone?size > 0)>
        <p>Телефон: ${phone}</p>
    </#if>
    <#if town?? && (town?size > 0)>
        <p>Город: ${town}</p>
    </#if>
    <#if street?? && (street?size > 0)>
        <p>Улица: ${street}</p>
    </#if>
    <#if home?? && (home?size > 0)>
        <p>Дом: ${home}</p>
    </#if>
    <#if apartmentNumber?? && (apartmentNumber?size > 0)>
        <p>Квартира: ${apartmentNumber}</p>
    </#if>
    <#if zipCode?? && (zipCode?size > 0)>
        <p>Индекс: ${zipCode}</p>
    </#if>
</body>
</html>