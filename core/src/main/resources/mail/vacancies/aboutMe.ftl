<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>#</title><meta name="viewport" id="viewport" content="width=device-width, user-scalable=1">

    <link rel="shortcut icon" href="#" />
</head>
<body>
    <#if fio?? && (fio?size > 0)>
        <p>ФИО: ${fio}</p>
    </#if>
    <#if email?? && (email?size > 0)>
        <p>Email: ${email}</p>
    </#if>
    <#if phone?? && (phone?size > 0)>
        <p>Телефон: +7 ${phone}</p>
    </#if>
    <#if letter?? && (letter?size > 0)>
        <p>Сопроводительное письмо:</p>
        <p>${letter}</p>
    </#if>
</body>
</html>