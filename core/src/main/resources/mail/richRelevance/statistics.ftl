<#-- @ftlvariable name="mail" type="ru.ulmart.api.services.counters.richRelevance.RichRelevanceMail" -->

<#function getErrorStyle item>
    <#assign errorStyle = "">
    <#if item.error>
        <#assign errorStyle = "background-color:#e36060; font-weight:bold;">
    <#elseif item.warning>
        <#assign errorStyle = "background-color:#ffff00; font-weight:bold;">
    </#if>
    <#return errorStyle/>
</#function>

<html>
<head>
    <title>#</title>
    <meta charset="utf-8">
</head>
<body>
<#if mail.dailyReport>
    <div>Это суточный отчет. После его отправки статистика была сброшена.</div>
</#if>
<div style="<#if mail.connectionError.error>color:red<#elseif mail.connectionError.warning>color:#ffa733</#if>">Ошибок соединения - ${mail.connectionError.count}</div>

<#list mail.cities as city>
    <div>
        <h3 style="font-family: Arial; font-size:13pt; font-weight:bold; <#if city.alarm>color: red</#if>">Город ${city.name}</h3>
        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 600px; font-family: Arial; font-size:10pt; border: 1px solid #666;">
            <tr>
                <th style="width: 50%; padding: 4px 13px; text-align:center; border: 1px solid #666; background-color:#ccc;">Тип ошибки</th>
                <th style="width: 30%; padding: 4px 13px; text-align:center; border: 1px solid #666; background-color:#ccc;">Тип плейсмента</th>
                <th style="width: 20%; padding: 4px 13px; text-align:center; border: 1px solid #666; background-color:#ccc;">Ошибок</th>
            </tr>
            <#list city.items as item>
                <#assign errorStyle = getErrorStyle(item)>
                <tr>
                    <td style="width: 50%; padding: 4px 13px; text-align:left; border: 1px solid #666; ${errorStyle}">
                        ${item.errorType}
                    </td>
                    <td style="width: 30%; padding: 4px 13px; text-align:center; border: 1px solid #666; ${errorStyle}">
                        ${item.placementType}
                    </td>
                    <td style="width: 20%; padding: 4px 13px; text-align:center; border: 1px solid #666; ${errorStyle}">
                        ${item.count}
                    </td>
                </tr>
            </#list>
        </table>
    </div>
</#list>
</body>
</html>