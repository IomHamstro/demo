<!doctype html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>ulmart email</title>

</head>
<body style="padding: 0;margin: 0;">
<table cellspacing="0" border="0" cellpadding="0" width="100%"  style="border-collapse:collapse;background-color: #ffffff;font-family:Arial,sans-serif;color: #000000;font-size: 14px;">
    <tbody>
    <tr>
        <td style="padding-left: 15px;padding-right: 15px;padding-bottom: 15px;padding-top: 15px" align="center">
            <table style="width: 600px;text-align: left;margin-left: auto;margin-right: auto;" cellspacing="0" border="0" cellpadding="0">
                <tr>
                    <td style="padding: 0 8px;text-align: left;">
                        <img src="${url}/resources/ulmart2/i/ecert/text-logo.png" alt="ulmart.ru"/>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 20px 5px 8px;">
                        <div style="padding-bottom: 10px;">
                            К сожалению, вы не оплатили заказ №${orderId}, поэтому он был отменен.
                            <br/><br/>Вы можете повторить заказ <a href="${url}/cart">здесь</a>
                            <br/><br/>Список товаров заказа:
                            <#list goodInfoList as good>
                                <br/><a href="${url}/goods/${good.goodId}">${good.name}</a>
                            </#list>
                            <br/><br/>Возникли вопросы? Звоните по телефону: ${contactInfo.countryPhonePrefix} (${contactInfo.phoneCode}) ${contactInfo.phone}
                            <br/>Или напишите нам электронное письмо: ${contactInfo.email}
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 15px 0 10px;text-align: center;">
                        <table cellspacing="0" border="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/autogoods.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Автотовары</div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/digital-content.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Цифровой контент</div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/pc-and-tablets.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Компьютеры и планшеты</div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/home-appliances.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Бытовая техника</div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/beauty-and-health.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Красота и здоровье</div>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;margin-left: -10px;" src="${url}/resources/ulmart2/i/ecert/for-kids.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Детские товары</div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/house-and-repair.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Дом и ремонт</div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/sport.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Спорт и туризм</div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a style="text-align: center;margin-bottom:14px;display: inline-block;vertical-align:top;border:2px solid #446f85;color: #446f85;font-size: 11px;width: 100px;height: 100px;padding-top: 5px;">
                                        <div style="height: 70px;line-height: 70px;">
                                            <img alt="" style="vertical-align: bottom;" src="${url}/resources/ulmart2/i/ecert/selfphones.png"/>
                                        </div>
                                        <div style="line-height: 22px;padding-top: 5px;letter-spacing: 1px;">
                                            <div style="display: inline-block;vertical-align: middle;line-height: 11px;">Смартфоны и гаджеты</div>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 15px 8px 10px;">
                        <table cellspacing="0" border="0" cellpadding="0" width="100%" style="vertical-align: middle;">
                            <tr>
                                <td>
                                    <a href="http://www.akit.ru/1234567/" style="display: inline-block;text-decoration: none;vertical-align: top;">
                                        <img src="${url}/resources/ulmart2/i/ecert/akit.png" alt=""/>
                                    </a>
                                </td>
                                <td style="padding: 0 20px;">
                                    <a href="https://market.yandex.ru/shop/76117/reviews?from=76117" style="display: inline-block;text-decoration: none;vertical-align: top;">
                                        <img src="${url}/resources/ulmart2/i/ecert/yandex.png" alt=""/>
                                    </a>
                                </td>
                                <td style="padding-right: 20px;">
                                    <a href="https://itunes.apple.com/ru/app/ulmart/id867679958" style="display: inline-block;text-decoration: none;vertical-align: top;">
                                        <img src="${url}/resources/ulmart2/i/ecert/appstore.png" alt=""/>
                                    </a>
                                </td>
                                <td>
                                    <a href="https://play.google.com/store/apps/details?id=ru.ulmart.mobapp&referrer=utm_source%3Dulmart%26utm_medium%3Dicon" style="display: inline-block;text-decoration: none;vertical-align: top;">
                                        <img alt="" src="${url}/resources/ulmart2/i/ecert/googleplay.png"/>
                                    </a>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="padding:15px 20px;background: #446f85;color: #ffffff;text-align: center;line-height: 15px;">
                            <a href="https://vk.com/ulmart" style="display: inline-block;vertical-align: top;margin: 0 8px;cursor: pointer">
                                <img src="${url}/resources/ulmart2/i/ecert/vk.png" alt="" style="vertical-align: middle;"/>
                            </a>
                            <a href="https://www.facebook.com/I.love.Ulmart" style="display: inline-block;vertical-align: top;margin: 0 8px;cursor: pointer">
                                <img src="${url}/resources/ulmart2/i/ecert/fb.png"/>
                            </a>

                            <a href="https://www.odnoklassniki.ru/iloveulmart" style="display: inline-block;vertical-align: top;margin: 0 8px;cursor: pointer">
                                <img src="${url}/resources/ulmart2/i/ecert/ok.png" alt="" style="vertical-align: middle;"/>
                            </a>
                            <a href="https://my.mail.ru/community/ulmart_ru/" style="display: inline-block;vertical-align: top;margin: 0 8px;cursor: pointer">
                                <img src="${url}/resources/ulmart2/i/ecert/moimir.png" alt="" style="vertical-align: middle;"/>
                            </a>
                            <a href="https://www.twitter.com/Ulmart_ru" style="display: inline-block;vertical-align: top;margin: 0 8px;cursor: pointer">
                                <img style="vertical-align: middle;" alt="" src="${url}/resources/ulmart2/i/ecert/twitter.png" />
                            </a>
                            <a href="https://www.instagram.com/iloveulmart" style="display: inline-block;vertical-align: top;margin: 0 8px;cursor: pointer">
                                <img style="vertical-align: middle;" alt="" src="${url}/resources/ulmart2/i/ecert/inst.png" />
                            </a>
                            <a href="https://www.youtube.com/user/ulmartofficial" style="display: inline-block;vertical-align: top;margin: 0 8px;cursor: pointer">
                                <img style="vertical-align: middle;" alt="" src="${url}/resources/ulmart2/i/ecert/youtube.png" />
                            </a>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
