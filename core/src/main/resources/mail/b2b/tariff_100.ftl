<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>#</title>
    <meta charset="utf-8">
</head>
<body style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 1; font-family: Arial; margin: 0; padding: 0; border: 0;">
<style type="text/css">
    blockquote:before { content: none !important; }
    blockquote:after { content: none !important; }
    q:before { content: none !important; }
    q:after { content: none !important; }
</style>
<table width="100%" style="width: 1030px; font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0; border: 0;"><tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
    <td style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0 20px; border: 0;" valign="baseline">
        <table width="100%" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0; padding: 0 0 14px; border: 0;"><tr style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
            <td style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;" valign="baseline">
                <img src="http://corp.ulmart.ru/resources/desktop.blocks/b-head-logo/ulmart-logo-business.png" height="34" width="150" style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;">
            </td>
            <td style="vertical-align: top; text-align: right; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: 20px; font-family: Arial; margin: 0; padding: 0; border: 0;" align="right" valign="top">
                +7 812 
                <span style="vertical-align: top; color: #000000; top: -1px; position: relative; font-style: normal; font-variant: normal; font-weight: normal; font-size: 18px; line-height: normal; font-family: Arial; margin: 0; padding: 0; border: 0;">
                    336-3777, 389-3677
                </span>
            </td>
        </tr></table>
        <hr style="border-style: none none solid; border-bottom-width: 1px; border-bottom-color: #E5E5E5; margin-bottom: 16px;">

        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
            Поздравляем!
        </p>
        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
            Вы достигли 100% от необходимой суммы для перехода на следующий тариф.
        </p>
        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
            С завтрашнего дня Вам будут доступны все товары Юлмарт по тарифу ${userType.priceColumn}. Подробнее о тарифах можно 
            <a href="https://corp.ulmart.ru/help_b2b/spb/discount_b2b" style="vertical-align: baseline; color: #137FB7; text-decoration: none; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">
                прочитать здесь
            </a>.
        </p>
        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
            Благодарим Вас за выбор нашей компании!
        </p>
        <hr style="border-bottom-width: 1px; border-bottom-color: #E5E5E5; margin-bottom: 16px; margin-top: 20px; border-style: none none solid;">
        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
            Перейти в 
            <a href="https://corp.ulmart.ru/cabinet/settings" style="vertical-align: baseline; color: #137FB7; text-decoration: none; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">
                личный кабинет
            </a>.
        </p>
        <p style="vertical-align: baseline; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0 0 9px; padding: 0; border: 0;">
            Если у Вас есть вопросы, воспользуйтесь функцией 
            <a href="https://corp.ulmart.ru/cabinet/feedback" style="vertical-align: baseline; color: #137FB7; text-decoration: none; font-style: normal; font-variant: normal; font-weight: normal; font-size: 15px; line-height: 25px; font-family: Arial; margin: 0; padding: 0; border: 0;">
                обратной связи
            </a>.
        </p>
    </td>
</tr></table>
</body>
</html>
