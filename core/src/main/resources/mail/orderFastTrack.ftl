<#-- @ftlvariable name="orderPrice" type="java.lang.Long" -->
<#-- @ftlvariable name="orderPriceTotal" type="java.lang.Long" -->
<#-- @ftlvariable name="xxlBonus" type="java.lang.Long" -->
<#-- @ftlvariable name="goods" type="java.util.List<ru.ulmart.domain.CartGoodInfo>" -->
<#-- @ftlvariable name="tspGoods" type="java.util.List<ru.ulmart.domain.cart.CartTspGoodInfo>" -->
<#-- @ftlvariable name="telecomGoods" type="java.util.List<ru.ulmart.domain.tele2.CartTelecomInfo>" -->
<#-- @ftlvariable name="checkoutTentCartGoodInfos" type="java.util.List<ru.ulmart.domain.CartGoodInfo>" -->
<#-- @ftlvariable name="checkoutUlmartDriveCartGoodInfos" type="java.util.List<ru.ulmart.domain.CartGoodInfo>" -->
<#-- @ftlvariable name="deliveryCity" type="java.lang.String" -->
<#-- @ftlvariable name="paymentType" type="java.lang.String" -->
<#-- @ftlvariable name="deliveryType" type="boolean" -->
<#-- @ftlvariable name="tariff" type="java.lang.String" -->
<#-- @ftlvariable name="serviceCode" type="java.lang.String" -->
<#-- @ftlvariable name="deliveryAddress" type="java.lang.String" -->
<#-- @ftlvariable name="deliveryCost" type="java.math.BigDecimal" -->
<#-- @ftlvariable name="deliveryDate" type="java.util.Date" -->
<#-- @ftlvariable name="postWorktime" type="java.lang.String" -->
<#-- @ftlvariable name="postType" type="java.lang.String" -->
<#-- @ftlvariable name="shopCatalogAddress" type="java.lang.String" -->
<#-- @ftlvariable name="storageDueToDate" type="java.lang.String" -->
<#-- @ftlvariable name="storageUntilDate" type="java.lang.String" -->
<#-- @ftlvariable name="phone" type="java.lang.String" -->
<#-- @ftlvariable name="postOffice" type="java.lang.String" -->

<#include "image.ftl"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Изменения сроков получения заказа через сервис FastTrack Business</title><meta name="viewport" id="viewport" content="width=device-width, user-scalable=1">
    <link rel="shortcut icon" href="#" />
    <link rel="stylesheet" href="${url}/resources/s/reset.css" />
    <link rel="stylesheet" href="${url}/resources/s/style.css" />
    <link rel="stylesheet" href="${url}/resources/s/bootstrap.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${url}/resources/s/style-ie78.css" />
    <![endif]-->
    <#if orderId?? && serviceType?? && agentId??>
        <#assign orderLink="https://www.ulmart.ru/cabinet/orders?orderId=${orderId}&type=${serviceType}&agentId=${agentId}"/>
        <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                    "@type": "Order",
                    "merchant": {
                        "@type": "Organization",
                        "name": "ЗАО Юлмарт"
                    },
                "orderNumber": "${orderId}",
                "priceCurrency": "RUR",
                "price": "${orderPrice?string.number}",
                "acceptedOffer": [
                    <#list goods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "${good.name}",
                                "url": "https://www.ulmart.ru/goods/${good.goodId}",
                                "image": "<@imageUrl good.photoId!good.goodId 'large'/>",
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "${good.amount}"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                    <#list telecomGoods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "<@telecomGoodName good=good />",
                                "url": "https://www.ulmart.ru/telecom/tariffs/${(good.tariff.id)!}?operator=${(good.telecomType.title)!}",
                                "image": "${(good.tariff.imageUrl)!}",
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "1"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                    <#list tspGoods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "${good.name}",
                                "url": "https://www.ulmart.ru/autoparts/${good.goodId}?ultimaId=${good.ultimaId!}&cat=${cat!0}",
                                 <#if good.photoUrl??>
                                        "image": "<img src="${good.photoUrl}" alt="" width="100px" class="b-img2__img"/>",
                                    <#else>
                                        "image": "<@imageUrl good.photoId!good.goodId 'large'/>",
                                </#if>
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "${good.count}"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                ],
                "url": "${orderLink}",
                "priceSpecification": [
                    {
                        "@type": "UnitPriceSpecification",
                        "price": "${orderPrice?string.number}",
                        "priceCurrency": "RUR"
                    }<#if deliveryType?? && deliveryCost?? && deliveryCost?has_content>,
                        {
                           "@type": "DeliveryChargeSpecification",
                           "price": "${deliveryCost?string.number}",
                           "priceCurrency": "RUR"
                        }
                    </#if>
                ],
                "orderStatus": "http://schema.org/OrderProcessing"
            }
        </script>
    </#if>
</head>
<body>
<div style="padding:0 30px;">
    <div style="width:100%;margin-bottom:35px;">
			<span style="float: right; font: 16px Arial; color: rgb(118, 122, 123); margin-top:15px;">
				<span class="wmi-callto"><small style="position:relative; top:-.2em;">(${phonePrefix})</small> <span style="color:#CC0000; font-size:25px;">${phoneSuffix}</span></span>
			</span>
        <div style="padding:10px 0 8px;">
            <img src="${url}/resources/desktop.blocks/b-head-logo/ulmart-logo.png">
        </div>
        <div style="width: 100%; background-color: rgb(215, 215, 215); height:1px;"></div>
    </div>
    <div style="font-size: 1.077em; line-height: 1.43; margin:35px 0;">
        <p style="margin:0 0 1.15em;">Добрый день.</p>
        <p style="margin:0 0 1.15em;">
            Уведомляем Вас, что заказ № <b style="font-weight:bold;"><span class="wmi-callto">${orderId}</span></b>  был оплачен позднее выбранной даты его получения по сервису FastTrack Business.</p>
        <p style="margin:0 0 1.15em;">Для предоставления услуги FastTrack Business необходимо выбрать новое время получения заказа в личном кабинете на сайте <a href="https://corp.ulmart.ru/" target="_blank">www.ulmart.ru</a> либо Вы можете получить заказ в стандартном порядке.</p>
        <table width="100%" cellspacing="0" cellpadding="3px" border="0" style="font-size:100%; margin-bottom: 1.15em; margin-left: -10px; margin-top: 0.385em;"><tbody>
        <tr>
            <td width="5%" align="left" style="padding:5px 10px; font-weight:bold;">Артикул</td>
            <td width="60%" align="left" style="padding:5px 10px; font-weight:bold;">Наименование товара</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Кол-во</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Цена</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">Сумма</td>
            <td width="10%" align="right" style="padding:5px 10px; font-weight:bold;">XXL-Бонус</td>
        </tr>
        <tr>
            <td colspan="6" style="border-top:1px solid #E4E4E4; height:5px;"></td>
        </tr>

        <#list goods as good>
        <tr>
            <#assign sum = good.price * good.amount>
            <#assign bonus = good.amount * good.xxlBonus!0>

            <td aling="left" style="padding:5px 10px;">${good.goodId}</td>
            <td aling="left" style="padding:5px 10px;"><a href="https://www.ulmart.ru/goods/${good.goodId}">${good.name}</a></td>
            <td align="right" style="padding:5px 10px;">${good.amount}</td>
            <td align="right" style="padding:5px 10px;">${good.price?string.number}</td>
            <td align="right" style="padding:5px 10px;">${sum?string.number}</td>
            <td align="right" style="padding:5px 10px;">${bonus?string.number}</td>
        </tr>
        </#list>
        <#list telecomGoods as good>
        <tr>
            <td aling="left" style="padding:5px 10px;"></td>
            <td aling="left" style="padding:5px 10px;"><@telecomGoodName good=good /></td>
            <td align="right" style="padding:5px 10px;">1</td>
            <td align="right" style="padding:5px 10px;">${good.price?string.number}</td>
            <td align="right" style="padding:5px 10px;">${good.price?string.number}</td>
            <td align="right" style="padding:5px 10px;">-</td>
        </tr>
        </#list>
        <#list tspGoods as good>
        <tr>
            <#assign sum = good.price * good.count>
            <#assign bonus =  good.xxlBonus!0 * good.count>
            <td aling="left" style="padding:5px 10px;">${good.goodId}</td>
            <td aling="left" style="padding:5px 10px;"><a href="https://www.ulmart.ru/autoparts/${good.goodId}?ultimaId=${good.ultimaId!}&cat=${cat!0}">${good.name}</a></td>
            <td align="right" style="padding:5px 10px;">${good.count}</td>
            <td align="right" style="padding:5px 10px;">${good.price?string.number}</td>
            <td align="right" style="padding:5px 10px;">${sum?string.number}</td>
            <td align="right" style="padding:5px 10px;">${bonus?string.number}</td>
        </tr>
        </#list>
        <#list certs as cert>
        <tr>
            <#if cert.id?? && cert.amount?? && cert.nominal??>
            <#assign sum2 = cert.nominal * cert.amount>
            <td aling="left" style="padding:5px 10px;">${cert.id}</td>
            <td aling="left" style="padding:5px 10px;"><a href="https://www.ulmart.ru/goods/${cert.id}">${cert.name!"Подарочный сертификат"}</a></td>
            <td align="right" style="padding:5px 10px;">${cert.amount}</td>
            <td align="right" style="padding:5px 10px;">${cert.nominal?string.number}</td>
            <td align="right" style="padding:5px 10px;">${sum2?string.number}</td>
            <td align="right" style="padding:5px 10px;">&mdash;</td>
            </#if>
        </tr>
        </#list>
        <#if deliveryType?? && deliveryCost?? && deliveryCost?has_content><tr>
            <td aling="left" style="padding:5px 10px;"></td>
            <td aling="left" style="padding:5px 10px;">Доставка</td>
            <td align="right" style="padding:5px 10px;"></td>
            <td align="right" style="padding:5px 10px;">${deliveryCost?string.number}</td>
            <td align="right" style="padding:5px 10px;">${deliveryCost?string.number}</td>
        </tr></#if>

        <tr>
            <td colspan="6" style="border-bottom:1px solid #E4E4E4;height:5px;"></td>
        </tr>
        <#if xxlBonus?? && xxlBonus != 0>
        <tr>
            <td colspan="2" style="padding:5px 10px;">&nbsp;</td>
            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Итого:</td>
            <td align="right" style="padding:5px 10px;">${orderPrice?string.number}&nbsp;р.</b></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="padding:5px 10px;">&nbsp;</td>
            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Оплачено бонусами:</td>
            <td align="right" style="padding:5px 10px;">${xxlBonus?string.number}&nbsp;р.</b></td>
            <td>&nbsp;</td>
        </tr>
        </#if>
        <tr>
            <td colspan="2" style="padding:5px 10px;">&nbsp;</td>
            <td align="right" colspan="2" style="padding:5px 10px; font-weight:bold;">Итого к оплате:</td>
            <td align="right" style="padding:5px 10px;"><b>${orderPriceTotal?string.number} р.</b></td>
            <td>&nbsp;</td>
        </tr>
        </tbody></table>

        <#if checkoutTentCartGoodInfos?has_content>
            <p style="margin:0 0 1.15em;color:#ff8a00;">
                В вашем заказе есть товары, выдача которых производится в шатре (Gate F) рядом с Ulmart Drive
                <#list checkoutTentCartGoodInfos as tentGoods>
                    <br>Арт. ${tentGoods.goodId!""}. ${tentGoods.name!""}
                </#list>
            </p>
        </#if>

        <#if checkoutUlmartDriveCartGoodInfos?has_content>
            <p style="margin:0 0 1.15em;color:#ff8a00;">
                Сервис Ю.Драйв доступен только при предварительной <b style="font-weight:bold;">онлайн-оплате</b> заказа.
                <br>При оплате в кибермаркете вы можете получить заказ в торговом зале.
            </p>
        </#if>

        <#if trackId != "">
            <a class="btn btn_theme_normal btn-lg btn-primary" style="background-color: red; border-radius: 0;" href="${url}/help/post/tracking?track=${trackId}">
               <p style="display: inline; vertical-align: super;"> Отслеживать заказ</p> <img src="${url}/resources/img/play_icon.png" width="25" height="25">
            </a>
        </#if>

        <ul style="margin: 0.77em 0 1.15em;">
            <#if (isTaksaOrder!false)>
                <li style="margin: 0.385em 0;">Ваша программа Такса будет активирована в личном кабинете в течение нескольких минут.</li>
            <#else>
                <li style="margin: 0.385em 0;">Способ оплаты: <b style="font-weight:bold;">${paymentType}</b></li>
            </#if>
            <#if deliveryType??&&deliveryType>
                <li style="margin: 0.385em 0;">Способ доставки: <b style="font-weight:bold;">доставка курьером</b></li>
                <#if tariff??><li style="margin: 0.385em 0;">Тариф: <b style="font-weight:bold;">${tariff}</b></li></#if>
                <li style="margin: 0.385em 0;">Время доставки: <b style="font-weight:bold;">${deliveryDate}</b></li>
                <li style="margin: 0.385em 0;">Адрес доставки: <b style="font-weight:bold;">${deliveryCity}, ${deliveryAddress}</b></li>
            <#elseif deliveryType??&&!deliveryType>
                <li style="margin: 0.385em 0;">Тип отправления: <b style="font-weight:bold;">${postType}</b></li>
                <li style="margin: 0.385em 0;">Дата доставки: <b style="font-weight:bold;">ориентировочно ${deliveryDate}</b></li>
                <li style="margin: 0.385em 0;"><#if serviceCode=='1'||serviceCode=='5'>Где забрать: <#else>Адрес доставки: </#if><b style="font-weight:bold;">${deliveryAddress}</b></li>
                <#if serviceCode=='post'><li style="margin: 0.385em 0;">Почтовое отделение: <b style="font-weight:bold;">${postOffice}</b></li></#if>
                <#if postWorktime??&&(postWorktime?length>0)><li style="margin: 0.385em 0;">Режим работы: <b style="font-weight:bold;">${postWorktime}</b></li></#if>
            <#else>
                <li style="margin: 0.385em 0;">Способ доставки: <b style="font-weight:bold;">самовывоз</b></li>
                <li style="margin: 0.385em 0;">Когда можно забрать: <b style="font-weight:bold;"><#if storageDueToDate??>${storageDueToDate}<#else>после оплаты</#if></b></li>
                <li style="margin: 0.385em 0;">Где забрать: <a href="${baseUrl}${shopUrl!"/help/current/contacts"}"><b style="font-weight:bold;">г. ${cityName}, ${shopCatalogAddress}</b></a></li>
                <li style="margin: 0.385em 0;">Срок хранения: <b style="font-weight:bold;">${storageUntilDate!}</b></li>
            </#if>
            <#if phone?? && phone != ""><li style="margin: 0.385em 0;">Телефон: <b style="font-weight:bold;"><span class="wmi-callto">+7 ${phone}</span></b></li></#if>
        </ul>
        <#if deliveryType??&&deliveryType><p style="margin:0 0 1.15em;">
            В резерве присутствуют позиции, которые будут доставлены по адресу: ${deliveryCity}, ${deliveryAddress}, <b>${deliveryDate}</b>. В течение трех рабочих часов оператор свяжется с Вами для уточнения деталей доставки.
        </p></#if>
        <#if yandexRatingId?? && yandexMarketRating??>
            <a href="https://market.yandex.ru/shop/${yandexRatingId}/reviews?from=${yandexRatingId}" target="_blank">
                <img src="${url}/resources/img/ya.market-${yandexMarketRating}.png" width="310" height="29" />
            </a>
        </#if>

        <div style="width: 100%; text-align: center; border-top:1px solid rgb(242, 242, 242); padding:5px 0 0;">
            <a href="http://www.ulmart.ru/loyalty/landing/?from=taksatriggerbanner&utm_source=email&utm_medium=trigger&utm_campaign=taksatriggerbanner" target="_blank">
                <img src="https://fast.ulmart.ru/ns/promo/taksa_970x90.jpg" width="970" height="90">
            </a>
        </div>

        <div style="width: 100%; text-align: right; padding:5px 0; font-style:italic;">
            <span style="font-weight: bold;">Мы знаем, что у Вас есть выбор. Спасибо, что выбрали нас</span>
            <br/>
            <em>Команда Юлмарт</em>
        </div>
    </div>
</div>
</body>
</html>

<#macro telecomGoodName good>
Тариф ${good.tariff.telecomType.title} ${good.tariff.name!}&nbsp;
Номер ${(good.phoneNumber.numberType.title)!} <@formattedNumber number=good.phoneNumber.msisdn />
</#macro>
<#macro formattedNumber number>
    <#if number??>
        <#if number?length == 11>
        + ${number?substring(0,1)}&nbsp;${number?substring(1,4)}&nbsp;${number?substring(4,7)}-${number?substring(7,9)}-${number?substring(9)}<#--
     --><#else>
        + ${number}
        </#if>
    </#if>
</#macro>