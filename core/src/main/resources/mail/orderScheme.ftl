<#include "image.ftl"/>
<#if orderId?? && serviceType?? && agentId??>
    <#assign orderLink="https://www.ulmart.ru/cabinet/orders?orderId=${orderId}&type=${serviceType}&agentId=${agentId}"/>
<script type="application/ld+json">
            {
                "@context": "http://schema.org",
                    "@type": "Order",
                    "merchant": {
                        "@type": "Organization",
                        "name": "ЗАО Юлмарт"
                    },
                "orderNumber": "${orderId}",
                "priceCurrency": "RUR",
                "price": "${orderPrice?string.number}",
                "acceptedOffer": [
                    <#list goods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "${good.name}",
                                "url": "https://www.ulmart.ru/goods/${good.goodId}",
                                "image": "<@imageUrl good.photoId!good.goodId 'large'/>",
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "${(good.amount)!}"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                    <#list telecomGoods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "<@telecomGoodName good=good />",
                                "url": "https://www.ulmart.ru/telecom/tariffs/${(good.tariff.id)!}?operator=${(good.telecomType.title)!}",
                                "image": "${(good.tariff.imageUrl)!}",
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "1"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                    <#list tspGoods as good>
                        {
                            "@type": "Offer",
                            "itemOffered": {
                                "@type": "Product",
                                "name": "${good.name}",
                                "url": "https://www.ulmart.ru/autoparts/${good.goodId}?ultimaId=${good.ultimaId!}&cat=${cat!0}",
                                 <#if good.photoUrl??>
                                        "image": "<img src="${good.photoUrl}" alt="" width="100px" class="b-img2__img"/>",
                                    <#else>
                                        "image": "<@imageUrl good.photoId!good.goodId 'large'/>",
                                </#if>
                                "sku": "${good.goodId}"
                            },
                            "price": "${good.price?string.number}",
                            "priceCurrency": "RUR",
                            "eligibleQuantity": {
                                "@type": "QuantitativeValue",
                                "value": "${good.count}"
                            }
                        }<#if good_has_next>,</#if>
                    </#list>
                ],
                "url": "${orderLink}",
                "priceSpecification": [
                    {
                        "@type": "UnitPriceSpecification",
                        "price": "${orderPrice?string.number}",
                        "priceCurrency": "RUR"
                    }<#if deliveryType?? && deliveryCost?? && deliveryCost?has_content>,
                        {
                           "@type": "DeliveryChargeSpecification",
                           "price": "${deliveryCost?string.number}",
                           "priceCurrency": "RUR"
                        }
                    </#if>
                ],
                "orderStatus": "http://schema.org/OrderProcessing"
            }
        </script>
</#if>