<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">
 <tbody>
  <tr>
   <td>Регион</td>
   <td>${request.regionName}</td>
  </tr>
  <tr>
   <td>Номер заказа</td>
   <td>${request.orderId}</td>
  </tr>
  <tr>
   <td>Наименование юридического лица</td>
   <td>${request.contractor.name}</td>
  </tr>
  <tr>
   <td>Email</td>
   <td>${request.user.email}</td>
  </tr>
  <tr>
   <td>Ultima ID(пользователь)</td>
   <td>${request.user.agentId}</td>
  </tr>
  <tr>
   <td>Ultima ID(контрагент)</td>
   <td>${request.contractor.id}</td>
  </tr>
 </tbody>
</table>
