<#function getStringDescription value oneDescription twoToFourDescription fiveAndMoreDescription>
    <#if (value >= 0)>
        <#assign remainder = value % 100/>
        <#assign tens = (remainder - remainder % 10) / 10/>
        <#if tens == 1>
            <#return fiveAndMoreDescription/>
        <#else>
            <#assign digits = remainder % 10/>
            <#if digits == 1>
                <#return oneDescription/>
            <#elseif (digits > 4 || digits == 0)>
                <#return fiveAndMoreDescription/>
            <#else>
                <#return twoToFourDescription/>
            </#if>
        </#if>
    <#else>
        <#return ""/>
    </#if>
</#function>