<#-- @ftlvariable name="categories" type="java.util.List<ru.ulmart.domain.utuning.StaticCategory>" -->
<#if categories?has_content>
<#setting locale="hu">
<table style="width:90%;border:1px solid #000">
<tbody>
    <colgroup>
        <col width="auto" />
        <col width="130em" />
    </colgroup>
<#list categories as category>
    <tr>
        <td style="background-color:#c0c0c0">
            <span style="background-color:#c0c0c0">
                <strong>${category_index + 1}. ${category.name}</strong>
            </span>
        </td>
        <td style="background-color:#c0c0c0"><br></td>
    </tr>
    <#list category.services as service>
    <tr>
        <td>
            <#if service.description?? && service.description?trim != "">
            <div class="js-toggle"><span class="b-pseudolink js-toggle-handle">${service.name}</span>
                <div class="js-toggle-block">
                    <p>${service.description}</p>
                </div>
            </div>
            <#else>
                ${service.name}
            </#if>
        </td>
        <td style="text-align:center"><#if service.price??>${service.price?string(",##0.##")} руб.</#if>
        <br> <a href="/about/current/requestInstallation?service=${service.id}">Заказать&nbsp;услугу!</a>
        </td>
    </tr>
    </#list>
</#list>
</tbody>
</table>
</#if>